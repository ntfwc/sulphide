#!/bin/sh
set -eu

if [ $# -ne 1 ]
then
	echo "Usage: $0 version"
	exit 1
fi

VERSION=$1

SCRIPT_DIR=$(dirname "$0")

"$SCRIPT_DIR/../client_mod/packaging/package_client_mod.sh" "$VERSION"
"$SCRIPT_DIR/../server/packaging/package_linux_server.sh" "$VERSION"
"$SCRIPT_DIR/../server/packaging/package_windows_server.sh" "$VERSION"
"$SCRIPT_DIR/../server/packaging/package_macos_x86_64_server.sh" "$VERSION"
