#version 110

attribute vec3 absolutePointPosition;

uniform vec3 centerPosition;
uniform float distanceScale;
uniform float radarRadius;
uniform float heightDifferenceForMaxColorChange;
uniform vec3 aboveColor;
uniform vec3 belowColor;

// The viewport dimensions divided by 2
uniform vec2 viewportHalvedDimensions;

varying vec3 pointColor;
varying vec2 pointScreenCoordinates;

const vec3 SAME_HEIGHT_COLOR = vec3(1.0);

vec2 calculateRadarPosition()
{
  vec2 relativeVector = absolutePointPosition.xy - centerPosition.xy;
  vec2 scaledVector = relativeVector * distanceScale;

  vec2 drawVector = scaledVector;
  float length = length(scaledVector);
  if (length > 1.0)
    drawVector = scaledVector / length;

  return drawVector * radarRadius;
}

vec3 calculateColor()
{
  float heightDifference = absolutePointPosition.z - centerPosition.z;
  float t = abs(heightDifference / heightDifferenceForMaxColorChange);
  if (t > 1.0)
    t = 1.0;

  vec3 indicatorColor = heightDifference > 0.0 ? aboveColor : belowColor;
  return mix(SAME_HEIGHT_COLOR, indicatorColor, t);
}

void main()
{
  vec4 position = gl_ModelViewProjectionMatrix * vec4(calculateRadarPosition(), 0, 1);
  gl_Position = position;
  pointScreenCoordinates = (position.xy + vec2(1.0)) * viewportHalvedDimensions;

  pointColor = calculateColor();
}
