#version 110

uniform float pointRadiusSquared;

varying vec3 pointColor;
varying vec2 pointScreenCoordinates;

float linearStep(float edge0, float edge1, float x)
{
  return clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0);
}

void main()
{
    vec2 offsetFromPoint = gl_FragCoord.xy - pointScreenCoordinates;
    float distanceSquared = dot(offsetFromPoint, offsetFromPoint);

    float perFragmentDistanceSquaredDelta = fwidth(distanceSquared);
    float alpha = 1.0 - linearStep(pointRadiusSquared - perFragmentDistanceSquaredDelta, pointRadiusSquared, distanceSquared);

    if (alpha != 0.0)
      gl_FragColor = vec4(pointColor, alpha);
    else
      discard;
}
