package ntfwc.mod.multiplayer.buffer

import ntfwc.mod.multiplayer.model.avatar.array.PlayerArrayState
import ntfwc.mod.multiplayer.model.avatar.array.TimestampedPlayerArrayState
import java.util.*

/** The time that we expect between states, when they are actively changing */
private const val EXPECTED_MIN_TIME_BETWEEN_STATES_WHEN_ACTIVE: Long = 1_000_000_000 / 30
private const val MAX_QUEUE_SIZE = 30

/** The minimum time that a half full buffer represents */
private const val HALF_BUFFER_TIME: Long = EXPECTED_MIN_TIME_BETWEEN_STATES_WHEN_ACTIVE * MAX_QUEUE_SIZE / 2
private const val ANTI_JITTER_TIME_ADJUSTMENT = EXPECTED_MIN_TIME_BETWEEN_STATES_WHEN_ACTIVE / 2

/**
 * Buffers player states.
 *
 * This is not thread-safe.
 */
class PlayerStateBuffer(initialState: TimestampedPlayerArrayState) {
    private val updateQueue = ArrayDeque<TimestampedPlayerArrayState>(MAX_QUEUE_SIZE)
    private var currentState = initialState

    private var targetTimeOffset = calculateTargetTimeOffset(initialState.timestamp, System.nanoTime())
    private var lastResetTimestamp = -1L

    fun getNext(timestampNow: Long): PlayerArrayState {
        advanceToTimestamp(calculateTargetTimestamp(timestampNow))
        return this.currentState.arrayState
    }

    fun addUpdate(state: TimestampedPlayerArrayState, timestampNow: Long) {
        handleTimeOffsetResets(state.timestamp, timestampNow)

        // If the queue was full, we must take an item off the queue by advancing
        if (updateQueue.size >= MAX_QUEUE_SIZE) {
            // This should never be null in this case
            val nextState = peekNext()!!
            advanceState(nextState)
        }

        this.updateQueue.add(state)
    }

    /**
     * Handles resetting the time offset when the buffering behavior indicates we are too out of sync.
     */
    private fun handleTimeOffsetResets(stateTimestamp: Long, timestampNow: Long) {
        if (lastResetTimestamp == timestampNow)
            return

        val targetTimestamp = calculateTargetTimestamp(timestampNow)
        // Check if our time is too far ahead
        if ((updateQueue.size == 0 && stateTimestamp < targetTimestamp))
            resetTimeOffset(stateTimestamp, timestampNow)
        // Check if our time is too far behind
        else if (updateQueue.size >= MAX_QUEUE_SIZE && stateTimestamp > (targetTimestamp + 2 * HALF_BUFFER_TIME))
            resetTimeOffset(stateTimestamp, timestampNow)
    }

    private fun resetTimeOffset(stateTimestamp: Long, timestampNow: Long) {
        this.targetTimeOffset = calculateTargetTimeOffset(stateTimestamp, timestampNow)
        lastResetTimestamp = timestampNow
    }

    private fun peekNext(): TimestampedPlayerArrayState? = updateQueue.peek()

    private fun advanceToTimestamp(targetTimestamp: Long) {
        var nextState = peekNext() ?: return

        // Apply adjustements so it will, with a normal average rate of updates, take one state each
        // time it is called, even if there is some significant timing variation.
        if (nextState.timestamp > targetTimestamp + ANTI_JITTER_TIME_ADJUSTMENT)
            return

        advanceState(nextState)

        val decreasedTimestamp = targetTimestamp - ANTI_JITTER_TIME_ADJUSTMENT
        while (true) {
            nextState = peekNext() ?: return
            if (nextState.timestamp > decreasedTimestamp)
                return

            advanceState(nextState)
        }
    }

    private fun advanceState(peekedState: TimestampedPlayerArrayState) {
        this.updateQueue.poll()
        this.currentState = peekedState
    }

    private fun calculateTargetTimestamp(timestampNow: Long): Long = timestampNow + targetTimeOffset

    /**
     * Calculates the offset to apply to timestamps to calculate the target time. In includes an adjustement by the
     * difference between the base timestamp and a timestamp for the current time, so significant differences between
     * these values does not break the buffering behavior.
     */
    private fun calculateTargetTimeOffset(baseTimestamp: Long, timestampNow: Long): Long = (baseTimestamp - timestampNow) - HALF_BUFFER_TIME
}