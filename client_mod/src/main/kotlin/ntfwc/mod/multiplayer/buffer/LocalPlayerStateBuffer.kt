package ntfwc.mod.multiplayer.buffer

import ntfwc.mod.multiplayer.model.avatar.array.TimestampedPlayerArrayState
import java.util.*

private const val MAX_QUEUE_SIZE = 6

/**
 * A buffer for local player states, which can be sent to the server.
 *
 * This is thread-safe.
 */
class LocalPlayerStateBuffer(initialState: TimestampedPlayerArrayState) {
    private val stateQueue = ArrayDeque<TimestampedPlayerArrayState>(MAX_QUEUE_SIZE)
    // The state to fall back to if the queue is empty when we are trying to connect
    private var fallbackStateForConnection = initialState

    /**
     * Adds a state to this buffer. If the buffer is full, it will remove
     * the oldest state before adding the given state.
     */
    @Synchronized
    fun addState(state: TimestampedPlayerArrayState) {
        if (stateQueue.size >= MAX_QUEUE_SIZE)
            fallbackStateForConnection = stateQueue.pop()

        stateQueue.push(state)
    }

    /**
     * Drains the content of this buffer to a list. Items will be ordered from oldest to newest.
     */
    @Synchronized
    fun drainToList(): List<TimestampedPlayerArrayState> {
        if (stateQueue.isEmpty())
            return Collections.emptyList()

        val list = ArrayList<TimestampedPlayerArrayState>(stateQueue.size)
        list.addAll(stateQueue)
        list.reverse()
        fallbackStateForConnection = stateQueue.last
        stateQueue.clear()
        return list
    }

    /**
     * Clears the buffer and returns its last state, if there is one.
     */
    @Synchronized
    fun clearAndGetLastState(): TimestampedPlayerArrayState? {
        if (stateQueue.isEmpty())
            return null

        val lastState = stateQueue.last
        fallbackStateForConnection = lastState
        stateQueue.clear()
        return lastState
    }

    /**
     * Gets the initial state used when creating a connection
     */
    @Synchronized
    fun getInitialStateForConnection(): TimestampedPlayerArrayState {
        return clearAndGetLastState() ?: this.fallbackStateForConnection
    }
}