package ntfwc.mod.multiplayer.game.interfaces.player

import com.jogamp.opengl.GL2
import graphics.GFX
import main.GameState
import ntfwc.mod.multiplayer.gl.RenderedTextTexture
import ntfwc.mod.multiplayer.gl.TagRendering
import java.awt.Color
import java.awt.Font

/**
 * The missing disguise tag. It is a singleton.
 */
object MissingDisguiseTag {
    private var textTexture: RenderedTextTexture? = null

    fun draw(gameState: GameState, x: Double, y: Double, z: Double) {
        val gl = GFX.gl
        val textTexture = getOrCreateTextTexture(gl)
        TagRendering.draw(textTexture, gameState, x, y, z)
    }

    private fun getOrCreateTextTexture(gl: GL2): RenderedTextTexture {
        var textTexture = this.textTexture
        if (textTexture == null) {
            val font = GFX.gfx.anon48.rasterizer.font.deriveFont(Font.PLAIN, 20f)
            textTexture = RenderedTextTexture.renderTextToTexture(gl, "Missing Disguise", font, 1, Color.RED)
            this.textTexture = textTexture
        }

        return textTexture
    }
}