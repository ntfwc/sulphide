package ntfwc.mod.multiplayer.game.interfaces.player

import game.Entity
import ntfwc.mod.multiplayer.game.interfaces.player.avatar.PlayerAppearanceStateFromNet
import ntfwc.mod.multiplayer.model.PlayerPosition
import ntfwc.mod.multiplayer.util.avatar.AvatarEnablingUtil
import util.V3D

/**
 * Persistent manager for player renderers. Must be initialized on the game's graphics thread.
 */
class PlayerRendererManager(initialMaxEnabledAvatarCount: Int) : Entity(V3D(0.0, 0.0, 0.0)) {
    private val playerRendererMap: MutableMap<String, PlayerRenderer> = HashMap()
    private var maxEnabledAvatarCount = initialMaxEnabledAvatarCount

    init {
        isPermanent = true
    }

    fun addOrUpdatePlayerRenderer(name: String, position: PlayerPosition, appearanceState: PlayerAppearanceStateFromNet?) {
        val playerRenderer = playerRendererMap[name]
        if (playerRenderer == null)
            playerRendererMap[name] = PlayerRenderer(name, position, appearanceState, playerRendererMap.size < maxEnabledAvatarCount)
        else
            playerRenderer.update(position, appearanceState)
    }

    fun removePlayerRenderer(name: String) {
        playerRendererMap.remove(name)?.destroy()
    }

    fun clearPlayerRenderers() {
        for (renderer in playerRendererMap.values)
            renderer.destroy()

        playerRendererMap.clear()
    }

    fun getPlayerNames(): Collection<String> = playerRendererMap.keys

    fun getPlayerPositions(): Iterable<PlayerPosition> = object : Iterable<PlayerPosition> {
        override fun iterator(): Iterator<PlayerPosition> {
            val rendererIterator = playerRendererMap.values.iterator()
            return object : Iterator<PlayerPosition> {
                override fun hasNext(): Boolean = rendererIterator.hasNext()
                override fun next(): PlayerPosition {
                    return rendererIterator.next().getPosition()
                }
            }
        }
    }

    fun updateEnabledAvatars(maxAvatarsToEnable: Int, playerPosition: PlayerPosition) {
        this.maxEnabledAvatarCount = maxAvatarsToEnable
        AvatarEnablingUtil.updateAvatarEnabledStates(playerRendererMap.values, maxAvatarsToEnable, playerPosition)
    }
}