package ntfwc.mod.multiplayer.game.interfaces.player.avatar.animation

import ntfwc.mod.multiplayer.util.range.FloatRange
import ntfwc.mod.multiplayer.util.range.IntValueRange
import ntfwc.mod.multiplayer.util.range.ShortRange

object AnimationConstants {
    val wingFrameRange = FrameRange(1, 32)
    val wingInterpRange = FloatRange(0.23333335f, 1.1500001f)
    val dashTimerRange = ShortRange(0, 60)
    val sideSkidFaderRange = FloatRange(-1, 1)
    val pitchRange = FloatRange(-0.3f, 0.3f)
    val rollRange = FloatRange(-0.5f, 0.5f)
    val yawRange = FloatRange(-0.4f, 0.4f)
    val forwardValueRange = FloatRange(-13, 13)
    val druggednessRange = ShortRange(0, 300)
    val glowyTimerRange = IntValueRange(0, 450)
}