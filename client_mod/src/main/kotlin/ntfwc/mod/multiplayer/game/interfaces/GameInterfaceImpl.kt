package ntfwc.mod.multiplayer.game.interfaces

import game.Entity
import game.Player
import game.environment.World
import main.GameState
import main.GameStateListener
import main.GameStateManager
import main.Scene
import ntfwc.mod.multiplayer.game.interfaces.player.PlayerRendererManager
import ntfwc.mod.multiplayer.game.interfaces.player.avatar.PlayerAppearanceState
import ntfwc.mod.multiplayer.model.*
import ntfwc.mod.multiplayer.model.avatar.array.NetPlayerAppearanceArrayState
import ntfwc.mod.multiplayer.model.avatar.array.PlayerArrayState
import ntfwc.mod.multiplayer.util.log
import util.V3D
import java.util.*
import java.util.concurrent.atomic.AtomicReference
import kotlin.collections.ArrayList

/**
 * Implementation of the interface to the game.
 */
class GameInterfaceImpl : GameInterface {
    private val mapChangeListeners: MutableList<(MapName?) -> Unit> = ArrayList()
    private val playerStateListeners: MutableList<(PlayerArrayState) -> Unit> = ArrayList()
    private val stepListeners: MutableList<() -> Unit> = ArrayList()
    private val playerRadarColorConfiguration = AtomicReference<RadarColorConfiguration>(RadarColorConfiguration.DEFAULT_CONFIGURATION)

    private var lastMapName: MapName? = null
    private var playerRendererManager: PlayerRendererManager? = null
    private var connectionStatusEntity: ConnectionStatusEntity? = null
    private var playerRadarEntity: PlayerRadarEntity? = null
    private var initdOccurred = false
    private var initialMaxAvatarsToEnable = 0

    init {
        GameStateManager.addGameStateListener(object : GameStateListener {
            /**
             * Called when the first map is loaded.
             */
            override fun gameStateInitd(g: GameState?) {
                if (g !is Scene)
                    return

                log("initd scene")
                MonitorEntity()
                val playerRendererManager = PlayerRendererManager(initialMaxAvatarsToEnable)
                this@GameInterfaceImpl.playerRendererManager = playerRendererManager
                connectionStatusEntity = ConnectionStatusEntity()
                playerRadarEntity = PlayerRadarEntity({ getPlayerPosition() }, playerRendererManager::getPlayerPositions, playerRadarColorConfiguration)
                initdOccurred = true
            }

            /**
             * Called when a map is unloaded, or when a user switched to a menu.
             */
            override fun gameStateUninstated(g: GameState?) {
            }

            /**
             * It is called when a map is loaded from the main menu or when the game otherwise starts,
             * and called when you return to the main menu. It is not called when switching between maps.
             */
            override fun gameStateInstated(g: GameState?) {
                val world = g?.world
                if (world !is World)
                    return

                checkForAndHandleMapChange(world.currentMapData.name)
            }

            /**
             * Called whenever the state stack is changed.
             */
            override fun stackChanged(op: GameStateManager.StackOp?) {
            }

            /**
             * Called just before the game exits. Will only be run for the last map that was open.
             */
            override fun gameStateDisposed(g: GameState?) {
            }
        })
    }

    private fun checkForAndHandleMapChange(currentMapName: String) {
        if (lastMapName?.value != currentMapName) {
            val newMapName = MapName(currentMapName)
            lastMapName = newMapName
            when (currentMapName) {
                "exitWorld" -> onSwitchedToMainMenu()
                else -> onMapChanged(newMapName)
            }
        }
    }

    private fun onMapChanged(map: MapName) {
        for (listener in mapChangeListeners) {
            listener.invoke(map)
        }
    }

    private fun onSwitchedToMainMenu() {
        log("Switched to main menu")
        for (listener in mapChangeListeners)
            listener.invoke(null)
    }

    private fun notifyPlayerStateListeners(playerState: PlayerArrayState) {
        for (listener in playerStateListeners)
            listener.invoke(playerState)
    }

    private fun onStep() {
        val currentState: GameState? = GameStateManager.getCurrent()
        val world = currentState?.world
        if (world is World)
            checkForAndHandleMapChange(world.currentMapData.name)

        for (listener in stepListeners)
            listener.invoke()
    }

    override fun addMapChangeListener(listener: (MapName?) -> Unit) {
        mapChangeListeners.add(listener)
    }

    override fun addPlayerStateListener(listener: (PlayerArrayState) -> Unit) {
        playerStateListeners.add(listener)
    }

    override fun addOrUpdatePlayerRenderer(name: String, state: PlayerArrayState) {
        val manager = playerRendererManager
        if (manager == null) {
            log("Warning: Tried to add a player renderer without a player renderer manager")
            return
        }

        val appearanceState = state.appearanceState?.let {
            PlayerAppearanceState.from(it.toAppearanceState())
        }
        manager.addOrUpdatePlayerRenderer(name, state.position, appearanceState)
    }

    override fun removePlayerOrb(name: String) {
        val manager = playerRendererManager
        if (manager == null) {
            log("Warning: Tried to remove a player renderer without a player renderer manager")
            return
        }

        manager.removePlayerRenderer(name)
    }

    override fun clearPlayerOrbs() {
        val manager = playerRendererManager
        if (manager == null) {
            log("Warning: Tried to clear player renderers without a player renderer manager")
            return
        }

        manager.clearPlayerRenderers()
    }

    override fun getPlayerOrbPlayers(): Collection<String> {
        val manager = playerRendererManager ?: return Collections.emptyList()
        return manager.getPlayerNames()
    }

    override fun setConnectionStatus(connectionStatus: ConnectionStatus?) {
        val statusEntity = connectionStatusEntity
        if (statusEntity == null) {
            log("Warning: Tried to set the connection status without a status entity")
            return
        }

        statusEntity.setStatus(connectionStatus)
    }

    override fun addStepListener(stepListener: () -> Unit) {
        stepListeners.add(stepListener)
    }

    override fun setPlayerRadarVisible(visible: Boolean) {
        playerRadarEntity?.visible = visible
    }

    override fun setPlayerRadarColorConfiguration(radarColorConfiguration: RadarColorConfiguration) {
        playerRadarColorConfiguration.set(radarColorConfiguration)
    }

    override fun setInitialMaxEnabledAvatarsCount(initialMaxAvatarsToEnable: Int) {
        this.initialMaxAvatarsToEnable = initialMaxAvatarsToEnable
    }

    override fun updateEnabledAvatars(maxAvatarsToEnable: Int) {
        val playerPosition = getPlayerPosition() ?: return
        this.playerRendererManager?.updateEnabledAvatars(maxAvatarsToEnable, playerPosition)
    }

    private fun getPlayerPosition(): PlayerPosition? {
        val position = Player.me.position ?: return null
        return PlayerPosition.from(position)
    }

    private fun getPlayerState(): PlayerArrayState? {
        val player: Player = Player.me ?: return null
        val position = PlayerPosition.from(player.pos)
        val appearanceState = PlayerAppearanceState.from(player)

        return PlayerArrayState(position, NetPlayerAppearanceArrayState.from(appearanceState.toNetState()))
    }

    private inner class MonitorEntity : Entity(V3D(0.0, 0.0, 0.0)) {
        init {
            isPermanent = true
        }

        override fun step() {
            super.step()
            onStep()

            val playerState = getPlayerState() ?: return
            notifyPlayerStateListeners(playerState)
        }
    }
}
