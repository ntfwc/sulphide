package ntfwc.mod.multiplayer.game.interfaces.player.avatar.animation

import game.Pony
import graphics.Armature
import ntfwc.mod.multiplayer.model.avatar.AnimationId
import java.util.*

/**
 * Maps AnimationIds to frame ranges. It should only be used after the "gameStateInitd" event has
 * occurred, otherwise it can cause a runtime exception because required information is not initialized.
 */
object AnimationRangeMap {
    private val animationIdToFrameRange: Map<AnimationId, FrameRange>

    init {
        val animationIdToFrameRange = EnumMap<AnimationId, FrameRange>(AnimationId::class.java)
        animations@ for (id in AnimationId.values()) {
            // By using a when statement here, we guarantee that each animationId gets some handling
            val frameRange: FrameRange = when (id) {
                AnimationId.TRIPLET_1 -> createRangeOffsetByStartFrameAttack(0, 18)
                AnimationId.TRIPLET_2 -> createRangeOffsetByStartFrameAttack(19, 35)
                AnimationId.TRIPLET_3 -> createRangeOffsetByStartFrameAttack(36, 64)
                AnimationId.TURNKICK -> createRangeOffsetByStartFrameAttack(65, 96)
                AnimationId.FOLLOWUP_1 -> createRangeOffsetByStartFrameAttack(98, 121)
                AnimationId.FOLLOWUP_2 -> createRangeOffsetByStartFrameAttack(129, 149)
                AnimationId.FOLLOWUP_3 -> createRangeOffsetByStartFrameAttack(150, 169)
                AnimationId.LAUNCH -> createRangeOffsetByStartFrameAttack(170, 185)
                AnimationId.SWIM -> FrameRange(681, 710)
                AnimationId.JUMP -> FrameRange(438, 528)
                AnimationId.FLY -> FrameRange(620, 680)
                AnimationId.ALT_SKID_TROT -> FrameRange(776, 788)
                AnimationId.SKID_TROT -> FrameRange(290, 305)
                AnimationId.ALT_RUN_TROT -> FrameRange(200, 225)
                AnimationId.RUN_TROT -> FrameRange(90, 138)
                AnimationId.NORMAL_TROT -> FrameRange(30, 59)
                AnimationId.START_TROT -> FrameRange(0, 30)
                AnimationId.RUN -> FrameRange(45, 120)
                AnimationId.TURN45 -> FrameRange(191, 204)
                AnimationId.TURN105 -> FrameRange(171, 190)
                AnimationId.TURN180 -> FrameRange(139, 170)
                AnimationId.FASTTURN -> createRangeOffsetByStartFrameAttack(187, 201)
                AnimationId.FASTTURN105 -> createRangeOffsetByStartFrameAttack(205, 215)
                AnimationId.TURNTROT12_5 -> createRangeOffsetByStartFrameAttack(216, 221)
                AnimationId.TURNTROT25 -> createRangeOffsetByStartFrameAttack(222, 230)
                AnimationId.TURNTROT45 -> createRangeOffsetByStartFrameAttack(231, 242)
                AnimationId.TURNTROT75 -> createRangeOffsetByStartFrameAttack(242, 254)
                AnimationId.SKIDSTAND -> FrameRange(375, 405)
                AnimationId.FLINCH -> createRangeOffsetByStartFrameAttack(256, 263)
                AnimationId.SKID -> FrameRange(315, 436)
                AnimationId.ROLL -> FrameRange(310, 314)
                AnimationId.EXTERNAL -> getFullFrameRange()
                // These animations have no frame range. They don't use the frame value.
                AnimationId.DEAD,
                AnimationId.IDLE -> continue@animations
            }
            animationIdToFrameRange[id] = frameRange
        }
        AnimationRangeMap.animationIdToFrameRange = animationIdToFrameRange
    }

    private val trotFrameToAnimationId: NavigableMap<Float, AnimationId>

    init {
        val trotFrameToAnimationId = TreeMap<Float, AnimationId>()
        for (id in AnimationId.values()) {
            if (id.avatarState != Pony.TROT)
                continue

            // Every trot animation must have a frame range
            val frameRange = getFrameRange(id)!!
            trotFrameToAnimationId[frameRange.min] = id
        }
        AnimationRangeMap.trotFrameToAnimationId = trotFrameToAnimationId
    }

    fun getFrameRange(animationId: AnimationId): FrameRange? = animationIdToFrameRange[animationId]

    /**
     * Finds the correct trot animation ID for the given trot animation frame.
     */
    fun findTrotAnimationId(frame: Float): AnimationId {
        return trotFrameToAnimationId.floorEntry(frame)?.value ?: AnimationId.START_TROT
    }

    private fun createRangeOffsetByStartFrameAttack(min: Int, max: Int): FrameRange {
        val offset = Pony.START_FRAME_ATTACK
        return FrameRange(min + offset, max + offset)
    }

    private fun getFullFrameRange(): FrameRange {
        val mainArmature = Armature.get("pony.bvh")
        return FrameRange(0, mainArmature.frames - 1)
    }
}