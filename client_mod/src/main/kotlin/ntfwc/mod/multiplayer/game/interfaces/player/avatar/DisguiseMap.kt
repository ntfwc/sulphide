package ntfwc.mod.multiplayer.game.interfaces.player.avatar

import mechanics.PonyData

/**
 * Maps from PonyData names to PonyData objects. Should only be used on the game thread.
 */
object DisguiseMap {
    private val fallbackPonyData: PonyData

    init {
        val fallbackDisguiseName = "SKELETON"
        fallbackPonyData = getPonyData(fallbackDisguiseName)
                ?: throw IllegalStateException("The fallback disguise, '$fallbackDisguiseName', was not found")
    }

    fun getPonyData(name: String): PonyData? {
        return kotlin.runCatching {
            PonyData.valueOf(PonyData::class.java, name)
        }.getOrNull()
    }

    fun getFallbackPonyData(): PonyData = this.fallbackPonyData
}