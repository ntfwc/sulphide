package ntfwc.mod.multiplayer.game.interfaces

import game.Camera
import game.Entity
import game.WorldEditor
import graphics.GFX
import main.Scene
import ntfwc.mod.multiplayer.gl.Matrix4x4Array
import ntfwc.mod.multiplayer.model.PlayerPosition
import ntfwc.mod.multiplayer.model.RadarColorConfiguration
import ntfwc.mod.multiplayer.radar.PlayerRadarRenderer
import ntfwc.mod.multiplayer.util.vector.Vector2f
import util.V3D
import java.util.concurrent.atomic.AtomicReference

/**
 * An entity that draws the player radar.
 */
class PlayerRadarEntity(private val getPlayerPosition: () -> PlayerPosition?,
                        private val getOtherPlayerPositions: () -> Iterable<PlayerPosition>,
                        private val colorConfiguration: AtomicReference<RadarColorConfiguration>) :
        Entity(V3D(0.0, 0.0, 0.0), Scene.OVERLAY_DEPTH) {
    private val radarRenderer = PlayerRadarRenderer.createInstance(100f, 6f, 1 / 5000f, 1000f)

    init {
        visible = false
    }

    override fun render() {
        if (isEditorOpen())
            return

        val gl = GFX.gl
        val playerPosition = getPlayerPosition() ?: return
        val scaledSize = radarRenderer.getScaledSize(Scene.height)
        radarRenderer.render(gl,
                Scene.width - scaledSize - 10f,
                Scene.height - scaledSize - Scene.height * 40f / 360f,
                Scene.height,
                calculateRotationAngle(),
                playerPosition,
                getOtherPlayerPositions(),
                colorConfiguration.get())
    }

    private fun isEditorOpen(): Boolean {
        return WorldEditor.me?.editorUI?.frame?.isVisible ?: false
    }

    private fun calculateRotationAngle(): Float {
        val cameraMatrix = Camera.me?.cameraMatrix ?: return 0f
        val testVector = Vector2f(1f, 0f)
        val directionVector = multiply2DDirection(Matrix4x4Array(cameraMatrix.array), testVector)
        if (directionVector.x == 0f && directionVector.y == 0f)
            return 0f

        return Vector2f(1f, 0f).signedDegreesBetween(directionVector)
    }

    /**
     * A limited matrix multiplication to figure an X and Y direction vector.
     */
    private fun multiply2DDirection(matrix4x4: Matrix4x4Array, vector: Vector2f): Vector2f {
        val x = matrix4x4.getValue(0, 0) * vector.x +
                matrix4x4.getValue(1, 0) * vector.y
        val y = matrix4x4.getValue(0, 1) * vector.x +
                matrix4x4.getValue(1, 1) * vector.y
        return Vector2f(x, y)
    }
}