package ntfwc.mod.multiplayer.game.interfaces.player.avatar

import game.MagicWings
import game.MelindaRiding
import game.Pony
import game.TailLink
import game.TailLink.ZAligned
import game.environment.World
import game.particles.Smoke
import game.particles.Spark
import graphics.Bone
import graphics.GFX
import main.Main
import main.Scene
import mechanics.PonyData
import mechanics.PonyQuirk
import ntfwc.mod.multiplayer.game.interfaces.player.MissingDisguiseTagEntity
import ntfwc.mod.multiplayer.game.interfaces.player.NameTag
import ntfwc.mod.multiplayer.game.interfaces.player.NameTagEntity
import ntfwc.mod.multiplayer.game.interfaces.player.avatar.animation.PlayerAnimationState
import ntfwc.mod.multiplayer.model.PlayerPosition
import ntfwc.mod.multiplayer.reflection.MelindaRidingDataAccessor
import ntfwc.mod.multiplayer.reflection.TailLinkDataAccessor
import util.Calc
import util.V3D
import kotlin.math.max

private val INFINITE_FLIGHT_SPARK_COLOR: FloatArray = floatArrayOf(0.0f, 0.5f, 0.5f, 0.5f)
private val DEFAULT_EYE_ANGLE_X = 90.4182f

/**
 * Renders a player avatar.
 *
 * Note: This is under the MPL2 license because it copies and modifies some code from the game, which is licensed
 * under the MPL2.
 */
class PlayerAvatar(nameTag: NameTag, initialPosition: PlayerPosition, initialAppearanceState: PlayerAppearanceStateFromNet) :
        Pony(initialPosition.toV3D(), prepareDisguise(initialAppearanceState.appearanceState.disguise)) {
    companion object {
        private fun prepareDisguise(disguise: PonyData): PonyData {
            var preparedDisguise = disguise
            if (disguise.wingArmature == null) {
                preparedDisguise = disguise.clone(false, disguise.name + "(Disguise)")
                preparedDisguise.wingArmature = "ponyWings.bvh"
                preparedDisguise.wingModel = "mechaWings.obj"
            }

            return preparedDisguise
        }
    }

    private var rider: MelindaRiding? = null
    private var hasInfiniteFlag = false
    private var lastDruggednessValue: Short = 0
    private var druggednessBlushFactor = 0f
    private var glowyTimer = 0
    private var glowyColor: FloatArray = GFX.WHITE

    private var breathIntensity = 0f
    private var breathIntensityTarget = 0f
    private var breathCycle = 0f

    private val nameTagEntity = NameTagEntity(nameTag, adjustForNameTag(initialPosition))
    private val missingDisguiseTagEntity: MissingDisguiseTagEntity = MissingDisguiseTagEntity(adjustForMissingDisguiseTag(initialPosition))

    init {
        eyeAngleXL = DEFAULT_EYE_ANGLE_X
        eyeAngleXR = DEFAULT_EYE_ANGLE_X
        eyeAngleZL = 36.815773f
        eyeAngleZR = -36.584396f
        update(initialPosition, initialAppearanceState)
    }

    override fun waterDamage(drown: Boolean) {}

    override fun step() {
        handleRollState()
        handleDashEffects()
        handleDeadStateEyes()
        handleJetSmoke()
        handleWingRotation()
        stepInfiniteFlapSparks()
        stepCondenseLines()
        stepShadow()
        stepBlush()
        stepGlowySparks()
        stepCountersUpdate()
    }

    override fun modelWingUpdate(step: Boolean) {
        // This can sometimes end up less than zero, which will cause an exception. So, when this happens we must set
        // the value to zero. This can happen in cases where the player gets teleported.
        if (airRunFrame < 0f)
            airRunFrame = 0f

        super.modelWingUpdate(step)
        handleBreath(step)
    }

    fun update(position: PlayerPosition, appearanceStateFromNet: PlayerAppearanceStateFromNet) {
        val appearanceState = appearanceStateFromNet.appearanceState

        setDisguise(appearanceState.disguise)
        this.missingDisguiseTagEntity.setTagVisible(appearanceStateFromNet.isMissingDisguise)

        setMelindaIsRiding(appearanceState.melindaIsRiding)
        updateSpeedUsingPosDifference(pos, position)
        setPosition(position)

        apply(appearanceState.rotationTransform)
        apply(appearanceState.animationState)
        this.currentMood = appearanceState.currentMood
        this.nextMoodMultiplier = appearanceState.nextMoodMultiplier
        this.nextMood = appearanceState.nextMood
        this.armored = appearanceState.armoredState.internalValue
        this.hasInfiniteFlag = appearanceState.hasInfiniteFlap
        updateDruggedness(appearanceState.druggedness)
        this.bloody = appearanceState.bloody
        this.glowyTimer = appearanceState.glowyTimer
        this.glowyColor = appearanceState.glowyColor.to4ValueColorFloatArray()
    }

    private fun setPosition(position: PlayerPosition) {
        position.applyTo(pos)
        nameTagEntity.setPosition(adjustForNameTag(position))
        missingDisguiseTagEntity.setPosition(adjustForMissingDisguiseTag(position))
    }

    private fun updateSpeedUsingPosDifference(oldPosition: V3D, newPosition: PlayerPosition) {
        this.speed.x(newPosition.x - oldPosition.x())
        this.speed.y(newPosition.y - oldPosition.y())
        this.speed.z(newPosition.z - oldPosition.z())
    }

    private fun apply(rotationTransform: PlayerRotationTransform) {
        rotationTransform.vX.applyTo(vX)
        rotationTransform.vY.applyTo(vY)
        rotationTransform.vZ.applyTo(vZ)

        rotationTransform.vX.applyTo(sVX)
        rotationTransform.vY.applyTo(sVY)
        rotationTransform.vZ.applyTo(sVZ)
    }

    private fun apply(animationState: PlayerAnimationState) {
        applyState(animationState.state)
        this.frame = animationState.frame
        this.wingExtend = animationState.wingExtend
        this.wingFrame = animationState.wingFrame
        this.wingInterp = animationState.wingInterp
        this.mirror = animationState.mirror

        val rollAxis = animationState.rollAxis
        this.rollAxis = if (rollAxis != null) floatArrayOf(rollAxis.x, rollAxis.y, rollAxis.z) else null
        this.rollAmount = animationState.rollAmount

        this.dashTimer = animationState.dashTimer
        this.sideSkidFader = animationState.sideSkidFader

        this.flapping = animationState.flapping
        this.pitch = animationState.pitch
        this.roll = animationState.roll
        this.yaw = animationState.yaw
        this.forwardValue = animationState.forwardValue
    }

    private fun handleRollState() {
        if (state != ROLL || rollAxis == null)
            return

        // Close the eyes
        blinkTimer = BLINK_TIMER_CLOSED + 2

        //Create euler rotation matrix about the axis, with the angular velocity
        val rollMatrix = Calc.makeEulerMatrix(rollAxis, rollAmount)
        //Perform rotation
        Calc.matrixMultiply3OptLast(rollMatrix, master.postMatrix)

        //Find the up vector in the near future
        val futureZ = floatArrayOf(master.units[0][2], master.units[1][2], master.units[2][2])
        Calc.rotationMatrixPostMultiplyOpt(rollMatrix, futureZ)
        val futureY = floatArrayOf(master.units[0][1], master.units[1][1], master.units[2][1])
        Calc.rotationMatrixPostMultiplyOpt(rollMatrix, futureY)
    }

    private fun handleDashEffects() {
        var rainbowTrail = 0f
        if (dashTimer in DASH_DAMAGE_BEGIN until DASH_DAMAGE_END) {
            rainbowTrail = 1f - (dashTimer - DASH_DAMAGE_BEGIN).toFloat() / (DASH_DAMAGE_END - DASH_DAMAGE_BEGIN)
        }

        if (superMode)
            rainbowTrail = Calc.limit((relativeSpeed.toFloat() - 100f) / 100f, rainbowTrail, 1f)

        this.lastRainbowTrail?.speed?.set(V3D.ZERO)

        if (rainbowTrail > 0)
            rainbowLine(rainbowTrail)
        else
            this.lastRainbowTrail = null
    }

    private fun handleWingRotation() {
        if (state == FLY) {
            wingR.postMatrixMode = Bone.PM_EARLY
            Calc.makeXYZMatrixOpt(-pitch * 12 + roll * 3 + yaw * 3 + forwardValue / 10, if (flapping) 0f else Calc.limit(-speed.clone().sub(world.wind(pos)).dot(vZ) * 0.06, -1.0, 1.0).toFloat(), 0f, false, wingR.postMatrix)
            wingL.postMatrixMode = Bone.PM_EARLY
            Calc.makeXYZMatrixOpt(-pitch * 12 - roll * 3 - yaw * 3 + forwardValue / 10, if (flapping) 0f else Calc.limit(speed.clone().sub(world.wind(pos)).dot(vZ) * 0.06, -1.0, 1.0).toFloat(), 0f, false, wingL.postMatrix)
        }
    }

    private fun stepInfiniteFlapSparks() {
        if (hasInfiniteFlag && (wingExtend > 0 || Scene.fourstage < 2)) {
            val b: Bone
            val xm: Byte
            if (Scene.blink.toInt() == 0) {
                b = wingArmature.getBone("Wing0E.L")
                xm = 1
            } else {
                b = wingArmature.getBone("Wing0E.R")
                xm = -1
            }
            val sL = Spark(pos.clone().add(
                    scale * (b.currentPosition[0] + b.units[0][0] * 0.78 * xm),
                    scale * (b.currentPosition[1] + b.units[1][0] * 0.78 * xm),
                    scale * (b.currentPosition[2] + b.units[2][0] * 0.78 * xm)))
            sL.color = INFINITE_FLIGHT_SPARK_COLOR
            sL.xScale *= (0.25 + 0.25 * Math.random()).toFloat()
            sL.yScale = sL.xScale
        }
    }

    private fun setDisguise(disguise: PonyData) {
        if (appearance.name == disguise.name)
            return

        setAppearance(prepareDisguise(disguise))
    }

    private fun setMelindaIsRiding(melindaIsRiding: Boolean) {
        val rider = this.rider
        if (melindaIsRiding && rider == null) {
            spawnMelinda()
            this.superMode = true
        } else if (!melindaIsRiding && rider != null) {
            rider.destroy()
            this.superMode = false
            this.rider = null
        }
    }

    private fun spawnMelinda() {
        val rider = MelindaRiding(this, false, null)
        val wings = MagicWings(this, PonyData.MELINDA.magicColor)
        MelindaRidingDataAccessor.setMagicWings(rider, wings)
        this.rider = rider
    }

    override fun destroy() {
        this.rider?.destroy()
        this.nameTagEntity.destroy()
        this.missingDisguiseTagEntity.destroy()
        super.destroy()
    }

    // Override this to cut out the logic that modifies the mood fields
    override fun moodUpdate() {
        if (currentMood != null)
            headExt.compute(START_FRAME_MOODS + currentMood.ordinal, 1.0f, false, true)
        if (nextMood != null)
            headExt.compute(START_FRAME_MOODS + nextMood.ordinal, nextMoodMultiplier, false, true)

        if (blah != preBlah) {
            val jaw = armature.getBone("Jaw")
            val lowerTeeth = armature.getBone("LowerTeeth")
            val jawMat = Calc.makeEulerMatrix(floatArrayOf(jaw.units[0][0], jaw.units[1][0], jaw.units[2][0]), 0.1f * blah.toFloat())
            Calc.matrixMultiply3OptLast(jawMat, jaw.units)
            Calc.matrixMultiply3OptLast(jawMat, lowerTeeth.units)
            val jawOffVec = floatArrayOf(jaw.units[0][2] - jaw.units[0][1], jaw.units[1][2] - jaw.units[1][1], jaw.units[2][2] - jaw.units[2][1])
            Calc.addOptFirst(jaw.currentPosition, jawOffVec, -0.06f * blah.toFloat())
            Calc.addOptFirst(lowerTeeth.currentPosition, jawOffVec, -0.06f * blah.toFloat())
            val nose = armature.getBone("Nose")
            val upperTeeth = armature.getBone("UpperTeeth")
            val noseOffVec = floatArrayOf(nose.units[0][2], nose.units[1][2], nose.units[2][2])
            Calc.addOptFirst(nose.currentPosition, noseOffVec, 0.03f * blah.toFloat())
            Calc.addOptFirst(upperTeeth.currentPosition, noseOffVec, 0.03f * blah.toFloat())
            val noseMat = Calc.makeEulerMatrix(floatArrayOf(nose.units[0][0], nose.units[1][0], nose.units[2][0]), -0.1f * blah.toFloat())
            Calc.matrixMultiply3OptLast(noseMat, nose.units)
            Calc.matrixMultiply3OptLast(noseMat, upperTeeth.units)
            preBlah = blah
        }
    }

    private fun applyState(newState: Int) {
        if (this.state == newState)
            return

        this.state = newState

        if (state == DEAD) {
            if (ragdoll == null)
                setupRagdollForDeadState()
        } else if (ragdoll != null) {
            for (rr in ragdoll) {
                for (r in rr) r.destroy()
            }
            ragdoll = null
            tailBehaviour = TAIL_NORMAL
        }

        if (state != FLY) {
            wingL.postMatrixMode = Bone.PM_OFF
            wingR.postMatrixMode = Bone.PM_OFF
        }
    }

    // Copied from setState to handle the "DEAD" state correctly
    private fun setupRagdollForDeadState() {
        this.ragdoll = Array(5) { arrayOfNulls<TailLink>(2) }
        val names = arrayOf("Thigh.L", "Thigh.R", "Shoulder.L", "Shoulder.R", "Neck")
        var t: ZAligned
        for (i in this.ragdoll.indices) {
            val b = armature.getBone(names[i])
            val r: Byte
            val up: Float
            if (i < 2) {
                r = 6
                up = 0.0f
            } else if (i == 4) {
                r = 18
                up = 2f
            } else {
                r = 5
                up = 0.0f
            }

            t = ZAligned(pos.clone().add(V3D(b.currentPosition, 0), scale.toDouble()).add(z, radius * up), 0, ragdoll[i], depth - 1, r.toDouble(), r.toDouble(), 1.1, armature, names[i] + "#", this, i < 4, true)
            this.ragdoll[i][1].gravity = gravity.clone().multiply(0.5)
            if (i < 4)
                TailLinkDataAccessor.multiplyMaxDist(ragdoll[i][1], 2.0)

            if (i in 2..3)
                t.mode = 2

            t.prev = this
        }

        deadTimer = 0
        tailBehaviour = TAIL_DEAD
    }

    private fun handleJetSmoke() {
        if (!appearance.quirks.contains(PonyQuirk.JET_WINGS) || this.wingExtend <= 0f)
            return

        // Copied from Player.step
        val choices = arrayOf("Wing1E.L", "Wing1E.R", "Wing2E.L", "Wing2E.R")
        val end = wingArmature.getBone(choices[Scene.fourstage.toInt()])

        val smk = Smoke(pos.clone().add((
                scale * end.currentPosition[0]).toDouble(), (
                scale * end.currentPosition[1]).toDouble(), (
                scale * end.currentPosition[2]).toDouble()))
        smk.xScale *= 0.25f
        smk.yScale = smk.xScale
        smk.angle = Calc.random() * Calc.PI * 2.0f
        smk.color = GFX.DKGRAY
    }

    private fun handleDeadStateEyes() {
        if (state == DEAD)
            blinkTimer = BLINK_TIMER_CLOSED
    }

    private fun updateDruggedness(druggedness: Short) {
        if (lastDruggednessValue == druggedness)
            return
        lastDruggednessValue = druggedness

        val angleAdjustment = Math.min(8f, druggedness.toFloat() / 15f)
        eyeAngleXL = DEFAULT_EYE_ANGLE_X + angleAdjustment
        eyeAngleXR = DEFAULT_EYE_ANGLE_X - angleAdjustment

        druggednessBlushFactor = Calc.limit(druggedness.toFloat() / 300, 0f, 1f)
    }

    private fun stepBlush() {
        // For some reason there is no way to compare with a short with a literal in Kotlin...
        val zero: Short = 0
        blush = if (lastDruggednessValue == zero) {
            0f
        } else {
            val c = 0.25f * Calc.fastCos(Scene.currentStep.toFloat() / 15)
            druggednessBlushFactor * (0.25f + c)
        }
    }

    private fun stepGlowySparks() {
        val glowyTimer = glowyTimer
        if (glowyTimer == 0)
            return

        // Copied from Player.step()
        var f = Math.min(0.5f * glowyTimer / 400f, 0.5f)
        if (glowyTimer > 400)
            f = Math.min(f + 0.5 * (glowyTimer - 400) / 50f, 1.0).toFloat()

        if (Math.random() < f / 2) {
            val a = Math.random() * Math.PI * 2
            val r = 1 - Math.pow(Math.random(), 2.0)
            val h = Math.cos(a)
            val v = -Math.sin(a)
            val s = Spark(pos.clone().add(vZ, radius * (1 - r)).add(vX, h * r * radius).add(vY, v * r * radius))
            s.color = GFX.mix4f(glowyColor, GFX.WHITE, Calc.random())
            s.color[3] = 0f //Additive blending
            s.tex = GFX.spr("sparkSphere")
            s.gravity = Main.gravity.clone().multiply(-0.075)
            s.xScale *= (0.1 + 0.2 * Math.random()).toFloat()
            s.yScale = s.xScale
            s.scaleIncrease = 0.01f
            s.speedIncrease = -0.025f
            s.duration = 30 + Calc.random() * 30
            s.speed.set(vX).multiply(h * r * 2).add(vY, v * r * 2).add(vZ, 1.5 - r)
            s.speed.add(speed)
        }
    }

    private fun handleBreath(step: Boolean) {
        if (step && !appearance.quirks.contains(PonyQuirk.INORGANIC)) {
            val spd = relativeSpeed.toFloat()
            if (this.isAttacking || dashTimer < DASH_DAMAGE_END) {
                breathIntensity = (breathIntensity * 9f + 0.25f) / 10f
                breathIntensityTarget = (breathIntensityTarget * 99f + 1f) / 100f
            } else if (state == SKID || state == SKIDSTAND || state == ROLL) {
                breathIntensity *= 0.98f
            } else {
                val target: Float
                val div: Float
                if (state == FLY) {
                    if (flapping)
                        breathIntensityTarget = Math.min(1f, breathIntensityTarget + 0.0033f)

                    target = 0.5f - 0.25f * Calc.sq(flapGauge / FLAP_GAUGE_MAX)
                    div = 49f
                } else {
                    target = targetVector.len().toFloat() * spd / 16
                    div = 9 + kotlin.math.floor(110f / (1 + spd / 7))
                }
                breathIntensityTarget = if (breathIntensityTarget < target)
                    (breathIntensityTarget * div + Calc.limit(target, 0f, 1f)) / (div + 1f)
                else
                    max(0f, breathIntensityTarget * 0.9994f - 1.0f / 600)
                breathIntensity = (breathIntensity * 19 + breathIntensityTarget) / 20
            }
            val b = (0.025f + 0.075f * breathIntensity) * Calc.fastCos(breathCycle)
            Calc.multiplyOpt(back.units[0], 1 + b)
            Calc.multiplyOpt(back.units[1], 1 + b)
            Calc.multiplyOpt(back.units[2], 1 + b)
            val pc = breathCycle
            val period = 22 + 210 * Calc.sq(1 - breathIntensity)
            breathCycle = (breathCycle + 2 * Calc.PI / period) % (2 * Calc.PI)

            val heat = breathIntensityTarget + 1 - World.me.temperature * 2
            if (breathCycle >= Calc.PI / 2 && pc < Calc.PI / 2 && heat > 0) {
                val s = Smoke(pos.clone().add(V3D(head.getPositionFor(floatArrayOf(0f, -1.9f, 2.66f)), 0), scale.toDouble()))
                s.fadeIn = (16 - 12 * breathIntensity).toShort()
                s.speed.add(speed).add(vX, 0.3 + spd / 6).add(vZ, (spd / 13).toDouble())
                s.xScale = 0.3f
                s.yScale = s.xScale
                s.maxAlpha = 0.2f * heat * (1 - 0.5f * breathIntensity)
                s.speedIncrease = -0.06f
                s.gravityMultiplier = -0.05f
                s.scaleIncrease = 0.0075f * (1 + spd / 3)
                s.duration = (4 + heat * 96 / (1 + spd)).toShort()
                s.color = GFX.WHITE
            }
        }
    }

    private fun adjustForNameTag(position: PlayerPosition): PlayerPosition = position.addZ(50.0)
    private fun adjustForMissingDisguiseTag(position: PlayerPosition): PlayerPosition = position.addZ(40.0)
}
