package ntfwc.mod.multiplayer.game.interfaces

import java.util.*

/**
 * Handler that takes care of providing fade-in/fade-out alpha values when something's visibility changes.
 *
 * @param fadeTimeMillis The number of milliseconds to fade-in/fade-out between changes.
 */
class FadeInOutHandler(private val fadeTimeMillis: Int) {
    private var lastSetVisible = false
    private var state: State = StaticState(0f)

    fun setVisible(visible: Boolean) {
        if (lastSetVisible == visible)
            return

        this.lastSetVisible = visible
        this.state = if (visible) FadingIn(fadeTimeMillis) { this.state = StaticState(1f) }
        else FadingOut(fadeTimeMillis) { this.state = StaticState(0f) }
    }

    fun getAlpha(): Float {
        return state.getAlpha()
    }
}

private sealed class State {
    abstract fun getAlpha(): Float
}

private class StaticState(private val alpha: Float) : State() {
    override fun getAlpha(): Float {
        return alpha
    }
}

private class FadingIn(private val fadeMillis: Int, private val setNextState: () -> Unit) : State() {
    private val startTime = Date()

    override fun getAlpha(): Float {
        val millisecondDifference = Date().time - startTime.time
        if (millisecondDifference > fadeMillis) {
            setNextState()
            return 1f
        }

        // Handle a corner case
        if (millisecondDifference < 0) {
            return 0f
        }

        return millisecondDifference.toFloat() / fadeMillis
    }
}

private class FadingOut(private val fadeMillis: Int, private val setNextState: () -> Unit) : State() {
    private val startTime = Date()

    override fun getAlpha(): Float {
        val millisecondDifference = Date().time - startTime.time
        if (millisecondDifference > fadeMillis) {
            setNextState()
            return 0f
        }

        // Handle a corner case
        if (millisecondDifference < 0) {
            return 1f
        }

        return 1f - millisecondDifference.toFloat() / fadeMillis
    }
}
