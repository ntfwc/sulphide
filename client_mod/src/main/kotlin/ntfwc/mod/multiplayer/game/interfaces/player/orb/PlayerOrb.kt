package ntfwc.mod.multiplayer.game.interfaces.player.orb

import game.particles.OrthoEntity
import graphics.GFX
import graphics.Tex
import ntfwc.mod.multiplayer.game.interfaces.player.NameTag
import ntfwc.mod.multiplayer.model.PlayerPosition
import util.V3D
import java.io.File
import java.io.FileNotFoundException


/**
 * An orb that represents a player.
 */
class PlayerOrb(private val nameTag: NameTag, position: PlayerPosition) : OrthoEntity(V3D(position.x, position.y, position.z), 0) {
    companion object {
        private var loadedSprite: Tex? = null

        @Synchronized
        private fun getOrLoadSprite(): Tex {
            val loadedSprite = this.loadedSprite
            if (loadedSprite != null)
                return loadedSprite

            val sprite = loadSprite()
            this.loadedSprite = sprite
            return sprite
        }

        private fun loadSprite(): Tex {
            val spriteFiles = arrayOf(
                    "playerOrb_pre_mag_15_0.png",
                    "playerOrb_pre_mag_15_1.png",
                    "playerOrb_pre_mag_15_2.png",
                    "playerOrb_pre_mag_15_3.png",
                    "playerOrb_pre_mag_15_4.png",
                    "playerOrb_pre_mag_15_5.png",
                    "playerOrb_pre_mag_15_6.png",
                    "playerOrb_pre_mag_15_7.png",
                    "playerOrb_pre_mag_15_8.png",
                    "playerOrb_pre_mag_15_9.png",
                    "playerOrb_pre_mag_15_10.png",
                    "playerOrb_pre_mag_15_11.png",
                    "playerOrb_pre_mag_15_12.png",
                    "playerOrb_pre_mag_15_13.png",
                    "playerOrb_pre_mag_15_14.png"
            )

            val textureMap = HashMap<String, Tex>()
            var texture: Tex? = null
            for (spriteFile in spriteFiles) {
                val path = "mod/sulphide/res/sprites/$spriteFile"
                val file = File(path)
                if (!file.exists())
                    throw FileNotFoundException("Sprite file not found: ${file.absolutePath}")

                texture = GFX.loadTexture(file, textureMap)
            }

            if (texture == null)
                throw IllegalStateException("Had no texture files")

            return texture
        }
    }

    init {
        color = GFX.WHITE
        tex = getOrLoadSprite()
        image = id % tex.length.toFloat()
        imageSpeed = 0.5f
        stepOnlyInSection = true
    }

    override fun render() {
        super.render()
        val ipos = iPos()
        nameTag.draw(gameState, ipos.x(), ipos.y(), ipos.z() + 20)
    }

    fun setPosition(position: PlayerPosition) {
        position.applyTo(pos)
    }
}