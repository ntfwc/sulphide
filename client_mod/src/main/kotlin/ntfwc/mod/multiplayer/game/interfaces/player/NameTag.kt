package ntfwc.mod.multiplayer.game.interfaces.player

import com.jogamp.opengl.GL2
import graphics.GFX
import main.GameState
import ntfwc.mod.multiplayer.gl.RenderedTextTexture
import ntfwc.mod.multiplayer.gl.TagRendering
import java.awt.Font

/**
 * A player name tag that can be rendered. It is drawn with orthographic projection,
 * always facing the camera and it is scaled by the distance from the camera.
 */
class NameTag(private val playerName: String) {
    private var textTexture: RenderedTextTexture? = null

    fun draw(gameState: GameState, x: Double, y: Double, z: Double) {
        val gl = GFX.gl
        val textTexture = getOrCreateTextTexture(gl)
        TagRendering.draw(textTexture, gameState, x, y, z)
    }

    private fun getOrCreateTextTexture(gl: GL2): RenderedTextTexture {
        var textTexture = this.textTexture
        if (textTexture == null) {
            val font = GFX.gfx.anon48.rasterizer.font.deriveFont(Font.PLAIN, 28f)
            textTexture = RenderedTextTexture.renderTextToTexture(gl, playerName, font, 2)
            this.textTexture = textTexture
        }

        return textTexture
    }
}