package ntfwc.mod.multiplayer.game.interfaces.player.avatar

data class PlayerAppearanceStateFromNet(val appearanceState: PlayerAppearanceState, val isMissingDisguise: Boolean)
