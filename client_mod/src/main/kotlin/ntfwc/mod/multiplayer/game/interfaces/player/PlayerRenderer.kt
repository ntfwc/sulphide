package ntfwc.mod.multiplayer.game.interfaces.player

import ntfwc.mod.multiplayer.game.interfaces.player.avatar.PlayerAppearanceStateFromNet
import ntfwc.mod.multiplayer.game.interfaces.player.avatar.PlayerAvatar
import ntfwc.mod.multiplayer.game.interfaces.player.orb.PlayerOrb
import ntfwc.mod.multiplayer.model.PlayerPosition
import ntfwc.mod.multiplayer.util.avatar.AvatarEnablingUtil

/**
 * A player renderer. It can render the player as either an avatar or an orb.
 */
class PlayerRenderer(playerName: String, initialPosition: PlayerPosition, initialAppearanceState: PlayerAppearanceStateFromNet?, private var enableAvatar: Boolean) : AvatarEnablingUtil.AvatarInterface {
    private val nameTag = NameTag(playerName)

    private var renderObject: RenderObject = if (enableAvatar && initialAppearanceState != null)
        createAvatarRenderObject(initialPosition, initialAppearanceState)
    else
        createOrbRenderObject(initialPosition)

    private var currentPosition = initialPosition
    private var currentAppearanceState = initialAppearanceState

    /**
     * Updates the rendered player state.
     */
    fun update(position: PlayerPosition, appearanceState: PlayerAppearanceStateFromNet?) {
        if (appearanceState == null)
            update(position)
        else
            updateWithAppearanceState(position, appearanceState)

        this.currentPosition = position
        this.currentAppearanceState = appearanceState
    }

    private fun update(position: PlayerPosition) {
        val renderObject = this.renderObject
        if (renderObject is OrbRenderObject) {
            renderObject.playerOrb.setPosition(position)
            return
        }

        setRenderObject(OrbRenderObject(PlayerOrb(nameTag, position)))
    }

    private fun updateWithAppearanceState(position: PlayerPosition, appearanceState: PlayerAppearanceStateFromNet) {
        if (!enableAvatar) {
            update(position)
            return
        }

        val renderObject = this.renderObject
        if (renderObject is AvatarRenderObject) {
            renderObject.playerAvatar.update(position, appearanceState)
            return
        }

        setRenderObject(AvatarRenderObject(PlayerAvatar(nameTag, position, appearanceState)))
    }

    override fun setAvatarEnabled(enable: Boolean) {
        if (this.enableAvatar == enable)
            return

        this.enableAvatar = enable
        if (enable) {
            val currentAppearanceState = currentAppearanceState
            if (currentAppearanceState != null)
                setRenderObject(createAvatarRenderObject(this.currentPosition, currentAppearanceState))
        } else if (this.renderObject is AvatarRenderObject) {
            setRenderObject(createOrbRenderObject(this.currentPosition))
        }
    }

    fun destroy() = this.renderObject.destroy()

    override fun getPosition(): PlayerPosition = this.currentPosition

    override fun canShowAvatar(): Boolean = this.currentAppearanceState != null

    private fun createAvatarRenderObject(position: PlayerPosition, appearanceState: PlayerAppearanceStateFromNet): AvatarRenderObject {
        return AvatarRenderObject(PlayerAvatar(nameTag, position, appearanceState))
    }

    private fun createOrbRenderObject(position: PlayerPosition): OrbRenderObject {
        return OrbRenderObject(PlayerOrb(nameTag, position))
    }

    private fun setRenderObject(renderObject: RenderObject) {
        this.renderObject.destroy()
        this.renderObject = renderObject
    }

    private interface RenderObject {
        fun destroy()
    }

    private class OrbRenderObject(val playerOrb: PlayerOrb) : RenderObject {
        override fun destroy() {
            playerOrb.destroy()
        }
    }

    private class AvatarRenderObject(val playerAvatar: PlayerAvatar) : RenderObject {
        override fun destroy() {
            playerAvatar.destroy()
        }
    }
}