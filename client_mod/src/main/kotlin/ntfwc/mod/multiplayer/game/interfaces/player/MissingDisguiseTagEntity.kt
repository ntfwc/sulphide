package ntfwc.mod.multiplayer.game.interfaces.player

import game.particles.OrthoEntity
import ntfwc.mod.multiplayer.model.PlayerPosition
import util.V3D

/**
 * An entity that renders the tag for a missing disguise.
 */
class MissingDisguiseTagEntity(position: PlayerPosition) : OrthoEntity(V3D(position.x, position.y, position.z), 0) {
    override fun render() {
        val ipos = iPos()
        MissingDisguiseTag.draw(gameState, ipos.x(), ipos.y(), ipos.z())
    }

    fun setPosition(position: PlayerPosition) {
        position.applyTo(pos)
    }

    fun setTagVisible(visible: Boolean) {
        this.visible2 = visible
    }
}