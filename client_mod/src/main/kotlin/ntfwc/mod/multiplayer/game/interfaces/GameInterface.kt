package ntfwc.mod.multiplayer.game.interfaces

import ntfwc.mod.multiplayer.model.ConnectionStatus
import ntfwc.mod.multiplayer.model.MapName
import ntfwc.mod.multiplayer.model.RadarColorConfiguration
import ntfwc.mod.multiplayer.model.avatar.array.PlayerArrayState

/**
 * An interface to the game.
 */
interface GameInterface {

    /**
     * Add a map change listener. This listener will be given the map name when the game changes maps.
     */
    fun addMapChangeListener(listener: (MapName?) -> Unit)

    /**
     * Adds a listener for the current player state.
     */
    fun addPlayerStateListener(listener: (PlayerArrayState) -> Unit)

    /**
     * Adds or updates a player renderer.
     *
     * @param name The name of the player.
     * @param state The state of the player.
     */
    fun addOrUpdatePlayerRenderer(name: String, state: PlayerArrayState)

    /**
     * Removes a player orb, if it exists.
     *
     * @param name The name of the player.
     */
    fun removePlayerOrb(name: String)

    /**
     * Removes all player orbs.
     */
    fun clearPlayerOrbs()

    /**
     * Gets a view of all the players for which we have player orbs.
     */
    fun getPlayerOrbPlayers(): Collection<String>

    /**
     * Sets a connection status to be shown to the user.
     *
     * @param connectionStatus The connection status.
     */
    fun setConnectionStatus(connectionStatus: ConnectionStatus?)

    /**
     * Adds a listener for game steps. Useful if something needs to sync with the main loop.
     */
    fun addStepListener(stepListener: () -> Unit)

    /**
     * Sets whether the player radar should be visible.
     */
    fun setPlayerRadarVisible(visible: Boolean)

    /**
     * Sets the player radar's color configuration.
     */
    fun setPlayerRadarColorConfiguration(radarColorConfiguration: RadarColorConfiguration)

    /**
     * Sets the configured max avatar count during initialization.
     *
     * Note: This value will be effectively overridden when [updateEnabledAvatars] is called.
     */
    fun setInitialMaxEnabledAvatarsCount(initialMaxAvatarsToEnable: Int)

    /**
     * Updates which avatars are enabled. No more than the given max count will be enabled.
     * If there are more player renderers than the max count, then the ones closest to the player
     * will have their avatar set enabled.
     */
    fun updateEnabledAvatars(maxAvatarsToEnable: Int)
}