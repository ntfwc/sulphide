package ntfwc.mod.multiplayer.game.interfaces.player.avatar.animation

import ntfwc.mod.multiplayer.util.range.FloatRange

/**
 * A range of "frame" values. Used to represent the valid range of values for
 * an individual animation.
 */
typealias FrameRange = FloatRange
