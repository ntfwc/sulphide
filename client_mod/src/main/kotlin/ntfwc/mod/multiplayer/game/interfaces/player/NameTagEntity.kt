package ntfwc.mod.multiplayer.game.interfaces.player

import game.particles.OrthoEntity
import ntfwc.mod.multiplayer.model.PlayerPosition
import util.V3D

/**
 * An entity that renders the given name tag.
 */
class NameTagEntity(private val nameTag: NameTag, position: PlayerPosition) : OrthoEntity(V3D(position.x, position.y, position.z), 0) {
    override fun render() {
        val ipos = iPos()
        nameTag.draw(gameState, ipos.x(), ipos.y(), ipos.z())
    }

    fun setPosition(position: PlayerPosition) {
        position.applyTo(pos)
    }
}