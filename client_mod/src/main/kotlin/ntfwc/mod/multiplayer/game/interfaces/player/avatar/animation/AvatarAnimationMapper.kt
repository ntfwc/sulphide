package ntfwc.mod.multiplayer.game.interfaces.player.avatar.animation

import game.Pony
import ntfwc.mod.multiplayer.model.avatar.AnimationId
import ntfwc.mod.multiplayer.util.UnitIntervalFloat

/**
 * Provides functions for mapping between internal avatar animation data
 * representations to other representations.
 */
object AvatarAnimationMapper {
    private val simpleStateMap: Map<Int, AnimationId>

    init {
        val simpleStateMap = HashMap<Int, AnimationId>()
        for (id in AnimationId.values()) {
            when (id.avatarState) {
                Pony.TROT -> {
                }
                else -> simpleStateMap[id.avatarState] = id
            }
        }
        this.simpleStateMap = simpleStateMap
    }

    fun convertToAnimationIdAndValue(avatarState: Int, frame: Float): Pair<AnimationId, UnitIntervalFloat> {
        val animationId = findAnimationId(avatarState, frame)
        return Pair(animationId, AnimationRangeMap.getFrameRange(animationId)?.mapToUnitInterval(frame)
                ?: UnitIntervalFloat.ZERO)
    }

    fun convertFromAnimationIdAndValue(animationId: AnimationId, unitFrameValue: UnitIntervalFloat): Pair<Int, Float> {
        val frame = AnimationRangeMap.getFrameRange(animationId)?.mapFromUnitInterval(unitFrameValue) ?: 0f
        return Pair(animationId.avatarState, frame)
    }

    /**
     * Finds the correct animation ID for the given avatar state and frame.
     */
    private fun findAnimationId(avatarState: Int, frame: Float): AnimationId {
        return when (avatarState) {
            Pony.TROT -> AnimationRangeMap.findTrotAnimationId(frame)
            else -> simpleStateMap[avatarState] ?: AnimationId.IDLE
        }
    }
}