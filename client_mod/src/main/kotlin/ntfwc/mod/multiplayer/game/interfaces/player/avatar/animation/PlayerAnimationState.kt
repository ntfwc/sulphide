package ntfwc.mod.multiplayer.game.interfaces.player.avatar.animation

import game.Pony
import ntfwc.mod.multiplayer.model.avatar.NetPlayerAnimationState
import ntfwc.mod.multiplayer.reflection.PonyDataAccessor
import ntfwc.mod.multiplayer.util.RadianAngle
import ntfwc.mod.multiplayer.util.UnitIntervalFloat
import ntfwc.mod.multiplayer.util.vector.NormalizedVector3f
import ntfwc.mod.multiplayer.util.vector.Vector3f

/**
 * An animation state for a player.
 */
data class PlayerAnimationState(val state: Int,
                                val frame: Float,
                                val wingFrame: Float,
                                val wingExtend: Float,
                                val wingInterp: Float,
                                val mirror: Boolean,
                                val rollAxis: Vector3f?,
                                val rollAmount: Float,
                                val dashTimer: Short,
                                val sideSkidFader: Float,
                                val flapping: Boolean,
                                val pitch: Float,
                                val roll: Float,
                                val yaw: Float,
                                val forwardValue: Float) {
    companion object {
        fun from(pony: Pony): PlayerAnimationState {
            return PlayerAnimationState(
                    pony.state,
                    pony.frame,
                    pony.wingFrame,
                    pony.wingExtend,
                    pony.wingInterp,
                    pony.mirror,
                    getRollAxisValue(pony),
                    PonyDataAccessor.getRollAmount(pony),
                    pony.dashTimer,
                    PonyDataAccessor.getSideSkidFader(pony),
                    PonyDataAccessor.getFlapping(pony),
                    PonyDataAccessor.getPitch(pony),
                    PonyDataAccessor.getRoll(pony),
                    PonyDataAccessor.getYaw(pony),
                    PonyDataAccessor.getForwardValue(pony))
        }

        private fun getRollAxisValue(pony: Pony): Vector3f? {
            val rollAxis = PonyDataAccessor.getRollAxis(pony) ?: return null
            if (rollAxis.size != 3)
                return null

            return Vector3f(rollAxis[0], rollAxis[1], rollAxis[2])
        }

        fun from(netState: NetPlayerAnimationState): PlayerAnimationState {
            val (state, frame) = AvatarAnimationMapper.convertFromAnimationIdAndValue(netState.animationId, netState.frame)
            return PlayerAnimationState(
                    state,
                    frame,
                    AnimationConstants.wingFrameRange.mapFromUnitInterval(netState.wingFrame),
                    // wingExtend should be between 0 and 1, so no range mapping is required
                    netState.wingExtend.value,
                    AnimationConstants.wingInterpRange.mapFromUnitInterval(netState.wingInterp),
                    netState.mirror,
                    netState.rollAxis?.inner,
                    netState.rollAmount.value,
                    AnimationConstants.dashTimerRange.mapFromUnitInterval(netState.dashTimer),
                    AnimationConstants.sideSkidFaderRange.mapFromUnitInterval(netState.sideSkidFader),
                    netState.flapping,
                    AnimationConstants.pitchRange.mapFromUnitInterval(netState.pitch),
                    AnimationConstants.rollRange.mapFromUnitInterval(netState.roll),
                    AnimationConstants.yawRange.mapFromUnitInterval(netState.yaw),
                    AnimationConstants.forwardValueRange.mapFromUnitInterval(netState.forwardValue))
        }
    }

    fun toNetState(): NetPlayerAnimationState {
        val (animationId, frame) = AvatarAnimationMapper.convertToAnimationIdAndValue(this.state, this.frame)
        val rollAxisValue = this.rollAxis
        val rollAxis = if (rollAxisValue != null)
            NormalizedVector3f(rollAxisValue)
        else
            null

        return NetPlayerAnimationState(
                animationId,
                frame,
                AnimationConstants.wingFrameRange.mapToUnitInterval(this.wingFrame),
                // wingExtend should be between 0 and 1, but the actual value does sometimes end up as a very small negative value, so clamp it
                UnitIntervalFloat.clamp(this.wingExtend),
                AnimationConstants.wingInterpRange.mapToUnitInterval(this.wingInterp),
                this.mirror,
                rollAxis,
                RadianAngle(this.rollAmount),
                AnimationConstants.dashTimerRange.mapToUnitInterval(this.dashTimer),
                AnimationConstants.sideSkidFaderRange.mapToUnitInterval(this.sideSkidFader),
                this.flapping,
                AnimationConstants.pitchRange.mapToUnitInterval(this.pitch),
                AnimationConstants.rollRange.mapToUnitInterval(this.roll),
                AnimationConstants.yawRange.mapToUnitInterval(this.yaw),
                AnimationConstants.forwardValueRange.mapToUnitInterval(this.forwardValue))
    }
}
