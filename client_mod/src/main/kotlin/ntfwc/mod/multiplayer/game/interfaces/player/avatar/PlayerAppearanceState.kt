package ntfwc.mod.multiplayer.game.interfaces.player.avatar

import game.Player
import game.Pony
import mechanics.PonyData
import ntfwc.mod.multiplayer.game.interfaces.player.avatar.animation.AnimationConstants
import ntfwc.mod.multiplayer.game.interfaces.player.avatar.animation.PlayerAnimationState
import ntfwc.mod.multiplayer.model.avatar.NetPlayerAppearanceState
import ntfwc.mod.multiplayer.model.avatar.PlayerArmoredState
import ntfwc.mod.multiplayer.util.Color3f
import ntfwc.mod.multiplayer.util.UnitIntervalFloat
import ntfwc.mod.multiplayer.util.ValidatedColor3f
import ntfwc.mod.multiplayer.util.quaternion.NormalizedQuaternion

/**
 * State of a player appearance.
 */
data class PlayerAppearanceState(
        val disguise: PonyData,
        val rotationTransform: PlayerRotationTransform,
        val animationState: PlayerAnimationState,
        val currentMood: Pony.Mood?,
        val nextMoodMultiplier: Float,
        val nextMood: Pony.Mood?,
        val armoredState: PlayerArmoredState,
        val melindaIsRiding: Boolean,
        val hasInfiniteFlap: Boolean,
        val druggedness: Short,
        val bloody: Float,
        val glowyTimer: Int,
        val glowyColor: Color3f
) {
    companion object {
        /**
         * Used to map from modified clone names to disguise names, for the disguises with no wings,
         * which are cloned and renamed when applied.
         */
        private val noWingsDisguiseNameMap: Map<String, String>

        init {
            val noWingsDisguiseNameMap = HashMap<String, String>()
            for (ponyData in PonyData.values(PonyData::class.java)) {
                if (ponyData.wingArmature == null) {
                    noWingsDisguiseNameMap[ponyData.name + "(Disguise)"] = ponyData.getName()
                }
            }
            this.noWingsDisguiseNameMap = noWingsDisguiseNameMap
        }

        fun from(player: Player): PlayerAppearanceState {
            return PlayerAppearanceState(
                    player.appearance,
                    PlayerRotationTransform.from(player),
                    PlayerAnimationState.from(player),
                    player.currentMood,
                    player.nextMoodMultiplier,
                    player.nextMood,
                    PlayerArmoredState.fromInternalValue(player.armored),
                    player.superMode,
                    player.infiniteFlap > 0,
                    player.druggedness,
                    player.bloody,
                    player.glowyTimer,
                    Color3f.from(player.glowyColor))
        }

        fun from(netState: NetPlayerAppearanceState): PlayerAppearanceStateFromNet {
            val rotationTransform = netState.rotation.toPlayerRotationTransform()
            val animationState = PlayerAnimationState.from(netState.animationState)

            var disguise = DisguiseMap.getPonyData(netState.disguiseName)
            val isMissingDisguise = if (disguise == null) {
                disguise = DisguiseMap.getFallbackPonyData()
                true
            } else {
                false
            }

            val appearanceState = PlayerAppearanceState(
                    disguise,
                    rotationTransform,
                    animationState,
                    netState.currentMood,
                    // nextMoodMultiplier should be between 0 and 1, so no range mapping is required
                    netState.nextMoodMultiplier.value,
                    netState.nextMood,
                    netState.armoredState,
                    netState.melindaIsRiding,
                    netState.hasInfiniteFlap,
                    AnimationConstants.druggednessRange.mapFromUnitInterval(netState.druggedness),
                    // bloody should be between 0 and 1, so no range mapping is required
                    netState.bloody.value,
                    AnimationConstants.glowyTimerRange.mapFromUnitInterval(netState.glowyTimer),
                    netState.glowyColor.inner)
            return PlayerAppearanceStateFromNet(appearanceState, isMissingDisguise)
        }
    }

    fun toNetState(): NetPlayerAppearanceState {
        val rotation = NormalizedQuaternion.from(this.rotationTransform)
        val animationState = this.animationState.toNetState()

        return NetPlayerAppearanceState(
                getDisguiseName(this.disguise),
                rotation,
                animationState,
                this.currentMood,
                // This should already be between 0 and 1
                UnitIntervalFloat(this.nextMoodMultiplier),
                this.nextMood,
                this.armoredState,
                this.melindaIsRiding,
                this.hasInfiniteFlap,
                AnimationConstants.druggednessRange.mapToUnitInterval(this.druggedness),
                // This should already be between 0 and 1, but sometimes it is slightly larger than 1
                UnitIntervalFloat.clamp(this.bloody),
                AnimationConstants.glowyTimerRange.mapToUnitInterval(this.glowyTimer),
                ValidatedColor3f.clamp(this.glowyColor)
        )
    }

    private fun getDisguiseName(ponyData: PonyData): String {
        val getNameResult = ponyData.getName()

        // Handle cloned disguises
        val noWingsName = noWingsDisguiseNameMap[getNameResult]
        if (noWingsName != null)
            return noWingsName

        return getNameResult
    }
}
