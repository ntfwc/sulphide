package ntfwc.mod.multiplayer.game.interfaces.player.avatar

import game.Pony
import ntfwc.mod.multiplayer.reflection.PonyDataAccessor
import ntfwc.mod.multiplayer.util.vector.Vector3d

/**
 * Stores an immutable copy of the vectors, from a Player, used to make the rotation matrix it uses.
 */
data class PlayerRotationTransform(val vX: Vector3d, val vY: Vector3d, val vZ: Vector3d) {
    /**
     * Constructs a rotation transform using the given 3x3 rotation matrix values.
     */
    constructor(m00: Double, m01: Double, m02: Double,
                m10: Double, m11: Double, m12: Double,
                m20: Double, m21: Double, m22: Double) :
            this(Vector3d(m00, m10, m20),
                    Vector3d(m01, m11, m21),
                    Vector3d(m02, m12, m22))

    companion object {
        fun from(pony: Pony): PlayerRotationTransform {
            return PlayerRotationTransform(
                    Vector3d.from(PonyDataAccessor.getSVX(pony)),
                    Vector3d.from(PonyDataAccessor.getSVY(pony)),
                    Vector3d.from(PonyDataAccessor.getSVZ(pony)))
        }
    }

    /**
     * Get m00 of the rotation matrix.
     */
    fun getM00() = vX.x

    /**
     * Get m01 of the rotation matrix.
     */
    fun getM01() = vY.x

    /**
     * Get m02 of the rotation matrix.
     */
    fun getM02() = vZ.x

    /**
     * Get m10 of the rotation matrix.
     */
    fun getM10() = vX.y

    /**
     * Get m11 of the rotation matrix.
     */
    fun getM11() = vY.y

    /**
     * Get m12 of the rotation matrix.
     */
    fun getM12() = vZ.y

    /**
     * Get m20 of the rotation matrix.
     */
    fun getM20() = vX.z

    /**
     * Get m21 of the rotation matrix.
     */
    fun getM21() = vY.z

    /**
     * Get m22 of the rotation matrix.
     */
    fun getM22() = vZ.z
}