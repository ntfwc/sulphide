package ntfwc.mod.multiplayer.game.interfaces

import game.Entity
import graphics.GFX
import main.Scene
import ntfwc.mod.multiplayer.model.ConnectionStatus
import util.V3D
import java.util.*

/**
 * Draws a connection status in the overlay.
 */
class ConnectionStatusEntity : Entity(V3D(0.0, 0.0, 0.0), Scene.OVERLAY_DEPTH) {
    private var connectionStatus: ConnectionStatus? = null
    private var lastTextToDraw: String? = null
    private var textToDraw: String? = null
    private val fadeInOutHandler = FadeInOutHandler(500)

    init {
        visible = true
    }

    fun setStatus(connectionStatus: ConnectionStatus?) {
        this.connectionStatus = connectionStatus
    }

    override fun step() {
        super.step()

        val connectionStatus = this.connectionStatus
        val textToDraw: String?
        val nextVisibleState: Boolean
        if (connectionStatus != null &&
                (connectionStatus.timeToStopShowingStatus == null ||
                        Date() < connectionStatus.timeToStopShowingStatus)) {
            textToDraw = connectionStatus.text
            this.lastTextToDraw = textToDraw
            nextVisibleState = true
        } else {
            textToDraw = this.lastTextToDraw
            nextVisibleState = false
        }
        this.textToDraw = textToDraw
        fadeInOutHandler.setVisible(nextVisibleState)
    }

    override fun render() {
        val textToDraw = this.textToDraw ?: return
        drawText(textToDraw, fadeInOutHandler.getAlpha())
    }

    private fun drawText(text: String, alpha: Float) {
        if (alpha == 0f)
            return

        drawText(text, 0f, 0f, 0f, alpha, 19f, 19f)
        drawText(text, 1f, 1f, 1f, alpha, 20f, 20f)
    }
}

private fun drawText(text: String, red: Float, green: Float, blue: Float, alpha: Float, posX: Float, posY: Float) {
    val textRenderer = GFX.gfx.anon16
    textRenderer.setColor(red, green, blue, alpha)
    textRenderer.begin()
    textRenderer.draw(text, posX, posY)
    textRenderer.end()
}
