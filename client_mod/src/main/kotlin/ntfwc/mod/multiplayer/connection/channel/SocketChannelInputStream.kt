package ntfwc.mod.multiplayer.connection.channel

import java.io.InputStream
import java.net.SocketTimeoutException
import java.nio.ByteBuffer
import java.nio.channels.SelectionKey
import java.nio.channels.Selector
import java.nio.channels.SocketChannel

/**
 * An input stream wrapping an asynchronous socket channel.
 */
class SocketChannelInputStream(private val socketChannel: SocketChannel, private val readTimeout: Int) : InputStream() {
    private var selector = Selector.open()

    init {
        socketChannel.register(selector, SelectionKey.OP_READ)
    }

    override fun read(): Int {
        val byteBuffer = ByteBuffer.allocate(1)
        val readResult = read(byteBuffer)
        if (readResult <= 0)
            return -1

        return byteBuffer[0].toInt() and 0xff
    }

    override fun read(b: ByteArray): Int {
        return read(ByteBuffer.wrap(b))
    }

    override fun read(b: ByteArray, off: Int, len: Int): Int {
        return read(ByteBuffer.wrap(b, off, len))
    }

    /**
     * Attempts to read at least 1 byte into the given buffer.
     *
     * @return The number of bytes read, or -1 if EOF is reached.
     */
    private fun read(b: ByteBuffer): Int {
        val startTime = System.currentTimeMillis()

        var result = 0
        while (true) {
            result = socketChannel.read(b)
            if (result != 0)
                return result

            val readTime = System.currentTimeMillis() - startTime
            val remainingTimeoutTime = readTimeout - readTime
            if (remainingTimeoutTime < 0)
                throw SocketTimeoutException()

            selector.select(remainingTimeoutTime)
        }
    }

    override fun close() {
        selector.close()
        socketChannel.close()
    }
}