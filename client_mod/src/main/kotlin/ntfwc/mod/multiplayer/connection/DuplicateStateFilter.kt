package ntfwc.mod.multiplayer.connection

import ntfwc.mod.multiplayer.model.HasPlayerState
import ntfwc.mod.multiplayer.model.avatar.array.PlayerArrayState

/**
 * Filters out duplicate states.
 */
class DuplicateStateFilter(firstState: PlayerArrayState) {
    private var lastState: PlayerArrayState = firstState

    fun <T : HasPlayerState> handleStates(stateItems: List<T>): List<T> {
        val filteredStateItems = ArrayList<T>(stateItems.size)
        for (stateItem in stateItems) {
            val filteredStateItem = handleState(stateItem)
            if (filteredStateItem != null)
                filteredStateItems.add(filteredStateItem)
        }
        return filteredStateItems
    }

    /**
     * If the state of the given object is not equal to the last state, it is returned. Otherwise null is returned.
     */
    fun <T : HasPlayerState> handleState(obj: T): T? {
        val state = obj.getState()
        if (this.lastState == state)
            return null

        this.lastState = state
        return obj
    }
}