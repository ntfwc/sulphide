package ntfwc.mod.multiplayer.connection.channel

import java.io.OutputStream
import java.nio.ByteBuffer
import java.nio.channels.SelectionKey
import java.nio.channels.Selector
import java.nio.channels.SocketChannel

/**
 * An output stream wrapping an asynchronous socket channel.
 */
class SocketChannelOutputStream(private var socketChannel: SocketChannel) : OutputStream() {
    private var selector = Selector.open()

    init {
        socketChannel.register(selector, SelectionKey.OP_WRITE)
    }

    override fun write(b: Int) {
        write(byteArrayOf(b.toByte()))
    }

    override fun write(b: ByteArray) {
        write(ByteBuffer.wrap(b))
    }

    override fun write(b: ByteArray, off: Int, len: Int) {
        write(ByteBuffer.wrap(b, off, len))
    }

    private fun write(byteBuffer: ByteBuffer) {
        while (true) {
            socketChannel.write(byteBuffer)
            if (!byteBuffer.hasRemaining())
                return

            selector.select()
        }
    }

    override fun close() {
        selector.close()
        socketChannel.close()
    }
}