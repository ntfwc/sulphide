package ntfwc.mod.multiplayer.connection

import ntfwc.mod.multiplayer.connection.channel.SocketChannelInputStream
import ntfwc.mod.multiplayer.connection.channel.SocketChannelOutputStream
import java.nio.channels.SocketChannel

private const val READ_TIMEOUT_MILLISECONDS = 5000

/**
 * An initial established TCP connection. Used to exchange headers to
 * verify the connection is with a server.
 */
class InitialConnection(socketChannel: SocketChannel) {
    val inputStream = SocketChannelInputStream(socketChannel, READ_TIMEOUT_MILLISECONDS).buffered()
    val outputStream = SocketChannelOutputStream(socketChannel).buffered()

    fun close() {
        kotlin.runCatching {
            inputStream.close();
            outputStream.close();
        }
    }
}
