package ntfwc.mod.multiplayer.connection

/**
 * A general connection status. Will be shown in the menu that controls things.
 */
class GeneralConnectionStatus(val description: String, val usernameOnServer: String = "")