package ntfwc.mod.multiplayer.connection

import ntfwc.mod.multiplayer.messages.from_client.ClientMessage
import ntfwc.mod.multiplayer.util.log
import ntfwc.mod.multiplayer.util.traceLog
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.EOFException
import java.net.SocketTimeoutException
import java.nio.charset.Charset
import java.util.zip.Deflater
import java.util.zip.DeflaterOutputStream
import java.util.zip.Inflater
import java.util.zip.InflaterInputStream

private val UTF_8_CHARSET: Charset = Charset.forName("UTF-8")

/**
 * The max client update message size I created was 6108 bytes. For the max server message size, it should be
 * this size plus additional data, like the username, and some additional overhead multiplied by up to 500. We won't
 * be able to support anywhere close to 500 players in a single map.
 */
private const val MAX_SERVER_MESSAGE_SIZE = 500 * (6108 + 60 + 12 + 3)
private const val LEFT_CURLY_BRACE_BYTE_VALUE = '{'.toByte()
private const val MAX_UNSIGNED_SHORT = 65535

class ServerConnection(initialConnection: InitialConnection) {
    private val inputStream = DataInputStream(InflaterInputStream(initialConnection.inputStream, Inflater(true)))
    private val outputStream = DataOutputStream(DeflaterOutputStream(initialConnection.outputStream, Deflater(1, true), true))

    /**
     * Reads the first message. Checks for incorrect input early so we fail quickly if we are trying to connect to the
     * wrong kind of server.
     *
     * @return The message text.
     */
    fun readFirstMessage(): String? {
        try {
            val messageSize = readMessageSize()
            traceLog("Read message size: $messageSize")
            if (!validateMessageSize(messageSize))
                return null

            val message = ByteArray(messageSize)
            val firstMessageByte = inputStream.readByte()
            if (firstMessageByte != LEFT_CURLY_BRACE_BYTE_VALUE) {
                log("The server gave us an invalid first byte for the first message")
                return null
            }
            message[0] = firstMessageByte
            inputStream.readFully(message, 1, messageSize - 1)

            return String(message, UTF_8_CHARSET)
        } catch (e: SocketTimeoutException) {
            log("Timed out waiting to read a message from the server")
            return null
        } catch (e: Throwable) {
            return null
        }
    }

    /**
     * Reads the next message.
     *
     * @return A pair with the timestamp, of when we started reading the message, and the message text.
     */
    fun readNextMessage(): String? {
        try {
            val messageSize = readMessageSize()
            traceLog("Read message size: $messageSize")
            if (!validateMessageSize(messageSize))
                return null

            val message = ByteArray(messageSize)
            inputStream.readFully(message)
            return String(message, UTF_8_CHARSET)
        } catch (e: SocketTimeoutException) {
            log("Timed out waiting to read a message from the server")
            return null
        } catch (e: Throwable) {
            return null
        }
    }

    /**
     * Reads the server message size, which is a 3 byte big-endian number.
     */
    private fun readMessageSize(): Int {
        val ch1 = inputStream.read()
        val ch2 = inputStream.read()
        val ch3 = inputStream.read()
        if (ch1 or ch2 or ch3 < 0)
            throw EOFException()
        return (ch1 shl 16) + (ch2 shl 8) + ch3
    }

    /**
     * Checks if the given message size, from the server, is valid.
     *
     * @return true if validation succeeds, false otherwise.
     */
    private fun validateMessageSize(messageSize: Int): Boolean {
        if (messageSize == 0) {
            log("The server gave us a message size of zero, which is not valid")
            return false
        }

        if (messageSize > MAX_SERVER_MESSAGE_SIZE) {
            log("The server gave us a message size that is way too big to be valid")
            return false
        }

        return true
    }

    fun writeMessage(message: ClientMessage): Boolean {
        val messageJson = message.toJson()
        traceLog("Writing JSON message: $messageJson")
        return writeMessage(messageJson.toByteArray(UTF_8_CHARSET))
    }

    private fun writeMessage(message: ByteArray): Boolean {
        val messageSize = message.size
        validateClientMessageSize(messageSize)

        return kotlin.runCatching {
            writeMessageSize(messageSize)
            outputStream.write(message)
            outputStream.flush()
            return true
        }.getOrDefault(false)
    }

    private fun validateClientMessageSize(messageSize: Int) {
        if (messageSize > MAX_UNSIGNED_SHORT) {
            error("Tried to send a message with a length too long to be encoded")
        }
    }

    private fun writeMessageSize(messageSize: Int) {
        outputStream.writeShort(messageSize)
    }

    fun close() {
        kotlin.runCatching {
            inputStream.close()
            outputStream.close()
        }
    }
}
