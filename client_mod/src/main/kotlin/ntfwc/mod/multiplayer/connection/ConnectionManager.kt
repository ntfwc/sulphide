package ntfwc.mod.multiplayer.connection

import com.beust.klaxon.JsonObject
import ntfwc.mod.multiplayer.buffer.LocalPlayerStateBuffer
import ntfwc.mod.multiplayer.messages.ProtocolHeaders
import ntfwc.mod.multiplayer.messages.from_client.ClientMessage
import ntfwc.mod.multiplayer.messages.from_client.ClientStateUpdateMessage
import ntfwc.mod.multiplayer.messages.from_client.IntroductionMessage
import ntfwc.mod.multiplayer.messages.from_client.MapSwitchMessage
import ntfwc.mod.multiplayer.messages.from_server.ClientRejectionMessage
import ntfwc.mod.multiplayer.messages.from_server.IntroductionReplyMessage
import ntfwc.mod.multiplayer.messages.from_server.MapSwitchReplyMessage
import ntfwc.mod.multiplayer.messages.from_server.StateUpdateMessage
import ntfwc.mod.multiplayer.model.MapName
import ntfwc.mod.multiplayer.model.MessagePlayerStateList
import ntfwc.mod.multiplayer.model.avatar.array.PlayerArrayState
import ntfwc.mod.multiplayer.model.avatar.array.TimestampedPlayerArrayState
import ntfwc.mod.multiplayer.sync.MultiplayerStateContainer
import ntfwc.mod.multiplayer.sync.WaitOnCancelFuture
import ntfwc.mod.multiplayer.util.*
import java.net.InetSocketAddress
import java.nio.channels.ClosedByInterruptException
import java.nio.channels.SocketChannel
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.ThreadLocalRandom
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicReference

private const val MAX_RETRY_WAIT_SECONDS = 30
private const val POSITION_UPDATE_PERIOD_MILLISECONDS: Long = 1000 / 6
private const val MAX_INCOMING_MESSAGE_RATE_IN_MILLISECONDS = 1000 / 12

/**
 * A thread that manages connections with the server.
 */
class ConnectionManager(private val multiplayerStateContainer: MultiplayerStateContainer,
                        playerState: TimestampedPlayerArrayState,
                        private val generalConnectionStatus: AtomicReference<GeneralConnectionStatus>) {
    private val mainExecutor = ExecutorServiceWrapper(
            Executors.newSingleThreadExecutor(ModThreadFactory { "connection-manager-thread" }))
    private val ioExecutor = ScheduledExecutorServiceWrapper(Executors.newScheduledThreadPool(
            2,
            ModThreadFactory { threadNum -> "connection-io-thread-$threadNum" }))
    private val localPlayerStateBuffer = LocalPlayerStateBuffer(playerState)

    private var state: State = Idle()
    private val currentMapRef: AtomicReference<MapName> = AtomicReference()
    private val isNotConnectedOrTryingToConnect = AtomicBoolean(true)

    init {
        log("Started connection management threads")
    }

    /**
     * Begins connecting to the given address. If there is a connection with a server, it will be dropped.
     */
    fun startConnecting(host: String, port: Int, username: String, map: MapName) {
        currentMapRef.set(map)
        generalConnectionStatus.set(GeneralConnectionStatus("Connecting to '$host:$port'"))
        setState(null, Connect(ConnectionConfiguration(host, port, username)))
    }

    fun updatePlayerState(playerState: TimestampedPlayerArrayState) {
        localPlayerStateBuffer.addState(playerState)
    }

    /**
     * Updates the player map. This will be synchronized with the server.
     */
    fun updateMap(map: MapName) {
        currentMapRef.set(map)
    }

    fun getCurrentMap(): MapName? {
        return currentMapRef.get()
    }

    /**
     * Drops the active connection, if there is one, and stops trying to connect.
     */
    fun disconnectAndStop() {
        log("Disconnecting and stopping connection attempts")
        multiplayerStateContainer.setConnectionStatus("", null)
        setIdleState(null, "Disconnected")
    }

    /**
     * @return True if there is no connection and the program is not trying to (re-)establish a new connection.
     */
    fun isNotConnectedOrTryingToConnect(): Boolean {
        return isNotConnectedOrTryingToConnect.get()
    }

    private fun setState(requestingState: State?, newState: State) {
        setStateWithStopFlag(requestingState, newState, false)
    }

    private fun setIdleState(requestingState: State?, generalStatusMessage: String) {
        generalConnectionStatus.set(GeneralConnectionStatus(generalStatusMessage))
        setState(requestingState, Idle())
    }

    /**
     * A state switch procedure that takes a state specific stop flag. This can help avoid concurrency issues where
     * the state switch stop method behavior is dependent on some contextual information.
     *
     * @param requestingState The state that is requesting this state change. Should be null if the state change was
     *                        requested from outside a state.
     * @param newState The new state to switch to.
     * @param stateSpecificStopFlag The flag to send to the stop method. This will be false by default.
     */
    private fun setStateWithStopFlag(requestingState: State?, newState: State, stateSpecificStopFlag: Boolean) {
        mainExecutor.submit {
            if (requestingState != null && state != requestingState) {
                // State switch from an older state, it should be ignored
                return@submit
            }

            state.stop(stateSpecificStopFlag)
            log("Setting state to ${newState.javaClass.simpleName}")
            state = newState
            isNotConnectedOrTryingToConnect.set(newState is Idle)
            state.start()
        }
    }

    private fun parseJsonMessageObject(jsonMessage: String): JsonObject? {
        if (JsonSecurity.looksLikeMaliciousJson(jsonMessage)) {
            log("The JSON of the server message received looks excessive to a malicious extent. Parsing failed.")
            return null
        }

        return ParsingUtil.parseJsonObject(jsonMessage)
    }

    private abstract class State {
        open fun start() {
        }

        open fun stop() {
        }

        open fun stop(stateSpecificFlag: Boolean) {
            stop()
        }
    }

    /**
     * The do nothing state.
     */
    private class Idle : State()

    /**
     * A state for trying to connect to a server.
     */
    private inner class Connect(private val connectionConfiguration: ConnectionConfiguration, private val isReconnect: Boolean = false) : State() {
        private var connectionFuture: WaitOnCancelFuture? = null
        private var nextRetryWaitSeconds = 2

        override fun start() {
            log("Starting connect task. host='${connectionConfiguration.host}', port=${connectionConfiguration.port}")
            connectionFuture = ioExecutor.submit(this::connectToServer)
        }

        override fun stop() {
            connectionFuture?.cancelAndWait()
        }

        private fun connectToServer() {
            if (isReconnect) {
                multiplayerStateContainer.setConnectionStatus("Lost connection, will try to reconnect in about $nextRetryWaitSeconds seconds", 3)
                waitToRetry()
            }

            while (true) {
                val connectResult = tryToConnect()
                val initialConnection = connectResult.connection
                if (initialConnection != null) {
                    log("Got connection")
                    multiplayerStateContainer.setConnectionStatus("Connected. Initializing...", 3)
                    setState(this, Connected(connectionConfiguration, initialConnection))
                    return
                }

                // If we were interrupted while trying to connect, just return
                if (connectResult.interrupted)
                    return

                multiplayerStateContainer.setConnectionStatus("Connection failed, will retry in about $nextRetryWaitSeconds seconds", 3)
                waitToRetry()
            }
        }

        private fun waitToRetry() {
            // Add some random jitter to avoid having all clients trying to reconnecting at the same time if the server goes down
            val jitter: Float = ThreadLocalRandom.current().nextFloat() * 0.6f - 0.3f
            Thread.sleep(((nextRetryWaitSeconds + jitter) * 1000).toLong())
            nextRetryWaitSeconds += 2
            if (nextRetryWaitSeconds > MAX_RETRY_WAIT_SECONDS)
                nextRetryWaitSeconds = MAX_RETRY_WAIT_SECONDS
        }

        private fun tryToConnect(): ConnectResult {
            multiplayerStateContainer.setConnectionStatus("Connecting to server...")
            return try {
                val socketChannel = SocketChannel.open()
                socketChannel.configureBlocking(true)
                socketChannel.connect(InetSocketAddress(connectionConfiguration.host, connectionConfiguration.port))
                socketChannel.configureBlocking(false)
                ConnectResult(InitialConnection(socketChannel))
            } catch (e: ClosedByInterruptException) {
                ConnectResult(null, true)
            } catch (e: Exception) {
                log("Connection failed, reason: ${e.message}")
                ConnectResult(null, false)
            }
        }
    }

    /**
     * A state for working with an established TCP connection.
     */
    private inner class Connected(private val connectionConfiguration: ConnectionConfiguration, private val connection: InitialConnection) : State() {
        private var initTaskFuture: WaitOnCancelFuture? = null

        override fun start() {
            initTaskFuture = ioExecutor.submit(this::init)
        }

        @Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
        override fun stop(doNotCloseConnectionOnStop: Boolean) {
            if (!doNotCloseConnectionOnStop)
                connection.close()
            initTaskFuture?.cancelAndWait()
        }

        private fun init() {
            log("Exchanging protocol headers")
            if (!handleHeaderExchange()) {
                log("Protocol header exchange failed")
                multiplayerStateContainer.setConnectionStatus("Protocol header exchange failed, disconnected.", 5)
                setIdleState(this, "Disconnected due to protocol header exchange failure")
                return
            }

            setStateWithStopFlag(this, ConnectedToServer(connectionConfiguration, ServerConnection(connection)), true)
        }

        /**
         * Handles the protocol header exchange.
         *
         * @return True if the exchange succeeds, false otherwise.
         */
        private fun handleHeaderExchange(): Boolean {
            try {
                val outputStream = connection.outputStream
                outputStream.write(ProtocolHeaders.CLIENT_HEADER)
                outputStream.flush()
            } catch (e: Exception) {
                log("Writing the client header failed. reason: ${e.message}")
                return false
            }

            try {
                val inputStream = connection.inputStream
                for (byte in ProtocolHeaders.SERVER_HEADER) {
                    if (byte != inputStream.read().toByte()) {
                        log("Read unexpected header byte from server")
                        return false
                    }
                }
            } catch (e: Exception) {
                log("Writing the client header failed. reason: ${e.message}")
                return false
            }

            return true
        }
    }

    /**
     * A state for working with an established server connection.
     */
    private inner class ConnectedToServer(private val connectionConfiguration: ConnectionConfiguration, private val connection: ServerConnection) : State() {
        private var initTaskFuture: WaitOnCancelFuture? = null

        override fun start() {
            initTaskFuture = ioExecutor.submit(this::init)
        }

        @Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
        override fun stop(doNotCloseConnectionOnStop: Boolean) {
            if (!doNotCloseConnectionOnStop)
                connection.close()
            initTaskFuture?.cancelAndWait()
        }

        private fun init() {
            val map = getCurrentMap()
            if (map == null) {
                // This shouldn't happen, but lets handle this case
                log("Connected state init(): No current map. Switching to idle state.")
                setIdleState(this, "Disconnected due to error: Connected state init(): No current map")
                return
            }

            val desiredUsername = connectionConfiguration.username
            val initialState = localPlayerStateBuffer.getInitialStateForConnection()
            if (!sendIntroduction(desiredUsername, map, initialState)) {
                handleError("Failed to write introduction message")
                return
            }

            val message = connection.readFirstMessage()
            if (message == null) {
                handleError("Failed to read introduction reply message")
                return
            }

            traceLog("Received message: $message")
            if (!handleReply(message, desiredUsername, map, initialState.arrayState))
                handleError("Failed to parse introduction reply message")
        }

        private fun sendIntroduction(desiredUsername: String, map: MapName, state: TimestampedPlayerArrayState): Boolean {
            val introductionMessage = IntroductionMessage(desiredUsername, map, state)
            traceLog("Sending introduction message: $introductionMessage")
            return connection.writeMessage(introductionMessage)
        }

        private fun handleError(failureMessage: String) {
            multiplayerStateContainer.setConnectionStatus("The introduction exchange with the server failed, disconnected", 5)
            log(failureMessage)
            setIdleState(this, "Disconnected due to error: $failureMessage")
        }

        /**
         * Handles the reply.
         *
         * @return true if the message was handled, false if parsing failed.
         */
        private fun handleReply(message: String, desiredUsername: String, map: MapName, initialStateSent: PlayerArrayState): Boolean {
            val parsedJson = parseJsonMessageObject(message) ?: return false
            when (val type = ParsingUtil.getString(parsedJson, "t")) {
                null -> {
                    val reply = IntroductionReplyMessage.fromJson(parsedJson) ?: return false
                    log("Received introduction reply: $reply")
                    if (reply.actualUsername != desiredUsername)
                        multiplayerStateContainer.setConnectionStatus("Connected successfully. Username already taken. Username set to '${reply.actualUsername}'", 8)
                    else
                        multiplayerStateContainer.setConnectionStatus("Connected successfully", 3)

                    generalConnectionStatus.set(GeneralConnectionStatus("Connected to '${connectionConfiguration.host}:${connectionConfiguration.port}'", reply.actualUsername))
                    setStateWithStopFlag(this, ConnectedAndInitialized(connectionConfiguration, map, connection, initialStateSent), true)
                }
                ClientRejectionMessage.TYPE_ID -> {
                    val rejectionMessage = ClientRejectionMessage.fromJson(parsedJson) ?: return false
                    log("Received client rejection message. reason='${rejectionMessage.reason}'")
                    multiplayerStateContainer.setConnectionStatus("Server rejected client: '${rejectionMessage.reason}'", 10)
                    setIdleState(this, "Disconnected due to client rejection")
                }
                else -> {
                    log("Server responded to the introduction with a message with an unknown type: '$type'")
                    return false
                }
            }
            return true
        }
    }

    /**
     * A state for working with an established server connection where the initial setup has completed.
     */
    private inner class ConnectedAndInitialized(private val connectionConfiguration: ConnectionConfiguration, private var mapOnServer: MapName, private val connection: ServerConnection, firstStateSent: PlayerArrayState) : State() {
        private var readTaskFuture: WaitOnCancelFuture? = null
        private var writeTaskFuture: WaitOnCancelFuture? = null
        private val duplicateStateFilter = DuplicateStateFilter(firstStateSent)
        private val messageHandler = SynchronizedMessageHandler()

        override fun start() {
            readTaskFuture = ioExecutor.submit(this::readFromServer)
            writeTaskFuture = ioExecutor.scheduleAtFixedRate(this::updatePlayerPosition, POSITION_UPDATE_PERIOD_MILLISECONDS)
        }

        override fun stop() {
            // We have to close the connection first, otherwise ongoing I/O operations will be uninterruptible
            connection.close()
            readTaskFuture?.cancelAndWait()
            writeTaskFuture?.cancelAndWait()
            multiplayerStateContainer.clearPlayers()
        }

        private fun updatePlayerPosition() {
            val currentMap = getCurrentMap()
            if (currentMap == null) {
                // This shouldn't happen, but lets handle this case
                log("ConnectedAndInitialized state updatePlayerPosition(): No current map. Switching to idle state")
                setIdleState(this, "Disconnected due to error: ConnectedAndInitialized state updatePlayerPosition(): No current map")
                return
            }

            if (currentMap != mapOnServer) {
                val lastState = localPlayerStateBuffer.clearAndGetLastState() ?: return
                duplicateStateFilter.handleState(lastState)

                messageHandler.prepareForMapSwitch()
                writeMessage(MapSwitchMessage(currentMap, lastState))
                mapOnServer = currentMap
                return
            }

            val bufferItems = localPlayerStateBuffer.drainToList()
            if (bufferItems.isEmpty())
                return

            val filteredStates = duplicateStateFilter.handleStates(bufferItems)
            val nonEmptyFilteredStateList = NonEmptyList.from(filteredStates) ?: return
            val updateStateList = MessagePlayerStateList(nonEmptyFilteredStateList)
            writeMessage(ClientStateUpdateMessage(updateStateList))
        }

        private fun readFromServer() {
            log("Starting to read from server")
            val throttler = Throttler(MAX_INCOMING_MESSAGE_RATE_IN_MILLISECONDS)
            while (true) {
                val message = connection.readNextMessage() ?: break
                traceLog("Received message: $message")
                if (!messageHandler.handleMessage(message)) {
                    log("Could not parse message from server")
                    multiplayerStateContainer.setConnectionStatus("Given unrecognized message from server, disconnected", 5)
                    setIdleState(this, "Disconnected due to error: Could not parse message from server")
                    return
                }

                throttler.tick()
            }

            log("Server connection lost")
            generalConnectionStatus.set(GeneralConnectionStatus("Reconnecting to '${connectionConfiguration.host}:${connectionConfiguration.port}'"))
            setState(this, Connect(connectionConfiguration, true))
        }

        private fun writeMessage(clientMessage: ClientMessage) {
            traceLog("Writing message to server: $clientMessage")
            if (!connection.writeMessage(clientMessage)) {
                log("Failed to send message, connection lost")
                setState(this, Connect(connectionConfiguration, true))
            }
        }
    }

    /**
     * A message handler that synchronizes handling messages with related events.
     */
    private inner class SynchronizedMessageHandler {
        private var waitingForMapSwitchReply = false

        /**
         * Tries to handle the given message from the client.
         *
         * @return true if parsing succeeded, false otherwise.
         */
        @Synchronized
        fun handleMessage(message: String): Boolean {
            val parsedJson = parseJsonMessageObject(message) ?: return false
            when (ParsingUtil.getString(parsedJson, "t")) {
                StateUpdateMessage.TYPE_ID -> {
                    if (waitingForMapSwitchReply) {
                        traceLog("Skipped update, waiting for map switch reply")
                        return true
                    }

                    val stateUpdateMessage = StateUpdateMessage.fromJson(parsedJson) ?: return false
                    handleStateUpdate(stateUpdateMessage)
                }
                MapSwitchReplyMessage.TYPE_ID -> {
                    val mapSwitchReplyMessage = MapSwitchReplyMessage.fromJson(parsedJson) ?: return false
                    handleMapSwitchReply(mapSwitchReplyMessage)
                }
                else -> {
                    return false
                }
            }

            return true
        }

        @Synchronized
        fun prepareForMapSwitch() {
            multiplayerStateContainer.clearPlayers()
            waitingForMapSwitchReply = true
        }

        private fun handleStateUpdate(message: StateUpdateMessage) {
            traceLog("Received a state update message. updates=${message.updates}, removedPlayers=${message.removedPlayers}")
            multiplayerStateContainer.applyNewPlayerUpdates(message.updates, message.removedPlayers)
        }

        private fun handleMapSwitchReply(message: MapSwitchReplyMessage) {
            traceLog("Received a map switch reply. playerStateLists=${message.playerStateLists}")
            multiplayerStateContainer.applyNewPlayerUpdates(message.playerStateLists, Collections.emptyList())
            waitingForMapSwitchReply = false
        }
    }

    private data class ConnectionConfiguration(val host: String, val port: Int, val username: String)

    private class ConnectResult(val connection: InitialConnection?, val interrupted: Boolean = false)
}
