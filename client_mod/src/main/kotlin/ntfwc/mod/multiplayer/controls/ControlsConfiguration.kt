package ntfwc.mod.multiplayer.controls

import ntfwc.mod.multiplayer.controls.mapping.ControlMapping

/**
 * A controls configuration.
 */
class ControlsConfiguration(private val firstMenuButtonMapping: ControlMapping, private val secondMenuButtonMapping: ControlMapping? = null) : ControlsInterface {
    override fun hasMenuButtonBeenPressed(): Boolean {
        if (firstMenuButtonMapping.isButtonPressed())
            return true

        return secondMenuButtonMapping?.isButtonPressed() ?: false
    }

    /**
     * Gets the button name for the first menu button mapping.
     *
     * @return The button name.
     */
    override fun getFirstMenuButtonName(): String = firstMenuButtonMapping.getButtonName()

    /**
     * Gets the button name for the second menu button mapping.
     *
     * @return The button name, or nothing if there is no second mapping.
     */
    override fun getSecondMenuButtonName(): String? = secondMenuButtonMapping?.getButtonName()
}