package ntfwc.mod.multiplayer.controls.mapping

/**
 * Interface for control mappings.
 */
interface ControlMapping {
    /**
     * Checks if the button for this mapping is pressed.
     */
    fun isButtonPressed(): Boolean

    /**
     * Gets the name of the button.
     */
    fun getButtonName(): String
}