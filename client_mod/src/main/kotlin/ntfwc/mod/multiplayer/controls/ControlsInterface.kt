package ntfwc.mod.multiplayer.controls

/**
 * Interface for the mod's controls
 */
interface ControlsInterface {

    /**
     * Checks if the button for opening the menu has been pressed (without repeats).
     *
     * @return True if it has been clicked, false otherwise.
     */
    fun hasMenuButtonBeenPressed(): Boolean

    /**
     * Gets the name of the button for the first menu button mapping.
     *
     * @return The button name
     */
    fun getFirstMenuButtonName(): String

    /**
     * Gets the name of the button for the second menu button mapping.
     */
    fun getSecondMenuButtonName(): String?
}