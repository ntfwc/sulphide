package ntfwc.mod.multiplayer.controls.mapping

import io.IOH

/**
 * A gamepad, or joystick, button control mapping.
 */
class GamepadButtonMapping(private val buttonValue: Int) : ControlMapping {
    override fun isButtonPressed(): Boolean = IOH.checkButtonPressed(buttonValue)

    override fun getButtonName(): String = "B$buttonValue"
}