package ntfwc.mod.multiplayer.controls.mapping

import io.IOH

/**
 * A keyboard key control mapping.
 */
class KeyboardControlMapping(private val key: Int, private val keyChar: Char) : ControlMapping {
    override fun isButtonPressed() = IOH.checkKeyPressedNoRepeat(key)

    override fun getButtonName(): String = keyChar.toString()
}