package ntfwc.mod.multiplayer.menu.control

import com.jogamp.opengl.GL2
import menu.MenuMan
import menu.Selectable
import ntfwc.mod.multiplayer.menu.UIRenderable

/**
 * A button.
 */
abstract class Button(x: Int,
                      y: Int,
                      width: Int,
                      height: Int,
                      /**
                       * Either text or an icon drawn on top of the button. It will be drawn
                       * with the origin point of the coordinate system set at the upper left
                       * hand corner of the button.
                       */
                      private val textOrIcon: ButtonTextOrIcon) :
        Selectable<Any>(null, x, y, width, height),
        UIRenderable {
    var enabled: Boolean = true
        set(value) {
            if (field == value)
                return

            textOrIcon.setEnabledState(value)
            field = value
        }

    override fun render(gl: GL2) {
        MenuMan.drawButton(gl, this, getHighlight(), getMouseHover(), false)

        gl.glPushMatrix()
        gl.glTranslatef(x().toFloat(), y().toFloat(), 0f)
        textOrIcon.render(gl)
        gl.glPopMatrix()
    }

    override fun primaryRelease(mouse: Boolean, isStillHighlighted: Boolean): Boolean {
        if (!isStillHighlighted || !enabled)
            return false

        onClicked()
        return true
    }

    abstract fun getHighlight(): Selectable<*>

    abstract fun getMouseHover(): Boolean

    abstract fun onClicked()
}