package ntfwc.mod.multiplayer.menu.field.stringToType

/**
 * A string converter for optional ints.
 */
class StringToOptionalIntConverter : StringToTypeConverter<Int?> {
    override fun valueToString(value: Int?): String = value?.toString() ?: ""

    override fun stringToValue(string: String): Int? {
        if (string.isEmpty())
            return null

        return runCatching { string.toInt() }.getOrNull()
    }
}