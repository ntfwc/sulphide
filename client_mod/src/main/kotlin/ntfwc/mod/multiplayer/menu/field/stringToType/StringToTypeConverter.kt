package ntfwc.mod.multiplayer.menu.field.stringToType

/**
 * Interface for a converter between strings and a type.
 */
interface StringToTypeConverter<T> {
    fun valueToString(value: T): String

    fun stringToValue(string: String): T
}