package ntfwc.mod.multiplayer.menu.field.stringToType

import ntfwc.mod.multiplayer.menu.field.ConfigurationValue

/**
 * A string configuration value that represents its value as a different type.
 */
class StringToTypeConfigurationValue<T>(name: String, comment: String, private var typedValue: T, private val converter: StringToTypeConverter<T>) :
        ConfigurationValue<String>(name, comment, "") {

    override var value: String = converter.valueToString(typedValue)
        set(newValue) {
            typedValue = converter.stringToValue(newValue)
            field = newValue
        }

    fun getTypedValue(): T = typedValue
}