package ntfwc.mod.multiplayer.menu.entry.screen

import menu.StringEntryScreen
import ntfwc.mod.multiplayer.menu.entry.screen.ValidatingStringEntryScreen.Companion.SC_NONE_MASK

private val NUMBER_CHARACTERS = arrayOf(charArrayOf('1', '2', '3', '4', '5', '6', '7', '8', '9', '0'))
private val NUMBER_CHAR_SET = StringEntryScreen.CharacterSet("Numbers", "123", SC_NONE_MASK, NUMBER_CHARACTERS)

private val HOST_CHARACTERS1 = arrayOf(
        charArrayOf('1', '2', '3', '4', '5', '6', '7', '8', '9', '0'),
        charArrayOf('q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p'),
        charArrayOf('a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', '-'),
        // Note: Strictly speaking, hostnames should not have underscores, but DNS records can have them.
        charArrayOf('z', 'x', 'c', 'v', 'b', 'n', 'm', ':', '.', '_')
)
private val HOST_CHAR_SET1 = StringEntryScreen.CharacterSet("Host Chars 1", "abc", SC_NONE_MASK, HOST_CHARACTERS1)

private val HOST_CHARACTERS2 = arrayOf(
        charArrayOf('%')
)
private val HOST_CHAR_SET2 = StringEntryScreen.CharacterSet("Host Char 2", "%", SC_NONE_MASK, HOST_CHARACTERS2)

private val USERNAME_CHARACTERS3 = arrayOf(
        charArrayOf('!', '@', '#', '$', '%', '^', '&', '*', '(', ')'),
        charArrayOf('-', '=', '+', ';', ':', '"', '{', '}', '[', ']'),
        charArrayOf('`', '~', '?', '/', '\\', '|', '<', '>')
)
private val USERNAME_CHAR_SET3 = StringEntryScreen.CharacterSet("Username Char 3", "!@#", SC_NONE_MASK, USERNAME_CHARACTERS3)

/**
 * The character sets available for string entry screens.
 */
enum class StringEntryCharSet(val gameCharSets: Array<StringEntryScreen.CharacterSet>) {
    NUMERIC(arrayOf(NUMBER_CHAR_SET)),
    HOST_NAME_CHARS(arrayOf(HOST_CHAR_SET1, HOST_CHAR_SET2)),
    USERNAME(createCharacterSetArray(arrayOf(StringEntryScreen.FILE_NAME_CHARS[0], StringEntryScreen.FILE_NAME_CHARS[1], USERNAME_CHAR_SET3),
            generateCharacterSets(createStringOfOtherCharacters())));
}

private fun createCharacterSetArray(characterSets: Array<StringEntryScreen.CharacterSet>, otherCharacterSets: List<StringEntryScreen.CharacterSet>): Array<StringEntryScreen.CharacterSet> {
    val list = ArrayList<StringEntryScreen.CharacterSet>(characterSets.size + otherCharacterSets.size)
    list.addAll(characterSets)
    list.addAll(otherCharacterSets)
    return list.toTypedArray()
}

private fun generateCharacterSets(characters: String): MutableList<StringEntryScreen.CharacterSet> {
    val iterator = characters.iterator()
    val characterSets = ArrayList<StringEntryScreen.CharacterSet>()
    var index = 0
    while (true) {
        val characterSet = createCharacterSet(++index, iterator) ?: break
        characterSets.add(characterSet)
    }

    return characterSets
}

private fun createCharacterSet(index: Int, iterator: Iterator<Char>): StringEntryScreen.CharacterSet? {
    val listOfArrays = ArrayList<CharArray>(4)
    val firstArray = createCharacterSetArray(iterator) ?: return null
    listOfArrays.add(firstArray)
    for (i in 0..2) {
        val array = createCharacterSetArray(iterator) ?: break
        listOfArrays.add(array)
    }

    val displayName = createDisplayName(firstArray)
    return StringEntryScreen.CharacterSet("Generated Char Set $index", displayName, SC_NONE_MASK, listOfArrays.toTypedArray())
}

private fun createDisplayName(array: CharArray): String = String(array).substring(0, 3)

private fun createCharacterSetArray(iterator: Iterator<Char>): CharArray? {
    if (!iterator.hasNext())
        return null

    val charList = ArrayList<Char>(10)
    var i = 0
    while (iterator.hasNext() && i < 10) {
        charList.add(iterator.next())
        i++
    }

    return charList.toCharArray()
}

private fun createStringOfOtherCharacters(): String {
    val stringBuilder = StringBuilder()
    // Some other western characters
    appendCharacterRange(stringBuilder, '\u00a1', '\u017f')
    stringBuilder.append('\u0129')
    appendCharacterRange(stringBuilder, '\u01fc', '\u01ff')
    appendCharacterRange(stringBuilder, '\u0218', '\u021b')
    // Greek characters
    appendCharacterRange(stringBuilder, '\u0384', '\u0386')
    stringBuilder.append('\u038c')
    appendCharacterRange(stringBuilder, '\u038e', '\u03a1')
    appendCharacterRange(stringBuilder, '\u03a3', '\u03ce')
    // Cyrillic characters
    appendCharacterRange(stringBuilder, '\u0401', '\u040c')
    appendCharacterRange(stringBuilder, '\u040e', '\u044f')
    appendCharacterRange(stringBuilder, '\u0451', '\u045c')
    stringBuilder.append('\u045e')
    stringBuilder.append('\u045f')
    stringBuilder.append('\u0490')
    stringBuilder.append('\u0491')

    return stringBuilder.toString()
}

private fun appendCharacterRange(stringBuilder: StringBuilder, startingChar: Char, endingChar: Char) {
    if (startingChar > endingChar)
        throw IllegalArgumentException("Given ending char that does not occur after the starting char")

    for (c in startingChar..endingChar)
        stringBuilder.append(c)
}