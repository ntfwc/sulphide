package ntfwc.mod.multiplayer.menu.control

import com.jogamp.opengl.GL2
import com.jogamp.opengl.util.awt.TextRenderer
import ntfwc.mod.multiplayer.menu.UIRenderable

/**
 * A control that displays the number of players in the map.
 */
class PlayerCountInMapDisplay(textRenderer: TextRenderer, x: Float, y: Float, lineOffset: Float) : UIRenderable {
    private val label1 = LabelWithShadow("Other players in", textRenderer, x, y)
    private val label2 = LabelWithShadow("map: ", textRenderer, x, y + lineOffset)
    private var currentCount: Int? = null

    init {
        // It should be shown disabled since there is no count to begin with
        label1.setEnabledState(false)
        label2.setEnabledState(false)
    }

    override fun render(gl: GL2) {
        label1.render(gl)
        label2.render(gl)
    }

    /**
     * Updates the player count to display.
     *
     * @param count The new count.
     */
    fun updateCount(count: Int?) {
        if (currentCount != count) {
            currentCount = count
            label2.setText("map: ${count ?: ""}")
            label1.setEnabledState(count != null)
            label2.setEnabledState(count != null)
        }
    }
}