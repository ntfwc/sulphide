package ntfwc.mod.multiplayer.menu.field.editor

import com.jogamp.opengl.GL2
import com.jogamp.opengl.util.awt.TextRenderer
import menu.FieldEditor
import menu.MenuMan
import menu.Selectable
import ntfwc.mod.multiplayer.menu.ConfigurationMenu
import ntfwc.mod.multiplayer.menu.field.AbstractConfigurationValue
import ntfwc.mod.multiplayer.menu.field.ConfigurationValue

abstract class AbstractConfigurationFieldEditor<T>(private val configurationValue: ConfigurationValue<T>,
                                                   private val parentMenu: ConfigurationMenu,
                                                   private val index: Int,
                                                   width: Int,
                                                   height: Int) :
        Selectable<AbstractConfigurationValue>(configurationValue,
                parentMenu.xOffset,
                -1,
                width,
                height) {

    open fun renderButton(gl: GL2) {
        MenuMan.drawButton(gl, this, parentMenu.highlight, parentMenu.mouseHover(), false)
    }

    /**
     * Renders the text for this field. Based off the * text rendering in [FieldEditor]
     *
     * @param gl The OpenGL context.
     * @param font The font renderer.
     */
    open fun renderText(gl: GL2, font: TextRenderer) {
        font.setColor(1f, 1f, 1f, 1f)
        font.draw3D(configurationValue.name, x() + 32.toFloat(), -y().toFloat(), 0f, 0.5f)
    }

    /**
     * Called when the field is selected for editing.
     *
     * @return True if this succeeded, false otherwise. If it is false, then a failure chime will be played.
     */
    abstract fun handleSelected(): Boolean

    override fun primaryRelease(mouse: Boolean, isStillHighlighted: Boolean): Boolean {
        return handleSelected()
    }

    override fun highlight() {
        parentMenu.highlight = this
        parentMenu.preferredY = index
        parentMenu.scrollTo(index)
    }

    override fun y(): Int {
        return parentMenu.yOffset + height() * (index - parentMenu.scroll)
    }

    override fun up(): Selectable<*> {
        if (index == 0)
            return parentMenu.connectButton

        return parentMenu.element(index - 1)
    }

    override fun down(): Selectable<*> {
        if (index == parentMenu.getLastIndex())
            return parentMenu.connectButton

        return parentMenu.element(index + 1)
    }

    override fun right(): Selectable<*> {
        return parentMenu.goBackButton
    }
}
