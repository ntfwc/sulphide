package ntfwc.mod.multiplayer.menu

import com.jogamp.opengl.GL2

interface UIRenderable {

    /**
     * Renders the UI element.
     *
     * @param gl The OpenGL context to use.
     */
    fun render(gl: GL2)
}