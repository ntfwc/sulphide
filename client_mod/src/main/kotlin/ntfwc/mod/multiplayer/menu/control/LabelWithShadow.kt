package ntfwc.mod.multiplayer.menu.control

import com.jogamp.opengl.GL2
import com.jogamp.opengl.util.awt.TextRenderer

/**
 * A label that draws a dark shadow copy of the text behind the main text rendering, so it can be seen clearly even with a bright background.
 */
class LabelWithShadow(text: String, textRenderer: TextRenderer, x: Float, y: Float, private val shadowOffset: Float = 0.75f) :
        Label(text, textRenderer, x, y) {
    override fun render(gl: GL2) {
        renderText(gl, x - shadowOffset, y - shadowOffset, 0f)

        super.render(gl)
    }
}