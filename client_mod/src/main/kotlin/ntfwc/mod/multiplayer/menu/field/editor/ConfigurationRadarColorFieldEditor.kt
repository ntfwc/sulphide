package ntfwc.mod.multiplayer.menu.field.editor

import com.jogamp.opengl.GL2
import menu.MenuMan
import ntfwc.mod.multiplayer.menu.ConfigurationMenu
import ntfwc.mod.multiplayer.menu.field.ConfigurationValue
import ntfwc.mod.multiplayer.radar.RadarHeightDifferenceColor
import ntfwc.mod.multiplayer.util.Color3f

private val BORDER_COLOR = Color3f(0f, 0f, 0f)

/**
 * Field for a radar color configuration value.
 */
class ConfigurationRadarColorFieldEditor(private val configurationValue: ConfigurationValue<RadarHeightDifferenceColor>,
                                         parentMenu: ConfigurationMenu,
                                         index: Int,
                                         width: Int,
                                         height: Int) :
        AbstractConfigurationFieldEditor<RadarHeightDifferenceColor>(configurationValue,
                parentMenu,
                index,
                width,
                height) {

    override fun renderButton(gl: GL2) {
        super.renderButton(gl)
        drawColorBoxWithBorder(gl, (x() + width() - 32).toFloat(), y() + (height() - 24f) / 2f, 25f, configurationValue.value.color)
    }

    override fun handleSelected(): Boolean {
        MenuMan.playConfirm()
        configurationValue.value = getNextColor(configurationValue.value)
        return true
    }

    private fun drawColorBoxWithBorder(gl: GL2, x: Float, y: Float, size: Float, color: Color3f) {
        val borderSize = 2f
        drawColorBox(gl, x, y, size, BORDER_COLOR)
        drawColorBox(gl, x + borderSize, y + borderSize, size - borderSize * 2f, color)
    }

    private fun drawColorBox(gl: GL2, x: Float, y: Float, size: Float, color: Color3f) {
        gl.glColor3f(color.red, color.green, color.blue)
        val rightX = x + size
        val topY = y + size

        gl.glBegin(GL2.GL_TRIANGLE_FAN)
        gl.glVertex2f(rightX, y)
        gl.glVertex2f(x, y)
        gl.glVertex2f(x, topY)
        gl.glVertex2f(rightX, topY)
        gl.glEnd()
    }

    private fun getNextColor(color: RadarHeightDifferenceColor): RadarHeightDifferenceColor {
        val allValues = RadarHeightDifferenceColor.values()
        val indexOfCurrent = allValues.indexOf(color)
        var indexOfNext = indexOfCurrent + 1
        if (indexOfNext >= allValues.size)
            indexOfNext = 0
        return allValues[indexOfNext]
    }
}