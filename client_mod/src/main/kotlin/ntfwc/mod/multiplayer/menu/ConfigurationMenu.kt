package ntfwc.mod.multiplayer.menu

import com.jogamp.opengl.GL2
import com.jogamp.opengl.util.awt.TextRenderer
import graphics.GFX
import menu.*
import ntfwc.mod.multiplayer.connection.GeneralConnectionStatus
import ntfwc.mod.multiplayer.menu.control.*
import ntfwc.mod.multiplayer.menu.entry.screen.StringEntryCharSet
import ntfwc.mod.multiplayer.menu.field.AbstractConfigurationValue
import ntfwc.mod.multiplayer.menu.field.ConfigurationValue
import ntfwc.mod.multiplayer.menu.field.editor.AbstractConfigurationFieldEditor
import ntfwc.mod.multiplayer.menu.field.editor.ConfigurationBooleanFieldEditor
import ntfwc.mod.multiplayer.menu.field.editor.ConfigurationRadarColorFieldEditor
import ntfwc.mod.multiplayer.menu.field.editor.ConfigurationStringFieldEditor
import ntfwc.mod.multiplayer.menu.field.stringToType.StringToIntConverter
import ntfwc.mod.multiplayer.menu.field.stringToType.StringToOptionalIntConverter
import ntfwc.mod.multiplayer.menu.field.stringToType.StringToTypeConfigurationValue
import ntfwc.mod.multiplayer.model.Configuration
import ntfwc.mod.multiplayer.model.RadarColorConfiguration
import ntfwc.mod.multiplayer.radar.RadarHeightDifferenceColor
import java.awt.Font

private const val MAX_NUMBER_OF_ROWS = 4
private val STATUS_FONT = GFX.gfx.anton28.rasterizer
private val USERNAME_FONT = TextRenderer(GFX.gfx.anon48.rasterizer.font.deriveFont(Font.PLAIN, 32f), true, true)

/**
 * The configuration menu for multiplayer options.
 */
class ConfigurationMenu(menuMan: MenuMan,
                        private val currentConfiguration: Configuration,
                        private val modControlInterface: UIModControlInterface,
                        private val openedFromPauseMenu: Boolean) :
        ScrollingMenuScreen<AbstractConfigurationValue, AbstractConfigurationValue>(menuMan, MenuMan.WIDTH - 144, 48) {
    private var switchingToSubMenu = false

    val goBackButton: Button
    val connectButton: Button
    private val disconnectButton: Button

    private val generalConnectionStatusLabel = LabelWithShadow("",
            STATUS_FONT,
            15f,
            yOffset + buttonHeight * MAX_NUMBER_OF_ROWS + 25.toFloat())
    private val usernameOnServerDescriptionLabel = LabelWithShadow("Username on server:",
            STATUS_FONT,
            15f,
            yOffset + buttonHeight * MAX_NUMBER_OF_ROWS + 65.toFloat())
    private val usernameOnServerLabel = LabelWithShadow("",
            USERNAME_FONT,
            135f,
            yOffset + buttonHeight * MAX_NUMBER_OF_ROWS + 65.toFloat())
    private val lastConnectionStatusUpdateLabel = LabelWithShadow("",
            STATUS_FONT,
            15f,
            yOffset + buttonHeight * MAX_NUMBER_OF_ROWS + 45.toFloat())
    private val playerCountInMapDisplay = PlayerCountInMapDisplay(
            STATUS_FONT,
            15f,
            MenuMan.HEIGHT - 45.0f,
            15f
    )

    private val fieldEditorList: List<AbstractConfigurationFieldEditor<*>>

    private var lastHighlight: Any? = null
    private var time: Long = 0

    private val hostValue: ConfigurationValue<String>
    private val portValue: StringToTypeConfigurationValue<Int?>
    private val usernameValue: ConfigurationValue<String>
    private val autoConnectValue: ConfigurationValue<Boolean>
    private val enableRadarValue: ConfigurationValue<Boolean>
    private val radarAboveColor: ConfigurationValue<RadarHeightDifferenceColor>
    private val radarBelowColor: ConfigurationValue<RadarHeightDifferenceColor>
    private val maxAvatarCountValue: StringToTypeConfigurationValue<Int>

    private val otherRenderables: List<UIRenderable>

    private var shownLastGeneralConnectionStatus: GeneralConnectionStatus? = null
    private var shownLastStatusUpdate: String? = null

    private var lastConfigurationUsedByConnectButton: Configuration? = null

    init {
        maxButtonsV = 4

        val goBackButtonHeight = Math.max(buttonHeight * 2, yOffset + buttonHeight * MAX_NUMBER_OF_ROWS - 32)
        goBackButton = object : Button(MenuMan.WIDTH - 64,
                32,
                32,
                goBackButtonHeight,
                object : ButtonTextOrIcon {
                    override fun setEnabledState(state: Boolean) {
                        // Won't be used
                    }

                    override fun render(gl: GL2) {
                        gl.glColor3f(1f, 1f, 1f)
                        gl.glPushMatrix()
                        gl.glTranslated((32 - 24) / 2 + 24.toDouble(), 0.0, 0.0)
                        gl.glRotated(90.0, 0.0, 0.0, 1.0)
                        MenuMan.drawIcon(gl, goBackButtonHeight - 28, 0, 24, 24, GFX.tex("m_goback").tex[0])
                        gl.glPopMatrix()

                        val tyOffset = kirsty48.font.size / 5
                        gl.glPushMatrix()
                        gl.glTranslatef(tyOffset.toFloat(), 9f, 0f)
                        gl.glRotated(90.0, 0.0, 0.0, 1.0)
                        gl.glScalef(1f, -1f, 0f)
                        kirsty48.setColor(1f, 1f, 1f, 1f)
                        kirsty48.begin3DRendering()
                        kirsty48.draw3D(text("Go Back", null), 0f, 0f, 0f, 0.5f)
                        kirsty48.end3DRendering()
                        gl.glPopMatrix()
                    }
                }) {
            override fun left(): Selectable<*>? {
                return scrollingList[preferredY]
            }

            override fun getHighlight(): Selectable<*> = highlight

            override fun getMouseHover(): Boolean = mouseHover

            override fun onClicked() {
                escape(false)
            }

            override fun highlight() {
                highlight = this
            }
        }

        connectButton = object : Button(MenuMan.WIDTH / 2 + 5,
                MenuMan.HEIGHT - 64,
                180,
                48,
                Label("Connect", kirsty48, 48f, 48f / 2 + kirsty48.font.size / 5)) {

            override fun getHighlight(): Selectable<*> = highlight

            override fun getMouseHover(): Boolean = mouseHover

            override fun highlight() {
                highlight = this
            }

            override fun onClicked() {
                MenuMan.playConfirm()
                val configuration = createConfigurationFromEnteredValues()
                lastConfigurationUsedByConnectButton = configuration
                modControlInterface.startConnecting(configuration)
            }

            override fun left(): Selectable<*> = getDisconnectButton()

            override fun right(): Selectable<*> = getDisconnectButton()

            override fun up(): Selectable<*> = scrollingList.last()

            override fun down(): Selectable<*> = scrollingList.first()
        }

        disconnectButton = object : Button(MenuMan.WIDTH / 2 - 185,
                MenuMan.HEIGHT - 64,
                180,
                48,
                Label("Disconnect", kirsty48, 40f, 48f / 2 + kirsty48.font.size / 5)) {

            override fun getHighlight(): Selectable<*> = highlight

            override fun getMouseHover(): Boolean = mouseHover

            override fun highlight() {
                highlight = this
            }

            override fun onClicked() {
                MenuMan.playConfirm()
                modControlInterface.disconnect()
            }

            override fun left(): Selectable<*> = connectButton

            override fun right(): Selectable<*> = connectButton

            override fun up(): Selectable<*> = scrollingList.last()

            override fun down(): Selectable<*> = scrollingList.first()
        }

        otherRenderables = listOf(
                goBackButton,
                connectButton,
                disconnectButton,
                generalConnectionStatusLabel,
                usernameOnServerDescriptionLabel,
                usernameOnServerLabel,
                lastConnectionStatusUpdateLabel,
                playerCountInMapDisplay
        )

        val newFieldEditorList = ArrayList<AbstractConfigurationFieldEditor<*>>()
        var index = 0
        val addEditor = { editor: AbstractConfigurationFieldEditor<*> ->
            newFieldEditorList.add(editor)
            scrollingList.add(editor)
        }

        val addStringEditor = { value: ConfigurationValue<String>, lengthLimit: Int, charSet: StringEntryCharSet, validator: (newValue: CharSequence) -> Boolean ->
            addEditor(ConfigurationStringFieldEditor(value,
                    this,
                    index++,
                    buttonWidth,
                    buttonHeight,
                    lengthLimit,
                    charSet,
                    validator
            ))
        }

        hostValue = ConfigurationValue("Host",
                "The domain name/IP of the host to connect to",
                currentConfiguration.host)
        addStringEditor(hostValue,
                253,
                StringEntryCharSet.HOST_NAME_CHARS) { newValue -> !Configuration.isInvalidHost(newValue) }

        portValue = StringToTypeConfigurationValue("Port",
                "The port of the host to connect to",
                currentConfiguration.port,
                StringToOptionalIntConverter())
        addStringEditor(portValue,
                5,
                StringEntryCharSet.NUMERIC) { newValue ->
            if (newValue.isEmpty())
                return@addStringEditor true

            val intValue = kotlin.runCatching { newValue.toString().toInt() }.getOrNull()
                    ?: return@addStringEditor false

            Configuration.isValidPort(intValue)
        }

        usernameValue = ConfigurationValue("Username", "The username to use on the server (if available)", currentConfiguration.username)
        addStringEditor(usernameValue, 20, StringEntryCharSet.USERNAME) { true }

        autoConnectValue = ConfigurationValue("Auto Connect", "Whether to connect automatically when the game starts", currentConfiguration.autoConnect)
        addEditor(ConfigurationBooleanFieldEditor(autoConnectValue, this, index++, buttonWidth, buttonHeight))

        enableRadarValue = ConfigurationValue("Enable Radar", "If enabled, the in-game player radar is shown when there is a server connection", currentConfiguration.enableRadar)
        addEditor(ConfigurationBooleanFieldEditor(enableRadarValue, this, index++, buttonWidth, buttonHeight))

        radarAboveColor = ConfigurationValue("Radar Above Color", "The radar point color that is shown when a point is above you", currentConfiguration.radarColorConfiguration.aboveColor)
        addEditor(ConfigurationRadarColorFieldEditor(radarAboveColor, this, index++, buttonWidth, buttonHeight))

        radarBelowColor = ConfigurationValue("Radar Below Color", "The radar point color that is shown when a point is below you", currentConfiguration.radarColorConfiguration.belowColor)
        addEditor(ConfigurationRadarColorFieldEditor(radarBelowColor, this, index++, buttonWidth, buttonHeight))

        maxAvatarCountValue = StringToTypeConfigurationValue("Max Avatar Count",
                "The maximum number of avatars to show, for other players. Reducing this can help performance. If there are more players than this, then players further away will just be shown as orbs.",
                currentConfiguration.maxAvatarCount,
                StringToIntConverter(0))
        addStringEditor(maxAvatarCountValue,
                5,
                StringEntryCharSet.NUMERIC) { newValue ->
            if (newValue.isEmpty())
                return@addStringEditor false

            val intValue = kotlin.runCatching { newValue.toString().toInt() }.getOrNull()
                    ?: return@addStringEditor false

            Configuration.isValidMaxAvatarCount(intValue)
        }

        fieldEditorList = newFieldEditorList

        highlight = scrollingList[0]
        scrollTo(0)
        updateStatuses()
    }

    override fun escape(mouse: Boolean) {
        MenuMan.playBack()
        if (openedFromPauseMenu)
            menuMan.showMenu(ModsScreen(menuMan, ModsScreen.MenuContext.PAUSE_MENU), mouse)
        else
            menuMan.goBack(null)
    }

    /**
     * Render function based on the one in [GenericConfigScreen].
     */
    override fun render(gl: GL2, interp: Float) {
        super.render(gl, interp)
        updateStatuses()
        updateButtonEnabledStates()

        for (i in scroll until Math.min(fieldEditorList.size, scroll + maxButtonsV)) {
            val fieldEditor = fieldEditorList[i]
            fieldEditor.renderButton(gl)
        }

        var tyOffset = buttonHeight / 2 + kirsty48.font.size / 5
        gl.glPushMatrix()
        gl.glTranslatef(0f, tyOffset.toFloat(), 0f)
        gl.glScalef(1f, -1f, 0f)
        kirsty48.setColor(1f, 1f, 1f, 1f)
        kirsty48.begin3DRendering()
        for (i in scroll until Math.min(fieldEditorList.size, scroll + maxButtonsV)) {
            val fieldEditor = fieldEditorList[i]
            fieldEditor.renderText(gl, kirsty48)
        }

        kirsty48.end3DRendering()
        gl.glPopMatrix()
        val highlight = highlight

        if (highlight is AbstractConfigurationFieldEditor<*>) {
            tyOffset = kirsty48.font.size / 5

            gl.glPushMatrix()
            gl.glTranslatef(0f, tyOffset.toFloat(), 0f)
            gl.glScalef(1f, -1f, 0f)
            kirsty48.begin3DRendering()
            val str = highlight.thing.comment
            kirsty48.setColor(1f, 1f, 1f, 0.5f * Math.min(1f, (time + interp) / 60))
            kirsty48.draw3D(str, MenuMan.WIDTH - ((time + interp) * 4 + MenuMan.WIDTH / 2) % (kirsty48.getBounds(str).width / 2 + MenuMan.WIDTH).toInt(), -14f, 0f, 0.5f)
            kirsty48.setColor(0f, 0f, 0f, 0.75f * Math.min(1f, (time + interp) / 60))
            kirsty48.draw3D(str, MenuMan.WIDTH + 1 - ((time + interp) * 4 + MenuMan.WIDTH / 2) % (kirsty48.getBounds(str).width / 2 + MenuMan.WIDTH).toInt(), -13f, 0f, 0.5f)
            kirsty48.end3DRendering()
            gl.glPopMatrix()
        }

        for (otherRenderable in otherRenderables) {
            otherRenderable.render(gl)
        }
    }

    override fun destroy() {
        if (switchingToSubMenu)
            return

        val newConfiguration = createConfigurationFromEnteredValues()
        if (currentConfiguration != newConfiguration)
            modControlInterface.updateConfiguration(newConfiguration)
    }

    private fun createConfigurationFromEnteredValues(): Configuration {
        return Configuration(hostValue.value,
                portValue.getTypedValue(),
                usernameValue.value,
                autoConnectValue.value,
                enableRadarValue.value,
                RadarColorConfiguration(radarAboveColor.value, radarBelowColor.value),
                maxAvatarCountValue.getTypedValue())
    }

    override fun putSelectables(buf: ArrayList<Selectable<*>?>) {
        super.putSelectables(buf)
        buf.add(goBackButton)
        buf.add(connectButton)
        buf.add(disconnectButton)
    }

    @Synchronized
    override fun stepWithoutConfiguration() {
        super.stepWithoutConfiguration()
        if (lastHighlight !== highlight) {
            time = 0
            lastHighlight = highlight
        }
        time++
    }

    override fun stepConfiguration(configuring: AbstractConfigurationValue?) {
        super.stepConfiguration(configuring)
        if (lastHighlight !== highlight) {
            time = 0
            lastHighlight = highlight
        }
        time++
    }

    override fun show() {
        updateButtonEnabledStates()
    }

    fun openSubMenu(menuScreen: MenuScreen<Configurable>) {
        switchingToSubMenu = true
        menuMan.showMenu(menuScreen, false)
        switchingToSubMenu = false
    }

    fun getLastIndex(): Int = scrollingList.lastIndex

    private fun updateButtonEnabledStates() {
        val isNotConnectedOrTryingToConnect = modControlInterface.isNotConnectedOrTryingToConnect()
        this.connectButton.enabled = shouldEnableConnectButton(isNotConnectedOrTryingToConnect)
        this.disconnectButton.enabled = !isNotConnectedOrTryingToConnect
    }

    private fun shouldEnableConnectButton(isNotConnectedOrTryingToConnect: Boolean): Boolean {
        if (!isConfigurationValid())
            return false

        if (isNotConnectedOrTryingToConnect)
            return true

        val activeConfiguration = lastConfigurationUsedByConnectButton ?: currentConfiguration

        val enteredConfiguration = createConfigurationFromEnteredValues()
        val configurationToCompare = Configuration(enteredConfiguration.host,
                enteredConfiguration.port,
                enteredConfiguration.username,
                // The auto-connect, radar configuration, and avatar configuration shouldn't affect this, so just copy them
                activeConfiguration.autoConnect,
                activeConfiguration.enableRadar,
                activeConfiguration.radarColorConfiguration,
                activeConfiguration.maxAvatarCount)
        return activeConfiguration != configurationToCompare
    }

    private fun isConfigurationValid(): Boolean {
        return this.hostValue.value.isNotEmpty() && this.portValue.value.isNotEmpty() && this.usernameValue.value.isNotEmpty()
    }

    private fun getDisconnectButton(): Selectable<*> = disconnectButton

    private fun updateStatuses() {
        val generalConnectionStatus = modControlInterface.getGeneralConnectionStatus()
        updateGeneralConnectionStatus(generalConnectionStatus)
        updateLastConnectionStatusUpdateMessage()
        updatePlayerCountInMapDisplay(generalConnectionStatus.usernameOnServer.isNotEmpty())
    }

    private fun updateGeneralConnectionStatus(status: GeneralConnectionStatus) {
        if (shownLastGeneralConnectionStatus == status)
            return

        shownLastGeneralConnectionStatus = status
        generalConnectionStatusLabel.setText("Status: ${status.description}")
        usernameOnServerDescriptionLabel.setEnabledState(status.usernameOnServer.isNotEmpty())
        val usernameOnServerText = if (status.usernameOnServer.isEmpty())
            ""
        else
            "'${status.usernameOnServer}'"
        usernameOnServerLabel.setText(usernameOnServerText)
    }

    private fun updateLastConnectionStatusUpdateMessage() {
        val lastStatusUpdate = modControlInterface.getLastConnectionStatusUpdate()
        if (shownLastStatusUpdate == lastStatusUpdate)
            return

        shownLastStatusUpdate = lastStatusUpdate
        lastConnectionStatusUpdateLabel.setText("Last update: $lastStatusUpdate")
    }

    private fun updatePlayerCountInMapDisplay(hasInitializedConnection: Boolean) {
        val count = if (hasInitializedConnection)
            modControlInterface.getPlayersInMapCount()
        else
            null

        playerCountInMapDisplay.updateCount(count)
    }
}