package ntfwc.mod.multiplayer.menu.control

import com.jogamp.opengl.GL2
import com.jogamp.opengl.util.awt.TextRenderer

/**
 * A control that just displays text.
 */
open class Label(private var text: String, private val textRenderer: TextRenderer, protected val x: Float, protected val y: Float) : ButtonTextOrIcon {
    protected var enabled = true

    override fun render(gl: GL2) {
        val colorValue = if (enabled) {
            1f
        } else {
            0.4f
        }
        renderText(gl, x, y, colorValue)
    }

    protected fun renderText(gl: GL2, x: Float, y: Float, colorValue: Float) {
        gl.glPushMatrix()
        gl.glScalef(1f, -1f, 0f)

        textRenderer.setColor(colorValue, colorValue, colorValue, 1f)
        textRenderer.begin3DRendering()
        textRenderer.draw3D(text, x, -y, 0f, 0.5f)
        textRenderer.end3DRendering()

        gl.glPopMatrix()
    }

    override fun setEnabledState(state: Boolean) {
        enabled = state
    }

    fun setText(text: String) {
        this.text = text
    }
}