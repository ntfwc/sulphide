package ntfwc.mod.multiplayer.menu.control

import ntfwc.mod.multiplayer.menu.UIRenderable

interface ButtonTextOrIcon : UIRenderable {
    /**
     * Sets a display state for enabled/disabled. The state should
     * be enabled by default.
     *
     * @param state The state.
     */
    fun setEnabledState(state: Boolean)
}