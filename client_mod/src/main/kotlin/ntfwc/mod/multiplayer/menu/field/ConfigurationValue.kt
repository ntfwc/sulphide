package ntfwc.mod.multiplayer.menu.field

/**
 * A configurable value in the configuration menu.
 */
open class ConfigurationValue<T>(name: String, comment: String, open var value: T) : AbstractConfigurationValue(name, comment)
