package ntfwc.mod.multiplayer.menu.field.editor

import com.jogamp.opengl.GL2
import menu.MenuMan
import ntfwc.mod.multiplayer.menu.ConfigurationMenu
import ntfwc.mod.multiplayer.menu.field.ConfigurationValue

/**
 * Field for a boolean configuration value.
 */
class ConfigurationBooleanFieldEditor(private val configurationValue: ConfigurationValue<Boolean>,
                                      parentMenu: ConfigurationMenu,
                                      index: Int,
                                      width: Int,
                                      height: Int) :
        AbstractConfigurationFieldEditor<Boolean>(configurationValue,
                parentMenu,
                index,
                width,
                height) {

    override fun renderButton(gl: GL2) {
        super.renderButton(gl)
        MenuMan.drawCheckmark(gl, (x() + width() - 32).toFloat(), y() + (height() - 24) / 2f, 24, configurationValue.value)
    }

    override fun handleSelected(): Boolean {
        MenuMan.playConfirm()
        configurationValue.value = !configurationValue.value
        return true
    }
}