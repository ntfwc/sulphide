package ntfwc.mod.multiplayer.menu

import audio.Audio
import game.Root
import main.GameStateManager
import menu.MenuMan
import menu.SkipCutsceneScreen
import ntfwc.mod.multiplayer.controls.ControlsInterface

/**
 * Handles opening the configuration menu.
 */
class MenuHandler(private val modControlInterface: UIModControlInterface,
                  private val controlsInterface: ControlsInterface,
                  /**
                   * Something to be done before the first time the menu is opened
                   */
                  private val firstTimeOpenedAction: () -> Unit) {
    private var hasOpenedMenu = false

    fun step() {
        if (controlsInterface.hasMenuButtonBeenPressed())
            onMenuKeyPressed()
    }

    private fun onMenuKeyPressed() {
        if (!hasOpenedMenu) {
            hasOpenedMenu = true
            firstTimeOpenedAction()
        }

        playMenuOpenedSound()
        GameStateManager.pushGameState(MenuMan(true) { m ->
            if (Root.canSkipCutscene()) {
                SkipCutsceneScreen(m)
            } else {
                ConfigurationMenu(m, modControlInterface.getConfiguration(), modControlInterface, false)
            }
        })
    }

    private fun playMenuOpenedSound() {
        Audio.play("pause", null, 1.0, null, 1.0)
    }
}