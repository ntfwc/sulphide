package ntfwc.mod.multiplayer.menu.field

import menu.Configurable

/**
 * Base class for configuration values.
 */
abstract class AbstractConfigurationValue(val name: String, val comment: String) : Configurable