package ntfwc.mod.multiplayer.menu.entry.screen

import menu.MenuMan
import menu.MenuScreen.MenuAction
import menu.StringEntryScreen
import menu.StringEntryScreen.StringAction
import ntfwc.mod.multiplayer.menu.ConfigurationMenu


/**
 * A string entry screen that uses a given text validator.
 */
class ValidatingStringEntryScreen(private val parentMenu: ConfigurationMenu,
                                  title: String,
                                  initialValue: String,
                                  editValue: (String) -> Unit,
                                  lengthLimit: Int,
                                  charSet: StringEntryCharSet,
                                  private val validator: (CharSequence) -> Boolean) :
        StringEntryScreen(parentMenu.menuMan, title, initialValue,
                StringAction { value, _ ->
                    MenuMan.playConfirm()
                    editValue(value)
                    parentMenu.menuMan.showMenu(parentMenu, false)
                },
                MenuAction {
                    MenuMan.playBack()
                    parentMenu.menuMan.showMenu(parentMenu, false)
                }, lengthLimit, *charSet.gameCharSets) {

    companion object {
        // To make the mask constants available
        val SC_NONE_MASK = SC_NONE
        val SC_SPACE_MASK = SC_SPACE
    }

    override fun textIsValid(text: CharSequence): Boolean = validator(text)
}