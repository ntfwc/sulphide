package ntfwc.mod.multiplayer.menu.field.stringToType

class StringToIntConverter(private val fallbackValue: Int) : StringToTypeConverter<Int> {
    override fun valueToString(value: Int): String = value.toString()

    override fun stringToValue(string: String): Int {
        if (string.isEmpty())
            return fallbackValue

        return runCatching { string.toInt() }.getOrDefault(fallbackValue)
    }
}