package ntfwc.mod.multiplayer.menu

import ntfwc.mod.multiplayer.connection.GeneralConnectionStatus
import ntfwc.mod.multiplayer.model.Configuration

/**
 * Interface for controlling the mod from the UI.
 */
interface UIModControlInterface {
    /**
     * Gets the current configuration.
     */
    fun getConfiguration(): Configuration

    /**
     * Updates the current configuration and saves it.
     *
     * @param configuration The new configuration.
     */
    fun updateConfiguration(configuration: Configuration)

    /**
     * Starts trying to connect, using the given configuration. This will
     * not save the given configuration.
     *
     * @param configuration The configuration to use.
     */
    fun startConnecting(configuration: Configuration)

    /**
     * Disconnects any current connection and halts connection attempts.
     */
    fun disconnect()

    /**
     * @return The general status to be displayed in the menu.
     */
    fun getGeneralConnectionStatus(): GeneralConnectionStatus

    /**
     * @return The last connection status update message, which would have been shown at the top of the in-game screen.
     */
    fun getLastConnectionStatusUpdate(): String

    /**
     * @return True if there is no connection and the program is not trying to (re-)establish a new connection.
     */
    fun isNotConnectedOrTryingToConnect(): Boolean

    /**
     * @return The number of players in the current map.
     */
    fun getPlayersInMapCount(): Int
}