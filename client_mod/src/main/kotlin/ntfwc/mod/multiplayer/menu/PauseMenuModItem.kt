package ntfwc.mod.multiplayer.menu

import menu.MenuMan
import menu.ModsScreen.MenuContext
import menu.ModsScreen.ModButtonInfo

/**
 * A pause menu item for the mod.
 */
class PauseMenuModItem(private val modControlInterface: UIModControlInterface, private val firstTimeMenuOpenedAction: () -> Unit) : ModButtonInfo("Sulphide", MenuContext.PAUSE_MENU) {
    private var hasOpenedMenu = false

    override fun activate(menuMan: MenuMan?, mouse: Boolean, menuContext: MenuContext?): Boolean {
        menuMan ?: return false
        if (!hasOpenedMenu) {
            hasOpenedMenu = true
            firstTimeMenuOpenedAction()
        }

        menuMan.showMenu(ConfigurationMenu(menuMan, modControlInterface.getConfiguration(), modControlInterface, true), mouse)
        return true
    }
}