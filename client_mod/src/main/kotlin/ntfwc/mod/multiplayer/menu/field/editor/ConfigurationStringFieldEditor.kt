package ntfwc.mod.multiplayer.menu.field.editor

import com.jogamp.opengl.GL2
import com.jogamp.opengl.util.awt.TextRenderer
import graphics.GFX
import menu.MenuMan
import ntfwc.mod.multiplayer.menu.ConfigurationMenu
import ntfwc.mod.multiplayer.menu.entry.screen.StringEntryCharSet
import ntfwc.mod.multiplayer.menu.entry.screen.ValidatingStringEntryScreen
import ntfwc.mod.multiplayer.menu.field.ConfigurationValue

/**
 * Field for a string configuration value.
 */
class ConfigurationStringFieldEditor(private val configurationValue: ConfigurationValue<String>,
                                     private val parentMenu: ConfigurationMenu,
                                     index: Int,
                                     width: Int,
                                     height: Int,
                                     private val lengthLimit: Int,
                                     private val charSet: StringEntryCharSet,
                                     private val validator: (newValue: CharSequence) -> Boolean) :
        AbstractConfigurationFieldEditor<String>(configurationValue,
                parentMenu,
                index,
                width,
                height) {

    override fun handleSelected(): Boolean {
        MenuMan.playConfirm()
        parentMenu.openSubMenu(ValidatingStringEntryScreen(
                parentMenu,
                "Enter value",
                configurationValue.value,
                { newValue -> configurationValue.value = newValue },
                lengthLimit,
                charSet,
                validator
        ))
        return true
    }

    override fun renderText(gl: GL2, font: TextRenderer) {
        super.renderText(gl, font)

        val str: String = configurationValue.value
        // Use a different font
        font.end3DRendering()

        val myFont = GFX.gfx.anon48.rasterizer
        myFont.setColor(1f, 1f, 1f, 1f)
        myFont.begin3DRendering()

        val stringToDisplay = if (str.length <= 26) str else str.substring(0, 26) + "..."
        myFont.draw3D(stringToDisplay, x() + width() - 40 - (myFont.getBounds(str).width.toInt() / 2).toFloat(), -y().toFloat(), 0f, 0.5f)
        myFont.end3DRendering()

        // Return to the old font
        font.begin3DRendering()
    }
}