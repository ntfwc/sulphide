package ntfwc.mod.multiplayer.util

/**
 * Provides JSON security checks.
 */
object JsonSecurity {
    private const val MAX_JSON_DEPTH = 10
    private const val MAX_STRING_SIZE = 300

    // 106404 is the number of values in a state update for 400 clients
    private const val MAX_JSON_VALUES = 106404 * 2

    /**
     * Checks if the given JSON string looks malicious. This means
     * that it is unreasonably deep, wide, or includes strings that are huge.
     */
    fun looksLikeMaliciousJson(json: String): Boolean {
        var depth = 0
        var assignmentCount = 0

        var inQuotes = false
        var lastWasEscape = false
        var stringSize = 0

        for (char in json) {
            // Handle quote cases
            if (!inQuotes && char == '"') {
                inQuotes = true
                stringSize = 0
                continue
            }

            if (inQuotes) {
                if (lastWasEscape) {
                    lastWasEscape = false
                    continue
                }

                if (char == '\\') {
                    lastWasEscape = true
                    continue
                }

                if (char == '"') {
                    inQuotes = false
                    continue
                }

                stringSize++
                if (stringSize > MAX_STRING_SIZE)
                    return true

                continue
            }

            // Handle structure characters
            when (char) {
                '{' -> {
                    depth++
                    if (depth > MAX_JSON_DEPTH)
                        return true
                }
                '}' -> depth--
                ':' -> {
                    assignmentCount++
                    if (assignmentCount > MAX_JSON_VALUES)
                        return true
                }
            }
        }

        return false
    }

    /**
     * Collects statistics about the given JSON string. Can be used to debug malicious JSON detection behavior.
     */
    fun collectStats(json: String): JsonStats {
        var depth: Long = 0
        var maxDepth: Long = 0
        var assignmentCount: Long = 0
        var stringSize: Long = 0
        var maxStringSize: Long = 0

        var inQuotes = false
        var lastWasEscape = false

        for (char in json) {
            // Handle quote cases
            if (!inQuotes && char == '"') {
                inQuotes = true
                stringSize = 0
                continue
            }

            if (inQuotes) {
                if (lastWasEscape) {
                    lastWasEscape = false
                    continue
                }

                if (char == '\\') {
                    lastWasEscape = true
                    continue
                }

                if (char == '"') {
                    inQuotes = false
                    continue
                }

                stringSize++
                if (maxStringSize < stringSize)
                    maxStringSize = stringSize

                continue
            }

            // Handle structure characters
            when (char) {
                '{' -> {
                    depth++
                    if (depth > maxDepth)
                        maxDepth = depth
                }
                '}' -> depth--
                ':' -> {
                    assignmentCount++
                }
            }
        }

        return JsonStats(maxDepth, assignmentCount, maxStringSize)
    }

    data class JsonStats(val maxDepth: Long, val assignmentCount: Long, val maxStringSize: Long)
}