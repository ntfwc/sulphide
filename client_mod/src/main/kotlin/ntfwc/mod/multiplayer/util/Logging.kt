package ntfwc.mod.multiplayer.util

import java.text.SimpleDateFormat
import java.util.*

const val ENABLE_TRACE = false

private val ISO_8601_DATE_FORMAT = object : ThreadLocal<SimpleDateFormat>() {
    override fun initialValue(): SimpleDateFormat {
        val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        format.timeZone = TimeZone.getTimeZone("UTC")
        return format
    }
}

/**
 * Logs the given message to stdout.
 *
 * @param message The message.
 */
fun log(message: String) {
    println("${formatCurrentTimeToIso8601()} [sulphide] - $message")
}

private fun formatCurrentTimeToIso8601(): String {
    val now = Date()
    return ISO_8601_DATE_FORMAT.get().format(now)
}

/**
 * Logs the given message to stdout, if trace is enabled.
 *
 * @param message The message.
 */
@Suppress("NOTHING_TO_INLINE")
inline fun traceLog(message: String) {
    if (ENABLE_TRACE) {
        log(message)
    }
}
