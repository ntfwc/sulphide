package ntfwc.mod.multiplayer.util

/**
 * A Color3f value that has been validated, meaning we know all values are between 0 and 1.
 */
data class ValidatedColor3f(val inner: Color3f) {
    companion object {
        fun clamp(color: Color3f): ValidatedColor3f {
            val red = clampToUnitInterval(color.red)
            val green = clampToUnitInterval(color.green)
            val blue = clampToUnitInterval(color.blue)
            val inner = if (red == color.red && green == color.green && blue == color.blue)
                color
            else
                Color3f(red, green, blue)

            return ValidatedColor3f(inner)
        }

        private fun clampToUnitInterval(value: Float): Float {
            if (!value.isFinite())
                return 0f

            if (value < 0f)
                return 0f
            if (value > 1f)
                return 1f
            return value
        }
    }
}