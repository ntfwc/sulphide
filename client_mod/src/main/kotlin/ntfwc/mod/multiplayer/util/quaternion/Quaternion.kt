package ntfwc.mod.multiplayer.util.quaternion

import com.beust.klaxon.JsonObject
import ntfwc.mod.multiplayer.util.ParsingUtil

/**
 * A quaternion. This can be used for representing rotations.
 */
data class Quaternion(val w: Double, val x: Double, val y: Double, val z: Double) {
    companion object {
        fun fromJson(jsonObject: JsonObject): Quaternion? {
            val w = ParsingUtil.getFiniteDouble(jsonObject, "w") ?: return null
            val x = ParsingUtil.getFiniteDouble(jsonObject, "x") ?: return null
            val y = ParsingUtil.getFiniteDouble(jsonObject, "y") ?: return null
            val z = ParsingUtil.getFiniteDouble(jsonObject, "z") ?: return null
            return Quaternion(w, x, y, z)
        }
    }

    /**
     * Adds this quaternion to the given quaternion.
     */
    fun add(other: Quaternion): Quaternion = Quaternion(
            this.w + other.w,
            this.x + other.x,
            this.y + other.y,
            this.z + other.z
    )

    /**
     * Multiplies this quaternion's values by the given value.
     */
    fun scale(value: Double): Quaternion = Quaternion(
            this.w * value,
            this.x * value,
            this.y * value,
            this.z * value
    )

    fun addToJson(jsonObject: JsonObject) {
        jsonObject["w"] = this.w
        jsonObject["x"] = this.x
        jsonObject["y"] = this.y
        jsonObject["z"] = this.z
    }
}