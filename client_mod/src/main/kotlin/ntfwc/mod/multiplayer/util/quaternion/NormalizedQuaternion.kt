package ntfwc.mod.multiplayer.util.quaternion

import ntfwc.mod.multiplayer.game.interfaces.player.avatar.PlayerRotationTransform
import kotlin.math.sqrt

/**
 * A quaternion that has been normalized.
 */
data class NormalizedQuaternion(val inner: Quaternion) {
    companion object {
        /**
         * Creates a quaternion from the given transform, interpreted as a rotation matrix. Based on
         * the JOGL quaternion implementation. As long as the player rotation transform is a valid
         * rotation matrix, the quaternion should always end up normalized.
         */
        fun from(transform: PlayerRotationTransform): NormalizedQuaternion {
            val w: Double
            val x: Double
            val y: Double
            val z: Double

            val m00 = transform.getM00()
            val m01 = transform.getM01()
            val m02 = transform.getM02()
            val m10 = transform.getM10()
            val m11 = transform.getM11()
            val m12 = transform.getM12()
            val m20 = transform.getM20()
            val m21 = transform.getM21()
            val m22 = transform.getM22()

            val t = m00 + m11 + m22 + 1
            if (t > 0f) {
                val s = 0.5 / sqrt(t) // S = 1 / ( 2 t )
                w = 0.25 / s // w = 1 / ( 4 S ) = t / 2
                x = (m21 - m12) * s
                y = (m02 - m20) * s
                z = (m10 - m01) * s
            } else if (m00 > m11 && m00 > m22) {
                val s = 0.5 / sqrt(1.0 + m00 - m11 - m22) // S=4*qx
                w = (m21 - m12) * s
                x = 0.25 / s
                y = (m10 + m01) * s
                z = (m02 + m20) * s
            } else if (m11 > m22) {
                val s = 0.5 / sqrt(1.0 + m11 - m00 - m22) // S=4*qy
                w = (m02 - m20) * s
                x = (m20 + m01) * s
                y = 0.25 / s
                z = (m21 + m12) * s
            } else {
                val s = 0.5 / sqrt(1.0 + m22 - m00 - m11) // S=4*qz
                w = (m10 - m01) * s
                x = (m02 + m20) * s
                y = (m21 + m12) * s
                z = 0.25 / s
            }

            return from(Quaternion(w, x, y, z))
            // It is very unlikely that we would get a player transform that results in an invalid vector quaternion,
            // but let's have a fallback value just in case.
                    ?: NormalizedQuaternion(Quaternion(0.0, 1.0, 0.0, 0.0))
        }

        fun from(quaternion: Quaternion): NormalizedQuaternion? {
            val x = quaternion.x
            val y = quaternion.y
            val z = quaternion.z
            val w = quaternion.w

            val inverseLength = 1 / sqrt(w * w + x * x + y * y + z * z)
            val normalizedQuaternion = if (inverseLength == 1.0) {
                quaternion
            } else {
                Quaternion(
                        w * inverseLength,
                        x * inverseLength,
                        y * inverseLength,
                        z * inverseLength
                )
            }

            if (!isValidVectorQuaternion(normalizedQuaternion))
                return null

            return NormalizedQuaternion(normalizedQuaternion)
        }

        private fun isValidVectorQuaternion(quaternion: Quaternion): Boolean {
            val w = quaternion.w
            val x = quaternion.x
            val y = quaternion.y
            val z = quaternion.z

            if (!w.isFinite() ||
                    !x.isFinite() ||
                    !y.isFinite() ||
                    !z.isFinite())
                return false

            if (w == 0.0 && x == 0.0 && y == 0.0 && z == 0.0)
                return false

            return true
        }
    }

    fun toPlayerRotationTransform(): PlayerRotationTransform {
        val w = inner.w
        val x = inner.x
        val y = inner.y
        val z = inner.z

        val xx = x * x
        val yy = y * y
        val zz = z * z
        val m00 = 1.0 - 2.0 * (yy + zz)
        val m11 = 1.0 - 2.0 * (xx + zz)
        val m22 = 1.0 - 2.0 * (xx + yy)

        val xy = x * y
        val zw = z * w
        val m01 = 2.0 * (xy - zw)
        val m10 = 2.0 * (xy + zw)

        val xz = x * z
        val yw = y * w
        val m02 = 2.0 * (xz + yw)
        val m20 = 2.0 * (xz - yw)

        val yz = y * z
        val xw = x * w
        val m12 = 2.0 * (yz - xw)
        val m21 = 2.0 * (yz + xw)

        return PlayerRotationTransform(m00, m01, m02,
                m10, m11, m12,
                m20, m21, m22)
    }

    /**
     * Normalized linear interpolation.
     * @param t A value from 0 to 1. The closer it is to 0, the closer
     * the result will be to this quaternion. The closer it is to 1, the
     * closer the result will be to the other quaternion.
     */
    fun nlerp(other: NormalizedQuaternion, t: Double): NormalizedQuaternion = from(this.inner.scale(1 - t).add(other.inner.scale(t)))
            ?: this
}

