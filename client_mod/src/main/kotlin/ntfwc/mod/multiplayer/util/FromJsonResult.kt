package ntfwc.mod.multiplayer.util

/**
 * Represents the result of trying to get a value from JSON. If the JSON was invalid,
 * then a failure result should be returned.
 */
sealed class FromJsonResult<T> {
    data class Success<T>(val value: T) : FromJsonResult<T>()

    class Failure<T> : FromJsonResult<T>() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false
            return true
        }

        override fun hashCode(): Int {
            return javaClass.hashCode()
        }
    }
}

