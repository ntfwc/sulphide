package ntfwc.mod.multiplayer.util

/**
 * Creates a type ID entry for a structured message.
 */
fun createTypeIdEntry(typeId: String): Pair<String, String> = Pair("t", typeId)
