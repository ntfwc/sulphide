package ntfwc.mod.multiplayer.util

/**
 * A finite radian angle value.
 */
data class RadianAngle(val value: Float) {
    companion object {
        fun from(value: Float): RadianAngle? {
            if (!value.isFinite())
                return null

            return RadianAngle(value)
        }
    }

    /**
     * Interpolates between this value and the given value in a linear fashion. The closer the factor is to 0, the closer
     * the result will be to this value. The closer the factor is to 1, the closer the result will be to the other value.
     */
    fun lerp(other: RadianAngle, factor: Float): RadianAngle {
        val interpolatedValue = this.value * (1f - factor) + other.value * factor
        if (!interpolatedValue.isFinite())
            return this

        return RadianAngle(interpolatedValue)
    }
}