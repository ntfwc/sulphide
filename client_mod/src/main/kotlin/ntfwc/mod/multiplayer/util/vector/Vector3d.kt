package ntfwc.mod.multiplayer.util.vector

import util.V3D

/**
 * A three double value vector.
 */
data class Vector3d(val x: Double, val y: Double, val z: Double) {
    companion object {
        fun from(v3d: V3D): Vector3d = Vector3d(v3d.x(), v3d.y(), v3d.z())
    }

    /**
     * Sets the values of the given V3D to match
     * this vector.
     */
    fun applyTo(v3d: V3D) {
        v3d.x(this.x)
        v3d.y(this.y)
        v3d.z(this.z)
    }
}