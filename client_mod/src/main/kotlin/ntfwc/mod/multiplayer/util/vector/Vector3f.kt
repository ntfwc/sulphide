package ntfwc.mod.multiplayer.util.vector

import com.beust.klaxon.JsonObject
import ntfwc.mod.multiplayer.util.ParsingUtil

/**
 * A vector with three float values.
 */
data class Vector3f(val x: Float, val y: Float, val z: Float) {
    companion object {
        fun fromJson(jsonObject: JsonObject): Vector3f? {
            val x = ParsingUtil.getFiniteFloat(jsonObject, "x") ?: return null
            val y = ParsingUtil.getFiniteFloat(jsonObject, "y") ?: return null
            val z = ParsingUtil.getFiniteFloat(jsonObject, "z") ?: return null
            return Vector3f(x, y, z)
        }
    }

    fun scale(scale: Float): Vector3f = Vector3f(x * scale, y * scale, z * scale)
    fun add(other: Vector3f): Vector3f = Vector3f(this.x + other.x, this.y + other.y, this.z + other.z)

    fun addToJson(jsonObject: JsonObject) {
        jsonObject["x"] = this.x
        jsonObject["y"] = this.y
        jsonObject["z"] = this.z
    }
}