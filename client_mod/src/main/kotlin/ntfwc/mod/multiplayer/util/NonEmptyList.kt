package ntfwc.mod.multiplayer.util

/**
 * Wrapper for a list that marks it as non-empty.
 */
data class NonEmptyList<T>(val inner: List<T>) {
    companion object {
        fun <T> from(list: List<T>): NonEmptyList<T>? {
            if (list.isEmpty())
                return null

            return NonEmptyList(list)
        }
    }

    fun last(): T = inner.last()
}