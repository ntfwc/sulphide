package ntfwc.mod.multiplayer.util.vector

import kotlin.math.atan2
import kotlin.math.sqrt

data class Vector2f(val x: Float, val y: Float) {
    fun normalize(): Vector2f {
        val length = length()
        if (length == 1f)
            return this

        return scale(1f / length)
    }

    fun scale(scale: Float): Vector2f = Vector2f(x * scale, y * scale)

    fun length(): Float = sqrt(x * x + y * y)

    /**
     * @return The signed degrees of rotation between this vector and the given vector.
     */
    fun signedDegreesBetween(other: Vector2f): Float {
        return toDegrees(signedRadiansBetween(other))
    }

    /**
     * @return The signed radians of rotation between this vector and the given vector.
     */
    fun signedRadiansBetween(other: Vector2f): Float {
        val normalized = normalize()
        val otherNormalized = other.normalize()
        return atan2(otherNormalized.x, otherNormalized.y) - atan2(normalized.x, normalized.y)
    }

    private fun toDegrees(radians: Float): Float {
        return (radians * 180 / Math.PI).toFloat()
    }
}