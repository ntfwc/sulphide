package ntfwc.mod.multiplayer.util

import com.beust.klaxon.JsonArray
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import java.io.StringReader

private val INVALID_USERNAME_CHAR_REGEX: Regex = createInvalidCharacterRegex()
private const val MAX_USERNAME_LENGTH = 25

object ParsingUtil {
    /**
     * Parses the given json as a json object.
     *
     * @return The parsed object, or null if parsing fails.
     */
    fun parseJsonObject(json: String): JsonObject? {
        val parsed = kotlin.runCatching { Parser.default().parse(StringReader(json)) }.getOrNull()
        if (parsed !is JsonObject)
            return null

        return parsed
    }

    /**
     * Tries to get a boolean value from the given json object.
     */
    fun getBool(jsonObject: JsonObject, key: String): Boolean? {
        val value = jsonObject[key]
        return if (value is Boolean) value else null
    }

    /**
     * Tries to get a finite double value from the given json object. Will fail if the value was too large to be
     * represented as a finite double.
     *
     * @param jsonObject the json object.
     * @param key The key for the value.
     */
    fun getFiniteDouble(jsonObject: JsonObject, key: String): Double? {
        val value = when (val value = jsonObject[key]) {
            is Double -> value
            is Number -> value.toDouble()
            else -> return null
        }

        if (!value.isFinite()) {
            log("Error getFiniteDouble(): Given unrepresentable number")
            return null
        }

        return value
    }

    /**
     * Tries to get a finite float value from the given json object. Will fail if the value was too large to be
     * represented as a finite float.
     *
     * @param jsonObject the json object.
     * @param key The key for the value.
     */
    fun getFiniteFloat(jsonObject: JsonObject, key: String): Float? {
        val value = getFloat(jsonObject, key) ?: return null
        if (!value.isFinite()){
            log("Error getFiniteFloat(): Given unrepresentable number")
            return null
        }

        return value
    }

    fun getFloat(jsonObject: JsonObject, key: String): Float? {
        val value = when (val value = jsonObject[key]) {
            is Float -> value
            is Number -> value.toFloat()
            else -> return null
        }

        return value
    }

    /**
     * Tries to get an integer value from the given json object.
     */
    fun getInt(jsonObject: JsonObject, key: String): Int? {
        return when (val value = jsonObject[key]) {
            is Int -> value
            is Short -> value.toInt()
            is Byte -> value.toInt()
            else -> null
        }
    }

    /**
     * Tries to get a long value from the given json object.
     */
    fun getLong(jsonObject: JsonObject, key: String): Long? {
        return when (val value = jsonObject[key]) {
            is Long -> value
            is Int -> value.toLong()
            is Short -> value.toLong()
            is Byte -> value.toLong()
            else -> null
        }
    }

    /**
     * Tries to get a string value from the given json object.
     */
    fun getString(jsonObject: JsonObject, key: String): String? {
        val value = jsonObject[key]
        if (value !is String)
            return null

        return value
    }

    /**
     * Tries to get a string value from the given json object, which must be non-empty
     * and have a length at or below the given max length.
     *
     * @param jsonObject The json object.
     * @param key The key for the value.
     * @param maxLength The maximum string length.
     * @return The value, or null if it is missing or invalid.
     */
    fun getNonEmptyString(jsonObject: JsonObject, key: String, maxLength: Int): String? {
        val value = getString(jsonObject, key) ?: return null
        if (value.isEmpty() || value.length > maxLength) {
            log("Error: Given string for key '$key' with an invalid length, from the server")
            return null
        }

        return value
    }

    /**
     * Tries to get a valid username from the given parsed json object.
     *
     * @param jsonObject The json object.
     * @param key The key for the value.
     * @return A username, or null if the value was not found or was invalid.
     */
    fun getUsername(jsonObject: JsonObject, key: String): String? {
        val username = getNonEmptyString(jsonObject, key, MAX_USERNAME_LENGTH) ?: return null
        return username.replace(INVALID_USERNAME_CHAR_REGEX, "?")
    }

    /**
     * Validates the given value as a username and filtered characters not handled, by this version of the client.
     *
     * @param value The value.
     * @return A valid username, or null if it was invalid.
     */
    fun validateAndFilterUsername(value: Any?): String? {
        if (value !is String)
            return null

        if (value.isEmpty() || value.length > MAX_USERNAME_LENGTH) {
            log("Error: Given username string with an invalid length, from the server")
            return null
        }

        return value.replace(INVALID_USERNAME_CHAR_REGEX, "\ufffd")
    }

    /**
     * Tries to get an array value from the given json object.
     */
    fun getArray(jsonObject: JsonObject, key: String): JsonArray<*>? {
        return when (val value = jsonObject[key]) {
            is JsonArray<*> -> value
            else -> null
        }
    }

    fun getObject(jsonObject: JsonObject, key: String): JsonObject? {
        return when (val value = jsonObject[key]) {
            is JsonObject -> value
            else -> null
        }
    }

    inline fun <reified T: Enum<T>> safeCaseInsensitiveEnumValueOf(name: String): T? {
            for (value in enumValues<T>()) {
                if (value.name.equals(name, true))
                    return value
            }
            return null
    }
}

/**
 * Creates the invalid username character regex. It matches characters that cannot be displayed by the client fonts.
 */
private fun createInvalidCharacterRegex(): Regex {
    val stringBuilder = StringBuilder()
    stringBuilder.append("[^")

    // Add displayable character ranges
    // English keyboard characters
    stringBuilder.append("""\u0020-\u007e""")
    // Some other western characters
    stringBuilder.append("""\u00a1-\u017f\u0129\u01fc-\u01ff\u0218-\u021b""")
    // Greek characters
    stringBuilder.append("""\u0384-\u0386\u038c\u038e-\u03a1\u03a3-\u03ce""")
    // Cyrillic characters
    stringBuilder.append("""\u0401-\u040c\u040e-\u044f\u0451-\u045c\u045e\u045f\u0490\u0491""")

    stringBuilder.append("]")
    return Regex(stringBuilder.toString())
}