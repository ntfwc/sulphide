package ntfwc.mod.multiplayer.util

import java.util.concurrent.ExecutorService
import java.util.concurrent.Future

/**
 * Wraps an executor service so tasks print exceptions that occur.
 */
class ExecutorServiceWrapper(private val executorService: ExecutorService) {
    fun submit(task: () -> Unit): Future<*> {
        return executorService.submit {
            try {
                task.invoke()
            } catch (e: InterruptedException) {
                // Do nothing, this is expected to happen
                log("Thread '${Thread.currentThread().name}' task interrupted")
            } catch (e: Throwable) {
                e.printStackTrace()
            }
        }
    }
}

