package ntfwc.mod.multiplayer.util.range

import ntfwc.mod.multiplayer.util.UnitIntervalFloat
import kotlin.math.roundToInt

/**
 * A positive range of short values
 */
data class ShortRange(val min: Short, val max: Short) {
    init {
        if (min < 0)
            throw IllegalArgumentException("Given min is less than 0")
        if (max < 0)
            throw IllegalArgumentException("Given max is less than 0")

        if (max <= min)
            throw IllegalArgumentException("Given max is less than or equal to the given min")
    }

    private val length = max - min
    private val inverseLength = 1f / length

    /**
     * Maps the given value, which should be in the range, to a unit interval representing where it falls the range.
     */
    fun mapToUnitInterval(valueInRange: Short): UnitIntervalFloat = UnitIntervalFloat.clamp(inverseLength * (valueInRange - min))

    /**
     * Maps the given unit interval value to a value in the range.
     */
    fun mapFromUnitInterval(unitIntervalValue: UnitIntervalFloat): Short = (length * unitIntervalValue.value + min).roundToInt().toShort()
}