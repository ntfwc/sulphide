package ntfwc.mod.multiplayer.util

/**
 * A read-only array wrapper.
 */
class ReadOnlyArray<T>(private val array: Array<T>) {
    val size get() = array.size

    operator fun get(index: Int): T = array[index]
    operator fun iterator() = array.iterator()

    override fun toString(): String {
        return "ReadOnlyArray(array=${array.contentToString()})"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ReadOnlyArray<*>

        if (!array.contentEquals(other.array)) return false

        return true
    }

    override fun hashCode(): Int {
        return array.contentHashCode()
    }
}