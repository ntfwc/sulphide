package ntfwc.mod.multiplayer.util

/**
 * Makes sure ticks do not occur faster than the given rate. For example, if run in a loop, it prevents iterations of that
 * loop from occurring faster than the given rate. Note: The creation of the throttler counts as the first tick.
 */
class Throttler(private val periodInMilliseconds: Int) {
    private var timestampOfLastTick: Long? = null

    /**
     * Runs a tick. If less time has passed between this tick and the last than the period, then it will sleep for the
     * time difference.
     */
    fun tick() {
        val now = System.currentTimeMillis()
        val lastTimestamp = this.timestampOfLastTick
        if (lastTimestamp == null) {
            this.timestampOfLastTick = now
            return
        }

        val timeDiff = now - lastTimestamp
        if (timeDiff >= this.periodInMilliseconds) {
            this.timestampOfLastTick = now
            return
        }

        Thread.sleep(this.periodInMilliseconds - timeDiff)
        this.timestampOfLastTick = System.currentTimeMillis()
    }
}