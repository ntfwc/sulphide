package ntfwc.mod.multiplayer.util

import com.beust.klaxon.JsonObject

/**
 * A float value in the range of [0, 1].
 */
data class UnitIntervalFloat(val value: Float) {
    companion object {
        val ZERO = UnitIntervalFloat(0f)

        /**
         * Clamps the given value to a unit interval.
         */
        fun clamp(value: Float): UnitIntervalFloat {
            if (!value.isFinite())
                return UnitIntervalFloat(0f)

            if (value < 0f)
                return UnitIntervalFloat(0f)
            if (value > 1f)
                return UnitIntervalFloat(1f)

            return UnitIntervalFloat(value)
        }

        fun fromJson(jsonObject: JsonObject, key: String): UnitIntervalFloat? {
            val floatValue = ParsingUtil.getFloat(jsonObject, key) ?: return null
            return clamp(floatValue)
        }
    }

    /**
     * Interpolates between this value and the given value in a linear fashion. The closer the factor is to 0, the closer
     * the result will be to this value. The closer the factor is to 1, the closer the result will be to the other value.
     */
    fun lerp(other: UnitIntervalFloat, factor: Float): UnitIntervalFloat = UnitIntervalFloat(lerp(this.value, other.value, factor))

    /**
     * Interpolates between this value and the given value in a linear fashion, wrapping around if the other value is
     * less than this one. The closer the factor is to 0, the closer the result will be to this value. The closer the
     * factor is to 1, the closer the result will be to the other value.
     */
    fun lerpWrapped(other: UnitIntervalFloat, factor: Float): UnitIntervalFloat {
        if (other.value >= this.value)
            return lerp(other, factor)

        var lerpedValue = lerp(this.value, other.value + 1f, factor)
        if (lerpedValue > 1f)
            lerpedValue -= 1f

        return UnitIntervalFloat(lerpedValue)
    }

    private fun lerp(value1: Float, value2: Float, factor: Float): Float = value1 * (1f - factor) + value2 * factor

    fun getJsonValue(): Float = this.value
}