package ntfwc.mod.multiplayer.util.vector

import kotlin.math.sqrt

/**
 * A normalized vector with 3 floating point values.
 */
data class NormalizedVector3f(val inner: Vector3f) {
    companion object {
        fun from(vector: Vector3f): NormalizedVector3f? {
            val x = vector.x
            val y = vector.y
            val z = vector.z

            val inverseLength = 1f / sqrt(x * x + y * y + z * z)
            val normalizedVector = if (inverseLength == 1f) {
                vector
            } else {
                vector.scale(inverseLength)
            }

            if (!isValidVector(normalizedVector))
                return null

            return NormalizedVector3f(normalizedVector)
        }

        private fun isValidVector(vector: Vector3f): Boolean {
            val x = vector.x
            val y = vector.y
            val z = vector.z

            if (!x.isFinite() || !y.isFinite() || !z.isFinite())
                return false

            if (x == 0f && y == 0f && z == 0f)
                return false

            return true
        }
    }

    /**
     * Normalized linear interpolation.
     * @param t A value from 0 to 1. The closer it is to 0, the closer
     * the result will be to this quaternion. The closer it is to 1, the
     * closer the result will be to the other quaternion.
     */
    fun nlerp(other: NormalizedVector3f, t: Float): NormalizedVector3f = from(this.inner.scale(1 - t).add(other.inner.scale(t)))
            ?: this
}
