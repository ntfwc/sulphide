package ntfwc.mod.multiplayer.util.range

import ntfwc.mod.multiplayer.util.UnitIntervalFloat
import kotlin.math.roundToInt

/**
 * A positive range of integer values.
 * Note: It is not named IntRange because that is the name of a built-in Kotlin class.
 */
data class IntValueRange(val min: Int, val max: Int) {
    init {
        if (max <= min)
            throw IllegalArgumentException("Given max is less than or equal to the given min")
    }

    private val length = max - min
    private val inverseLength = 1f / length

    /**
     * Maps the given value, which should be in the range, to a unit interval representing where it falls the range.
     */
    fun mapToUnitInterval(valueInRange: Int): UnitIntervalFloat = UnitIntervalFloat.clamp(inverseLength * (valueInRange - min))

    /**
     * Maps the given unit interval value to a value in the range.
     */
    fun mapFromUnitInterval(unitIntervalValue: UnitIntervalFloat): Int = (length * unitIntervalValue.value + min).roundToInt()
}