package ntfwc.mod.multiplayer.util.range

import ntfwc.mod.multiplayer.util.UnitIntervalFloat

/**
 * A range of float values.
 */
data class FloatRange(val min: Float, val max: Float) {
    constructor(min: Int, max: Int) : this(min.toFloat(), max.toFloat())

    init {
        if (max <= min)
            throw IllegalArgumentException("Given max is less than or equal to the given min")
    }

    private val length = max - min
    private val inverseLength = 1 / length

    /**
     * Maps the given value, which should be in the range, to a unit interval representing where it falls the range.
     */
    fun mapToUnitInterval(valueInRange: Float): UnitIntervalFloat = UnitIntervalFloat.clamp(inverseLength * (valueInRange - min))

    /**
     * Maps the given unit interval value to a value in the range.
     */
    fun mapFromUnitInterval(unitIntervalValue: UnitIntervalFloat): Float = length * unitIntervalValue.value + min
}
