package ntfwc.mod.multiplayer.util

import com.beust.klaxon.JsonObject

/**
 * A color represented as three floating point values.
 */
data class Color3f(val red: Float, val green: Float, val blue: Float) {
    companion object {
        fun from(array: FloatArray): Color3f = Color3f(array[0], array[1], array[2])

        fun fromJson(jsonObject: JsonObject): Color3f? {
            val red = ParsingUtil.getFiniteFloat(jsonObject, "red") ?: return null
            val green = ParsingUtil.getFiniteFloat(jsonObject, "green") ?: return null
            val blue = ParsingUtil.getFiniteFloat(jsonObject, "blue") ?: return null
            return Color3f(red, green, blue)
        }
    }

    fun to4ValueColorFloatArray(): FloatArray {
        val colorArray = FloatArray(4)
        colorArray[0] = red
        colorArray[1] = red
        colorArray[2] = red
        colorArray[3] = 1f
        return colorArray
    }

    fun addToJson(jsonObject: JsonObject) {
        jsonObject["red"] = this.red
        jsonObject["green"] = this.green
        jsonObject["blue"] = this.blue
    }
}