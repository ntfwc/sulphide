package ntfwc.mod.multiplayer.util

import ntfwc.mod.multiplayer.sync.WaitOnCancelFuture
import java.util.concurrent.ScheduledExecutorService

/**
 * Wraps a scheduled executor service so tasks print exceptions that occur.
 */
class ScheduledExecutorServiceWrapper(private val executorService: ScheduledExecutorService) {
    fun submit(task: () -> Unit): WaitOnCancelFuture {
        return WaitOnCancelFuture.submitTask(executorService) {
            runAndHandleExceptions(task)
        }
    }

    fun scheduleAtFixedRate(task: () -> Unit, periodMilliseconds: Long): WaitOnCancelFuture {
        return WaitOnCancelFuture.scheduleAtFixedRate(executorService, {
            runAndHandleExceptions(task)
        }, periodMilliseconds)
    }
}

private fun runAndHandleExceptions(task: () -> Unit) {
    try {
        task.invoke()
    } catch (e: InterruptedException) {
        // Do nothing, this is expected to happen
        log("Thread '${Thread.currentThread().name}' scheduled task interrupted")
    } catch (e: Throwable) {
        e.printStackTrace()
    }
}