package ntfwc.mod.multiplayer.util.avatar

import ntfwc.mod.multiplayer.model.PlayerPosition

/**
 * Provides utility functions for enabling avatars.
 */
object AvatarEnablingUtil {
    /**
     * Updates avatar enabled states. It will enable up to the given max count. If it can't enable all avatars, it
     * will enable the avatars closest to the player, which can be displayed.
     */
    fun updateAvatarEnabledStates(avatars: Collection<AvatarInterface>, maxAvatarsToEnable: Int, playerPosition: PlayerPosition) {
        // Handle easy cases
        if (avatars.size <= maxAvatarsToEnable) {
            setAllEnabledStates(avatars, true)
            return
        }
        if (maxAvatarsToEnable == 0) {
            setAllEnabledStates(avatars, false)
            return
        }

        // Since it was not the earlier cases, we have to figure out what avatars can be shown
        val displayableAvatars = filterAndDisableAvatarsThatCannotBeShown(avatars)

        // If we can enable all of the displayable avatars, then we can shortcut here
        if (displayableAvatars.size <= maxAvatarsToEnable) {
            setAllEnabledStates(displayableAvatars, true)
            return
        }

        enableClosestAvatarsAndDisableOthers(displayableAvatars, maxAvatarsToEnable, playerPosition)
    }

    private fun setAllEnabledStates(avatars: Collection<AvatarInterface>, enabled: Boolean) {
        avatars.forEach { it.setAvatarEnabled(enabled) }
    }

    private fun filterAndDisableAvatarsThatCannotBeShown(avatars: Collection<AvatarInterface>): List<AvatarInterface> {
        val filteredAvatars = ArrayList<AvatarInterface>(avatars.size)
        for (avatar in avatars) {
            if (!avatar.canShowAvatar()) {
                avatar.setAvatarEnabled(false)
                continue
            }

            filteredAvatars.add(avatar)
        }

        return filteredAvatars
    }

    private fun enableClosestAvatarsAndDisableOthers(avatars: Collection<AvatarInterface>, maxAvatarsToEnable: Int, playerPosition: PlayerPosition) {
        val avatarsWithDistances = calculateDistances(avatars, playerPosition)
        avatarsWithDistances.sort()
        for (i in avatarsWithDistances.indices) {
            avatarsWithDistances[i].avatarInterface.setAvatarEnabled(i < maxAvatarsToEnable)
        }
    }

    private fun calculateDistances(avatars: Collection<AvatarInterface>, playerPosition: PlayerPosition): MutableList<AvatarInterfaceWithDistance> {
        val avatarsWithDistances = ArrayList<AvatarInterfaceWithDistance>(avatars.size)
        for (avatar in avatars)
            avatarsWithDistances.add(AvatarInterfaceWithDistance.from(avatar, playerPosition))
        return avatarsWithDistances
    }

    interface AvatarInterface {
        fun getPosition(): PlayerPosition
        fun canShowAvatar(): Boolean
        fun setAvatarEnabled(enable: Boolean)
    }

    private class AvatarInterfaceWithDistance(val avatarInterface: AvatarInterface, private val distance: Double) : Comparable<AvatarInterfaceWithDistance> {
        companion object {
            fun from(avatarInterface: AvatarInterface, playerPosition: PlayerPosition): AvatarInterfaceWithDistance {
                val distance = playerPosition.distanceTo(avatarInterface.getPosition())
                return AvatarInterfaceWithDistance(avatarInterface, distance)
            }
        }

        override fun compareTo(other: AvatarInterfaceWithDistance): Int = this.distance.compareTo(other.distance)
    }
}