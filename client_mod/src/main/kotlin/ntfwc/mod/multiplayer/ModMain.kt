package ntfwc.mod.multiplayer

import main.Mod
import ntfwc.mod.multiplayer.game.interfaces.GameInterfaceImpl

class ModMain : Mod {
    val mod: MultiplayerMod

    init {
        val gameInterface = GameInterfaceImpl()
        mod = MultiplayerMod(gameInterface)
    }
}
