package ntfwc.mod.multiplayer

import menu.ModsScreen
import ntfwc.mod.multiplayer.connection.ConnectionManager
import ntfwc.mod.multiplayer.connection.GeneralConnectionStatus
import ntfwc.mod.multiplayer.controls.ControlsConfiguration
import ntfwc.mod.multiplayer.controls.ControlsInterface
import ntfwc.mod.multiplayer.controls.mapping.KeyboardControlMapping
import ntfwc.mod.multiplayer.game.interfaces.GameInterface
import ntfwc.mod.multiplayer.menu.MenuHandler
import ntfwc.mod.multiplayer.menu.PauseMenuModItem
import ntfwc.mod.multiplayer.menu.UIModControlInterface
import ntfwc.mod.multiplayer.model.Configuration
import ntfwc.mod.multiplayer.model.MapName
import ntfwc.mod.multiplayer.model.avatar.array.TimestampedPlayerArrayState
import ntfwc.mod.multiplayer.persistence.ConfigurationStore
import ntfwc.mod.multiplayer.persistence.ControlsConfigurationStore
import ntfwc.mod.multiplayer.persistence.SavingThread
import ntfwc.mod.multiplayer.sync.MultiplayerStateContainer
import ntfwc.mod.multiplayer.util.log
import java.awt.event.KeyEvent
import java.util.concurrent.atomic.AtomicReference

private const val NANOS_BETWEEN_VISIBLE_AVATAR_UPDATES = 2 * 1_000_000_000L

/**
 * Provides the primary mod setup.
 */
class MultiplayerMod(private val gameInterface: GameInterface) {
    private val multiplayerStateContainer = MultiplayerStateContainer()
    private val savingThread = SavingThread()
    private val generalConnectionStatus = AtomicReference<GeneralConnectionStatus>(GeneralConnectionStatus("Disconnected"))
    private var menuHandler: MenuHandler

    private var currentPlayerState: TimestampedPlayerArrayState? = null
    private var currentMap: MapName? = null
    private var connectionManager: ConnectionManager? = null

    private var configuration = readConfiguration() ?: Configuration.INITIAL_CONFIGURATION

    private var updateGameConfigInNextStep = true
    private var nextAvatarUpdateTime = System.nanoTime() + NANOS_BETWEEN_VISIBLE_AVATAR_UPDATES

    init {
        var firstTimeMenuOpenedAction: () -> Unit = {}

        val controlsInterface = readConfigAndCreateControlsInterface()
        // Show a helpful message if the user hasn't configured the mod yet
        if (!ConfigurationStore.doesSavedConfigurationExist()) {
            val firstButtonName = controlsInterface.getFirstMenuButtonName()
            val status = when (val secondButtonName = controlsInterface.getSecondMenuButtonName()) {
                null -> "Go to the mod/sulphide submenu in the pause menu or press \"$firstButtonName\" to open the multiplayer configuration menu"
                else -> "Go to the mod/sulphide submenu in the pause menu or press \"$firstButtonName\" or \"$secondButtonName\" to open the multiplayer configuration menu"
            }
            multiplayerStateContainer.setConnectionStatus(status)
            firstTimeMenuOpenedAction = {
                multiplayerStateContainer.clearConnectionStatus()
            }
        }

        val modControlInterface: UIModControlInterface = object : UIModControlInterface {
            override fun getConfiguration(): Configuration = configuration

            override fun updateConfiguration(configuration: Configuration) = this@MultiplayerMod.updateConfiguration(configuration)

            override fun startConnecting(configuration: Configuration) {
                val connectionManager = connectionManager ?: return
                val map = currentMap ?: return

                startConnectingToServerIfConfigured(connectionManager, map, configuration)
            }

            override fun disconnect() {
                connectionManager?.disconnectAndStop()
            }

            override fun getGeneralConnectionStatus(): GeneralConnectionStatus = generalConnectionStatus.get()

            override fun getLastConnectionStatusUpdate(): String {
                val status = multiplayerStateContainer.getConnectionStatus() ?: return ""
                return status.text
            }

            override fun isNotConnectedOrTryingToConnect(): Boolean {
                val connectionManager = connectionManager ?: return true
                return connectionManager.isNotConnectedOrTryingToConnect()
            }

            override fun getPlayersInMapCount(): Int = multiplayerStateContainer.getPlayersInMapCount()
        }

        menuHandler = MenuHandler(modControlInterface, controlsInterface, firstTimeMenuOpenedAction)

        gameInterface.addMapChangeListener { map: MapName? ->
            log("Map change: $map")
            if (map != null) {
                val connectionManager = this.connectionManager
                if (connectionManager == null) {
                    val playerState = this.currentPlayerState
                    if (playerState != null)
                        initConnectionManager(map, playerState)
                } else if (currentMap == null) {
                    if (this.currentPlayerState != null)
                        startAutoConnectIfEnabled(connectionManager, map)
                } else {
                    switchMap(connectionManager, map)
                }
            } else {
                connectionManager?.disconnectAndStop()
            }
            currentMap = map
        }

        gameInterface.addPlayerStateListener { state ->
            val now = System.nanoTime()
            val timestampedState = TimestampedPlayerArrayState(now, state)

            val connectionManager = this.connectionManager
            if (connectionManager == null) {
                val map = currentMap
                if (map != null)
                    initConnectionManager(map, timestampedState)
            } else {
                connectionManager.updatePlayerState(timestampedState)
            }

            this.currentPlayerState = timestampedState
        }

        gameInterface.addStepListener {
            multiplayerStateContainer.syncPlayerStates(gameInterface.getPlayerOrbPlayers(),
                    { player, state -> gameInterface.addOrUpdatePlayerRenderer(player, state) },
                    { player -> gameInterface.removePlayerOrb(player) })

            gameInterface.setConnectionStatus(multiplayerStateContainer.getConnectionStatus())
            menuHandler.step()

            val isNotConnectedOrTryingToConnect = connectionManager?.isNotConnectedOrTryingToConnect() ?: true
            gameInterface.setPlayerRadarVisible(configuration.enableRadar && !isNotConnectedOrTryingToConnect)
            handleConfigurationUpdateOnStep()

            handleVisibleAvatarsUpdates()
        }

        ModsScreen.modButtonInfos.add(PauseMenuModItem(modControlInterface, firstTimeMenuOpenedAction))
        gameInterface.setPlayerRadarColorConfiguration(configuration.radarColorConfiguration)
        gameInterface.setInitialMaxEnabledAvatarsCount(configuration.maxAvatarCount)
    }

    private fun readConfiguration(): Configuration? {
        val serializedConfiguration = ConfigurationStore.read() ?: return null
        val configuration = Configuration.deserialize(serializedConfiguration)
        if (configuration == null)
            log("Warning: Failed to parse configuration file")

        return configuration
    }

    private fun readConfigAndCreateControlsInterface(): ControlsInterface {
        val controlsInterface: ControlsInterface? = ControlsConfigurationStore.read()
        if (controlsInterface != null)
            return controlsInterface

        log("Failed to read the menu button configuration, using '\\' for the button")
        return ControlsConfiguration(KeyboardControlMapping(KeyEvent.VK_BACK_SLASH, '\\'))
    }

    private fun initConnectionManager(map: MapName, playerState: TimestampedPlayerArrayState) {
        val connectionManager = ConnectionManager(multiplayerStateContainer, playerState, generalConnectionStatus)
        this.connectionManager = connectionManager
        startAutoConnectIfEnabled(connectionManager, map)
    }

    /**
     * Automatically starts connecting, if auto-connect is enabled.
     *
     * @param connectionManager The connection manager.
     * @param map The map our player is in.
     */
    private fun startAutoConnectIfEnabled(connectionManager: ConnectionManager, map: MapName) {
        if (configuration.autoConnect)
            startConnectingToServerIfConfigured(connectionManager, map, configuration)
    }

    /**
     * Starts connecting to a server, if one is configured.
     *
     * @param connectionManager The connection manager.
     * @param map The map our player is in.
     * @param configuration The configuration to use.
     */
    private fun startConnectingToServerIfConfigured(connectionManager: ConnectionManager, map: MapName, configuration: Configuration) {
        val host = configuration.host
        val port = configuration.port
        val username = configuration.username
        if (host.isNotEmpty() && port != null && username.isNotEmpty())
            connectionManager.startConnecting(host, port, username, map)
    }

    private fun switchMap(connectionManager: ConnectionManager, newMap: MapName) {
        connectionManager.updateMap(newMap)
    }

    private fun updateConfiguration(newConfiguration: Configuration) {
        log("Updating the configuration. newConfiguration=$newConfiguration")
        configuration = newConfiguration
        savingThread.saveConfiguration(configuration)
        updateGameConfigInNextStep = true
    }

    private fun handleVisibleAvatarsUpdates() {
        val now = System.nanoTime()
        if (now >= this.nextAvatarUpdateTime) {
            gameInterface.updateEnabledAvatars(configuration.maxAvatarCount)
            this.nextAvatarUpdateTime = now + NANOS_BETWEEN_VISIBLE_AVATAR_UPDATES
        }
    }

    /**
     * Configuration updates should occur during step
     */
    private fun handleConfigurationUpdateOnStep() {
        if (updateGameConfigInNextStep) {
            updateGameConfigInNextStep = false
            gameInterface.setPlayerRadarColorConfiguration(configuration.radarColorConfiguration)
        }
    }
}
