package ntfwc.mod.multiplayer.messages.from_server

import com.beust.klaxon.JsonObject
import ntfwc.mod.multiplayer.util.ParsingUtil

/**
 * Message informing the client that it has been rejected, with some text
 * explaining why (e.g. because the client version is incompatible).
 */
data class ClientRejectionMessage(val reason: String) {
    companion object {
        const val TYPE_ID = "clientRejection"

        fun fromJson(jsonObject: JsonObject): ClientRejectionMessage? {
            val reason = ParsingUtil.getString(jsonObject, "reason") ?: return null
            return ClientRejectionMessage(reason)
        }
    }
}