package ntfwc.mod.multiplayer.messages.from_server

import com.beust.klaxon.JsonObject
import ntfwc.mod.multiplayer.messages.MessageParsingUtil
import ntfwc.mod.multiplayer.model.NamedPlayerStateList
import ntfwc.mod.multiplayer.util.ParsingUtil
import java.util.*
import kotlin.collections.ArrayList

/**
 * An update message sent from the server that tells the client the states of the other players.
 */
data class StateUpdateMessage(val updates: List<NamedPlayerStateList>, val removedPlayers: List<String>) {
    companion object {
        private const val UPDATES_KEY = "updates"
        private const val REMOVED_PLAYERS_KEY = "removed_players"
        const val TYPE_ID = "stateUpdate"

        fun fromJson(jsonObject: JsonObject): StateUpdateMessage? {
            val updates = if (jsonObject.containsKey(UPDATES_KEY)) {
                MessageParsingUtil.tryToGetNamedPlayerStateListsFromJson(jsonObject, UPDATES_KEY) ?: return null
            } else {
                Collections.emptyList()
            }

            val removedPlayers = if (jsonObject.containsKey(REMOVED_PLAYERS_KEY)) {
                tryToReadRemovedPlayers(jsonObject) ?: return null
            } else {
                Collections.emptyList()
            }

            return StateUpdateMessage(updates, removedPlayers)
        }

        private fun tryToReadRemovedPlayers(parsed: JsonObject): List<String>? {
            val removedPlayerArray = ParsingUtil.getArray(parsed, REMOVED_PLAYERS_KEY) ?: return null
            val removedPlayers: ArrayList<String> = ArrayList(removedPlayerArray.size)
            for (removedPlayerValue in removedPlayerArray) {
                val removedPlayer = ParsingUtil.validateAndFilterUsername(removedPlayerValue) ?: return null
                removedPlayers.add(removedPlayer)
            }

            return removedPlayers
        }
    }
}
