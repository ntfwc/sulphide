package ntfwc.mod.multiplayer.messages

import com.beust.klaxon.JsonObject
import ntfwc.mod.multiplayer.model.NamedPlayerStateList
import ntfwc.mod.multiplayer.util.ParsingUtil

object MessageParsingUtil {
    /**
     * Tries to get the named player state lists from a key in a json object.
     *
     * @return The parsed player state lists, or null if parsing failed.
     */
    fun tryToGetNamedPlayerStateListsFromJson(jsonObject: JsonObject, key: String): List<NamedPlayerStateList>? {
        val jsonArray = ParsingUtil.getArray(jsonObject, key) ?: return null
        val playerStates = ArrayList<NamedPlayerStateList>(jsonArray.size)
        for (item in jsonArray) {
            val playerState = NamedPlayerStateList.fromJson(item) ?: return null
            playerStates.add(playerState)
        }

        return playerStates
    }
}