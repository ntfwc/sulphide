package ntfwc.mod.multiplayer.messages

object ProtocolHeaders {
    val SERVER_HEADER = byteArrayOf(
            0x41,
            0x3e,
            0x44,
            0x5b
    )

    val CLIENT_HEADER = byteArrayOf(
            0xf0.toByte(),
            0x75,
            0x40,
            0x2c
    )
}