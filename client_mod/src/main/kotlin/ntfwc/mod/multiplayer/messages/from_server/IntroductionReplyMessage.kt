package ntfwc.mod.multiplayer.messages.from_server

import com.beust.klaxon.JsonObject
import ntfwc.mod.multiplayer.util.ParsingUtil

/**
 * A response to the introduction message. It tells us what our actual username is.
 */
data class IntroductionReplyMessage(val actualUsername: String) {
    companion object {
        fun fromJson(json: JsonObject): IntroductionReplyMessage? {
            return runCatching {
                val actualUsername = ParsingUtil.getUsername(json, "actual_username") ?: return null
                IntroductionReplyMessage(actualUsername)
            }.getOrNull()
        }
    }
}
