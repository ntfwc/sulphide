package ntfwc.mod.multiplayer.messages.from_server

import com.beust.klaxon.JsonObject
import ntfwc.mod.multiplayer.messages.MessageParsingUtil
import ntfwc.mod.multiplayer.model.NamedPlayerStateList

/**
 * Response message for a map switch.
 */
data class MapSwitchReplyMessage(val playerStateLists: List<NamedPlayerStateList>) {
    companion object {
        const val TYPE_ID = "mapSwitchReply"

        fun fromJson(jsonObject: JsonObject): MapSwitchReplyMessage? {
            val playerStates = MessageParsingUtil.tryToGetNamedPlayerStateListsFromJson(jsonObject, "player_state_lists")
                    ?: return null
            return MapSwitchReplyMessage(playerStates)
        }
    }
}