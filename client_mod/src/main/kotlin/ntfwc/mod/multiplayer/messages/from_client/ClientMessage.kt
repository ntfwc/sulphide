package ntfwc.mod.multiplayer.messages.from_client

/**
 * Interface for client messages.
 */
interface ClientMessage {
    /**
     * Converts the message to JSON.
     */
    fun toJson(): String
}
