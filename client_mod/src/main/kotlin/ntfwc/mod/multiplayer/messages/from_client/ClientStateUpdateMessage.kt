package ntfwc.mod.multiplayer.messages.from_client

import com.beust.klaxon.json
import ntfwc.mod.multiplayer.model.MessagePlayerStateList
import ntfwc.mod.multiplayer.util.createTypeIdEntry

/**
 * Message to update the current player states on the server.
 */
data class ClientStateUpdateMessage(val stateList: MessagePlayerStateList) : ClientMessage {
    override fun toJson(): String {
        return json {
            val jsonObject = obj(createTypeIdEntry("stateUpdate"))
            jsonObject["states"] = stateList.toJsonArray()
            jsonObject
        }.toJsonString()
    }
}