package ntfwc.mod.multiplayer.messages.from_client

import com.beust.klaxon.json
import ntfwc.mod.multiplayer.model.MapName
import ntfwc.mod.multiplayer.model.avatar.array.TimestampedPlayerArrayState
import ntfwc.mod.multiplayer.util.createTypeIdEntry

/**
 * Message to switch maps on the server.
 */
data class MapSwitchMessage(val newMap: MapName, val newState: TimestampedPlayerArrayState) : ClientMessage {
    override fun toJson(): String {
        return json {
            val stateObject = obj()
            newState.addToJson(stateObject)
            obj(createTypeIdEntry("mapSwitch"), Pair("new_map", newMap.value), Pair("new_state", stateObject))
        }.toJsonString()
    }
}