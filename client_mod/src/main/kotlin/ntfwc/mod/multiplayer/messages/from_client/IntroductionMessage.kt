package ntfwc.mod.multiplayer.messages.from_client

import com.beust.klaxon.json
import ntfwc.mod.multiplayer.model.MapName
import ntfwc.mod.multiplayer.model.avatar.array.TimestampedPlayerArrayState

private const val CLIENT_VERSION = "2.0.0"

/**
 * The introduction message the client sends when it first connects.
 */
data class IntroductionMessage(
        /**
         * This is the desired username. The username may be taken, so the server can choose to append something to it.
         */
        val username: String,
        val map: MapName,
        val state: TimestampedPlayerArrayState) : ClientMessage {
    override fun toJson(): String {
        return json {
            val jsonObject = obj(Pair("username", username),
                    Pair("map", map.value),
                    Pair("version", CLIENT_VERSION))
            state.addToJson(jsonObject)
            jsonObject.toJsonString()
        }
    }
}
