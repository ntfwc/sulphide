package ntfwc.mod.multiplayer.reflection

import game.TailLink

/**
 * Provides functions to interact with protected/private data
 * from the TailLink class.
 */
object TailLinkDataAccessor {
    private val fieldAccessorContainer = FieldAccessorContainer()

    fun multiplyMaxDist(tailLink: TailLink, factor: Double) =
            fieldAccessorContainer.maxDist.set(tailLink, fieldAccessorContainer.maxDist.get(tailLink) * factor)

    private class FieldAccessorContainer {
        val maxDist = TailLinkFieldAccessor<Double>("maxDist")
    }

    private class TailLinkFieldAccessor<R>(name: String) : FieldAccessorDelegate<TailLink, R>(TailLink::class.java, name)
}