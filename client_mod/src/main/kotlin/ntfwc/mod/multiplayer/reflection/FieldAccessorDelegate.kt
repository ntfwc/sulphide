package ntfwc.mod.multiplayer.reflection

import java.lang.reflect.Field

/**
 * A class that delegates accessing a field to an object that is setup once.
 *
 * @param C The object class.
 * @param R The return value of the field.
 * @param name The name of the field.
 */
open class FieldAccessorDelegate<C, R>(private val objClass: Class<C>, private val name: String) {
    private var accessor: FieldAccessor<C, R>? = null

    fun get(obj: C): R = getAccessor().get(obj)
    fun set(obj: C, value: R) = getAccessor().set(obj, value)

    @Synchronized
    private fun getAccessor(): FieldAccessor<C, R> {
        var accessor = this.accessor
        if (accessor == null) {
            accessor = FieldAccessor(objClass.getDeclaredField(name))
            this.accessor = accessor
        }
        return accessor
    }
}

private class FieldAccessor<C, R>(private val field: Field) {
    init {
        field.isAccessible = true
    }

    @Suppress("UNCHECKED_CAST")
    fun get(obj: C): R = field.get(obj) as R

    fun set(obj: C, value: R) = field.set(obj, value)
}
