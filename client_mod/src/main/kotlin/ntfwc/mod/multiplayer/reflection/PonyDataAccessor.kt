package ntfwc.mod.multiplayer.reflection

import game.Pony
import util.V3D

/**
 * Provides functions to interact with protected/private data
 * from the Pony class.
 */
object PonyDataAccessor {
    private val fieldAccessorContainer = FieldAccessorContainer()

    fun getSVX(pony: Pony): V3D = fieldAccessorContainer.sVX.get(pony)

    fun getSVY(pony: Pony): V3D = fieldAccessorContainer.sVY.get(pony)

    fun getSVZ(pony: Pony): V3D = fieldAccessorContainer.sVZ.get(pony)

    fun getRollAxis(pony: Pony): FloatArray? = fieldAccessorContainer.rollAxis.get(pony)

    fun getRollAmount(pony: Pony): Float = fieldAccessorContainer.rollAmount.get(pony)

    fun getSideSkidFader(pony: Pony): Float = fieldAccessorContainer.sideSkidFader.get(pony)

    fun getFlapping(pony: Pony): Boolean = fieldAccessorContainer.flapping.get(pony)

    fun getPitch(pony: Pony): Float = fieldAccessorContainer.pitch.get(pony)

    fun getRoll(pony: Pony): Float = fieldAccessorContainer.roll.get(pony)

    fun getYaw(pony: Pony): Float = fieldAccessorContainer.yaw.get(pony)

    fun getForwardValue(pony: Pony): Float = fieldAccessorContainer.forwardValue.get(pony)

    private class FieldAccessorContainer {
        val sVX = PonyFieldAccessor<V3D>("sVX")
        val sVY = PonyFieldAccessor<V3D>("sVY")
        val sVZ = PonyFieldAccessor<V3D>("sVZ")
        val rollAxis = PonyFieldAccessor<FloatArray?>("rollAxis")
        val rollAmount = PonyFieldAccessor<Float>("rollAmount")
        val sideSkidFader = PonyFieldAccessor<Float>("sideSkidFader")
        val flapping = PonyFieldAccessor<Boolean>("flapping")
        val pitch = PonyFieldAccessor<Float>("pitch")
        val roll = PonyFieldAccessor<Float>("roll")
        val yaw = PonyFieldAccessor<Float>("yaw")
        val forwardValue = PonyFieldAccessor<Float>("forwardValue")
    }

    private class PonyFieldAccessor<R>(name: String) : FieldAccessorDelegate<Pony, R>(Pony::class.java, name)
}
