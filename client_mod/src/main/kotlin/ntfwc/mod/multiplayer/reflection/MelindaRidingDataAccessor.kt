package ntfwc.mod.multiplayer.reflection

import game.MagicWings
import game.MelindaRiding

/**
 * Provides functions to interact with protected/private data
 * from the MelindaRiding class.
 */
object MelindaRidingDataAccessor {
    private val fieldAccessorContainer = FieldAccessorContainer()

    fun setMagicWings(melindaRiding: MelindaRiding, magicWings: MagicWings) {
        val oldMagicWings = fieldAccessorContainer.magicWings.get(melindaRiding)
        // If there is an old object, it must be destroyed
        if (oldMagicWings != null && !oldMagicWings.isDestroyed)
            oldMagicWings.destroy()

        fieldAccessorContainer.magicWings.set(melindaRiding, magicWings)
    }

    private class FieldAccessorContainer {
        val magicWings = MelindaRidingFieldAccessor<MagicWings?>("magicWings")
    }

    private class MelindaRidingFieldAccessor<R>(name: String) : FieldAccessorDelegate<MelindaRiding, R>(MelindaRiding::class.java, name)
}