package ntfwc.mod.multiplayer.sync

import ntfwc.mod.multiplayer.buffer.PlayerStateBuffer
import ntfwc.mod.multiplayer.model.ConnectionStatus
import ntfwc.mod.multiplayer.model.NamedPlayerStateList
import ntfwc.mod.multiplayer.model.avatar.array.PlayerArrayState
import java.util.*
import java.util.concurrent.atomic.AtomicReference

/**
 * Contains the shared multiplayer state in a thread-safe way.
 */
class MultiplayerStateContainer {
    private val connectionStatus: AtomicReference<ConnectionStatus> = AtomicReference<ConnectionStatus>(null)
    private val playerStateBuffers = HashMap<String, PlayerStateBuffer>()

    fun getConnectionStatus(): ConnectionStatus? = connectionStatus.get()

    /**
     * Sets the connection status shown to the user.
     *
     * @param text The text to show.
     * @param displayTimeInSeconds The amount of time, in seconds, to show the
     * message. If not specified it will be shown until the next status is set.
     */
    fun setConnectionStatus(text: String, displayTimeInSeconds: Int? = null) {
        val timeToStopShowing = if (displayTimeInSeconds != null) {
            Date(Date().time + displayTimeInSeconds.toLong() * 1000)
        } else {
            null
        }
        connectionStatus.set(ConnectionStatus(text, timeToStopShowing))
    }

    fun clearConnectionStatus() = connectionStatus.set(null)

    /**
     * Appends the given player state lists to the player state buffers. It will create buffers for new players.
     *
     * @param playerStatelists The player state lists to append.
     * @param removedPlayers The players to remove.
     */
    @Synchronized
    fun applyNewPlayerUpdates(playerStatelists: List<NamedPlayerStateList>, removedPlayers: List<String>) {
        for (removedPlayer in removedPlayers) {
            playerStateBuffers.remove(removedPlayer)
        }

        val timestampNow = System.nanoTime()
        for (playerStateList in playerStatelists) {
            val username = playerStateList.username
            val stateListIterator = playerStateList.playerStateList.iterator()

            // This list must have at least one state
            val firstState = stateListIterator.next()

            var playerStateBuffer = playerStateBuffers[username]
            if (playerStateBuffer != null) {
                playerStateBuffer.addUpdate(firstState, timestampNow)
            } else {
                playerStateBuffer = PlayerStateBuffer(firstState)
                playerStateBuffers[username] = playerStateBuffer
            }

            for (state in stateListIterator)
                playerStateBuffer.addUpdate(state, timestampNow)
        }
    }

    /**
     * Clears all players from the state.
     */
    @Synchronized
    fun clearPlayers() {
        playerStateBuffers.clear()
    }

    /**
     * Takes the the given list of players and uses the given procedures to update them with the current
     * state of the players. It will set interpolated player positions.
     *
     * @param players The current list of players to sync with.
     * @param addOrUpdate A procedure to add or update a player position.
     * @param removePlayer A procedure to remove a player that doesn't exist anymore.
     */
    @Synchronized
    fun syncPlayerStates(players: Collection<String>,
                         addOrUpdate: (name: String, position: PlayerArrayState) -> Unit,
                         removePlayer: (name: String) -> Unit) {
        val timestampNow = System.nanoTime()
        val playersNotFound = HashSet(players)

        for ((player, playerStateBuffer) in this.playerStateBuffers) {
            playersNotFound.remove(player)
            val state = playerStateBuffer.getNext(timestampNow)
            addOrUpdate(player, state)
        }

        for (player in playersNotFound)
            removePlayer(player)
    }

    @Synchronized
    fun getPlayersInMapCount(): Int = this.playerStateBuffers.size
}
