package ntfwc.mod.multiplayer.sync

import java.util.concurrent.Future
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

/**
 * A future that we can both cancel and wait for it to stop, if it is running.
 */
class WaitOnCancelFuture private constructor(private val waitOnCancelMonitor: WaitOnCancelMonitor, private val future: Future<*>) {
    companion object {
        fun submitTask(executor: ScheduledExecutorService, task: () -> Unit): WaitOnCancelFuture {
            val waitOnCancelMonitor = WaitOnCancelMonitor()
            val future = executor.submit {
                waitOnCancelMonitor.runTask(task)
            }

            return WaitOnCancelFuture(waitOnCancelMonitor, future)
        }

        fun scheduleAtFixedRate(executor: ScheduledExecutorService, task: () -> Unit, periodMilliseconds: Long): WaitOnCancelFuture {
            val waitOnCancelMonitor = WaitOnCancelMonitor()
            val future = executor.scheduleAtFixedRate({
                waitOnCancelMonitor.runTask(task)
            }, 0, periodMilliseconds, TimeUnit.MILLISECONDS)

            return WaitOnCancelFuture(waitOnCancelMonitor, future)
        }
    }

    /**
     * Cancels this future and waits for it to stop.
     */
    fun cancelAndWait() {
        future.cancel(true)
        waitOnCancelMonitor.waitForTaskToEnd()
    }

    private class WaitOnCancelMonitor {
        @Synchronized
        fun runTask(task: () -> Unit) {
            task()
        }

        @Synchronized
        fun waitForTaskToEnd() {
            // Do nothing, just wait for the lock to be available, meaning that the task is not running.
        }
    }
}