package ntfwc.mod.multiplayer.gl

import com.jogamp.opengl.GL2
import com.jogamp.opengl.GLException
import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.nio.ByteBuffer
import java.nio.IntBuffer

/**
 * A compiled GLSL shader.
 */
class CompiledShader private constructor(val shaderId: Int) {
    companion object {
        /**
         * Compiles a shader, reading the source from the given file. If compilation fails, an exception will be raised.
         *
         * @param gl The OpenGL context
         * @param filePath The path to the source file
         * @param isVertexShader If true, this is compiled as a vertex shader, otherwise it is compiled as a fragment shader.
         */
        fun compileShader(gl: GL2, filePath: String, isVertexShader: Boolean): CompiledShader {
            val source = readFile(filePath)
            val shaderId = gl.glCreateShader(if (isVertexShader) GL2.GL_VERTEX_SHADER else GL2.GL_FRAGMENT_SHADER)

            gl.glShaderSource(shaderId, 1, arrayOf(source), intArrayOf(source.length), 0)
            gl.glCompileShader(shaderId)

            val status = IntArray(1)
            gl.glGetShaderiv(shaderId, GL2.GL_COMPILE_STATUS, status, 0)

            val compileError = getCompileError(gl, shaderId)
            if (compileError != null)
                throw GLException("Shader compilation failed: $compileError")

            return CompiledShader(shaderId)
        }

        private fun readFile(filePath: String): String {
            val reader = BufferedReader(FileReader(File(filePath)))
            reader.use {
                return reader.readText()
            }
        }

        private fun getCompileError(gl: GL2, shaderId: Int): String? {
            if (readShaderInt(gl, shaderId, GL2.GL_COMPILE_STATUS) == GL2.GL_TRUE)
                return null

            return readLogInfo(gl, shaderId)
        }

        private fun readLogInfo(gl: GL2, shaderId: Int): String {
            val logLength = readShaderInt(gl, shaderId, GL2.GL_INFO_LOG_LENGTH)
            val buffer = ByteBuffer.allocate(logLength)
            gl.glGetShaderInfoLog(shaderId, logLength, IntBuffer.allocate(1), buffer)
            return bufferToString(buffer)
        }

        private fun readShaderInt(gl: GL2, shaderId: Int, key: Int): Int {
            val valueArray = IntArray(1)
            gl.glGetShaderiv(shaderId, key, valueArray, 0)
            return valueArray[0]
        }
    }
}
