package ntfwc.mod.multiplayer.gl

/**
 * Interface for a view that presents data as a read-only list of floats.
 */
interface FloatListView : Iterable<Float> {
    /**
     * @return The size of the list.
     */
    fun getSize(): Int

    /**
     * @param index The index of the list to read.
     * @return A value.
     */
    fun get(index: Int): Float

    override fun iterator(): Iterator<Float> {
        return object : Iterator<Float> {
            private var index = 0

            override fun hasNext(): Boolean = index < getSize()
            override fun next(): Float = get(index++)
        }
    }
}