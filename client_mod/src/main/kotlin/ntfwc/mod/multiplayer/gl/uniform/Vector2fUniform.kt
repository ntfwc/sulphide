package ntfwc.mod.multiplayer.gl.uniform

import com.jogamp.opengl.GL2
import ntfwc.mod.multiplayer.util.vector.Vector2f

/**
 * A uniform that takes a vec2.
 */
class Vector2fUniform(id: Int, programId: Int) : Uniform<Vector2f>(id, programId) {
    override fun setValue(gl: GL2, value: Vector2f) {
        gl.glProgramUniform2f(programId, id, value.x, value.y)
    }
}