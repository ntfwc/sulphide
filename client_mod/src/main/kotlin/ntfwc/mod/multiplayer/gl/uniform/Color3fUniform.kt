package ntfwc.mod.multiplayer.gl.uniform

import com.jogamp.opengl.GL2
import ntfwc.mod.multiplayer.util.Color3f

/**
 * A uniform that takes a vec3 which represents a color.
 */
class Color3fUniform(id: Int, programId: Int) : Uniform<Color3f>(id, programId) {
    override fun setValue(gl: GL2, value: Color3f) {
        gl.glProgramUniform3f(programId, id, value.red, value.green, value.blue)
    }
}