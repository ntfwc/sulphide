package ntfwc.mod.multiplayer.gl

import java.nio.ByteBuffer
import java.nio.charset.StandardCharsets

fun bufferToString(buffer: ByteBuffer): String {
    return StandardCharsets.UTF_8.decode(buffer).toString()
}