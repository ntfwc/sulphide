package ntfwc.mod.multiplayer.gl

/**
 * Wraps an array of 16 floats as a 4x4 matrix.
 */
class Matrix4x4Array(val array: FloatArray) {
    fun getValue(column: Int, row: Int) = array[column + row * 4]
}