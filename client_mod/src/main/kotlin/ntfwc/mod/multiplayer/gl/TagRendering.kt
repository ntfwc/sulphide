package ntfwc.mod.multiplayer.gl

import graphics.GFX
import main.GameState
import main.Scene
import util.V3D

/**
 * Provides functions for rendering tags, meaning camera aligned markers, like name tags.
 */
object TagRendering {
    /**
     * Draws the given text texture as a tag. It is drawn with orthographic projection,
     * always facing the camera and it is scaled by the distance from the camera.
     */
    fun draw(textTexture: RenderedTextTexture, gameState: GameState, x: Double, y: Double, z: Double) {
        val vp = gameState.viewportMatrix
        val gl = GFX.gl
        gl.glPushMatrix()

        val worldPos = V3D(x, y, z)
        val windowPos = V3D()
        Scene.particlePosition(worldPos, windowPos, 0.0)

        val normalizedDeviceDepth = calculateNormalizedDepthFromCamera(gameState, x, y, z)
        val r2 = 2 * normalizedDeviceDepth * gameState.cameraAngleTangent.toDouble()
        val cameraScale = vp[3] / r2
        val scale = 0.3 * 158 / 93
        val renderScale = (cameraScale * scale).toFloat()

        gl.glTranslated(windowPos.x(), windowPos.y(), 1 - 2 * windowPos.z())

        textTexture.drawCentered(gl, renderScale)

        gl.glPopMatrix()
    }

    private fun calculateNormalizedDepthFromCamera(gameState: GameState, x: Double, y: Double, z: Double): Double {
        val pos = V3D(x, y, z)
        return pos.clone().sub(gameState.cameraPos).dot(gameState.cameraForward)
    }
}