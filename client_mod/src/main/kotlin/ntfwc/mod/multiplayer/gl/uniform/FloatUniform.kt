package ntfwc.mod.multiplayer.gl.uniform

import com.jogamp.opengl.GL2

/**
 * A uniform that takes a float.
 */
class FloatUniform(id: Int, programId: Int) : Uniform<Float>(id, programId) {
    override fun setValue(gl: GL2, value: Float) {
        gl.glProgramUniform1f(programId, id, value)
    }
}