package ntfwc.mod.multiplayer.gl

import com.jogamp.common.nio.Buffers
import java.nio.FloatBuffer

/**
 * A RAM implementation of vertex float memory. Manages a direct float buffer that can be used for
 * vertex array OpenGL methods.
 */
class VertexFloatArrayBuffer {
    var buffer: FloatBuffer = Buffers.newDirectFloatBuffer(10)

    /**
     * Sets the data of the buffer by making sure enough space is allocated and copying from the given list view.
     *
     * @param floatList A list of values to copy.
     */
    fun setData(floatList: FloatListView) {
        val size = floatList.getSize()
        ensureCapacity(size)

        for ((position, value) in floatList.withIndex()) {
            buffer.put(position, value)
        }
    }

    private fun ensureCapacity(size: Int) {
        if (buffer.limit() < size) {
            val newSize = size + size / 2
            buffer = Buffers.newDirectFloatBuffer(newSize)
        }
    }
}