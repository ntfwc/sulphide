package ntfwc.mod.multiplayer.gl.uniform

import com.jogamp.opengl.GL2

/**
 * A uniform value in a shader program.
 */
abstract class Uniform<T : Any>(val id: Int, val programId: Int) {
    private var lastValue: T? = null

    fun set(gl: GL2, value: T) {
        if (value == lastValue)
            return

        lastValue = value
        setValue(gl, value)
    }

    protected abstract fun setValue(gl: GL2, value: T)
}