package ntfwc.mod.multiplayer.gl.uniform

import com.jogamp.opengl.GL2
import ntfwc.mod.multiplayer.util.vector.Vector3f

/**
 * A uniform that takes a vec3.
 */
class Vector3fUniform(id: Int, programId: Int) : Uniform<Vector3f>(id, programId) {
    override fun setValue(gl: GL2, value: Vector3f) {
        gl.glProgramUniform3f(programId, id, value.x, value.y, value.z)
    }
}