package ntfwc.mod.multiplayer.gl

import com.jogamp.opengl.GL
import com.jogamp.opengl.GL2
import com.jogamp.opengl.util.GLBuffers
import com.jogamp.opengl.util.texture.Texture
import com.jogamp.opengl.util.texture.TextureIO
import com.jogamp.opengl.util.texture.awt.AWTTextureIO
import graphics.BlendMode
import graphics.GFX
import ntfwc.mod.multiplayer.util.vector.Vector2f
import java.awt.Color
import java.awt.Font
import java.awt.RenderingHints
import java.awt.image.BufferedImage
import kotlin.math.ceil

/**
 * A texture that has rendered text.
 *
 * @param boundaryTexCoord Represents the corner coordinate of the part of the texture we are using, starting from (0,0).
 */
class RenderedTextTexture private constructor(private val texture: Texture,
                                              private val gl: GL2,
                                              private val height: Float,
                                              private val aspectRatio: Float,
                                              private val boundaryTexCoord: Vector2f) {
    companion object {
        private val measuringGraphics = BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB).graphics

        /**
         * Renders the given text, using the given font and shadow offset, to a texture.
         */
        fun renderTextToTexture(gl: GL2, text: String, font: Font, shadowOffset: Int, foregroundColor: Color = Color.WHITE): RenderedTextTexture {
            val useLimitedColorProfile = foregroundColor == Color.WHITE
            return if (GFX.gfxSettings.npots)
                renderTextToFittingTexture(gl, TextConfig(text, font, shadowOffset, foregroundColor, useLimitedColorProfile))
            else
                renderTextToPowerOf2Texture(gl, TextConfig(text, font, shadowOffset, foregroundColor, useLimitedColorProfile))
        }

        /**
         * Renders the given text, using the given font and shadow offset, to a texture with a size just big enough
         * to contain the text, according to the font.
         */
        private fun renderTextToFittingTexture(gl: GL2, textConfig: TextConfig): RenderedTextTexture {
            val textDrawingInfo = getTextDrawingInfo(textConfig.text, textConfig.font)

            val imageWidth = textDrawingInfo.width + textConfig.shadowOffset
            val imageHeight = textDrawingInfo.height + textConfig.shadowOffset

            val image = renderTextToImage(textConfig, textDrawingInfo.ascent, imageWidth, imageHeight)
            val texture = uploadImageToTexture(gl, image, textConfig.useLimitedColorProfile)
            return RenderedTextTexture(texture, gl, texture.height.toFloat(), texture.aspectRatio, Vector2f(1f, 1f))
        }

        /**
         * Renders the given text, using the given font and shadow offset, to a power of 2 texture that is big enough to
         * contain the text, according to the font.
         */
        private fun renderTextToPowerOf2Texture(gl: GL2, textConfig: TextConfig): RenderedTextTexture {
            val textDrawingInfo = getTextDrawingInfo(textConfig.text, textConfig.font)

            val textRegionWidth = textDrawingInfo.width + textConfig.shadowOffset
            val textRegionHeight = textDrawingInfo.height + textConfig.shadowOffset

            val imageWidth = GLBuffers.getNextPowerOf2(textRegionWidth)
            val imageHeight = GLBuffers.getNextPowerOf2(textRegionHeight)

            val image = renderTextToImage(textConfig, textDrawingInfo.ascent, imageWidth, imageHeight)
            val texture = uploadImageToTexture(gl, image, textConfig.useLimitedColorProfile)

            val boundaryTexCoord = Vector2f(textRegionWidth.toFloat() / imageWidth, textRegionHeight.toFloat() / imageHeight)
            return RenderedTextTexture(texture, gl, textRegionHeight.toFloat(), textRegionWidth.toFloat() / textRegionHeight, boundaryTexCoord)
        }

        private fun renderTextToImage(textConfig: TextConfig, ascent: Float, imageWidth: Int, imageHeight: Int): BufferedImage {
            val image = BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_ARGB)
            val graphics = image.createGraphics()
            graphics.font = textConfig.font
            graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)

            val baselineY = ascent + textConfig.shadowOffset

            // Draw the shadow
            graphics.color = Color.BLACK
            graphics.drawString(textConfig.text, 0f, baselineY - textConfig.shadowOffset)

            // Draw the foreground
            graphics.color = textConfig.foregroundColor
            graphics.drawString(textConfig.text, textConfig.shadowOffset.toFloat(), baselineY)

            return image
        }

        private fun uploadImageToTexture(gl: GL2, image: BufferedImage, useLimitedColorProfile: Boolean): Texture {
            val internalFormat = if (useLimitedColorProfile)
                GL2.GL_LUMINANCE4_ALPHA4
            else
                GL2.GL_RGBA8
            val textureData = AWTTextureIO.newTextureData(gl.glProfile, image, internalFormat, GL2.GL_RGBA, false)
            return TextureIO.newTexture(textureData)
        }

        private fun getTextDrawingInfo(text: String, font: Font): TextDrawingInfo {
            val fontMetrics = measuringGraphics.getFontMetrics(font)
            val stringBounds = fontMetrics.getStringBounds(text, measuringGraphics)
            return TextDrawingInfo(-stringBounds.y.toFloat(), ceil(stringBounds.width).toInt(), ceil(stringBounds.height).toInt())
        }
    }

    fun drawCentered(gl: GL2, scale: Float) {
        val height = height * scale
        val width = height * aspectRatio

        val leftX = -width / 2
        val bottomY = -height / 2
        val rightX = leftX + width
        val topY = bottomY + height

        draw(gl, leftX, topY, bottomY, rightX)
    }

    private fun draw(gl: GL2, leftX: Float, topY: Float, bottomY: Float, rightX: Float) {
        gl.glEnable(GL2.GL_TEXTURE_2D)
        BlendMode.NORMAL.set()
        texture.bind(gl)

        gl.glColor4fv(GFX.WHITE, 0)
        gl.glBegin(GL.GL_TRIANGLE_STRIP)

        gl.glNormal3f(0f, 0f, 1f)

        gl.glTexCoord2f(0f, boundaryTexCoord.y)
        gl.glVertex2f(leftX, topY)

        gl.glTexCoord2f(0f, 0f)
        gl.glVertex2f(leftX, bottomY)

        gl.glTexCoord2f(boundaryTexCoord.x, boundaryTexCoord.y)
        gl.glVertex2f(rightX, topY)

        gl.glTexCoord2f(boundaryTexCoord.x, 0f)
        gl.glVertex2f(rightX, bottomY)

        gl.glEnd()

        gl.glDisable(GL2.GL_TEXTURE_2D)
    }

    protected fun finalize() {
        texture.destroy(gl)
    }
}

private class TextDrawingInfo(val ascent: Float, val width: Int, val height: Int)

private class TextConfig(val text: String, val font: Font, val shadowOffset: Int, val foregroundColor: Color, val useLimitedColorProfile: Boolean)