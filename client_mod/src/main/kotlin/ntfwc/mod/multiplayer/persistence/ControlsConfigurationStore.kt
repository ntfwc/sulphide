package ntfwc.mod.multiplayer.persistence

import ntfwc.mod.multiplayer.controls.ControlsConfiguration
import ntfwc.mod.multiplayer.controls.mapping.ControlMapping
import ntfwc.mod.multiplayer.controls.mapping.GamepadButtonMapping
import ntfwc.mod.multiplayer.controls.mapping.KeyboardControlMapping
import ntfwc.mod.multiplayer.util.log
import java.awt.event.KeyEvent
import java.io.File
import java.io.FileInputStream
import java.io.InputStreamReader
import java.nio.charset.Charset

/**
 * Handles reading the controls configuration for this mod.
 */
class ControlsConfigurationStore private constructor() {
    companion object {
        private val CONFIGURATION_FILE = File("mod/sulphide/menu_button_mapping.txt")
        private val CHAR_SET = Charset.forName("UTF-8")

        /**
         * Reads the controls configuration from the menu button configuration file.
         *
         * @return The configuration, or nothing if the file doesn't exist or parsing fails.
         */
        fun read(): ControlsConfiguration? {
            val configurationText = readConfigurationText() ?: return null
            return when (val splitPosition = configurationText.indexOf("|")) {
                -1 -> {
                    val menuControlMapping = parseMapping(configurationText) ?: return null
                    ControlsConfiguration(menuControlMapping)
                }
                else -> {
                    // Handle two control mappings being defined
                    val controlMapping1 = parseMapping(configurationText.substring(0, splitPosition)) ?: return null
                    val controlMapping2 = parseMapping(configurationText.substring(splitPosition + 1)) ?: return null
                    ControlsConfiguration(controlMapping1, controlMapping2)
                }
            }
        }

        private fun parseMapping(mappingText: String): ControlMapping? {
            when (mappingText.length) {
                1 -> {
                    return parseKeyboardMapping(mappingText[0])
                }
                0 -> {
                    return null
                }
            }

            if (mappingText.startsWith("B")) {
                return parseGamepadButtonMapping(mappingText.substring(1))
            }

            return null
        }

        private fun parseKeyboardMapping(keyCharacter: Char): KeyboardControlMapping? {
            val code = convertCharToKeyCode(keyCharacter)
            if (code == null) {
                log("Warning failed to map configured key character '$keyCharacter' to an AWT KeyEvent code")
                return null
            }

            return KeyboardControlMapping(code, keyCharacter)
        }

        private fun convertCharToKeyCode(keyCharacter: Char): Int? {
            if (keyCharacter in 'a'..'z')
                return KeyEvent.VK_A + (keyCharacter - 'a')
            if (keyCharacter in '0'..'9')
                return KeyEvent.VK_0 + (keyCharacter - '0')

            return when (keyCharacter) {
                '`' -> KeyEvent.VK_BACK_QUOTE
                '-' -> KeyEvent.VK_MINUS
                '=' -> KeyEvent.VK_EQUALS
                '[' -> KeyEvent.VK_OPEN_BRACKET
                ']' -> KeyEvent.VK_CLOSE_BRACKET
                '\\' -> KeyEvent.VK_BACK_SLASH
                ';' -> KeyEvent.VK_SEMICOLON
                // For some reason quote doesn't work
                //'\'' -> KeyEvent.VK_QUOTE
                ',' -> KeyEvent.VK_COMMA
                '.' -> KeyEvent.VK_PERIOD
                '/' -> KeyEvent.VK_SLASH
                else -> null
            }
        }

        private fun parseGamepadButtonMapping(numberText: String): GamepadButtonMapping? {
            val value = kotlin.runCatching { numberText.toInt() }.getOrNull() ?: return null
            return GamepadButtonMapping(value)
        }

        private fun readConfigurationText(): String? {
            if (!CONFIGURATION_FILE.exists()) {
                log("Found no menu button configuration file")
                return null
            }

            try {
                val reader = InputStreamReader(FileInputStream(CONFIGURATION_FILE), CHAR_SET)
                reader.use {
                    return reader.readText().trim()
                }
            } catch (e: Exception) {
                log("Warning: Failed to read the button configuration file. Error: $e")
                return null
            }
        }
    }
}
