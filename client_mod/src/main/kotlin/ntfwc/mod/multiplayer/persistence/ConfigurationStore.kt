package ntfwc.mod.multiplayer.persistence

import ntfwc.mod.multiplayer.util.log
import java.io.*
import java.nio.charset.Charset

/**
 * Handles reading and saving serialized configuration.
 */
class ConfigurationStore private constructor() {
    companion object {

        private val CONFIGURATION_FILE = File("mod/sulphide/config.json")
        private val TEMP_FILE = File("mod/sulphide/config.json.temp")
        private val CHAR_SET = Charset.forName("UTF-8")

        /**
         * Tries to read the currently saved serialized configuration.
         *
         * @return A serialized configuration, or null if it can't be read.
         */
        fun read(): String? {
            if (!CONFIGURATION_FILE.exists())
                return null

            try {
                val reader = InputStreamReader(FileInputStream(CONFIGURATION_FILE), CHAR_SET)
                reader.use {
                    return reader.readText()
                }
            } catch (e: Exception) {
                log("Warning: Failed to read the configuration file. Error: $e")
                return null
            }
        }

        /**
         * Tries to save a serialized configuration. It will try to do this atomically. This should only be called
         * from one thread.
         *
         * @param serializedConfiguration The configuration to save.
         */
        fun save(serializedConfiguration: String) {
            try {
                writeAndSync(TEMP_FILE, serializedConfiguration)
            } catch (e: Exception) {
                log("Warning: Failed to write a temporary configuration file. Error: $e")
                return
            }

            try {
                // Update the configuration. It will replace any existing configuration file. This operation should be
                // atomic on most platforms.
                if (!TEMP_FILE.renameTo(CONFIGURATION_FILE)) {
                    log("Warning: Failed to update the configuration, the rename operation failed")

                    // Cleanup the temporary file, since it won't be used
                    TEMP_FILE.delete()
                }
            } catch (e: Exception) {
                log("Warning: Failed to update the configuration with a rename operation. Error: $e")
            }
        }

        /**
         * Writes the given content to the given file and waits for it to be synced to the secondary storage device.
         * It can throw I/O exceptions.
         *
         * @param outputFile The file to write to.
         * @param content The content to write.
         */
        private fun writeAndSync(outputFile: File, content: String) {
            val outputStream = FileOutputStream(outputFile)
            val writer = OutputStreamWriter(outputStream, CHAR_SET)
            writer.use {
                writer.write(content)
                writer.flush()
                outputStream.fd.sync()
            }
        }

        /**
         * To be run during startup. It will remove old temporary files.
         */
        fun runInitialCleanup() {
            if (TEMP_FILE.exists())
                TEMP_FILE.delete()
        }

        /**
         * @return True if a saved configuration exists, false otherwise
         */
        fun doesSavedConfigurationExist() = CONFIGURATION_FILE.exists()
    }
}