package ntfwc.mod.multiplayer.persistence

import ntfwc.mod.multiplayer.model.Configuration
import java.util.concurrent.Executors

/**
 * Thread for saving configuration changes.
 */
class SavingThread {
    private val executor = Executors.newSingleThreadExecutor()

    init {
        executor.submit { ConfigurationStore.runInitialCleanup() }
    }

    fun saveConfiguration(configuration: Configuration) {
        executor.submit {
            ConfigurationStore.save(configuration.serialize())
        }
    }
}