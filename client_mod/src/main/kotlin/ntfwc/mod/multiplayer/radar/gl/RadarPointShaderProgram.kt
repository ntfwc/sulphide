package ntfwc.mod.multiplayer.radar.gl

import com.jogamp.opengl.GL2
import com.jogamp.opengl.GLException
import ntfwc.mod.multiplayer.gl.CompiledShader
import ntfwc.mod.multiplayer.gl.VertexFloatArrayBuffer
import ntfwc.mod.multiplayer.gl.bufferToString
import ntfwc.mod.multiplayer.gl.uniform.Color3fUniform
import ntfwc.mod.multiplayer.gl.uniform.FloatUniform
import ntfwc.mod.multiplayer.gl.uniform.Vector2fUniform
import ntfwc.mod.multiplayer.gl.uniform.Vector3fUniform
import ntfwc.mod.multiplayer.util.Color3f
import ntfwc.mod.multiplayer.util.vector.Vector2f
import ntfwc.mod.multiplayer.util.vector.Vector3f
import java.nio.ByteBuffer
import java.nio.IntBuffer

/**
 * The shader program used for rendering radar points.
 */
class RadarPointShaderProgram private constructor(private val programId: Int,
                                                  private val absolutePointPositionAttribute: Int,
                                                  private val centerPositionUniform: Vector3fUniform,
                                                  private val distanceScaleUniform: FloatUniform,
                                                  private val radarRadiusUniform: FloatUniform,
                                                  private val heightDifferenceForMaxColorChangeUniform: FloatUniform,
                                                  private val aboveColorUniform: Color3fUniform,
                                                  private val belowColorUniform: Color3fUniform,
                                                  private val viewportHalvedDimensionsUniform: Vector2fUniform,
                                                  private val pointRadiusSquaredUniform: FloatUniform
) {
    companion object {
        private var shaderProgram: RadarPointShaderProgram? = null

        fun getOrInitialize(gl: GL2): RadarPointShaderProgram {
            var shaderProgram = this.shaderProgram
            if (shaderProgram != null)
                return shaderProgram

            shaderProgram = initialize(gl)
            this.shaderProgram = shaderProgram
            return shaderProgram
        }

        private fun initialize(gl: GL2): RadarPointShaderProgram {
            val vertexShader = CompiledShader.compileShader(gl, "mod/sulphide/res/shaders/radar_point.vert", true)
            val fragmentShader = CompiledShader.compileShader(gl, "mod/sulphide/res/shaders/radar_point.frag", false)

            val programId = gl.glCreateProgram()
            gl.glAttachShader(programId, vertexShader.shaderId)
            gl.glAttachShader(programId, fragmentShader.shaderId)

            gl.glLinkProgram(programId)
            val linkError = getLinkError(gl, programId)
            if (linkError != null)
                throw GLException("Linking shader program failed: $linkError")

            gl.glValidateProgram(programId)
            val validateError = getValidateError(gl, programId)
            if (validateError != null)
                throw GLException("Validating shader program failed: $validateError")

            val absolutePointPositionAttribute = getAttribute(gl, programId, "absolutePointPosition")
            val centerPositionUniform = Vector3fUniform(getUniform(gl, programId, "centerPosition"), programId)
            val distanceScaleUniform = FloatUniform(getUniform(gl, programId, "distanceScale"), programId)
            val radarRadiusUniform = FloatUniform(getUniform(gl, programId, "radarRadius"), programId)
            val heightDifferenceForMaxColorChangeUniform = FloatUniform(getUniform(gl, programId, "heightDifferenceForMaxColorChange"), programId)
            val aboveColorUniform = Color3fUniform(getUniform(gl, programId, "aboveColor"), programId)
            val belowColorUniform = Color3fUniform(getUniform(gl, programId, "belowColor"), programId)
            val viewportHalvedDimensionsUniform = Vector2fUniform(getUniform(gl, programId, "viewportHalvedDimensions"), programId)
            val pointRadiusSquaredUniform = FloatUniform(getUniform(gl, programId, "pointRadiusSquared"), programId)
            return RadarPointShaderProgram(programId,
                    absolutePointPositionAttribute,
                    centerPositionUniform,
                    distanceScaleUniform,
                    radarRadiusUniform,
                    heightDifferenceForMaxColorChangeUniform,
                    aboveColorUniform,
                    belowColorUniform,
                    viewportHalvedDimensionsUniform,
                    pointRadiusSquaredUniform)
        }

        private fun getAttribute(gl: GL2, programId: Int, name: String): Int {
            val result = gl.glGetAttribLocation(programId, "absolutePointPosition")
            if (result == -1)
                throw GLException("Failed to find program attribute '$name'")

            return result
        }

        private fun getUniform(gl: GL2, programId: Int, name: String): Int {
            val result = gl.glGetUniformLocation(programId, name)
            if (result == -1)
                throw GLException("Failed to find program uniform '$name'")

            return result
        }

        private fun getLinkError(gl: GL2, programId: Int): String? {
            if (readProgramInt(gl, programId, GL2.GL_LINK_STATUS) == GL2.GL_TRUE)
                return null

            return readLogInfo(gl, programId)
        }

        private fun getValidateError(gl: GL2, programId: Int): String? {
            if (readProgramInt(gl, programId, GL2.GL_VALIDATE_STATUS) == GL2.GL_TRUE)
                return null

            return readLogInfo(gl, programId)
        }

        private fun readLogInfo(gl: GL2, programId: Int): String {
            val logLength = readProgramInt(gl, programId, GL2.GL_INFO_LOG_LENGTH)
            val buffer = ByteBuffer.allocate(logLength)
            gl.glGetProgramInfoLog(programId, logLength, IntBuffer.allocate(1), buffer)
            return bufferToString(buffer)
        }

        private fun readProgramInt(gl: GL2, programId: Int, key: Int): Int {
            val valueArray = IntArray(1)
            gl.glGetProgramiv(programId, key, valueArray, 0)
            return valueArray[0]
        }
    }

    /**
     * Enables the program and sets up its data.
     */
    fun enableAndSetupProgram(gl: GL2,
                              pointAbsolutePositionBuffer: VertexFloatArrayBuffer,
                              centerPosition: Vector3f,
                              distanceScale: Float,
                              radarRadius: Float,
                              heightDifferenceForMaxColorChange: Float,
                              aboveColor: Color3f,
                              belowColor: Color3f,
                              viewportHalvedDimensions: Vector2f,
                              pointRadiusSquared: Float) {
        gl.glUseProgram(programId)
        gl.glEnableVertexAttribArray(absolutePointPositionAttribute)
        gl.glVertexAttribPointer(absolutePointPositionAttribute, 3, GL2.GL_FLOAT, false, 0, pointAbsolutePositionBuffer.buffer)
        centerPositionUniform.set(gl, centerPosition)
        distanceScaleUniform.set(gl, distanceScale)
        radarRadiusUniform.set(gl, radarRadius)
        heightDifferenceForMaxColorChangeUniform.set(gl, heightDifferenceForMaxColorChange)
        aboveColorUniform.set(gl, aboveColor)
        belowColorUniform.set(gl, belowColor)
        viewportHalvedDimensionsUniform.set(gl, viewportHalvedDimensions)
        pointRadiusSquaredUniform.set(gl, pointRadiusSquared)
    }

    /**
     * Disables the program.
     */
    fun disableProgram(gl: GL2) {
        gl.glDisableVertexAttribArray(absolutePointPositionAttribute)
        gl.glUseProgram(0)
    }
}