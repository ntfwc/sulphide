package ntfwc.mod.multiplayer.radar

import com.jogamp.opengl.GL2
import graphics.BlendMode
import graphics.GFX
import graphics.Tex
import ntfwc.mod.multiplayer.gl.FloatListView
import ntfwc.mod.multiplayer.gl.VertexFloatArrayBuffer
import ntfwc.mod.multiplayer.model.PlayerPosition
import ntfwc.mod.multiplayer.model.RadarColorConfiguration
import ntfwc.mod.multiplayer.radar.gl.RadarPointShaderProgram
import ntfwc.mod.multiplayer.util.vector.Vector2f
import ntfwc.mod.multiplayer.util.vector.Vector3f
import java.io.File

private const val IMAGE_SIZE = 256f
private const val BASE_SCREEN_HEIGHT = 360f

/**
 * Renderer for the player radar. Note that the actual radar face has some padding around it in the image, so it won't
 * line up exactly with the given position and size.
 */
class PlayerRadarRenderer private constructor(private val size: Float,
                                              private val pointSize: Float,
                                              private val distanceScale: Float,
                                              private val heightDifferenceForMaxColorChange: Float,
                                              private val radarFaceTexture: Tex) {
    companion object {
        private var radarFaceTexture: Tex? = null

        /**
         * Factory method for the renderer.
         *
         * @param size The value to use for the width and height of the radar. Note: When the window height changes, this will be scaled.
         * @param pointSize The size to use for each point.
         * @param distanceScale The value to scale distances by. This will be multiplied by all distances. Distances that end up
         * with a value of 1f or greater will end up putting the point at the edge of the radar.
         * @param heightDifferenceForMaxColorChange The height difference, up or down, at which points will fully change to one of the indicator colors.
         */
        fun createInstance(size: Float, pointSize: Float, distanceScale: Float, heightDifferenceForMaxColorChange: Float): PlayerRadarRenderer {
            return PlayerRadarRenderer(size, pointSize, distanceScale, heightDifferenceForMaxColorChange, getOrLoadRadarFaceTexture())
        }

        private fun getOrLoadRadarFaceTexture(): Tex {
            var radarFaceTexture = this.radarFaceTexture
            if (radarFaceTexture == null) {
                val textureMap = HashMap<String, Tex>()
                radarFaceTexture = GFX.loadTexture(File("mod/sulphide/res/radar_face_pre_mag.png"), textureMap)!!
                this.radarFaceTexture = radarFaceTexture
            }
            return radarFaceTexture
        }
    }

    private val absolutePointPositionList = ArrayList<PlayerPosition>(10)
    private val absolutePointPositionBuffer = VertexFloatArrayBuffer()


    /**
     * Renders the radar face.
     *
     * @param x The x position of the upper left hand corner to show the radar at.
     * @param y The y position of the upper left hand corner to show the radar at.
     * @param gl The OpenGL context
     * @param sceneHeight The height of the scene
     * @param rotationDegrees The rotation to apply to the radar, in degrees
     * @param centerPosition The position that represents the center of the radar
     * @param absolutePointPositions The positions to render points for. These will be rendered relative to the center
     * @param colorConfiguration The color configuration to use.
     * position on the radar.
     */
    fun render(gl: GL2,
               x: Float,
               y: Float,
               sceneHeight: Int,
               rotationDegrees: Float,
               centerPosition: PlayerPosition,
               absolutePointPositions: Iterable<PlayerPosition>,
               colorConfiguration: RadarColorConfiguration) {
        BlendMode.NORMAL.set()

        gl.glColor4fv(GFX.WHITE, 0)
        gl.glPushMatrix()

        val scaledSize = getScaledSize(sceneHeight)
        val positionAdjustment: Float = scaledSize / 2
        gl.glTranslatef(x + positionAdjustment, y + positionAdjustment, 0f)
        gl.glRotatef(rotationDegrees, 0f, 0f, 1f)

        drawFace(scaledSize)
        drawRadarPoints(gl, scaledSize, centerPosition, absolutePointPositions, colorConfiguration)

        gl.glPopMatrix()
    }

    private fun drawFace(scaledSize: Float) {
        val imageScale = scaledSize / IMAGE_SIZE
        GFX.drawTexture(radarFaceTexture, 0, 0f, 0f, 0f, GFX.CENTER, imageScale, imageScale, 0f)
    }

    private fun drawRadarPoints(gl: GL2,
                                scaledSize: Float,
                                centerPosition: PlayerPosition,
                                absolutePointPositions: Iterable<PlayerPosition>,
                                colorConfiguration: RadarColorConfiguration) {
        val absolutePointPositionList = collectAbsolutePointPositions(absolutePointPositions)
        if (absolutePointPositionList.isEmpty())
            return

        val viewportDimensions = getViewportDimensions(gl)
        val screenPointSize = calculateScreenPointSize(viewportDimensions)
        gl.glPointSize(screenPointSize)

        val shaderProgram = RadarPointShaderProgram.getOrInitialize(gl)
        absolutePointPositionBuffer.setData(object : FloatListView {
            override fun getSize(): Int = absolutePointPositionList.size * 3

            override fun get(index: Int): Float {
                val positionIndex = index / 3
                val position = absolutePointPositionList[positionIndex]

                return when (index % 3) {
                    0 -> position.x.toFloat()
                    1 -> position.y.toFloat()
                    else -> position.z.toFloat()
                }
            }
        })

        val centerPosition3f = Vector3f(centerPosition.x.toFloat(), centerPosition.y.toFloat(), centerPosition.z.toFloat())
        val radarRadius = scaledSize * 0.9f / 2f
        val pointRadius = screenPointSize / 2f
        shaderProgram.enableAndSetupProgram(gl,
                absolutePointPositionBuffer,
                centerPosition3f,
                distanceScale,
                radarRadius,
                heightDifferenceForMaxColorChange,
                colorConfiguration.aboveColor.color,
                colorConfiguration.belowColor.color,
                viewportDimensions.scale(0.5f),
                pointRadius * pointRadius)

        gl.glDrawArrays(GL2.GL_POINTS, 0, absolutePointPositionList.size)

        shaderProgram.disableProgram(gl)
    }

    fun getScaledSize(sceneHeight: Int): Float = size * sceneHeight / BASE_SCREEN_HEIGHT

    private fun calculateScreenPointSize(viewportDimensions: Vector2f): Float {
        val viewportHeight = viewportDimensions.y
        val heightRatio: Float = viewportHeight / BASE_SCREEN_HEIGHT
        return heightRatio * pointSize
    }

    private fun getViewportDimensions(gl: GL2): Vector2f {
        val array = IntArray(4)
        gl.glGetIntegerv(GL2.GL_VIEWPORT, array, 0)
        return Vector2f(array[2].toFloat(), array[3].toFloat())
    }

    private fun collectAbsolutePointPositions(absolutePointPositions: Iterable<PlayerPosition>): List<PlayerPosition> {
        absolutePointPositionList.clear()
        absolutePointPositionList.addAll(absolutePointPositions)
        return absolutePointPositionList
    }
}
