package ntfwc.mod.multiplayer.radar

import ntfwc.mod.multiplayer.util.Color3f
import ntfwc.mod.multiplayer.util.ParsingUtil

/**
 * Radar point colors used to indicate there is a height difference between you and another player.
 */
enum class RadarHeightDifferenceColor(val color: Color3f) {
    Blue(Color3f(0.078f, 0.117f, 0.937f)),
    Yellow(Color3f(0.921f, 0.902f, 0.117f)),
    Green(Color3f(0.176f, 0.812f, 0.176f)),
    Red(Color3f(0.816f, 0.180f, 0.180f)),
    Cyan(Color3f(0.254f, 0.742f, 0.719f)),
    Orange(Color3f(0.934f, 0.477f, 0.090f)),
    Purple(Color3f(0.488f, 0.094f, 0.938f));

    /**
     * Gets an ID string for this color.
     */
    fun getColorId(): String {
        return name.toLowerCase()
    }

    companion object {
        /**
         * Tries to get a color from the given ID string.
         */
        fun fromColorId(id: String): RadarHeightDifferenceColor? {
            return ParsingUtil.safeCaseInsensitiveEnumValueOf<RadarHeightDifferenceColor>(id)
        }
    }
}