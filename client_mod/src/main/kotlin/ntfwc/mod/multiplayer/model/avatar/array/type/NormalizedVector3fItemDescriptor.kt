package ntfwc.mod.multiplayer.model.avatar.array.type

import com.beust.klaxon.JsonObject
import ntfwc.mod.multiplayer.util.FromJsonResult
import ntfwc.mod.multiplayer.util.vector.NormalizedVector3f
import ntfwc.mod.multiplayer.util.vector.Vector3f

/**
 * Descriptor for a nullable normalized vector item.
 */
class NormalizedVector3fItemDescriptor(jsonName: String) : BaseArrayItemDescriptor<NormalizedVector3f?>(jsonName) {
    override fun interpolate(value1: NormalizedVector3f?, value2: NormalizedVector3f?, factor: Double): NormalizedVector3f? {
        if (value1 == null || value2 == null)
            return value1

        if (value1 == value2)
            return value1

        return value1.nlerp(value2, factor.toFloat())
    }

    override fun toJsonValue(value: NormalizedVector3f?): Any? {
        value ?: return null

        val jsonObject = JsonObject()
        value.inner.addToJson(jsonObject)
        return jsonObject
    }

    override fun fromJsonTyped(jsonObject: JsonObject): FromJsonResult<out NormalizedVector3f?> {
        val valueObject = when (val value = jsonObject[this.jsonName]) {
            is JsonObject -> value
            null -> return FromJsonResult.Success(null)
            else -> return FromJsonResult.Failure()
        }

        val vector3f = Vector3f.fromJson(valueObject) ?: return FromJsonResult.Failure()
        return FromJsonResult.Success(NormalizedVector3f.from(vector3f))
    }
}