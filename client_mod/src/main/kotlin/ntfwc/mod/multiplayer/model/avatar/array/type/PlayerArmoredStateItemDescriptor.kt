package ntfwc.mod.multiplayer.model.avatar.array.type

import com.beust.klaxon.JsonObject
import ntfwc.mod.multiplayer.model.avatar.PlayerArmoredState
import ntfwc.mod.multiplayer.util.FromJsonResult
import ntfwc.mod.multiplayer.util.ParsingUtil

/**
 * Descriptor for a player armored state item.
 */
class PlayerArmoredStateItemDescriptor(jsonName: String) : BaseArrayItemDescriptor<PlayerArmoredState>(jsonName) {
    override fun interpolate(value1: PlayerArmoredState, value2: PlayerArmoredState, factor: Double): PlayerArmoredState {
        return value1
    }

    override fun toJsonValue(value: PlayerArmoredState): Any = value.toName()

    override fun fromJsonTyped(jsonObject: JsonObject): FromJsonResult<out PlayerArmoredState> {
        val stateString = ParsingUtil.getString(jsonObject, jsonName) ?: return FromJsonResult.Failure()
        val armoredState = PlayerArmoredState.fromName(stateString) ?: PlayerArmoredState.HOOVES
        return FromJsonResult.Success(armoredState)
    }
}