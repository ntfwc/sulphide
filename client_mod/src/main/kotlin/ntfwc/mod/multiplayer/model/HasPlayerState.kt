package ntfwc.mod.multiplayer.model

import ntfwc.mod.multiplayer.model.avatar.array.PlayerArrayState

/**
 * Interface for data types that have a player state
 */
interface HasPlayerState {
    fun getState(): PlayerArrayState
}