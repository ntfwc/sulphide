package ntfwc.mod.multiplayer.model.avatar

import game.Pony
import ntfwc.mod.multiplayer.util.UnitIntervalFloat
import ntfwc.mod.multiplayer.util.ValidatedColor3f
import ntfwc.mod.multiplayer.util.quaternion.NormalizedQuaternion

/**
 * State of a player appearance. This representation is meant for
 * network transmission. It should try to avoid implementation
 * details for the current game version and enforce data validity.
 */
data class NetPlayerAppearanceState(
        val disguiseName: String,
        val rotation: NormalizedQuaternion,
        val animationState: NetPlayerAnimationState,
        val currentMood: Pony.Mood?,
        val nextMoodMultiplier: UnitIntervalFloat,
        val nextMood: Pony.Mood?,
        val armoredState: PlayerArmoredState,
        val melindaIsRiding: Boolean,
        val hasInfiniteFlap: Boolean,
        val druggedness: UnitIntervalFloat,
        val bloody: UnitIntervalFloat,
        val glowyTimer: UnitIntervalFloat,
        val glowyColor: ValidatedColor3f
)