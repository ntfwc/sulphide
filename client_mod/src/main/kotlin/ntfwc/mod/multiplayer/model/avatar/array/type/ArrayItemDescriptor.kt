package ntfwc.mod.multiplayer.model.avatar.array.type

import com.beust.klaxon.JsonObject
import ntfwc.mod.multiplayer.util.FromJsonResult

/**
 * Interface for an object that describes how to deal with a player state array item type.
 *
 * @param T The type of the array item.
 */
interface ArrayItemDescriptor<T> {
    /**
     * Interpolates between value1 and value2. The factor should be from 0 to 1. The closer the factor is to 0, the
     * closer the result will be to value1. The closer the factor is to 1, the closer the result will be to value2. The
     * value types must match the type expected by the implementation.
     */
    fun interpolateValue(value1: Any?, value2: Any?, factor: Double): Any?

    fun addToJson(value: Any?, jsonObject: JsonObject)

    fun fromJson(jsonObject: JsonObject): FromJsonResult<out Any?>
}