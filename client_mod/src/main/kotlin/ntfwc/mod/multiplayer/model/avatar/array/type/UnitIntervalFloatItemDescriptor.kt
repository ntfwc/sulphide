package ntfwc.mod.multiplayer.model.avatar.array.type

import com.beust.klaxon.JsonObject
import ntfwc.mod.multiplayer.util.FromJsonResult
import ntfwc.mod.multiplayer.util.UnitIntervalFloat

/**
 * Descriptor for a unit interval float item.
 */
class UnitIntervalFloatItemDescriptor(jsonName: String) : BaseArrayItemDescriptor<UnitIntervalFloat>(jsonName) {
    override fun interpolate(value1: UnitIntervalFloat, value2: UnitIntervalFloat, factor: Double): UnitIntervalFloat {
        if (value1 == value2)
            return value1

        return value1.lerp(value2, factor.toFloat())
    }

    override fun toJsonValue(value: UnitIntervalFloat): Any = value.value

    override fun fromJsonTyped(jsonObject: JsonObject): FromJsonResult<out UnitIntervalFloat> {
        return UnitIntervalFloat.fromJson(jsonObject, this.jsonName)?.let {
            FromJsonResult.Success(it)
        } ?: FromJsonResult.Failure()
    }
}