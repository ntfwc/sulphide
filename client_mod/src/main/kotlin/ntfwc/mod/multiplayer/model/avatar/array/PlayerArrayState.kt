package ntfwc.mod.multiplayer.model.avatar.array

import com.beust.klaxon.JsonObject
import ntfwc.mod.multiplayer.model.PlayerPosition
import ntfwc.mod.multiplayer.util.ParsingUtil

private const val POSITION_FIELD_NAME = "position"
private const val APPEARANCE_STATE_FIELD_NAME = "appearanceState"

/**
 * A player state with the, optional, main appearance data represented as an array.
 */
data class PlayerArrayState(val position: PlayerPosition, val appearanceState: NetPlayerAppearanceArrayState?) {
    companion object {
        fun fromJson(jsonObject: JsonObject): PlayerArrayState? {
            val position = getPosition(jsonObject) ?: return null
            val appearanceState = ParsingUtil.getObject(jsonObject, APPEARANCE_STATE_FIELD_NAME)?.let {
                NetPlayerAppearanceArrayState.fromJsonObject(it) ?: return null
            }
            return PlayerArrayState(position, appearanceState)
        }

        private fun getPosition(jsonObject: JsonObject): PlayerPosition? {
            val obj = ParsingUtil.getObject(jsonObject, POSITION_FIELD_NAME) ?: return null
            return PlayerPosition.fromJson(obj)
        }
    }

    /**
     * Interpolates between this state and the given state. The closer the factor is to 0, the closer
     * the result will be to this state. The closer the factor is to 1, the closer the result will be to the other state.
     */
    fun interpolate(other: PlayerArrayState, factor: Double): PlayerArrayState {
        val newAppearanceState = run {
            val appearanceState = appearanceState
            val otherAppearanceState = other.appearanceState
            if (appearanceState != null && otherAppearanceState != null)
                appearanceState.interpolate(otherAppearanceState, factor)
            else
                null
        }

        return PlayerArrayState(
                this.position.lerp(other.position, factor),
                newAppearanceState
        )
    }

    fun addToJson(jsonObject: JsonObject) {
        jsonObject[POSITION_FIELD_NAME] = positionToJsonObject()

        val appearanceState = this.appearanceState
        if (appearanceState != null)
            jsonObject[APPEARANCE_STATE_FIELD_NAME] = appearanceState.toJsonObject()
    }

    private fun positionToJsonObject(): JsonObject {
        val positionObject = JsonObject()
        this.position.addToJson(positionObject)
        return positionObject
    }
}
