package ntfwc.mod.multiplayer.model

/**
 * The name of a map.
 */
data class MapName(val value: String)
