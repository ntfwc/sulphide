package ntfwc.mod.multiplayer.model.avatar

import ntfwc.mod.multiplayer.util.RadianAngle
import ntfwc.mod.multiplayer.util.UnitIntervalFloat
import ntfwc.mod.multiplayer.util.vector.NormalizedVector3f

/**
 * An animation state for a player. This representation is meant for
 * network transmission. It should try to avoid implementation
 * details for the current game version and enforce data validity.
 */
data class NetPlayerAnimationState(val animationId: AnimationId,
                                   val frame: UnitIntervalFloat,
                                   val wingFrame: UnitIntervalFloat,
                                   val wingExtend: UnitIntervalFloat,
                                   val wingInterp: UnitIntervalFloat,
                                   val mirror: Boolean,
                                   val rollAxis: NormalizedVector3f?,
                                   val rollAmount: RadianAngle,
                                   val dashTimer: UnitIntervalFloat,
                                   val sideSkidFader: UnitIntervalFloat,
                                   val flapping: Boolean,
                                   val pitch: UnitIntervalFloat,
                                   val roll: UnitIntervalFloat,
                                   val yaw: UnitIntervalFloat,
                                   val forwardValue: UnitIntervalFloat)
