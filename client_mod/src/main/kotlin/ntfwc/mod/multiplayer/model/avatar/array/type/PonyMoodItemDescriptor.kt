package ntfwc.mod.multiplayer.model.avatar.array.type

import com.beust.klaxon.JsonObject
import game.Pony
import ntfwc.mod.multiplayer.util.FromJsonResult
import ntfwc.mod.multiplayer.util.ParsingUtil

/**
 * Descriptor for a pony mood item.
 */
class PonyMoodItemDescriptor(jsonName: String) : BaseArrayItemDescriptor<Pony.Mood?>(jsonName) {
    override fun interpolate(value1: Pony.Mood?, value2: Pony.Mood?, factor: Double): Pony.Mood? {
        return value1
    }

    override fun toJsonValue(value: Pony.Mood?): Any? {
        value ?: return null

        return value.name.toLowerCase()
    }

    override fun fromJsonTyped(jsonObject: JsonObject): FromJsonResult<out Pony.Mood?> {
        val valueString = when(val value = jsonObject[this.jsonName]) {
            is String -> value
            null -> return FromJsonResult.Success(null)
            else -> return FromJsonResult.Failure()
        }

        return ParsingUtil.safeCaseInsensitiveEnumValueOf<Pony.Mood>(valueString)?.let {
            FromJsonResult.Success(it)
        } ?:
        // This enum could possibly change, so it is ok if we get an unexpected value
        FromJsonResult.Success(null)
    }
}