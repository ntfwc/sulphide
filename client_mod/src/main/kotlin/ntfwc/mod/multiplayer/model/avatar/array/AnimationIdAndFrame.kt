package ntfwc.mod.multiplayer.model.avatar.array

import ntfwc.mod.multiplayer.model.avatar.AnimationId
import ntfwc.mod.multiplayer.util.UnitIntervalFloat

/**
 * An animation ID with a corresponding frame value.
 */
data class AnimationIdAndFrame(val animationId: AnimationId, val frame: UnitIntervalFloat)