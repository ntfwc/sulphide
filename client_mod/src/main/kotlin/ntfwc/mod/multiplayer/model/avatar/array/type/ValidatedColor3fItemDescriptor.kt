package ntfwc.mod.multiplayer.model.avatar.array.type

import com.beust.klaxon.JsonObject
import ntfwc.mod.multiplayer.util.Color3f
import ntfwc.mod.multiplayer.util.FromJsonResult
import ntfwc.mod.multiplayer.util.ParsingUtil
import ntfwc.mod.multiplayer.util.ValidatedColor3f

/**
 * Descriptor for a color item.
 */
class ValidatedColor3fItemDescriptor(jsonName: String) : BaseArrayItemDescriptor<ValidatedColor3f>(jsonName) {
    override fun interpolate(value1: ValidatedColor3f, value2: ValidatedColor3f, factor: Double): ValidatedColor3f {
        return value1
    }

    override fun toJsonValue(value: ValidatedColor3f): Any? {
        val jsonObject = JsonObject()
        value.inner.addToJson(jsonObject)
        return jsonObject
    }

    override fun fromJsonTyped(jsonObject: JsonObject): FromJsonResult<out ValidatedColor3f> {
        return ParsingUtil.getObject(jsonObject, this.jsonName)?.let {
            Color3f.fromJson(it)
        }?.let {
            ValidatedColor3f.clamp(it)
        }?.let {
            FromJsonResult.Success(it)
        } ?: FromJsonResult.Failure()
    }
}