package ntfwc.mod.multiplayer.model.avatar.array

import com.beust.klaxon.JsonObject
import ntfwc.mod.multiplayer.model.avatar.AnimationId
import ntfwc.mod.multiplayer.model.avatar.NetPlayerAnimationState
import ntfwc.mod.multiplayer.model.avatar.array.type.*
import ntfwc.mod.multiplayer.util.FromJsonResult
import ntfwc.mod.multiplayer.util.RadianAngle
import ntfwc.mod.multiplayer.util.ReadOnlyArray
import ntfwc.mod.multiplayer.util.UnitIntervalFloat
import ntfwc.mod.multiplayer.util.vector.NormalizedVector3f

/**
 * An array representation of a player animation state.
 */
class NetPlayerAnimationArrayState private constructor(private val array: ReadOnlyArray<Any?>) {
    companion object {
        private val registeredDescriptors = ArrayList<ArrayItemDescriptor<*>>()

        private val animationIdAndFrameAccessor = registerItem(AnimationIdAndFrameItemDescriptor("frame", "animationId"))
        private val wingFrameAccessor = registerItem(UnitIntervalFloatItemDescriptor("wingFrame"))
        private val wingExtendAccessor = registerItem(UnitIntervalFloatItemDescriptor("wingExtend"))
        private val wingInterpAccessor = registerItem(UnitIntervalFloatItemDescriptor("wingInterp"))
        private val mirrorAccessor = registerItem(BooleanItemDescriptor("mirror"))
        private val rollAxisAccessor = registerItem(NormalizedVector3fItemDescriptor("rollAxis"))
        private val rollAmountAccessor = registerItem(RadianAngleItemDescriptor("rollAmount"))
        private val dashTimerAccessor = registerItem(UnitIntervalFloatItemDescriptor("dashTimer"))
        private val sideSkidFaderAccessor = registerItem(UnitIntervalFloatItemDescriptor("sideSkidFader"))
        private val flappingAccessor = registerItem(BooleanItemDescriptor("flapping"))
        private val pitchAccessor = registerItem(UnitIntervalFloatItemDescriptor("pitch"))
        private val rollAccessor = registerItem(UnitIntervalFloatItemDescriptor("roll"))
        private val yawAccessor = registerItem(UnitIntervalFloatItemDescriptor("yaw"))
        private val forwardValueAccessor = registerItem(UnitIntervalFloatItemDescriptor("forwardValue"))

        fun from(state: NetPlayerAnimationState): NetPlayerAnimationArrayState {
            return NetPlayerAnimationArrayState(createArray(
                    state.animationId,
                    state.frame,
                    state.wingFrame,
                    state.wingExtend,
                    state.wingInterp,
                    state.mirror,
                    state.rollAxis,
                    state.rollAmount,
                    state.dashTimer,
                    state.sideSkidFader,
                    state.flapping,
                    state.pitch,
                    state.roll,
                    state.yaw,
                    state.forwardValue
            ))
        }

        private fun createArray(animationId: AnimationId,
                                frame: UnitIntervalFloat,
                                wingFrame: UnitIntervalFloat,
                                wingExtend: UnitIntervalFloat,
                                wingInterp: UnitIntervalFloat,
                                mirror: Boolean,
                                rollAxis: NormalizedVector3f?,
                                rollAmount: RadianAngle,
                                dashTimer: UnitIntervalFloat,
                                sideSkidFader: UnitIntervalFloat,
                                flapping: Boolean,
                                pitch: UnitIntervalFloat,
                                roll: UnitIntervalFloat,
                                yaw: UnitIntervalFloat,
                                forwardValue: UnitIntervalFloat): ReadOnlyArray<Any?> {
            val array = arrayOfNulls<Any?>(registeredDescriptors.size)
            animationIdAndFrameAccessor.set(array, AnimationIdAndFrame(animationId, frame))
            wingFrameAccessor.set(array, wingFrame)
            wingExtendAccessor.set(array, wingExtend)
            wingInterpAccessor.set(array, wingInterp)
            mirrorAccessor.set(array, mirror)
            rollAxisAccessor.set(array, rollAxis)
            rollAmountAccessor.set(array, rollAmount)
            dashTimerAccessor.set(array, dashTimer)
            sideSkidFaderAccessor.set(array, sideSkidFader)
            flappingAccessor.set(array, flapping)
            pitchAccessor.set(array, pitch)
            rollAccessor.set(array, roll)
            yawAccessor.set(array, yaw)
            forwardValueAccessor.set(array, forwardValue)
            return ReadOnlyArray(array)
        }

        private fun <T, D : ArrayItemDescriptor<T>> registerItem(descriptor: D): ArrayItemAccessor<T> {
            val index = registeredDescriptors.size
            registeredDescriptors.add(descriptor)
            return ArrayItemAccessor(index)
        }

        fun fromJsonObject(jsonObject: JsonObject): NetPlayerAnimationArrayState? {
            val array = arrayOfNulls<Any?>(registeredDescriptors.size)
            for (i in registeredDescriptors.indices) {
                array[i] = when (val result = registeredDescriptors[i].fromJson(jsonObject)) {
                    is FromJsonResult.Success<*> -> result.value
                    else -> return null
                }
            }
            return NetPlayerAnimationArrayState(ReadOnlyArray(array))
        }
    }

    fun toState(): NetPlayerAnimationState {
        val animationIdAndFrame = animationIdAndFrameAccessor.get(this.array)
        return NetPlayerAnimationState(
                animationIdAndFrame.animationId,
                animationIdAndFrame.frame,
                wingFrameAccessor.get(array),
                wingExtendAccessor.get(array),
                wingInterpAccessor.get(array),
                mirrorAccessor.get(array),
                rollAxisAccessor.get(array),
                rollAmountAccessor.get(array),
                dashTimerAccessor.get(array),
                sideSkidFaderAccessor.get(array),
                flappingAccessor.get(array),
                pitchAccessor.get(array),
                rollAccessor.get(array),
                yawAccessor.get(array),
                forwardValueAccessor.get(array)
        )
    }

    /**
     * Interpolates between this state and the given state. The closer the factor is to 0, the closer
     * the result will be to this state. The closer the factor is to 1, the closer the result will be to the other state.
     */
    fun interpolate(other: NetPlayerAnimationArrayState, factor: Double): NetPlayerAnimationArrayState {
        val resultArray = arrayOfNulls<Any?>(registeredDescriptors.size)
        for (i in registeredDescriptors.indices) {
            val descriptor = registeredDescriptors[i]
            val value1 = this.array[i]
            val value2 = other.array[i]
            resultArray[i] = descriptor.interpolateValue(value1, value2, factor)
        }
        return NetPlayerAnimationArrayState(ReadOnlyArray(resultArray))
    }

    fun toJsonObject(): JsonObject {
        val jsonObject = JsonObject()
        for (i in registeredDescriptors.indices) {
            registeredDescriptors[i].addToJson(this.array[i], jsonObject)
        }
        return jsonObject
    }

    override fun toString(): String {
        return "NetPlayerAnimationArrayState(array=$array)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as NetPlayerAnimationArrayState

        if (array != other.array) return false

        return true
    }

    override fun hashCode(): Int {
        return array.hashCode()
    }
}