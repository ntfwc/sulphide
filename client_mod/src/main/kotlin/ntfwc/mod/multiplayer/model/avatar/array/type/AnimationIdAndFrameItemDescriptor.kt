package ntfwc.mod.multiplayer.model.avatar.array.type

import com.beust.klaxon.JsonObject
import ntfwc.mod.multiplayer.model.avatar.AnimationId
import ntfwc.mod.multiplayer.model.avatar.array.AnimationIdAndFrame
import ntfwc.mod.multiplayer.util.FromJsonResult
import ntfwc.mod.multiplayer.util.ParsingUtil
import ntfwc.mod.multiplayer.util.UnitIntervalFloat

/**
 * Descriptor for an animation ID and frame item.
 */
class AnimationIdAndFrameItemDescriptor(private val frameJsonName: String, private val animationIdJsonName: String) : ArrayItemDescriptor<AnimationIdAndFrame> {
    override fun interpolateValue(value1: Any?, value2: Any?, factor: Double): Any {
        return interpolate(value1 as AnimationIdAndFrame, value2 as AnimationIdAndFrame, factor)
    }

    private fun interpolate(value1: AnimationIdAndFrame, value2: AnimationIdAndFrame, factor: Double): AnimationIdAndFrame {
        if (value1 == value2)
            return value1

        val id1 = value1.animationId
        val id2 = value2.animationId
        if (id1 != id2)
            return value1

        val frame = interpolateAnimationFrame(id1, value1.frame, value2.frame, factor.toFloat())
        return AnimationIdAndFrame(id1, frame)
    }

    private fun interpolateAnimationFrame(animationId: AnimationId,
                                          frameValue1: UnitIntervalFloat,
                                          frameValue2: UnitIntervalFloat,
                                          factor: Float): UnitIntervalFloat {
        return when (animationId) {
            AnimationId.NORMAL_TROT -> frameValue1.lerpWrapped(frameValue2, factor)
            else -> frameValue1
        }
    }

    override fun addToJson(value: Any?, jsonObject: JsonObject) {
        value as AnimationIdAndFrame
        jsonObject[frameJsonName] = value.frame.value
        jsonObject[animationIdJsonName] = value.animationId.toName()
    }

    override fun fromJson(jsonObject: JsonObject): FromJsonResult<out Any?> {
        val frameValue = UnitIntervalFloat.fromJson(jsonObject, frameJsonName) ?: return FromJsonResult.Failure()
        val animationIdString = ParsingUtil.getString(jsonObject, animationIdJsonName)
                ?: return FromJsonResult.Failure()
        val animationId = AnimationId.fromName(animationIdString) ?: AnimationId.IDLE
        return FromJsonResult.Success(AnimationIdAndFrame(animationId, frameValue))
    }
}