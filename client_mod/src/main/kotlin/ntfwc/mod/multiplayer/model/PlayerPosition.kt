package ntfwc.mod.multiplayer.model

import com.beust.klaxon.JsonObject
import ntfwc.mod.multiplayer.util.ParsingUtil
import util.V3D
import kotlin.math.sqrt

data class PlayerPosition(val x: Double, val y: Double, val z: Double) {
    companion object {
        /**
         * Gets a player position with the same values as the given V3D.
         */
        fun from(v3d: V3D): PlayerPosition = PlayerPosition(v3d.x(), v3d.y(), v3d.z())

        fun fromJson(jsonObject: JsonObject): PlayerPosition? {
            val x = ParsingUtil.getFiniteDouble(jsonObject, "x") ?: return null
            val y = ParsingUtil.getFiniteDouble(jsonObject, "y") ?: return null
            val z = ParsingUtil.getFiniteDouble(jsonObject, "z") ?: return null
            return PlayerPosition(x, y, z)
        }
    }

    fun addToJson(jsonObject: JsonObject) {
        jsonObject["x"] = x
        jsonObject["y"] = y
        jsonObject["z"] = z
    }

    /**
     * Sets the values of the given V3D to match
     * this position.
     */
    fun applyTo(v3d: V3D) {
        v3d.x(this.x)
        v3d.y(this.y)
        v3d.z(this.z)
    }

    fun toV3D(): V3D = V3D(x, y, z)

    fun addZ(zOffset: Double): PlayerPosition = PlayerPosition(x, y, z + zOffset)

    /**
     * Interpolates between this value and the given value in a linear fashion. The closer the factor is to 0, the closer
     * the result will be to this value. The closer the factor is to 1, the closer the result will be to the other value.
     */
    fun lerp(other: PlayerPosition, factor: Double): PlayerPosition {
        return PlayerPosition(
                lerp(this.x, other.x, factor),
                lerp(this.y, other.y, factor),
                lerp(this.z, other.z, factor)
        )
    }

    private fun lerp(value1: Double, value2: Double, factor: Double): Double = value1 * (1.0 - factor) + value2 * factor

    fun distanceTo(other: PlayerPosition): Double {
        val diffX = this.x - other.x
        val diffY = this.y - other.y
        val diffZ = this.z - other.z
        return sqrt(diffX * diffX + diffY * diffY + diffZ * diffZ)
    }
}
