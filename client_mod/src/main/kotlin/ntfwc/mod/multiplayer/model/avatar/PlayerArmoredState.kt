package ntfwc.mod.multiplayer.model.avatar

import game.Pony
import ntfwc.mod.multiplayer.util.ParsingUtil

/**
 * An avatar state of being armored.
 */
enum class PlayerArmoredState(val internalValue: Int) {
    NONE(Pony.ARMOR_NONE),
    HOOVES(Pony.ARMOR_HOOVES),
    FULL(Pony.ARMOR_FULL);

    companion object {
        fun fromInternalValue(internalValue: Int): PlayerArmoredState {
            return when (internalValue) {
                Pony.ARMOR_FULL -> FULL
                Pony.ARMOR_HOOVES -> HOOVES
                Pony.ARMOR_NONE -> NONE
                else -> NONE
            }
        }

        fun fromName(name: String): PlayerArmoredState? {
            return ParsingUtil.safeCaseInsensitiveEnumValueOf<PlayerArmoredState>(name)
        }
    }

    fun toName(): String = name.toLowerCase()
}