package ntfwc.mod.multiplayer.model.avatar.array.type

import com.beust.klaxon.JsonObject
import ntfwc.mod.multiplayer.util.FromJsonResult
import ntfwc.mod.multiplayer.util.ParsingUtil
import ntfwc.mod.multiplayer.util.quaternion.NormalizedQuaternion
import ntfwc.mod.multiplayer.util.quaternion.Quaternion

/**
 * Descriptor for a normalized quaternion item.
 */
class NormalizedQuaternionItemDescriptor(jsonName: String) : BaseArrayItemDescriptor<NormalizedQuaternion>(jsonName) {
    override fun interpolate(value1: NormalizedQuaternion, value2: NormalizedQuaternion, factor: Double): NormalizedQuaternion {
        if (value1 == value2)
            return value1

        return value1.nlerp(value2, factor)
    }

    override fun toJsonValue(value: NormalizedQuaternion): Any {
        val jsonObject = JsonObject()
        value.inner.addToJson(jsonObject)
        return jsonObject
    }

    override fun fromJsonTyped(jsonObject: JsonObject): FromJsonResult<out NormalizedQuaternion> {
        val quaternion = ParsingUtil.getObject(jsonObject, this.jsonName)?.let {
            Quaternion.fromJson(it)
        } ?: return FromJsonResult.Failure()

        return FromJsonResult.Success(NormalizedQuaternion.from(quaternion)
                ?: NormalizedQuaternion(Quaternion(1.0, 0.0, 0.0, 0.0)))
    }
}