package ntfwc.mod.multiplayer.model.avatar.array

import com.beust.klaxon.JsonObject
import ntfwc.mod.multiplayer.model.HasPlayerState
import ntfwc.mod.multiplayer.util.ParsingUtil

private const val TIMESTAMP_FIELD_NAME = "timestamp"

/**
 * A player state with a timestamp.
 */
data class TimestampedPlayerArrayState(val timestamp: Long, val arrayState: PlayerArrayState) : HasPlayerState {
    companion object {
        fun fromJson(jsonObject: JsonObject): TimestampedPlayerArrayState? {
            val timestamp = ParsingUtil.getLong(jsonObject, TIMESTAMP_FIELD_NAME) ?: return null
            val state = PlayerArrayState.fromJson(jsonObject) ?: return null
            return TimestampedPlayerArrayState(timestamp, state)
        }
    }

    fun addToJson(jsonObject: JsonObject) {
        jsonObject[TIMESTAMP_FIELD_NAME] = timestamp
        this.arrayState.addToJson(jsonObject)
    }

    override fun getState(): PlayerArrayState = this.arrayState
}