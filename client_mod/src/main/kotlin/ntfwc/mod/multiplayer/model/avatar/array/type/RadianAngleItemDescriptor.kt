package ntfwc.mod.multiplayer.model.avatar.array.type

import com.beust.klaxon.JsonObject
import ntfwc.mod.multiplayer.util.FromJsonResult
import ntfwc.mod.multiplayer.util.ParsingUtil
import ntfwc.mod.multiplayer.util.RadianAngle

/**
 * Descriptor for a radian angle item.
 */
class RadianAngleItemDescriptor(jsonName: String) : BaseArrayItemDescriptor<RadianAngle>(jsonName) {
    override fun interpolate(value1: RadianAngle, value2: RadianAngle, factor: Double): RadianAngle {
        if (value1 == value2)
            return value1

        return value1.lerp(value2, factor.toFloat())
    }

    override fun toJsonValue(value: RadianAngle): Any = value.value

    override fun fromJsonTyped(jsonObject: JsonObject): FromJsonResult<out RadianAngle> {
        return ParsingUtil.getFloat(jsonObject, this.jsonName)?.let {
            RadianAngle.from(it)
        }?.let {
            FromJsonResult.Success(it)
        } ?: FromJsonResult.Failure()
    }
}