package ntfwc.mod.multiplayer.model.avatar.array.type

import com.beust.klaxon.JsonObject
import ntfwc.mod.multiplayer.util.FromJsonResult
import ntfwc.mod.multiplayer.util.ParsingUtil

/**
 * Descriptor for a string item.
 */
class StringItemDescriptor(jsonName: String, private val maxLength: Int) : BaseArrayItemDescriptor<String>(jsonName) {
    override fun interpolate(value1: String, value2: String, factor: Double): String {
        return value1
    }

    override fun toJsonValue(value: String): Any {
        return if (value.length <= this.maxLength)
            value
        else
            value.substring(0, this.maxLength)
    }

    override fun fromJsonTyped(jsonObject: JsonObject): FromJsonResult<out String> {
        return ParsingUtil.getNonEmptyString(jsonObject, this.jsonName, this.maxLength)?.let {
            FromJsonResult.Success(it)
        } ?: FromJsonResult.Failure()
    }
}