package ntfwc.mod.multiplayer.model

import com.beust.klaxon.JsonArray
import com.beust.klaxon.JsonObject
import ntfwc.mod.multiplayer.util.ParsingUtil

/**
 * A player state list with a username.
 */
data class NamedPlayerStateList(val username: String, val playerStateList: MessagePlayerStateList) {
    companion object {
        private const val USERNAME_FIELD_NAME = "username"
        private const val STATE_LIST_FIELD_NAME = "state_list"

        fun fromJson(jsonObject: Any?): NamedPlayerStateList? {
            if (jsonObject !is JsonObject) {
                return null
            }

            val username = ParsingUtil.getUsername(jsonObject, USERNAME_FIELD_NAME) ?: return null
            val stateListArray = jsonObject[STATE_LIST_FIELD_NAME]
            if (stateListArray !is JsonArray<*>)
                return null

            val playerStateList = MessagePlayerStateList.fromJson(stateListArray) ?: return null
            return NamedPlayerStateList(username, playerStateList)
        }
    }
}