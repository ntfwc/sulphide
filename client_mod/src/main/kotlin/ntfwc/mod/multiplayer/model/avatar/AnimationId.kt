package ntfwc.mod.multiplayer.model.avatar

import game.Pony
import ntfwc.mod.multiplayer.util.ParsingUtil

/**
 * IDs for the possible animation states of a player avatar.
 *
 * @param avatarState The corresponding avatar state.
 */
enum class AnimationId(val avatarState: Int) {
    TRIPLET_1(Pony.TRIPLET_1),
    TRIPLET_2(Pony.TRIPLET_2),
    TRIPLET_3(Pony.TRIPLET_3),
    TURNKICK(Pony.TURNKICK),
    FOLLOWUP_1(Pony.FOLLOWUP_1),
    FOLLOWUP_2(Pony.FOLLOWUP_2),
    FOLLOWUP_3(Pony.FOLLOWUP_3),
    LAUNCH(Pony.LAUNCH),
    SWIM(Pony.SWIM),
    JUMP(Pony.JUMP),
    FLY(Pony.FLY),
    START_TROT(Pony.TROT),
    NORMAL_TROT(Pony.TROT),
    RUN_TROT(Pony.TROT),
    ALT_RUN_TROT(Pony.TROT),
    SKID_TROT(Pony.TROT),
    ALT_SKID_TROT(Pony.TROT),
    RUN(Pony.RUN),
    TURN45(Pony.TURN45),
    TURN105(Pony.TURN105),
    TURN180(Pony.TURN180),
    FASTTURN(Pony.FASTTURN),
    FASTTURN105(Pony.FASTTURN105),
    TURNTROT12_5(Pony.TURNTROT12_5),
    TURNTROT25(Pony.TURNTROT25),
    TURNTROT45(Pony.TURNTROT45),
    TURNTROT75(Pony.TURNTROT75),
    IDLE(Pony.IDLE),
    DEAD(Pony.DEAD),
    FLINCH(Pony.FLINCH),
    SKIDSTAND(Pony.SKIDSTAND),
    SKID(Pony.SKID),
    ROLL(Pony.ROLL),
    EXTERNAL(Pony.EXTERNAL);

    fun toName(): String = this.name.toLowerCase()

    companion object {
        fun fromName(name: String): AnimationId? {
            return ParsingUtil.safeCaseInsensitiveEnumValueOf<AnimationId>(name)
        }
    }

}
