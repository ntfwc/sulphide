package ntfwc.mod.multiplayer.model.avatar.array

import com.beust.klaxon.JsonObject
import game.Pony
import ntfwc.mod.multiplayer.model.avatar.NetPlayerAppearanceState
import ntfwc.mod.multiplayer.model.avatar.PlayerArmoredState
import ntfwc.mod.multiplayer.model.avatar.array.type.*
import ntfwc.mod.multiplayer.util.*
import ntfwc.mod.multiplayer.util.quaternion.NormalizedQuaternion

/**
 * An array representation of a player appearance state.
 */
class NetPlayerAppearanceArrayState private constructor(private val array: ReadOnlyArray<Any?>, private val animationArrayState: NetPlayerAnimationArrayState) {
    companion object {
        private const val MAX_DISGUISE_NAME_LENGTH = 30
        private const val ANIMATION_STATE_FIELD_NAME = "animationState"

        private val registeredDescriptors = ArrayList<ArrayItemDescriptor<*>>()

        private val disguiseNameAccessor = registerItem(StringItemDescriptor("disguise", MAX_DISGUISE_NAME_LENGTH))
        private val rotationAccessor = registerItem(NormalizedQuaternionItemDescriptor("rotation"))
        private val currentMoodAccessor = registerItem(PonyMoodItemDescriptor("currentMood"))
        private val nextMoodMultiplierAccessor = registerItem(UnitIntervalFloatItemDescriptor("nextMoodMultiplier"))
        private val nextMoodAccessor = registerItem(PonyMoodItemDescriptor("nextMood"))
        private val armoredStateAccessor = registerItem(PlayerArmoredStateItemDescriptor("armoredState"))
        private val melindaIsRidingAccessor = registerItem(BooleanItemDescriptor("melindaIsRiding"))
        private val hasInfiniteFlapAccessor = registerItem(BooleanItemDescriptor("hasInfiniteFlap"))
        private val druggednessAccessor = registerItem(UnitIntervalFloatItemDescriptor("druggedness"))
        private val bloodyAccessor = registerItem(UnitIntervalFloatItemDescriptor("bloody"))
        private val glowyTimerAccessor = registerItem(UnitIntervalFloatItemDescriptor("glowyTimer"))
        private val glowyColorAccessor = registerItem(ValidatedColor3fItemDescriptor("glowyColor"))

        fun from(state: NetPlayerAppearanceState): NetPlayerAppearanceArrayState {
            val array = createArray(
                    state.disguiseName,
                    state.rotation,
                    state.currentMood,
                    state.nextMoodMultiplier,
                    state.nextMood,
                    state.armoredState,
                    state.melindaIsRiding,
                    state.hasInfiniteFlap,
                    state.druggedness,
                    state.bloody,
                    state.glowyTimer,
                    state.glowyColor
            )
            val animationStateArray = NetPlayerAnimationArrayState.from(state.animationState)
            return NetPlayerAppearanceArrayState(array, animationStateArray)
        }

        private fun createArray(disguiseName: String,
                                rotation: NormalizedQuaternion,
                                currentMood: Pony.Mood?,
                                nextMoodMultiplier: UnitIntervalFloat,
                                nextMood: Pony.Mood?,
                                armoredState: PlayerArmoredState,
                                melindaIsRiding: Boolean,
                                hasInfiniteFlap: Boolean,
                                druggedness: UnitIntervalFloat,
                                bloody: UnitIntervalFloat,
                                glowyTimer: UnitIntervalFloat,
                                glowyColor: ValidatedColor3f
        ): ReadOnlyArray<Any?> {
            val array = arrayOfNulls<Any?>(registeredDescriptors.size)
            disguiseNameAccessor.set(array, disguiseName)
            rotationAccessor.set(array, rotation)
            currentMoodAccessor.set(array, currentMood)
            nextMoodMultiplierAccessor.set(array, nextMoodMultiplier)
            nextMoodAccessor.set(array, nextMood)
            armoredStateAccessor.set(array, armoredState)
            melindaIsRidingAccessor.set(array, melindaIsRiding)
            hasInfiniteFlapAccessor.set(array, hasInfiniteFlap)
            druggednessAccessor.set(array, druggedness)
            bloodyAccessor.set(array, bloody)
            glowyTimerAccessor.set(array, glowyTimer)
            glowyColorAccessor.set(array, glowyColor)
            return ReadOnlyArray(array)
        }

        private fun <T, D : ArrayItemDescriptor<T>> registerItem(descriptor: D): ArrayItemAccessor<T> {
            val index = registeredDescriptors.size
            registeredDescriptors.add(descriptor)
            return ArrayItemAccessor(index)
        }

        fun fromJsonObject(jsonObject: JsonObject): NetPlayerAppearanceArrayState? {
            val array = arrayOfNulls<Any?>(registeredDescriptors.size)
            for (i in registeredDescriptors.indices) {
                array[i] = when (val result = registeredDescriptors[i].fromJson(jsonObject)) {
                    is FromJsonResult.Success<*> -> result.value
                    else -> return null
                }
            }

            val animationStateObject = ParsingUtil.getObject(jsonObject, ANIMATION_STATE_FIELD_NAME) ?: return null
            val animationState = NetPlayerAnimationArrayState.fromJsonObject(animationStateObject) ?: return null

            return NetPlayerAppearanceArrayState(ReadOnlyArray(array), animationState)
        }
    }

    fun toAppearanceState(): NetPlayerAppearanceState {
        return NetPlayerAppearanceState(
                disguiseNameAccessor.get(array),
                rotationAccessor.get(array),
                animationArrayState.toState(),
                currentMoodAccessor.get(array),
                nextMoodMultiplierAccessor.get(array),
                nextMoodAccessor.get(array),
                armoredStateAccessor.get(array),
                melindaIsRidingAccessor.get(array),
                hasInfiniteFlapAccessor.get(array),
                druggednessAccessor.get(array),
                bloodyAccessor.get(array),
                glowyTimerAccessor.get(array),
                glowyColorAccessor.get(array)
        )
    }

    /**
     * Interpolates between this state and the given state. The closer the factor is to 0, the closer
     * the result will be to this state. The closer the factor is to 1, the closer the result will be to the other state.
     */
    fun interpolate(other: NetPlayerAppearanceArrayState, factor: Double): NetPlayerAppearanceArrayState {
        val resultArray = arrayOfNulls<Any?>(registeredDescriptors.size)
        for (i in registeredDescriptors.indices) {
            val descriptor = registeredDescriptors[i]
            val value1 = this.array[i]
            val value2 = other.array[i]
            resultArray[i] = descriptor.interpolateValue(value1, value2, factor)
        }

        val animationState = this.animationArrayState.interpolate(other.animationArrayState, factor)
        return NetPlayerAppearanceArrayState(ReadOnlyArray(resultArray), animationState)
    }

    fun toJsonObject(): JsonObject {
        val jsonObject = JsonObject()
        for (i in registeredDescriptors.indices) {
            registeredDescriptors[i].addToJson(this.array[i], jsonObject)
        }
        jsonObject[ANIMATION_STATE_FIELD_NAME] = this.animationArrayState.toJsonObject()
        return jsonObject
    }

    override fun toString(): String {
        return "NetPlayerAppearanceArrayState(array=$array, animationArrayState=$animationArrayState)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as NetPlayerAppearanceArrayState

        if (array != other.array) return false
        if (animationArrayState != other.animationArrayState) return false

        return true
    }

    override fun hashCode(): Int {
        var result = array.hashCode()
        result = 31 * result + animationArrayState.hashCode()
        return result
    }
}