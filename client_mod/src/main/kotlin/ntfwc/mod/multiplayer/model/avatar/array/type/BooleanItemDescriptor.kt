package ntfwc.mod.multiplayer.model.avatar.array.type

import com.beust.klaxon.JsonObject
import ntfwc.mod.multiplayer.util.FromJsonResult
import ntfwc.mod.multiplayer.util.ParsingUtil

/**
 * Descriptor for a boolean item.
 */
class BooleanItemDescriptor(jsonName: String) : BaseArrayItemDescriptor<Boolean>(jsonName) {
    override fun interpolate(value1: Boolean, value2: Boolean, factor: Double): Boolean {
        return value1
    }

    override fun toJsonValue(value: Boolean): Any = value

    override fun fromJsonTyped(jsonObject: JsonObject): FromJsonResult<out Boolean> {
        return ParsingUtil.getBool(jsonObject, this.jsonName)?.let {
            FromJsonResult.Success(it)
        } ?: FromJsonResult.Failure()
    }
}