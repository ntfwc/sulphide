package ntfwc.mod.multiplayer.model

import com.beust.klaxon.JsonArray
import com.beust.klaxon.JsonObject
import ntfwc.mod.multiplayer.model.avatar.array.TimestampedPlayerArrayState
import ntfwc.mod.multiplayer.util.NonEmptyList


/**
 * A list with 1-6 player states used in messages. If given more than 6 states, it will ignore the extra states.
 */
class MessagePlayerStateList(stateList: NonEmptyList<TimestampedPlayerArrayState>) : Iterable<TimestampedPlayerArrayState> {
    companion object {
        const val MAX_STATES = 6
        fun fromJson(jsonArray: JsonArray<*>): MessagePlayerStateList? {
            if (jsonArray.size > MAX_STATES)
                return null

            val stateList = ArrayList<TimestampedPlayerArrayState>(jsonArray.size)
            for (item in jsonArray) {
                if (item !is JsonObject)
                    return null

                val state = TimestampedPlayerArrayState.fromJson(item) ?: return null
                stateList.add(state)
            }

            val nonEmptyStateList = NonEmptyList.from(stateList) ?: return null
            return MessagePlayerStateList(nonEmptyStateList)
        }
    }

    val states: List<TimestampedPlayerArrayState>

    init {
        val states = ArrayList<TimestampedPlayerArrayState>(stateList.inner.size)

        val statesToAdd = if (stateList.inner.size <= MAX_STATES)
            stateList.inner
        else
            stateList.inner.subList(0, MAX_STATES)

        states.addAll(statesToAdd)
        this.states = states
    }

    fun toJsonArray(): JsonArray<JsonObject> {
        val objectList = ArrayList<JsonObject>(states.size)
        for (state in states) {
            val obj = JsonObject()
            state.addToJson(obj)
            objectList.add(obj)
        }
        return JsonArray(objectList)
    }

    override fun toString(): String = "MessagePlayerStateList{stateCount=${states.size}, states=$states}"

    override fun iterator(): Iterator<TimestampedPlayerArrayState> = this.states.iterator()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MessagePlayerStateList

        if (states != other.states) return false

        return true
    }

    override fun hashCode(): Int {
        return states.hashCode()
    }
}