package ntfwc.mod.multiplayer.model

import com.beust.klaxon.JsonObject
import com.beust.klaxon.json
import ntfwc.mod.multiplayer.util.ParsingUtil

private const val HOST_FIELD = "host"
private const val PORT_FIELD = "port"
private const val USERNAME_FIELD = "username"
private const val AUTO_CONNECT_FIELD = "auto_connect"
private const val ENABLE_RADAR_FIELD = "enable_radar"
private const val MAX_AVATAR_COUNT_FIELD = "max_avatar_count"

/**
 * A configuration for the mod.
 */
data class Configuration(val host: String,
                         val port: Int?,
                         val username: String,
                         val autoConnect: Boolean,
                         val enableRadar: Boolean,
                         val radarColorConfiguration: RadarColorConfiguration,
                         val maxAvatarCount: Int) {
    companion object {
        val INITIAL_CONFIGURATION = Configuration("",
                9099,
                "",
                false,
                true,
                RadarColorConfiguration.DEFAULT_CONFIGURATION,
                5)
        val DEFAULT_CONFIGURATION = Configuration("",
                null,
                "",
                false,
                true,
                RadarColorConfiguration.DEFAULT_CONFIGURATION,
                5)

        private val IPV4_REGEX = Regex("""([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})""")
        private val DOMAIN_NAME_REGEX = Regex("""[a-z0-9][a-z0-9-_]{0,62}(?<![-_])(\.[a-z0-9][a-z0-9-_]{0,62}(?<![-_]))*""")

        //IPv6 is rather complex, so we'll be pretty lenient
        private val IPV6_REGEX = Regex("""([0-9a-f]{0,4}:){2,7}[0-9a-f]{0,4}(%[a-z0-9]+|[0-9]{1,3}(\.[0-9]{1,3}){3})?""")


        /**
         * Tries to deserialize a configuration from the given JSON string. It will substitute default
         * values for missing or invalid field values.
         *
         * @param json A JSON string.
         * @return A configuration, or null if parsing failed.
         */
        fun deserialize(json: String): Configuration? {
            val parsedJson = ParsingUtil.parseJsonObject(json) ?: return null
            val host = getHost(parsedJson) ?: DEFAULT_CONFIGURATION.host
            val port = getPort(parsedJson) ?: DEFAULT_CONFIGURATION.port
            val username = ParsingUtil.getString(parsedJson, USERNAME_FIELD) ?: DEFAULT_CONFIGURATION.username
            val autoConnect = ParsingUtil.getBool(parsedJson, AUTO_CONNECT_FIELD) ?: DEFAULT_CONFIGURATION.autoConnect
            val enableRadar = ParsingUtil.getBool(parsedJson, ENABLE_RADAR_FIELD) ?: DEFAULT_CONFIGURATION.enableRadar
            val radarColorConfiguration = RadarColorConfiguration.fromJson(parsedJson)
            val maxAvatarCount = getMaxAvatarCount(parsedJson) ?: DEFAULT_CONFIGURATION.maxAvatarCount

            return Configuration(host, port, username, autoConnect, enableRadar, radarColorConfiguration, maxAvatarCount)
        }

        fun isValidPort(port: Int) = port in 1..65535

        fun isValidMaxAvatarCount(maxAvatarCount: Int) = maxAvatarCount >= 0

        /**
         * Determines if the given host domain name or IP address is not invalid. It is not super strict, but it should be good
         * enough for typing errors.
         *
         * @param host The domain name or IP address.
         * @return True if the given host is invalid, false otherwise.
         */
        fun isInvalidHost(host: CharSequence): Boolean {
            // Empty means the user wants to clear the host, this is ok
            if (host.isEmpty())
                return false

            if (host.contains(':'))
                return IPV6_REGEX.matchEntire(host) == null

            val ipv4Match = IPV4_REGEX.matchEntire(host)
            if (ipv4Match != null) {
                for (i in 1..4) {
                    val matchedValue = ipv4Match.groups[i]!!.value
                    if (matchedValue.toShort() > 255)
                        return true
                }
                return false
            }

            return DOMAIN_NAME_REGEX.matchEntire(host) == null
        }

        private fun getHost(parsedJson: JsonObject): String? {
            val host = ParsingUtil.getString(parsedJson, HOST_FIELD)
            if (host == null || isInvalidHost(host))
                return null

            return host
        }

        private fun getPort(parsedJson: JsonObject): Int? {
            val port = ParsingUtil.getInt(parsedJson, PORT_FIELD)
            if (port == null || !isValidPort(port))
                return null

            return port
        }

        private fun getMaxAvatarCount(parsedJson: JsonObject): Int? {
            val maxAvatarCount = ParsingUtil.getInt(parsedJson, MAX_AVATAR_COUNT_FIELD)
            if (maxAvatarCount == null || !isValidMaxAvatarCount(maxAvatarCount))
                return null

            return maxAvatarCount
        }
    }

    /**
     * Serializes the configuration using a JSON format.
     *
     * @return The serialized configuration.
     */
    fun serialize(): String {
        return json {
            val fields = ArrayList<Pair<String, *>>()

            if (host.isNotEmpty())
                fields.add(Pair(HOST_FIELD, host))

            if (port != null)
                fields.add(Pair(PORT_FIELD, port))

            if (username.isNotEmpty())
                fields.add(Pair(USERNAME_FIELD, username))

            fields.add(Pair(AUTO_CONNECT_FIELD, autoConnect))
            fields.add(Pair(ENABLE_RADAR_FIELD, enableRadar))

            radarColorConfiguration.toJson(fields)

            fields.add(Pair(MAX_AVATAR_COUNT_FIELD, maxAvatarCount))

            obj(fields)
        }.toJsonString(prettyPrint = true)
    }
}