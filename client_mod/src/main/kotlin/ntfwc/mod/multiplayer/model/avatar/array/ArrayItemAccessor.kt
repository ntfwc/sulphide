package ntfwc.mod.multiplayer.model.avatar.array

import ntfwc.mod.multiplayer.util.ReadOnlyArray

/**
 * An accessor for an Any type array value, which enforces a type.
 */
class ArrayItemAccessor<T>(private val index: Int) {
    @Suppress("UNCHECKED_CAST")
    fun get(array: Array<Any?>): T = array[index] as T

    @Suppress("UNCHECKED_CAST")
    fun get(array: ReadOnlyArray<Any?>): T = array[index] as T

    fun set(array: Array<Any?>, value: T) {
        array[index] = value
    }
}