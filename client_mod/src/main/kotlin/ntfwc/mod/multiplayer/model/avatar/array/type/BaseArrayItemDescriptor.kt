package ntfwc.mod.multiplayer.model.avatar.array.type

import com.beust.klaxon.JsonObject
import ntfwc.mod.multiplayer.util.FromJsonResult

/**
 * Base class for an object that describes how to deal with a player state array item type.
 */
abstract class BaseArrayItemDescriptor<T>(protected val jsonName: String) : ArrayItemDescriptor<T> {
    @Suppress("UNCHECKED_CAST")
    override fun interpolateValue(value1: Any?, value2: Any?, factor: Double): Any? {
        return interpolate(value1 as T, value2 as T, factor)
    }

    abstract fun interpolate(value1: T, value2: T, factor: Double): T

    @Suppress("UNCHECKED_CAST")
    override fun addToJson(value: Any?, jsonObject: JsonObject) {
        val jsonValue = toJsonValue(value as T)
        if (jsonValue != null)
            jsonObject[this.jsonName] = jsonValue
    }

    abstract fun toJsonValue(value: T): Any?

    override fun fromJson(jsonObject: JsonObject): FromJsonResult<out Any?> {
        return fromJsonTyped(jsonObject)
    }

    abstract fun fromJsonTyped(jsonObject: JsonObject): FromJsonResult<out T>
}