package ntfwc.mod.multiplayer.model

import java.util.*

/**
 * A connection status, which can be shown to the user.
 */
data class ConnectionStatus(val text: String,
                            /**
                             * The timestamp for when the status should no longer be shown. If not set, then
                             * the status should just be shown until the next one is set.
                             */
                            val timeToStopShowingStatus: Date?)