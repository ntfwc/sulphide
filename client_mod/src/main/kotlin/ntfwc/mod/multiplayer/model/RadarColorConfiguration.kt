package ntfwc.mod.multiplayer.model

import com.beust.klaxon.JsonObject
import ntfwc.mod.multiplayer.radar.RadarHeightDifferenceColor
import ntfwc.mod.multiplayer.util.ParsingUtil

private const val ABOVE_COLOR_KEY = "radarAboveColor"
private const val BELOW_COLOR_KEY = "radarBelowColor"

/**
 * Configuration for the radar colors.
 */
data class RadarColorConfiguration(val aboveColor: RadarHeightDifferenceColor, val belowColor: RadarHeightDifferenceColor) {
    companion object {
        val DEFAULT_CONFIGURATION = RadarColorConfiguration(RadarHeightDifferenceColor.Yellow, RadarHeightDifferenceColor.Blue)

        fun fromJson(parsedJson: JsonObject): RadarColorConfiguration {
            val aboveColor = parseColor(parsedJson, ABOVE_COLOR_KEY) ?: DEFAULT_CONFIGURATION.aboveColor
            val belowColor = parseColor(parsedJson, BELOW_COLOR_KEY) ?: DEFAULT_CONFIGURATION.belowColor

            return RadarColorConfiguration(aboveColor, belowColor)
        }

        private fun parseColor(parsedJson: JsonObject, key: String): RadarHeightDifferenceColor? {
            val colorId = ParsingUtil.getString(parsedJson, key) ?: return null
            return RadarHeightDifferenceColor.fromColorId(colorId)
        }
    }

    fun toJson(jsonFields: ArrayList<Pair<String, *>>) {
        jsonFields.add(Pair(ABOVE_COLOR_KEY, aboveColor.getColorId()))
        jsonFields.add(Pair(BELOW_COLOR_KEY, belowColor.getColorId()))
    }
}