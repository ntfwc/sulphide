package ntfwc.mod.multiplayer.game.interfaces.player.avatar

import junit.framework.TestCase.assertEquals
import ntfwc.mod.multiplayer.util.UnitIntervalFloat
import ntfwc.mod.multiplayer.util.range.FloatRange
import org.junit.Test

class FloatRangeTest {
    @Test
    fun testMapToUnitInterval() {
        run {
            val range = FloatRange(0f, 100f)
            assertUnitIntervalEqual(UnitIntervalFloat(0f), range.mapToUnitInterval(0f))
            assertUnitIntervalEqual(UnitIntervalFloat(0.01f), range.mapToUnitInterval(1f))
            assertUnitIntervalEqual(UnitIntervalFloat(0.35f), range.mapToUnitInterval(35f))
            assertUnitIntervalEqual(UnitIntervalFloat(0.5f), range.mapToUnitInterval(50f))
            assertUnitIntervalEqual(UnitIntervalFloat(0.9f), range.mapToUnitInterval(90f))
            assertUnitIntervalEqual(UnitIntervalFloat(1f), range.mapToUnitInterval(100f))

            // A value outside the range should be clamped in the output
            assertUnitIntervalEqual(UnitIntervalFloat(1f), range.mapToUnitInterval(200f))
            assertUnitIntervalEqual(UnitIntervalFloat(1f), range.mapToUnitInterval(Float.MAX_VALUE))
            assertUnitIntervalEqual(UnitIntervalFloat(0f), range.mapToUnitInterval(-20f))

            // Infinite and NaN should turn into 0
            assertUnitIntervalEqual(UnitIntervalFloat(0f), range.mapToUnitInterval(Float.NaN))
            assertUnitIntervalEqual(UnitIntervalFloat(0f), range.mapToUnitInterval(Float.POSITIVE_INFINITY))
            assertUnitIntervalEqual(UnitIntervalFloat(0f), range.mapToUnitInterval(Float.NEGATIVE_INFINITY))
        }
        run {
            val range = FloatRange(100f, 200f)
            assertUnitIntervalEqual(UnitIntervalFloat(0f), range.mapToUnitInterval(100f))
            assertUnitIntervalEqual(UnitIntervalFloat(0.2f), range.mapToUnitInterval(120f))
            assertUnitIntervalEqual(UnitIntervalFloat(0.7f), range.mapToUnitInterval(170f))
            assertUnitIntervalEqual(UnitIntervalFloat(1.0f), range.mapToUnitInterval(200f))

            // A value outside the range should be clamped in the output
            assertUnitIntervalEqual(UnitIntervalFloat(1f), range.mapToUnitInterval(210f))
            assertUnitIntervalEqual(UnitIntervalFloat(0f), range.mapToUnitInterval(50f))
        }
        run {
            val range = FloatRange(50f, 100f)
            assertUnitIntervalEqual(UnitIntervalFloat(0f), range.mapToUnitInterval(50f))
            assertUnitIntervalEqual(UnitIntervalFloat(0.1f), range.mapToUnitInterval(55f))
            assertUnitIntervalEqual(UnitIntervalFloat(0.8f), range.mapToUnitInterval(90f))
            assertUnitIntervalEqual(UnitIntervalFloat(1f), range.mapToUnitInterval(100f))
        }
        run {
            // Negative values should be supported
            val range = FloatRange(-1f, 1f)
            assertUnitIntervalEqual(UnitIntervalFloat(0.5f), range.mapToUnitInterval(0f))
            assertUnitIntervalEqual(UnitIntervalFloat(0.0f), range.mapToUnitInterval(-1f))
            assertUnitIntervalEqual(UnitIntervalFloat(1.0f), range.mapToUnitInterval(1f))
            assertUnitIntervalEqual(UnitIntervalFloat(0.75f), range.mapToUnitInterval(0.5f))
            assertUnitIntervalEqual(UnitIntervalFloat(0.25f), range.mapToUnitInterval(-0.5f))
        }
    }

    @Test
    fun testMapFromUnitInterval() {
        run {
            val range = FloatRange(0f, 100f)
            assertFloatEquals(0f, range.mapFromUnitInterval(UnitIntervalFloat(0f)))
            assertFloatEquals(1f, range.mapFromUnitInterval(UnitIntervalFloat(0.01f)))
            assertFloatEquals(50f, range.mapFromUnitInterval(UnitIntervalFloat(0.5f)))
            assertFloatEquals(90f, range.mapFromUnitInterval(UnitIntervalFloat(0.9f)))
            assertFloatEquals(100f, range.mapFromUnitInterval(UnitIntervalFloat(1f)))
        }
        run {
            val range = FloatRange(100f, 200f)
            assertFloatEquals(100f, range.mapFromUnitInterval(UnitIntervalFloat(0f)))
            assertFloatEquals(120f, range.mapFromUnitInterval(UnitIntervalFloat(0.2f)))
            assertFloatEquals(150f, range.mapFromUnitInterval(UnitIntervalFloat(0.5f)))
            assertFloatEquals(175f, range.mapFromUnitInterval(UnitIntervalFloat(0.75f)))
            assertFloatEquals(200f, range.mapFromUnitInterval(UnitIntervalFloat(1f)))
        }
        run {
            val range = FloatRange(50f, 100f)
            assertFloatEquals(50f, range.mapFromUnitInterval(UnitIntervalFloat(0f)))
            assertFloatEquals(55f, range.mapFromUnitInterval(UnitIntervalFloat(0.1f)))
            assertFloatEquals(75f, range.mapFromUnitInterval(UnitIntervalFloat(0.5f)))
            assertFloatEquals(100f, range.mapFromUnitInterval(UnitIntervalFloat(1f)))
        }
        run {
            // Negative values should be supported
            val range = FloatRange(-1f, 1f)
            assertFloatEquals(-1f, range.mapFromUnitInterval(UnitIntervalFloat(0f)))
            assertFloatEquals(-0.5f, range.mapFromUnitInterval(UnitIntervalFloat(0.25f)))
            assertFloatEquals(0f, range.mapFromUnitInterval(UnitIntervalFloat(0.5f)))
            assertFloatEquals(0.5f, range.mapFromUnitInterval(UnitIntervalFloat(0.75f)))
            assertFloatEquals(1f, range.mapFromUnitInterval(UnitIntervalFloat(1f)))
        }
    }

    private fun assertUnitIntervalEqual(expected: UnitIntervalFloat, actual: UnitIntervalFloat) {
        assertEquals(expected.value, actual.value, 0.00001f)
    }

    private fun assertFloatEquals(expected: Float, actual: Float) {
        assertEquals(expected, actual, 0.00001f)
    }
}