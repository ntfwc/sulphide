package ntfwc.mod.multiplayer.game.interfaces.player.avatar

import junit.framework.TestCase.assertEquals
import ntfwc.mod.multiplayer.util.UnitIntervalFloat
import ntfwc.mod.multiplayer.util.range.IntValueRange
import org.junit.Test

class IntValueRangeTest {
    @Test
    fun testMapToUnitInterval() {
        run {
            val range = IntValueRange(0, 100)
            assertUnitIntervalEqual(UnitIntervalFloat(0f), range.mapToUnitInterval(0))
            assertUnitIntervalEqual(UnitIntervalFloat(0.01f), range.mapToUnitInterval(1))
            assertUnitIntervalEqual(UnitIntervalFloat(0.35f), range.mapToUnitInterval(35))
            assertUnitIntervalEqual(UnitIntervalFloat(0.5f), range.mapToUnitInterval(50))
            assertUnitIntervalEqual(UnitIntervalFloat(0.9f), range.mapToUnitInterval(90))
            assertUnitIntervalEqual(UnitIntervalFloat(1f), range.mapToUnitInterval(100))

            // A value outside the range should be clamped in the output
            assertUnitIntervalEqual(UnitIntervalFloat(1f), range.mapToUnitInterval(200))
            assertUnitIntervalEqual(UnitIntervalFloat(1f), range.mapToUnitInterval(Int.MAX_VALUE))
            assertUnitIntervalEqual(UnitIntervalFloat(0f), range.mapToUnitInterval(-20))
            assertUnitIntervalEqual(UnitIntervalFloat(0f), range.mapToUnitInterval(Int.MIN_VALUE))
        }
        run {
            val range = IntValueRange(100, 200)
            assertUnitIntervalEqual(UnitIntervalFloat(0f), range.mapToUnitInterval(100))
            assertUnitIntervalEqual(UnitIntervalFloat(0.2f), range.mapToUnitInterval(120))
            assertUnitIntervalEqual(UnitIntervalFloat(0.7f), range.mapToUnitInterval(170))
            assertUnitIntervalEqual(UnitIntervalFloat(1.0f), range.mapToUnitInterval(200))

            // A value outside the range should be clamped in the output
            assertUnitIntervalEqual(UnitIntervalFloat(1f), range.mapToUnitInterval(210))
            assertUnitIntervalEqual(UnitIntervalFloat(0f), range.mapToUnitInterval(50))
        }
        run {
            val range = IntValueRange(50, 100)
            assertUnitIntervalEqual(UnitIntervalFloat(0f), range.mapToUnitInterval(50))
            assertUnitIntervalEqual(UnitIntervalFloat(0.1f), range.mapToUnitInterval(55))
            assertUnitIntervalEqual(UnitIntervalFloat(0.8f), range.mapToUnitInterval(90))
            assertUnitIntervalEqual(UnitIntervalFloat(1f), range.mapToUnitInterval(100))
        }
        run {
            // Negative values should work
            val range = IntValueRange(-50, 50)
            assertUnitIntervalEqual(UnitIntervalFloat(0f), range.mapToUnitInterval(-50))
            assertUnitIntervalEqual(UnitIntervalFloat(0.25f), range.mapToUnitInterval(-25))
            assertUnitIntervalEqual(UnitIntervalFloat(0.5f), range.mapToUnitInterval(0))
            assertUnitIntervalEqual(UnitIntervalFloat(0.75f), range.mapToUnitInterval(25))
            assertUnitIntervalEqual(UnitIntervalFloat(1f), range.mapToUnitInterval(50))
        }
    }

    @Test
    fun testMapFromUnitInterval() {
        run {
            val range = IntValueRange(0, 100)
            assertEquals(0, range.mapFromUnitInterval(UnitIntervalFloat(0f)))
            assertEquals(1, range.mapFromUnitInterval(UnitIntervalFloat(0.01f)))
            assertEquals(50, range.mapFromUnitInterval(UnitIntervalFloat(0.5f)))
            assertEquals(90, range.mapFromUnitInterval(UnitIntervalFloat(0.9f)))
            assertEquals(100, range.mapFromUnitInterval(UnitIntervalFloat(1f)))

            // Values should be rounded
            assertEquals(90, range.mapFromUnitInterval(UnitIntervalFloat(0.904f)))
            assertEquals(91, range.mapFromUnitInterval(UnitIntervalFloat(0.906f)))
        }
        run {
            val range = IntValueRange(100, 200)
            assertEquals(100, range.mapFromUnitInterval(UnitIntervalFloat(0f)))
            assertEquals(120, range.mapFromUnitInterval(UnitIntervalFloat(0.2f)))
            assertEquals(150, range.mapFromUnitInterval(UnitIntervalFloat(0.5f)))
            assertEquals(175, range.mapFromUnitInterval(UnitIntervalFloat(0.75f)))
            assertEquals(200, range.mapFromUnitInterval(UnitIntervalFloat(1f)))
        }
        run {
            val range = IntValueRange(50, 100)
            assertEquals(50, range.mapFromUnitInterval(UnitIntervalFloat(0f)))
            assertEquals(55, range.mapFromUnitInterval(UnitIntervalFloat(0.1f)))
            assertEquals(75, range.mapFromUnitInterval(UnitIntervalFloat(0.5f)))
            assertEquals(100, range.mapFromUnitInterval(UnitIntervalFloat(1f)))
        }
        run {
            // Ranges with negative values should work
            val range = IntValueRange(-50, 50)
            assertEquals(-50, range.mapFromUnitInterval(UnitIntervalFloat(0f)))
            assertEquals(-25, range.mapFromUnitInterval(UnitIntervalFloat(0.25f)))
            assertEquals(0, range.mapFromUnitInterval(UnitIntervalFloat(0.5f)))
            assertEquals(25, range.mapFromUnitInterval(UnitIntervalFloat(0.75f)))
            assertEquals(50, range.mapFromUnitInterval(UnitIntervalFloat(1f)))
        }
    }

    private fun assertUnitIntervalEqual(expected: UnitIntervalFloat, actual: UnitIntervalFloat) {
        assertEquals(expected.value, actual.value, 0.00001f)
    }
}