package ntfwc.mod.multiplayer

import ntfwc.mod.multiplayer.connection.ConnectionManager
import ntfwc.mod.multiplayer.connection.GeneralConnectionStatus
import ntfwc.mod.multiplayer.model.MapName
import ntfwc.mod.multiplayer.model.ModelTestConstants
import ntfwc.mod.multiplayer.model.PlayerPosition
import ntfwc.mod.multiplayer.model.avatar.array.NetPlayerAppearanceArrayState
import ntfwc.mod.multiplayer.model.avatar.array.PlayerArrayState
import ntfwc.mod.multiplayer.model.avatar.array.TimestampedPlayerArrayState
import ntfwc.mod.multiplayer.sync.MultiplayerStateContainer
import java.util.concurrent.atomic.AtomicReference

/**
 * This provides a way to run the connection manager without having to run the game too.
 */
fun main() {
    val generalConnectionStatus = AtomicReference<GeneralConnectionStatus>()

    val basePosition = PlayerPosition(15870.3009663473, 2316.8388426759266, 24.016)
    val appearanceArrayState = NetPlayerAppearanceArrayState.from(ModelTestConstants.APPEARANCE_STATE1)

    val connectionManager = ConnectionManager(MultiplayerStateContainer(), TimestampedPlayerArrayState(System.nanoTime(),
            PlayerArrayState(basePosition, appearanceArrayState)), generalConnectionStatus)
    connectionManager.startConnecting("localhost", 9090, "user", MapName("empty"))


    while (true) {
        for (i in 0..50) {
            Thread.sleep(1000 / 30)
            connectionManager.updatePlayerState(TimestampedPlayerArrayState(
                    System.nanoTime(),
                    PlayerArrayState(PlayerPosition(basePosition.x + i * 0.75, basePosition.y, basePosition.z), appearanceArrayState)
            ))
        }
    }
}
