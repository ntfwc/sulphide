package ntfwc.mod.multiplayer.util.vector

import junit.framework.TestCase.assertNotNull
import junit.framework.TestCase.assertNull
import org.junit.Test

class Vector3fTest {
    @Test
    fun testNormalizationFailureCases() {
        assertNull(NormalizedVector3f.from(Vector3f(0f, 0f, 0f)))
        assertNull(NormalizedVector3f.from(Vector3f(Float.NaN, 0f, 0f)))
        assertNull(NormalizedVector3f.from(Vector3f(Float.NaN, 0f, 0f)))
        assertNull(NormalizedVector3f.from(Vector3f(Float.POSITIVE_INFINITY, 0f, 0f)))
        assertNotNull(NormalizedVector3f.from(Vector3f(1f, 0f, 0f)))
        assertNotNull(NormalizedVector3f.from(Vector3f(2f, 0f, 0f)))
        assertNotNull(NormalizedVector3f.from(Vector3f(0f, 2f, 0f)))
        assertNotNull(NormalizedVector3f.from(Vector3f(0f, 0f, 2f)))
        assertNull(NormalizedVector3f.from(Vector3f(Float.MIN_VALUE, 0f, 0f)))
        assertNull(NormalizedVector3f.from(Vector3f(Float.MAX_VALUE, 0f, 0f)))
    }
}