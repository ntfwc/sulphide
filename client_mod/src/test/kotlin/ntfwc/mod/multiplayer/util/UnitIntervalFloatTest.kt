package ntfwc.mod.multiplayer.util

import junit.framework.TestCase.assertEquals
import org.junit.Test

class UnitIntervalFloatTest {
    @Test
    fun testLerp() {
        assertLerp(1f, 1f, 1f, 0.5f)
        assertLerp(1f, 1f, 1f, 0f)
        assertLerp(1f, 1f, 1f, 1f)
        assertLerp(1f, 1f, 1f, 0.99f)

        assertLerp(0f, 0f, 0f, 0.5f)
        assertLerp(0f, 0f, 0f, 0f)
        assertLerp(0f, 0f, 0f, 1f)
        assertLerp(0f, 0f, 0f, 0.99f)

        assertLerp(0.5f, 0f, 1f, 0.5f)
        assertLerp(0.5f, 1f, 0f, 0.5f)

        assertLerp(0.1f, 0f, 0.5f, 0.2f)
        assertLerp(0.1f, 0.5f, 0f, 0.8f)
        assertLerp(0.4f, 0.5f, 0f, 0.2f)
        assertLerp(0.4f, 0f, 0.5f, 0.8f)
    }

    private fun assertLerp(expectedValue: Float, unitIntervalValue1: Float, unitIntervalValue2: Float, factor: Float) {
        assertUnitIntervalValueEquals(expectedValue,
                UnitIntervalFloat(unitIntervalValue1).lerp(UnitIntervalFloat(unitIntervalValue2), factor))
    }

    @Test
    fun testLerpWrapped() {
        assertLerpWrapped(0f, 0f, 0f, 0f)
        assertLerpWrapped(0f, 0f, 0f, 1f)
        assertLerpWrapped(1f, 1f, 1f, 0f)
        assertLerpWrapped(1f, 1f, 1f, 1f)

        // Things should work the same as lerp if value2 is greater than value1
        assertLerpWrapped(0f, 0f, 0.5f, 0f)
        assertLerpWrapped(0.25f, 0f, 0.5f, 0.5f)
        assertLerpWrapped(0.5f, 0f, 0.5f, 1f)

        // When value2 is less than value1, it should wrap around
        assertLerpWrapped(0.5f, 0.5f, 0.3f, 0f)
        assertLerpWrapped(0.7f, 0.5f, 0.3f, 0.25f)
        assertLerpWrapped(0.9f, 0.5f, 0.3f, 0.5f)
        assertLerpWrapped(0.1f, 0.5f, 0.3f, 0.75f)
        assertLerpWrapped(0.3f, 0.5f, 0.3f, 1f)

        // Corner case: value1 is 1 and value2 is 0. These values are right next to each other. The result will always
        // be the same.
        assertLerpWrapped(1f, 1f, 0f, 0f)
        assertLerpWrapped(1f, 1f, 0f, 0.5f)
        assertLerpWrapped(1f, 1f, 0f, 1f)
    }

    private fun assertLerpWrapped(expectedValue: Float, unitIntervalValue1: Float, unitIntervalValue2: Float, factor: Float) {
        assertUnitIntervalValueEquals(expectedValue,
                UnitIntervalFloat(unitIntervalValue1).lerpWrapped(UnitIntervalFloat(unitIntervalValue2), factor))
    }

    private fun assertUnitIntervalValueEquals(expectedValue: Float, unitInterval: UnitIntervalFloat) {
        assertEquals(expectedValue, unitInterval.value, 0.0001f)
    }
}