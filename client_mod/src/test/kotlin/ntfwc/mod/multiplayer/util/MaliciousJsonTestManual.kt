package ntfwc.mod.multiplayer.util

import junit.framework.TestCase.assertTrue
import junit.framework.TestCase.fail
import org.junit.Ignore
import org.junit.Test

@Ignore
class MaliciousJsonTestManual {
    @Test
    fun testParsingExtremelyDeepTree() {
        initialParse()
        println(JsonSecurity.collectStats(constructDeepJsonTree(100000)))
        testParsing(constructDeepJsonTree(100000))
    }

    @Test
    fun testParsingExtremelyWideTree() {
        initialParse()
        testParsing(constructWideTree(300000))
    }

    @Test
    fun testParsingTreeWithHugeString() {
        initialParse()
        testParsing(constructTreeWithString(1000000))
    }

    @Test
    fun testSecurityCheckExtremelyDeepTree() {
        initialParse()
        println(JsonSecurity.collectStats(constructDeepJsonTree(100000)))
        testSecurityCheck(constructDeepJsonTree(100000))
    }

    @Test
    fun testSecurityCheckExtremelyWideTree() {
        initialParse()
        testSecurityCheck(constructWideTree(300000))
    }

    @Test
    fun testSecurityCheckTreeWithHugeString() {
        initialParse()
        testSecurityCheck(constructTreeWithString(1000000))
    }

    private fun testParsing(json: String) {
        val initialUsedMemory = getUsedMemory()
        val startTime = System.nanoTime()

        val parsedJson = ParsingUtil.parseJsonObject(json) ?: return fail("Invalid JSON")
        val endTime = System.nanoTime()

        reportStats(startTime, endTime, initialUsedMemory, json.length)
        println("parsedJson.size=${parsedJson.size}")
    }

    private fun testSecurityCheck(json: String) {
        val initialUsedMemory = getUsedMemory()
        val startTime = System.nanoTime()

        assertTrue(JsonSecurity.looksLikeMaliciousJson(json))
        val endTime = System.nanoTime()

        reportStats(startTime, endTime, initialUsedMemory, json.length)
    }

    private fun initialParse() {
        // Parse something to warm up the JSON parsing
        ParsingUtil.parseJsonObject("""{"blah": 4}""")
    }

    private fun reportStats(startTime: Long, endTime: Long, initialUsedMemory: Long, messageSize: Int) {
        val timeElapsed = (endTime - startTime).toDouble() / 1_000_000_000
        val finalUsedMemory = getUsedMemory()
        val usedMemoryDiff = finalUsedMemory - initialUsedMemory
        println("timeElaped=$timeElapsed")
        println("usedMemoryDiff=$usedMemoryDiff, initialUsedMemory=$initialUsedMemory, finalUsedMemory=$finalUsedMemory")
        println("messageSize=$messageSize")
    }

    private fun getUsedMemory(): Long {
        val runtime = Runtime.getRuntime()
        return runtime.totalMemory() - runtime.freeMemory()
    }

    private fun constructDeepJsonTree(levels: Int): String {
        val stringBuilder = StringBuilder()
        stringBuilder.append('{')
        for (i in 0 until levels) {
            stringBuilder.append("\"a\": {")
        }

        for (i in 0 until levels) {
            stringBuilder.append("}")
        }

        stringBuilder.append('}')
        return stringBuilder.toString()
    }

    private fun constructWideTree(leaves: Int): String {
        val stringBuilder = StringBuilder()
        stringBuilder.append('{')
        for (i in 0 until leaves) {
            stringBuilder.append("\"a$i\": 1")
        }

        stringBuilder.append('}')
        return stringBuilder.toString()
    }

    private fun constructTreeWithString(stringSize: Int): String {
        val stringBuilder = StringBuilder()
        stringBuilder.append("{\"a\": \"")
        stringBuilder.append(constructString(stringSize))
        stringBuilder.append("\"}")
        return stringBuilder.toString()
    }

    private fun constructString(stringSize: Int): String {
        val stringBuilder = StringBuilder()
        for (i in 0 until stringSize) {
            stringBuilder.append(i % 10)
        }
        return stringBuilder.toString()
    }
}