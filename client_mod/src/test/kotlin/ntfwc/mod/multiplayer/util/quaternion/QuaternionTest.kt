package ntfwc.mod.multiplayer.util.quaternion

import junit.framework.TestCase.*
import ntfwc.mod.multiplayer.game.interfaces.player.avatar.PlayerRotationTransform
import ntfwc.mod.multiplayer.util.vector.Vector3d
import org.junit.Test

class QuaternionTest {
    @Test
    fun testRotationTransformConversion() {
        testConvertToQuaternionAndBack(PlayerRotationTransform(Vector3d(1.0, 0.0, 0.0),
                Vector3d(0.0, 1.0, 0.0),
                Vector3d(0.0, 0.0, 1.0)))
        testConvertToQuaternionAndBack(PlayerRotationTransform(Vector3d(0.0, 1.0, 0.0),
                Vector3d(-1.0, 0.0, 0.0),
                Vector3d(0.0, 0.0, 1.0)))

        testConvertToQuaternionAndBack(PlayerRotationTransform(Vector3d(0.7116860489359671, 0.7024976638750567, -0.0),
                Vector3d(-0.7024976638750567, 0.7116860489359671, 0.0),
                Vector3d(0.0, 0.0, 1.0)))
        testConvertToQuaternionAndBack(PlayerRotationTransform(Vector3d(0.5193035006523132, -0.5456220507621765, 0.6577398180961609),
                Vector3d(0.7068871855735779, 0.7067644596099854, 0.02818300388753414),
                Vector3d(-0.4802441895008087, 0.45031237602233887, 0.7527180910110474)))
        testConvertToQuaternionAndBack(PlayerRotationTransform(Vector3d(0.48151832818984985, 0.634518563747406, -0.6045883893966675),
                Vector3d(-0.7999843955039978, 0.5999733209609985, -0.007463813293725252),
                Vector3d(0.3580017387866974, 0.4872559905052185, 0.7965032458305359)))
    }

    /**
     * These are some recorded rotation transforms that caused issues. Turning them into a quaternion
     * does not give you an already normalized quaternion.
     */
    @Test
    fun testRotationTransformConversion_trickyCases() {
        testConvertToQuaternionAndBack(PlayerRotationTransform(Vector3d(-0.9873424172401428, -0.0024026513565331697, 0.1585846245288849),
                Vector3d(-0.003793194657191634, -0.9992414712905884, -0.038755327463150024),
                Vector3d(0.15855742990970612, -0.0388663113117218, 0.986584484577179)))
        testConvertToQuaternionAndBack(PlayerRotationTransform(Vector3d(-0.9853947758674622, -0.0024447317700833082, 0.170268714427948),
                Vector3d(-8.0048234667629E-4, -0.9998193383216858, -0.01898787170648575),
                Vector3d(0.1702844649553299, -0.01884670928120613, 0.9852145314216614)))
    }

    @Test
    fun testNormalizationFailureCases() {
        assertNull(NormalizedQuaternion.from(Quaternion(0.0, 0.0, 0.0, 0.0)))
        assertNotNull(NormalizedQuaternion.from(Quaternion(1.0, 0.0, 0.0, 0.0)))
        assertNotNull(NormalizedQuaternion.from(Quaternion(0.0, 1.0, 0.0, 0.0)))
        assertNotNull(NormalizedQuaternion.from(Quaternion(0.0, 0.0, 1.0, 0.0)))
        assertNotNull(NormalizedQuaternion.from(Quaternion(0.0, 0.0, 0.0, 1.0)))
        assertNull(NormalizedQuaternion.from(Quaternion(0.0, Double.MIN_VALUE, 0.0, 0.0)))
        assertNull(NormalizedQuaternion.from(Quaternion(0.0, Double.MAX_VALUE, 0.0, 0.0)))
    }

    private fun testConvertToQuaternionAndBack(transform: PlayerRotationTransform) {
        val quaternion = NormalizedQuaternion.from(transform)
        val convertedTransform = quaternion.toPlayerRotationTransform()
        assertTransformEqual(transform, convertedTransform, 0.001)
    }

    private fun assertTransformEqual(expected: PlayerRotationTransform, actual: PlayerRotationTransform, delta: Double) {
        assertVectorEqual(expected.vX, actual.vX, delta)
        assertVectorEqual(expected.vY, actual.vY, delta)
        assertVectorEqual(expected.vZ, actual.vZ, delta)
    }

    private fun assertVectorEqual(expected: Vector3d, actual: Vector3d, delta: Double) {
        assertEquals(expected.x, actual.x, delta)
        assertEquals(expected.y, actual.y, delta)
        assertEquals(expected.z, actual.z, delta)
    }
}