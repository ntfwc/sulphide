package ntfwc.mod.multiplayer.model.array.type

import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNull
import ntfwc.mod.multiplayer.model.array.type.DescriptorTestCommon.tryFromJsonForNonNullableValue
import ntfwc.mod.multiplayer.model.avatar.array.type.StringItemDescriptor
import org.junit.Test

class StringItemDescriptorTest {
    private val maxLength = 20
    private val descriptor = StringItemDescriptor("str", maxLength)

    @Test
    fun testFromJsonTyped() {
        assertNull(tryFromJsonTyped("{}"))

        assertEquals("hey", tryFromJsonTyped("""{ "str": "hey" }"""))
        assertEquals("there", tryFromJsonTyped("""{ "str": "there" }"""))

        // Empty strings are not allowed
        assertNull(tryFromJsonTyped("""{ "str": "" }"""))

        // Strings at the max length should be ok
        assertEquals("01234567890123456789", tryFromJsonTyped("""{ "str": "01234567890123456789" }"""))

        // Strings that are longer than the max length should fail
        assertNull(tryFromJsonTyped("""{ "str": "012345678901234567890" }"""))

        // Wrong type
        assertNull(tryFromJsonTyped("""{ "str": 0 }"""))
    }

    @Test
    fun testToJson() {
        assertEquals("boo", descriptor.toJsonValue("boo"))
        assertEquals("01234567890123456789", descriptor.toJsonValue("01234567890123456789"))

        // Strings larger than the max size should get truncated
        assertEquals("01234567890123456789", descriptor.toJsonValue("012345678901234567890"))
    }

    private fun tryFromJsonTyped(jsonString: String): String? {
        return tryFromJsonForNonNullableValue(jsonString, descriptor::fromJsonTyped)
    }
}