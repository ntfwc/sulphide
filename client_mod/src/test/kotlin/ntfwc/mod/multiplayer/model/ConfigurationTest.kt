package ntfwc.mod.multiplayer.model

import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNull
import ntfwc.mod.multiplayer.radar.RadarHeightDifferenceColor
import org.junit.Test

class ConfigurationTest {
    @Test
    fun testSerialize() {
        assertEquals("""{
        |  "host": "localhost",
        |  "port": 1234,
        |  "auto_connect": false,
        |  "enable_radar": true,
        |  "radarAboveColor": "yellow",
        |  "radarBelowColor": "blue",
        |  "max_avatar_count": 5
        |}""".trimMargin(), Configuration("localhost", 1234).serialize())

        // Empty values should not be written
        assertEquals("""{
        |  "auto_connect": false,
        |  "enable_radar": true,
        |  "radarAboveColor": "yellow",
        |  "radarBelowColor": "blue",
        |  "max_avatar_count": 5
        |}""".trimMargin(), Configuration("", null).serialize())
        assertEquals("""{
        |  "host": "localhost",
        |  "auto_connect": false,
        |  "enable_radar": true,
        |  "radarAboveColor": "yellow",
        |  "radarBelowColor": "blue",
        |  "max_avatar_count": 5
        |}""".trimMargin(), Configuration("localhost", null).serialize())
        assertEquals("""{
        |  "port": 1234,
        |  "auto_connect": false,
        |  "enable_radar": true,
        |  "radarAboveColor": "yellow",
        |  "radarBelowColor": "blue",
        |  "max_avatar_count": 5
        |}""".trimMargin(), Configuration("", 1234).serialize())
    }

    @Test
    fun testDeserialize() {
        assertEquals(Configuration.DEFAULT_CONFIGURATION, Configuration.deserialize("{}"))
        assertEquals(Configuration("localhost", 1234, "beakface", false, false,
                RadarColorConfiguration(RadarHeightDifferenceColor.Green, RadarHeightDifferenceColor.Purple),
                12),
                Configuration.deserialize("""{"host": "localhost", "port": 1234, "username": "beakface",
                    |"auto_connect": false, "enable_radar": false, "radarAboveColor": "green", "radarBelowColor": "purple",
                    |"max_avatar_count": 12}""".trimMargin()))

        assertEquals(Configuration("localhost", null), Configuration.deserialize("""{"host": "localhost"}"""))
        assertEquals(Configuration("", 1234), Configuration.deserialize("""{"port": 1234}"""))

        // --- If values are invalid, default values should be substituted
        // Invalid host
        assertEquals(Configuration("", 1234), Configuration.deserialize("""{"host": 0, "port": 1234}"""))
        assertEquals(Configuration.DEFAULT_CONFIGURATION, Configuration.deserialize("""{"host": null}"""))
        assertEquals(Configuration.DEFAULT_CONFIGURATION, Configuration.deserialize("""{"host": []}"""))
        assertEquals(Configuration.DEFAULT_CONFIGURATION, Configuration.deserialize("""{"host": "-"}"""))
        // Invalid port
        assertEquals(Configuration("localhost", null), Configuration.deserialize("""{"host": "localhost", "port": []}"""))
        assertEquals(Configuration.DEFAULT_CONFIGURATION, Configuration.deserialize("""{"port": 0}"""))
        assertEquals(Configuration.DEFAULT_CONFIGURATION, Configuration.deserialize("""{"port": -1}"""))
        assertEquals(Configuration.DEFAULT_CONFIGURATION, Configuration.deserialize("""{"port": 80000}"""))
        // Invalid Long value for the port
        assertEquals(Configuration.DEFAULT_CONFIGURATION, Configuration.deserialize("""{"port": 10000000000}"""))
        // Invalid auto-connect value
        assertEquals(Configuration.DEFAULT_CONFIGURATION, Configuration.deserialize("""{"auto_connect": []}"""))
        assertEquals(Configuration.DEFAULT_CONFIGURATION, Configuration.deserialize("""{"auto_connect": "blah"}"""))
        // Invalid enable radar value
        assertEquals(Configuration.DEFAULT_CONFIGURATION, Configuration.deserialize("""{"enable_radar": []}"""))
        assertEquals(Configuration.DEFAULT_CONFIGURATION, Configuration.deserialize("""{"enable_radar": "blah"}"""))
        // Invalid radar color values
        assertEquals(Configuration.DEFAULT_CONFIGURATION, Configuration.deserialize("""{"radarAboveColor": []}"""))
        assertEquals(Configuration.DEFAULT_CONFIGURATION, Configuration.deserialize("""{"radarAboveColor": "something"}"""))
        assertEquals(Configuration.DEFAULT_CONFIGURATION, Configuration.deserialize("""{"radarBelowColor": 5}"""))

        // It should return null for unparseable strings
        assertNull(Configuration.deserialize(""))
        assertNull(Configuration.deserialize("{"))
    }

    @Test
    fun testDeserializeRadarColors() {
        assertEquals(Configuration(RadarColorConfiguration(RadarHeightDifferenceColor.Green,
                RadarColorConfiguration.DEFAULT_CONFIGURATION.belowColor)),
                Configuration.deserialize("""{"radarAboveColor": "green"}"""))
        assertEquals(Configuration(RadarColorConfiguration(RadarHeightDifferenceColor.Blue,
                RadarColorConfiguration.DEFAULT_CONFIGURATION.belowColor)),
                Configuration.deserialize("""{"radarAboveColor": "blue"}"""))

        //Different capitalization should be ok
        assertEquals(Configuration(RadarColorConfiguration(RadarHeightDifferenceColor.Blue,
                RadarColorConfiguration.DEFAULT_CONFIGURATION.belowColor)),
                Configuration.deserialize("""{"radarAboveColor": "Blue"}"""))
        assertEquals(Configuration(RadarColorConfiguration(RadarHeightDifferenceColor.Blue,
                RadarColorConfiguration.DEFAULT_CONFIGURATION.belowColor)),
                Configuration.deserialize("""{"radarAboveColor": "BLUE"}"""))

        assertEquals(Configuration(RadarColorConfiguration(RadarColorConfiguration.DEFAULT_CONFIGURATION.aboveColor,
                RadarHeightDifferenceColor.Green)),
                Configuration.deserialize("""{"radarBelowColor": "green"}"""))
        assertEquals(Configuration(RadarColorConfiguration(RadarColorConfiguration.DEFAULT_CONFIGURATION.aboveColor,
                RadarHeightDifferenceColor.Yellow)),
                Configuration.deserialize("""{"radarBelowColor": "yellow"}"""))

        assertEquals(Configuration(RadarColorConfiguration(RadarHeightDifferenceColor.Red,
                RadarHeightDifferenceColor.Blue)),
                Configuration.deserialize("""{"radarAboveColor": "red", "radarBelowColor": "blue"}"""))
        assertEquals(Configuration(RadarColorConfiguration(RadarHeightDifferenceColor.Red,
                RadarColorConfiguration.DEFAULT_CONFIGURATION.belowColor)),
                Configuration.deserialize("""{"radarAboveColor": "red", "radarBelowColor": "something"}"""))
    }

    @Test
    fun testSerializeAndDeserialize() {
        val testWithConfiguration = { config: Configuration ->
            val serialized = config.serialize()
            val deserialized = Configuration.deserialize(serialized)
            assertEquals(config, deserialized)
        }

        testWithConfiguration(Configuration.DEFAULT_CONFIGURATION)
        testWithConfiguration(Configuration("localhost", null))
        testWithConfiguration(Configuration("", 1234))
        testWithConfiguration(Configuration("localhost", 1234))
        testWithConfiguration(Configuration("127.0.0.1", 5124))
        testWithConfiguration(Configuration("::1", 33211))
        testWithConfiguration(Configuration("example.com", 53123))
        testWithConfiguration(Configuration("example.com", 53123, "", false, false,
                RadarColorConfiguration(RadarHeightDifferenceColor.Cyan, RadarHeightDifferenceColor.Orange),
                12))
        testWithConfiguration(Configuration("example.com", 53123, "", true, true,
                RadarColorConfiguration.DEFAULT_CONFIGURATION,
                7))
        testWithConfiguration(Configuration("example.com", 53123, "beakface", true, true,
                RadarColorConfiguration(RadarHeightDifferenceColor.Blue, RadarHeightDifferenceColor.Purple),
                12))
        testWithConfiguration(Configuration("example.com", 53123, "#!@%*(./\\", true, true,
                RadarColorConfiguration(RadarHeightDifferenceColor.Yellow, RadarHeightDifferenceColor.Red),
                10))
    }

    private fun Configuration(host: String, port: Int?): Configuration {
        return Configuration(host,
                port,
                Configuration.DEFAULT_CONFIGURATION.username,
                Configuration.DEFAULT_CONFIGURATION.autoConnect,
                Configuration.DEFAULT_CONFIGURATION.enableRadar,
                Configuration.DEFAULT_CONFIGURATION.radarColorConfiguration,
                Configuration.DEFAULT_CONFIGURATION.maxAvatarCount)
    }

    private fun Configuration(radarColorConfiguration: RadarColorConfiguration): Configuration {
        return Configuration(Configuration.DEFAULT_CONFIGURATION.host,
                Configuration.DEFAULT_CONFIGURATION.port,
                Configuration.DEFAULT_CONFIGURATION.username,
                Configuration.DEFAULT_CONFIGURATION.autoConnect,
                Configuration.DEFAULT_CONFIGURATION.enableRadar,
                radarColorConfiguration,
                Configuration.DEFAULT_CONFIGURATION.maxAvatarCount
        )
    }
}