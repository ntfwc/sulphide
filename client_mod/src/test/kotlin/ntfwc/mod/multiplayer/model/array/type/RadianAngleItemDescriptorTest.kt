package ntfwc.mod.multiplayer.model.array.type

import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNull
import ntfwc.mod.multiplayer.model.array.type.DescriptorTestCommon.tryFromJsonForNonNullableValue
import ntfwc.mod.multiplayer.model.avatar.array.type.RadianAngleItemDescriptor
import ntfwc.mod.multiplayer.util.RadianAngle
import org.junit.Test

class RadianAngleItemDescriptorTest {
    private val descriptor = RadianAngleItemDescriptor("radian")

    @Test
    fun testFromJsonTyped() {
        assertNull(tryFromJsonTyped("{}"))

        assertEquals(RadianAngle(0f), tryFromJsonTyped("""{ "radian": 0 }"""))
        assertEquals(RadianAngle(1f), tryFromJsonTyped("""{ "radian": 1 }"""))
        assertEquals(RadianAngle(0.5f), tryFromJsonTyped("""{ "radian": 0.5 }"""))
        assertEquals(RadianAngle(2.27f), tryFromJsonTyped("""{ "radian": 2.27 }"""))

        // Wrong type
        assertNull(tryFromJsonTyped("""{ "radian": "blah" }"""))
    }

    private fun tryFromJsonTyped(jsonString: String): RadianAngle? {
        return tryFromJsonForNonNullableValue(jsonString, descriptor::fromJsonTyped)
    }
}