package ntfwc.mod.multiplayer.model.array.type

import com.beust.klaxon.JsonObject
import junit.framework.TestCase
import ntfwc.mod.multiplayer.util.FromJsonResult
import ntfwc.mod.multiplayer.util.ParsingUtil

object DescriptorTestCommon {
    fun parseJson(jsonString: String): JsonObject {
        val jsonObject = ParsingUtil.parseJsonObject(jsonString)
        if (jsonObject == null) {
            TestCase.fail("Failed to parse the given JSON string")
            // We won't get here, but we need it to make the null checker happy
            throw IllegalStateException()
        }

        return jsonObject
    }

    fun <T> tryFromJsonForNonNullableValue(jsonString: String, getValue: (JsonObject) -> FromJsonResult<out T>): T? {
        val jsonObject = parseJson(jsonString)
        return when (val result = getValue(jsonObject)) {
            is FromJsonResult.Success -> result.value
            is FromJsonResult.Failure -> null
        }
    }
}