package ntfwc.mod.multiplayer.model.array.type

import game.Pony
import junit.framework.TestCase.assertEquals
import ntfwc.mod.multiplayer.model.avatar.array.type.PonyMoodItemDescriptor
import ntfwc.mod.multiplayer.util.FromJsonResult
import org.junit.Test

class PonyMoodItemDescriptorTest {
    private val descriptor = PonyMoodItemDescriptor("mood")
    private val failure = FromJsonResult.Failure<Pony.Mood?>()

    @Test
    fun testFromJsonTyped() {
        assertEquals(FromJsonResult.Success(null), tryFromJsonTyped("{}"))
        assertEquals(FromJsonResult.Success(Pony.Mood.AGGRESSIVE), tryFromJsonTyped("""{ "mood": "aggressive" }"""))
        assertEquals(FromJsonResult.Success(Pony.Mood.HURT), tryFromJsonTyped("""{ "mood": "hurt" }"""))

        // Case should not matter
        assertEquals(FromJsonResult.Success(Pony.Mood.HURT), tryFromJsonTyped("""{ "mood": "HURT" }"""))
        assertEquals(FromJsonResult.Success(Pony.Mood.HURT), tryFromJsonTyped("""{ "mood": "Hurt" }"""))

        // Should default to null for an unknown value
        assertEquals(FromJsonResult.Success(null), tryFromJsonTyped("""{ "mood": "someUnknownMood" }"""))

        // Incorrect type
        assertEquals(failure, tryFromJsonTyped("""{ "mood": {} }"""))
    }

    private fun tryFromJsonTyped(jsonString: String): FromJsonResult<out Pony.Mood?> {
        val jsonObject = DescriptorTestCommon.parseJson(jsonString)
        return descriptor.fromJsonTyped(jsonObject)
    }
}