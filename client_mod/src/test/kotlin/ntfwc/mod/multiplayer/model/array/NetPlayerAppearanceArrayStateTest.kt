package ntfwc.mod.multiplayer.model.array

import com.beust.klaxon.JsonObject
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.fail
import ntfwc.mod.multiplayer.model.ModelTestConstants
import ntfwc.mod.multiplayer.model.avatar.NetPlayerAppearanceState
import ntfwc.mod.multiplayer.model.avatar.array.NetPlayerAppearanceArrayState
import ntfwc.mod.multiplayer.util.ParsingUtil
import org.junit.Test

class NetPlayerAppearanceArrayStateTest {
    @Test
    fun testJsonRoundTrips() {
        testJsonRoundTrip(ModelTestConstants.APPEARANCE_STATE1)
        testJsonRoundTrip(ModelTestConstants.APPEARANCE_STATE2)
    }

    private fun testJsonRoundTrip(state: NetPlayerAppearanceState) {
        val arrayState = NetPlayerAppearanceArrayState.from(state)
        val jsonRepresentation = arrayState.toJsonObject().toJsonString()
        val parsedJson = ParsingUtil.parseJsonObject(jsonRepresentation)
        if (parsedJson !is JsonObject) {
            fail("JSON parsing failed")
            return
        }

        val restoredArrayState = NetPlayerAppearanceArrayState.fromJsonObject(parsedJson)
        if (restoredArrayState == null) {
            fail("Failed to restore array state from JSON")
            return
        }
        val restoredState = restoredArrayState.toAppearanceState()
        assertEquals(state, restoredState)
    }
}