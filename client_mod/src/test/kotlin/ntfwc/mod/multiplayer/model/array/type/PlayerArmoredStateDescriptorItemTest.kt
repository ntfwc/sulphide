package ntfwc.mod.multiplayer.model.array.type

import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNull
import ntfwc.mod.multiplayer.model.array.type.DescriptorTestCommon.tryFromJsonForNonNullableValue
import ntfwc.mod.multiplayer.model.avatar.PlayerArmoredState
import ntfwc.mod.multiplayer.model.avatar.array.type.PlayerArmoredStateItemDescriptor
import org.junit.Test

class PlayerArmoredStateDescriptorItemTest {
    private val descriptor = PlayerArmoredStateItemDescriptor("armoredState")

    @Test
    fun testFromJsonTyped() {
        assertNull(tryFromJsonTyped("{}"))
        assertEquals(PlayerArmoredState.HOOVES, tryFromJsonTyped("""{ "armoredState": "hooves" }"""))
        assertEquals(PlayerArmoredState.NONE, tryFromJsonTyped("""{ "armoredState": "none" }"""))
        assertEquals(PlayerArmoredState.FULL, tryFromJsonTyped("""{ "armoredState": "full" }"""))

        // Capitalization should not matter
        assertEquals(PlayerArmoredState.FULL, tryFromJsonTyped("""{ "armoredState": "FULL" }"""))
        assertEquals(PlayerArmoredState.FULL, tryFromJsonTyped("""{ "armoredState": "Full" }"""))

        // Unknown value names should default to the HOOVES value
        assertEquals(PlayerArmoredState.HOOVES, tryFromJsonTyped("""{ "armoredState": "blah" }"""))

        // Wrong type
        assertNull(tryFromJsonTyped("""{ "armoredState": 0 }"""))
    }

    private fun tryFromJsonTyped(jsonString: String): PlayerArmoredState? {
        return tryFromJsonForNonNullableValue(jsonString, descriptor::fromJsonTyped)
    }
}