package ntfwc.mod.multiplayer.model.array.type

import junit.framework.TestCase.assertEquals
import ntfwc.mod.multiplayer.model.array.type.DescriptorTestCommon.parseJson
import ntfwc.mod.multiplayer.model.avatar.array.type.NormalizedVector3fItemDescriptor
import ntfwc.mod.multiplayer.util.FromJsonResult
import ntfwc.mod.multiplayer.util.vector.NormalizedVector3f
import ntfwc.mod.multiplayer.util.vector.Vector3f
import org.junit.Test

class NormalizedVector3fItemDescriptorTest {
    private val descriptor = NormalizedVector3fItemDescriptor("Vec3f")
    private val failure = FromJsonResult.Failure<NormalizedVector3f?>()

    @Test
    fun testFromJsonTyped() {
        assertEquals(FromJsonResult.Success(null), tryFromJsonTyped("{}"))
        assertEquals(failure, tryFromJsonTyped("""{ "Vec3f": {} }"""))
        assertEquals(failure, tryFromJsonTyped("""{ "Vec3f": { "x": 1 } }"""))
        assertEquals(failure, tryFromJsonTyped("""{ "Vec3f": { "x": 1, "y": 0 } }"""))

        assertEquals(FromJsonResult.Success(NormalizedVector3f(Vector3f(1f, 0f, 0f))),
                tryFromJsonTyped("""{ "Vec3f": { "x": 1, "y": 0, "z": 0 } }"""))
        assertEquals(FromJsonResult.Success(NormalizedVector3f(Vector3f(0.8f, 0.6f, 0f))),
                tryFromJsonTyped("""{ "Vec3f": { "x": 0.8, "y": 0.6, "z": 0 } }"""))

        // If the vector is not normalized, it should get normalized
        assertEquals(FromJsonResult.Success(NormalizedVector3f(Vector3f(1f, 0f, 0f))),
                tryFromJsonTyped("""{ "Vec3f": { "x": 2, "y": 0, "z": 0 } }"""))
        assertEquals(FromJsonResult.Success(NormalizedVector3f(Vector3f(0f, 0.6f, 0.8f))),
                tryFromJsonTyped("""{ "Vec3f": { "x": 0, "y": 1.2, "z": 1.6 } }"""))

        // A zero length vector should be replaced with null
        assertEquals(FromJsonResult.Success(null),
                tryFromJsonTyped("""{ "Vec3f": { "x": 0, "y": 0, "z": 0 } }"""))

        // Wrong types
        assertEquals(failure, tryFromJsonTyped("""{ "Vec3f": "" }"""))
        assertEquals(failure, tryFromJsonTyped("""{ "Vec3f": { "x": true, "y": 0, "z": 0 } }"""))
    }

    private fun tryFromJsonTyped(jsonString: String): FromJsonResult<out NormalizedVector3f?> {
        val jsonObject = parseJson(jsonString)
        return descriptor.fromJsonTyped(jsonObject)
    }
}