package ntfwc.mod.multiplayer.model.array.type

import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNull
import ntfwc.mod.multiplayer.model.array.type.DescriptorTestCommon.tryFromJsonForNonNullableValue
import ntfwc.mod.multiplayer.model.avatar.array.type.UnitIntervalFloatItemDescriptor
import ntfwc.mod.multiplayer.util.UnitIntervalFloat
import org.junit.Test

class UnitIntervalFloatItemDescriptorTest {
    private val descriptor = UnitIntervalFloatItemDescriptor("unitFloat")

    @Test
    fun testFromJsonTyped() {
        assertNull(tryFromJsonTyped("{}"))

        assertEquals(UnitIntervalFloat(0f), tryFromJsonTyped("""{ "unitFloat": 0 }"""))
        assertEquals(UnitIntervalFloat(1f), tryFromJsonTyped("""{ "unitFloat": 1 }"""))
        assertEquals(UnitIntervalFloat(0.1f), tryFromJsonTyped("""{ "unitFloat": 0.1 }"""))
        assertEquals(UnitIntervalFloat(0.34f), tryFromJsonTyped("""{ "unitFloat": 0.34 }"""))
        assertEquals(UnitIntervalFloat(0.99f), tryFromJsonTyped("""{ "unitFloat": 0.99 }"""))

        // Values outside the unit interval should be clamped to the interval
        assertEquals(UnitIntervalFloat(1f), tryFromJsonTyped("""{ "unitFloat": 1.2 }"""))
        assertEquals(UnitIntervalFloat(0f), tryFromJsonTyped("""{ "unitFloat": -1 }"""))

        // Wrong type
        assertNull(tryFromJsonTyped("""{ "unitFloat": false }"""))
    }

    private fun tryFromJsonTyped(jsonString: String): UnitIntervalFloat? {
        return tryFromJsonForNonNullableValue(jsonString, descriptor::fromJsonTyped)
    }
}