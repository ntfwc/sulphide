package ntfwc.mod.multiplayer.model

import game.Pony
import ntfwc.mod.multiplayer.model.avatar.AnimationId
import ntfwc.mod.multiplayer.model.avatar.NetPlayerAnimationState
import ntfwc.mod.multiplayer.model.avatar.NetPlayerAppearanceState
import ntfwc.mod.multiplayer.model.avatar.PlayerArmoredState
import ntfwc.mod.multiplayer.util.Color3f
import ntfwc.mod.multiplayer.util.RadianAngle
import ntfwc.mod.multiplayer.util.UnitIntervalFloat
import ntfwc.mod.multiplayer.util.ValidatedColor3f
import ntfwc.mod.multiplayer.util.quaternion.NormalizedQuaternion
import ntfwc.mod.multiplayer.util.quaternion.Quaternion
import ntfwc.mod.multiplayer.util.vector.NormalizedVector3f
import ntfwc.mod.multiplayer.util.vector.Vector3f

object ModelTestConstants {
    val APPEARANCE_STATE1 = createAppearanceState1()
    val APPEARANCE_STATE2 = createAppearanceState2()

    private fun createAppearanceState1(): NetPlayerAppearanceState {
        val animationState = NetPlayerAnimationState(
                AnimationId.RUN,
                UnitIntervalFloat.ZERO,
                UnitIntervalFloat.ZERO,
                UnitIntervalFloat.ZERO,
                UnitIntervalFloat.ZERO,
                false,
                null,
                RadianAngle(0.1f),
                UnitIntervalFloat.ZERO,
                UnitIntervalFloat.ZERO,
                true,
                UnitIntervalFloat.ZERO,
                UnitIntervalFloat.ZERO,
                UnitIntervalFloat.ZERO,
                UnitIntervalFloat.ZERO
        )

        return NetPlayerAppearanceState(
                "sulphur",
                NormalizedQuaternion(Quaternion(1.0, 0.0, 0.0, 0.0)),
                animationState,
                null,
                UnitIntervalFloat.ZERO,
                null,
                PlayerArmoredState.HOOVES,
                false,
                true,
                UnitIntervalFloat.ZERO,
                UnitIntervalFloat.ZERO,
                UnitIntervalFloat.ZERO,
                ValidatedColor3f(Color3f(1f, 0.9f, 0.5f))
        )
    }

    private fun createAppearanceState2(): NetPlayerAppearanceState {
        val animationState = NetPlayerAnimationState(
                AnimationId.FLINCH,
                UnitIntervalFloat(0.1f),
                UnitIntervalFloat(0.2f),
                UnitIntervalFloat(0.3f),
                UnitIntervalFloat(0.4f),
                false,
                NormalizedVector3f(Vector3f(0.0f, 0.8f, 0.6f)),
                RadianAngle(0.12f),
                UnitIntervalFloat(0.45f),
                UnitIntervalFloat(0.5f),
                true,
                UnitIntervalFloat(0.6f),
                UnitIntervalFloat(0.7f),
                UnitIntervalFloat(0.8f),
                UnitIntervalFloat(0.9f)
        )

        return NetPlayerAppearanceState(
                "sulphur",
                NormalizedQuaternion(Quaternion(0.8, 0.4, 0.4, 0.2)),
                animationState,
                Pony.Mood.COOL,
                UnitIntervalFloat(0.31f),
                Pony.Mood.CONFUSED,
                PlayerArmoredState.FULL,
                true,
                false,
                UnitIntervalFloat(0.32f),
                UnitIntervalFloat(0.33f),
                UnitIntervalFloat(0.34f),
                ValidatedColor3f(Color3f(0.2f, 0.3f, 0.8f))
        )
    }
}