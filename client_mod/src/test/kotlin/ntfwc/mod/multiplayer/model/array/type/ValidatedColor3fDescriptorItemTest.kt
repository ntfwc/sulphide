package ntfwc.mod.multiplayer.model.array.type

import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNull
import ntfwc.mod.multiplayer.model.array.type.DescriptorTestCommon.tryFromJsonForNonNullableValue
import ntfwc.mod.multiplayer.model.avatar.array.type.ValidatedColor3fItemDescriptor
import ntfwc.mod.multiplayer.util.Color3f
import ntfwc.mod.multiplayer.util.ValidatedColor3f
import org.junit.Test

class ValidatedColor3fDescriptorItemTest {
    private val descriptor = ValidatedColor3fItemDescriptor("color")

    @Test
    fun testFromJsonTyped() {
        assertNull(tryFromJsonTyped("{}"))
        assertNull(tryFromJsonTyped("""{ "color": { } }"""))
        assertNull(tryFromJsonTyped("""{ "color": { "red": 1 } }"""))
        assertNull(tryFromJsonTyped("""{ "color": { "red": 1, "green": 1 } }"""))

        assertEquals(ValidatedColor3f(Color3f(1f, 1f, 1f)),
                tryFromJsonTyped("""{ "color": { "red": 1, "green": 1, "blue": 1 } }"""))
        assertEquals(ValidatedColor3f(Color3f(0f, 0f, 0f)),
                tryFromJsonTyped("""{ "color": { "red": 0, "green": 0, "blue": 0 } }"""))
        assertEquals(ValidatedColor3f(Color3f(0.4f, 0.3f, 0.8f)),
                tryFromJsonTyped("""{ "color": { "red": 0.4, "green": 0.3, "blue": 0.8 } }"""))

        // Values outside the unit interval should be clamped to the interval
        assertEquals(ValidatedColor3f(Color3f(1f, 1f, 1f)),
                tryFromJsonTyped("""{ "color": { "red": 1.2, "green": 1.4, "blue": 2 } }"""))
        assertEquals(ValidatedColor3f(Color3f(0f, 0f, 0f)),
                tryFromJsonTyped("""{ "color": { "red": -1, "green": -0.3, "blue": -0.5 } }"""))
        assertEquals(ValidatedColor3f(Color3f(1f, 0.5f, 0f)),
                tryFromJsonTyped("""{ "color": { "red": 1.2, "green": 0.5, "blue": -0.2 } }"""))

        // Wrong types
        assertNull(tryFromJsonTyped("""{ "color": 0 }"""))
        assertNull(tryFromJsonTyped("""{ "color": { "red": 1, "green": 1, "blue": "eh" } }"""))
    }

    private fun tryFromJsonTyped(jsonString: String): ValidatedColor3f? {
        return tryFromJsonForNonNullableValue(jsonString, descriptor::fromJsonTyped)
    }
}