package ntfwc.mod.multiplayer.model.array.type

import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNull
import ntfwc.mod.multiplayer.model.array.type.DescriptorTestCommon.tryFromJsonForNonNullableValue
import ntfwc.mod.multiplayer.model.avatar.array.type.BooleanItemDescriptor
import org.junit.Test

class BooleanItemDescriptorTest {
    private val descriptor = BooleanItemDescriptor("someBool")

    @Test
    fun testFromJsonTyped() {
        assertNull(tryFromJsonTyped("{}"))
        assertEquals(true, tryFromJsonTyped("""{ "someBool": true }"""))
        assertEquals(false, tryFromJsonTyped("""{ "someBool": false }"""))

        // Wrong type
        assertNull(tryFromJsonTyped("""{ "someBool": "blah" }"""))
    }

    private fun tryFromJsonTyped(jsonString: String): Boolean? {
        return tryFromJsonForNonNullableValue(jsonString, descriptor::fromJsonTyped)
    }
}