package ntfwc.mod.multiplayer.model.array.type

import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNull
import ntfwc.mod.multiplayer.model.array.type.DescriptorTestCommon.tryFromJsonForNonNullableValue
import ntfwc.mod.multiplayer.model.avatar.AnimationId
import ntfwc.mod.multiplayer.model.avatar.array.AnimationIdAndFrame
import ntfwc.mod.multiplayer.model.avatar.array.type.AnimationIdAndFrameItemDescriptor
import ntfwc.mod.multiplayer.util.UnitIntervalFloat
import org.junit.Test

class AnimationIdAndFrameItemDescriptorTest {
    private val descriptor = AnimationIdAndFrameItemDescriptor("frame", "animationId")

    @Test
    fun testFromJson() {
        assertNull(tryFromJson("{}"))
        assertNull(tryFromJson("""{ "animationId": "run" }"""))
        assertNull(tryFromJson("""{ "frame": 0.2 }"""))

        assertEquals(AnimationIdAndFrame(AnimationId.RUN, UnitIntervalFloat(0.2f)), tryFromJson("""{ "animationId": "run", "frame": 0.2 }"""))

        // Capitalization should not matter in the enum name
        assertEquals(AnimationIdAndFrame(AnimationId.RUN, UnitIntervalFloat(0.2f)), tryFromJson("""{ "animationId": "Run", "frame": 0.2 }"""))
        assertEquals(AnimationIdAndFrame(AnimationId.RUN, UnitIntervalFloat(0.2f)), tryFromJson("""{ "animationId": "RUN", "frame": 0.2 }"""))

        // Other values should work
        assertEquals(AnimationIdAndFrame(AnimationId.RUN, UnitIntervalFloat(0f)), tryFromJson("""{ "animationId": "run", "frame": 0 }"""))
        assertEquals(AnimationIdAndFrame(AnimationId.RUN, UnitIntervalFloat(1f)), tryFromJson("""{ "animationId": "run", "frame": 1 }"""))
        assertEquals(AnimationIdAndFrame(AnimationId.RUN, UnitIntervalFloat(0.92f)), tryFromJson("""{ "animationId": "run", "frame": 0.92 }"""))

        assertEquals(AnimationIdAndFrame(AnimationId.ROLL, UnitIntervalFloat(0.92f)), tryFromJson("""{ "animationId": "roll", "frame": 0.92 }"""))

        // If an enum name is not found, it should default to the IDLE animation ID
        assertEquals(AnimationIdAndFrame(AnimationId.IDLE, UnitIntervalFloat(0.2f)), tryFromJson("""{ "animationId": "blah", "frame": 0.2 }"""))

        // Incorrect types
        assertNull(tryFromJson("""{ "animationId": 1, "frame": 0.2 }"""))
        assertNull(tryFromJson("""{ "animationId": "run", "frame": "thatOneFrame" }"""))
    }

    private fun tryFromJson(jsonString: String): AnimationIdAndFrame? {
        return tryFromJsonForNonNullableValue(jsonString, descriptor::fromJson)?.let { it as AnimationIdAndFrame }
    }
}