package ntfwc.mod.multiplayer.model.array.type

import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNull
import ntfwc.mod.multiplayer.model.array.type.DescriptorTestCommon.tryFromJsonForNonNullableValue
import ntfwc.mod.multiplayer.model.avatar.array.type.NormalizedQuaternionItemDescriptor
import ntfwc.mod.multiplayer.util.quaternion.NormalizedQuaternion
import ntfwc.mod.multiplayer.util.quaternion.Quaternion
import org.junit.Test

class NormalizedQuaternionItemDescriptorTest {
    private val descriptor = NormalizedQuaternionItemDescriptor("Quat")

    @Test
    fun testFromJson() {
        assertNull(tryFromJsonTyped("{}"))

        assertNull(tryFromJsonTyped("""{ "Quat": { "w": 1 } }"""))
        assertNull(tryFromJsonTyped("""{ "Quat": { "w": 1, "x": 0 } }"""))
        assertNull(tryFromJsonTyped("""{ "Quat": { "w": 1, "x": 0, "y": 0 } }"""))

        assertEquals(NormalizedQuaternion(Quaternion(1.0, 0.0, 0.0, 0.0)),
                tryFromJsonTyped("""{ "Quat": { "w": 1, "x": 0, "y": 0, "z": 0 } }"""))
        assertEquals(NormalizedQuaternion(Quaternion(0.8, 0.6, 0.0, 0.0)),
                tryFromJsonTyped("""{ "Quat": { "w": 0.8, "x": 0.6, "y": 0, "z": 0 } }"""))
        assertEquals(NormalizedQuaternion(Quaternion(0.8, 0.4, 0.4, 0.2)),
                tryFromJsonTyped("""{ "Quat": { "w": 0.8, "x": 0.4, "y": 0.4, "z": 0.2 } }"""))

        // If the quaternion is not already normalized, it should get normalized
        assertEquals(NormalizedQuaternion(Quaternion(1.0, 0.0, 0.0, 0.0)),
                tryFromJsonTyped("""{ "Quat": { "w": 2, "x": 0, "y": 0, "z": 0 } }"""))
        assertEquals(NormalizedQuaternion(Quaternion(0.8, 0.4, 0.4, 0.2)),
                tryFromJsonTyped("""{ "Quat": { "w": 1.6, "x": 0.8, "y": 0.8, "z": 0.4 } }"""))

        // A zero length quaternion should be replaced with a default value
        assertEquals(NormalizedQuaternion(Quaternion(1.0, 0.0, 0.0, 0.0)),
                tryFromJsonTyped("""{ "Quat": { "w": 0, "x": 0, "y": 0, "z": 0 } }"""))

        // Wrong type
        assertNull(tryFromJsonTyped("""{ "Quat": "" }"""))
        assertNull(tryFromJsonTyped("""{ "Quat": { "w": 1, "x": 0, "y": 0, "z": "potato" } }"""))
    }

    private fun tryFromJsonTyped(jsonString: String): NormalizedQuaternion? {
        return tryFromJsonForNonNullableValue(jsonString, descriptor::fromJsonTyped)
    }
}