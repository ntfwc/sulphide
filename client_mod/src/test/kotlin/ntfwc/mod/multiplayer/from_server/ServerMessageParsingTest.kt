package ntfwc.mod.multiplayer.from_server

import com.beust.klaxon.JsonObject
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNull
import ntfwc.mod.multiplayer.messages.from_server.IntroductionReplyMessage
import ntfwc.mod.multiplayer.messages.from_server.MapSwitchReplyMessage
import ntfwc.mod.multiplayer.messages.from_server.StateUpdateMessage
import ntfwc.mod.multiplayer.model.MessagePlayerStateList
import ntfwc.mod.multiplayer.model.ModelTestConstants
import ntfwc.mod.multiplayer.model.NamedPlayerStateList
import ntfwc.mod.multiplayer.model.PlayerPosition
import ntfwc.mod.multiplayer.model.avatar.array.NetPlayerAppearanceArrayState
import ntfwc.mod.multiplayer.model.avatar.array.PlayerArrayState
import ntfwc.mod.multiplayer.model.avatar.array.TimestampedPlayerArrayState
import ntfwc.mod.multiplayer.util.NonEmptyList
import ntfwc.mod.multiplayer.util.ParsingUtil
import org.junit.Test
import java.util.*

class ServerMessageParsingTest {
    @Test
    fun parseIntroductionResponse() {
        assertNull(parseIntroductionReply(""))
        assertNull(parseIntroductionReply("{"))
        assertNull(parseIntroductionReply("{}"))

        // Wrong field name
        assertNull(parseIntroductionReply("""{"name": "name1"}"""))

        // Wrong field type
        assertNull(parseIntroductionReply("""{"actual_username": 5}"""))

        assertEquals(IntroductionReplyMessage("name1"),
                parseIntroductionReply("""{"actual_username": "name1"}"""))

        // Extra fields should be fine
        assertEquals(IntroductionReplyMessage("name1"),
                parseIntroductionReply("""{"actual_username": "name1", "occupation": "horsebirb"}"""))

        // An empty username should be rejected
        assertNull(parseIntroductionReply("""{"actual_username": ""}"""))

        // The max length for a username should be 25
        assertEquals(IntroductionReplyMessage("1234567890123456789012345"), parseIntroductionReply("""{"actual_username": "1234567890123456789012345"}"""))
        assertNull(parseIntroductionReply("""{"actual_username": "12345678901234567890123456"}"""))

        // Username characters not handled, at least by this client version, should be replaced
        assertEquals(IntroductionReplyMessage("some?user"), parseIntroductionReply("""{"actual_username": "some\tuser"}"""))
    }

    @Test
    fun parseStateUpdate() {
        assertEquals(StateUpdateMessage(Collections.emptyList()), parseStateUpdate("""{"updates":[]}"""))
        assertEquals(StateUpdateMessage(arrayListOf(NamedPlayerStateList("joe",
                201, 2.0, 3.2, 4.5))),
                parseStateUpdate("""{"updates":[{"username": "joe",
                    | "state_list": [{"timestamp": 201,"position":{"x":2.0,"y":3.2,"z":4.5}}]
                    |  }]}""".trimMargin()))
        assertEquals(StateUpdateMessage(arrayListOf(NamedPlayerStateList("joe", 201, 2.0, 3.2, 4.5),
                NamedPlayerStateList("jane", 300, 8.0, 9.0, 10.0))),
                parseStateUpdate("""{"updates":[{"username":"joe",
                    | "state_list": [{"timestamp": 201,"position":{"x":2.0,"y":3.2,"z":4.5}}]
                    | },{"username":"jane",
                    | "state_list": [{"timestamp": 300,"position":{"x":8.0,"y":9.0,"z":10.0}}]
                    | }]}""".trimMargin()))

        // Numbers do not have to have the dot
        assertEquals(StateUpdateMessage(arrayListOf(NamedPlayerStateList("name",
                201, 2.0, 2.0, 3.0))),
                parseStateUpdate("""{"updates":[{"username":"name",
                    | "state_list": [{"timestamp": 201,"position":{"x":2,"y":2,"z":3}}]
                    | }]}""".trimMargin()))

        // Should work with the type ID there
        assertEquals(StateUpdateMessage(arrayListOf(NamedPlayerStateList("name",
                201, 2.0, 2.0, 3.0))),
                parseStateUpdate("""{"t": "stateUpdate", "updates":[{"username":"name",
                    | "state_list": [{"timestamp": 201,"position":{"x":2,"y":2,"z":3}}]
                    | }]}""".trimMargin()))

        // If the updates field has the incorrect type, it should fail to parse it
        assertNull(parseStateUpdate("""{"updates":1}"""))

        // Null items in the updates array should be handled without causing an exception
        assertNull(parseStateUpdate("""{"updates":[null]}"""))
        assertNull(parseStateUpdate("""{"updates":[{"username":"name",
            | "state_list": [{"timestamp": 201,"position":{"x":2,"y":2,"z":3}}]
            | }, null]}""".trimMargin()))

        // State lists with multiple states should be supported
        assertEquals(StateUpdateMessage(arrayListOf(NamedPlayerStateList("joe",
                MessagePlayerStateList(NonEmptyList(listOf(
                        TimestampedPlayerArrayState(201, 2.0, 3.2, 4.5),
                        TimestampedPlayerArrayState(302, 4.0, 5.0, 6.3)
                )))
        ))),
                parseStateUpdate("""{"updates":[{"username": "joe",
                    | "state_list": [{"timestamp": 201,"position":{"x":2,"y":3.2,"z":4.5}}, {"timestamp": 302,"position":{"x":4,"y":5,"z":6.3}}]
                    |  }]}""".trimMargin()))

        assertEquals(StateUpdateMessage(arrayListOf(
                NamedPlayerStateList("joe", 100, 10.0, 11.0, 12.0),
                NamedPlayerStateList("jane",
                        MessagePlayerStateList(NonEmptyList(listOf(
                                TimestampedPlayerArrayState(201, 2.0, 3.2, 4.5),
                                TimestampedPlayerArrayState(302, 4.0, 5.0, 6.3)
                        )))
                )
        )),
                parseStateUpdate("""{"updates":[
                    | {"username": "joe",
                    | "state_list": [{"timestamp": 100,"position":{"x":10.0,"y":11.0,"z":12.0}}]
                    |  },
                    | {"username": "jane",
                    | "state_list": [{"timestamp": 201,"position":{"x":2,"y":3.2,"z":4.5}}, {"timestamp": 302,"position":{"x":4,"y":5,"z":6.3}}]
                    |  }
                    |  ]}""".trimMargin()))
    }

    @Test
    fun parseStateUpdate_validation() {
        // Invalid usernames should be rejected
        assertNull(parseStateUpdate("""{"updates":[{
            | "username":"",
            | "position":{"x":2,"y":2,"z":3}
            |}]}""".trimMargin()))
        assertNull(parseStateUpdate("""{"updates":[{
            | "username":"name",
            | "position":{"x":2,"y":2,"z":3}
            | }, {
            | "username":"",
            | "position":{"x":2,"y":2,"z":3}
            | }]}""".trimMargin()))
        assertNull(parseStateUpdate("""{"updates":[{
            | "username":"123456789012345678901234567",
            | "position":{"x":2,"y":2,"z":3}
            |}]}""".trimMargin()))

        // Messages with position numbers that can't be represented as doubles should be rejected
        assertNull(parseStateUpdate("""{"updates":[{
            | "username":"name",
            | "position":{"x":2e1025,"y":2,"z":3}
            |}]}""".trimMargin()))
        assertNull(parseStateUpdate("""{"updates":[{
            | "username":"name",
            | "position":{"x":-2e1025,"y":2,"z":3}
            |}]}""".trimMargin()))
        assertNull(parseStateUpdate("""{"updates":[{
            | "username":"name",
            | "position":{"x":2,"y":2e1025,"z":3}
            |}]}""".trimMargin()))
        assertNull(parseStateUpdate("""{"updates":[{
            | "username":"name",
            | "position":{"x":2,"y":2,"z":3e1025}
            |}]}""".trimMargin()))
        assertNull(parseStateUpdate("""{"updates":[{
            | "username":"name",
            | "position":{"x":2,"y":2,"z":3}
            |}, {
            | "username":"name",
            | "position":{"x":2e1025,"y":2,"z":3}
            |}]}""".trimMargin()))
    }

    @Test
    fun parseStateUpdate_withRemovedPlayers() {
        assertEquals(StateUpdateMessage(arrayListOf(NamedPlayerStateList("joe",
                100, 2.0, 3.2, 4.5)), ArrayList()),
                parseStateUpdate("""{"updates":[{"username": "joe",
                    | "state_list": [{"timestamp": 100,"position":{"x":2.0,"y":3.2,"z":4.5}}]
                    | }],
                    | "removed_players":[]}""".trimMargin()))
        assertEquals(StateUpdateMessage(arrayListOf(NamedPlayerStateList("joe",
                100, 2.0, 3.2, 4.5)), arrayListOf("player2")),
                parseStateUpdate("""{"updates":[{"username": "joe",
                    | "state_list": [{"timestamp": 100,"position":{"x":2.0,"y":3.2,"z":4.5}}]
                    | }],
                    | "removed_players":["player2"]}""".trimMargin()))
        assertEquals(StateUpdateMessage(arrayListOf(), arrayListOf("player2")),
                parseStateUpdate("""{"removed_players":["player2"]}"""))
        assertEquals(StateUpdateMessage(arrayListOf(NamedPlayerStateList("joe",
                100, 2.0, 3.2, 4.5)), arrayListOf("player2", "player3")),
                parseStateUpdate("""{"updates":[{"username": "joe",
                    | "state_list": [{"timestamp": 100,"position":{"x":2.0,"y":3.2,"z":4.5}}]
                    | }],
                    | "removed_players":["player2", "player3"]}""".trimMargin()))
        assertEquals(StateUpdateMessage(arrayListOf(), arrayListOf("player2", "player3")),
                parseStateUpdate("""{"removed_players":["player2", "player3"]}"""))

        // If the removed_players field has the incorrect type, it should fail to parse it
        assertNull(parseStateUpdate("""{"updates":[{"username": "joe",
            | "state_list": [{"timestamp": 100,"position":{"x":2.0,"y":3.2,"z":4.5}}]
            | }],
            | "removed_players":1}""".trimMargin()))

        // If the list entries are the incorrect type, it should fail to parse it
        assertNull(parseStateUpdate("""{"updates":[{"username": "joe",
            | "state_list": [{"timestamp": 100,"position":{"x":2.0,"y":3.2,"z":4.5}}]
            | }],
            | "removed_players":[1]}""".trimMargin()))

        // Invalid usernames, in the removed list, should be rejected
        assertNull(parseStateUpdate("""{"removed_players":[""]}"""))
        assertNull(parseStateUpdate("""{"removed_players":["player2", ""]}"""))

        // Username characters not handled, at least by this client version, should be replaced
        assertEquals(StateUpdateMessage(arrayListOf(), arrayListOf("some\ufffduser")), parseStateUpdate("""{"removed_players":["some\tuser"]}"""))
    }

    @Test
    fun parseStateUpdate_emptyMessage() {
        // A completely empty message, or one with just the type ID should be fine
        assertEquals(StateUpdateMessage(Collections.emptyList(), Collections.emptyList()),
                parseStateUpdate("{}"))
        assertEquals(StateUpdateMessage(Collections.emptyList(), Collections.emptyList()),
                parseStateUpdate("""{"t": "stateUpdate"}"""))
    }

    @Test
    fun parseStateUpdate_withAppearanceState() {
        assertEquals(StateUpdateMessage(arrayListOf(NamedPlayerStateList("joe",
                MessagePlayerStateList(NonEmptyList(listOf(
                        TimestampedPlayerArrayState(201, PlayerArrayState(PlayerPosition(2.0, 3.2, 4.5),
                                NetPlayerAppearanceArrayState.from(ModelTestConstants.APPEARANCE_STATE1)))
                )))))),
                parseStateUpdate("""{"updates":[{"username": "joe",
                    | "state_list": [{
                    | "timestamp": 201,
                    | "position":{"x":2.0,"y":3.2,"z":4.5}
                    | "appearanceState":${getAppearanceState1Json()}
                    | }]
                    |  }]}""".trimMargin()))

        assertEquals(StateUpdateMessage(arrayListOf(NamedPlayerStateList("joe",
                MessagePlayerStateList(NonEmptyList(listOf(
                        TimestampedPlayerArrayState(201, PlayerArrayState(PlayerPosition(2.0, 3.2, 4.5),
                                NetPlayerAppearanceArrayState.from(ModelTestConstants.APPEARANCE_STATE1))),
                        TimestampedPlayerArrayState(423, PlayerArrayState(PlayerPosition(4.0, 5.0, 6.3),
                                NetPlayerAppearanceArrayState.from(ModelTestConstants.APPEARANCE_STATE2)))
                )))))),
                parseStateUpdate("""{"updates":[{"username": "joe",
                    | "state_list": [{
                    | "timestamp": 201,
                    | "position":{"x":2.0,"y":3.2,"z":4.5}
                    | "appearanceState":${getAppearanceState1Json()}
                    | },{
                    | "timestamp": 423,
                    | "position":{"x":4,"y":5,"z":6.3}
                    | "appearanceState":${getAppearanceState2Json()}
                    | }]
                    |  }]}""".trimMargin()))
    }

    @Test
    fun parseMapSwitchReply() {
        assertNull(parseMapSwitchReply("{}"))

        assertEquals(MapSwitchReplyMessage(Collections.emptyList()), parseMapSwitchReply("""{"player_state_lists": []}"""))
        assertEquals(MapSwitchReplyMessage(arrayListOf(NamedPlayerStateList("joe",
                201, 1.0, 2.5, 5.3))), parseMapSwitchReply("""{"player_state_lists": [{"username": "joe",
                    | "state_list": [{"timestamp": 201, "position":{"x": 1, "y": 2.5, "z": 5.3}}]
                    | }]}""".trimMargin()))
        assertEquals(MapSwitchReplyMessage(arrayListOf(NamedPlayerStateList("joe",
                201, 1.0, 2.5, 5.3), NamedPlayerStateList("jane",
                300, 4.2, 5.0, 8.6))), parseMapSwitchReply("""{"player_state_lists":
                    | [{"username": "joe",
                    | "state_list": [{"timestamp": 201, "position":{"x": 1, "y": 2.5, "z": 5.3}}]
                    | }, {"username": "jane",
                    | "state_list": [{"timestamp": 300, "position":{"x": 4.2, "y": 5, "z": 8.6}}]
                    | }]}""".trimMargin()))

        // Should work with the type ID there
        assertEquals(MapSwitchReplyMessage(arrayListOf(NamedPlayerStateList("joe",
                201, 1.0, 2.5, 5.3))), parseMapSwitchReply("""{"t": "mapSwitchReply",
                    | "player_state_lists": [{"username": "joe",
                    | "state_list": [{"timestamp": 201, "position":{"x": 1, "y": 2.5, "z": 5.3}}]
                    | }]}""".trimMargin()))

        // State lists with multiple states should work
        assertEquals(MapSwitchReplyMessage(arrayListOf(NamedPlayerStateList("joe",
                createStateList(TimestampedPlayerArrayState(201, 1.0, 2.5, 5.3),
                        TimestampedPlayerArrayState(302, 4.2, 5.0, 6.0))))), parseMapSwitchReply("""{"player_state_lists": [{"username": "joe",
                    | "state_list": [
                    | {"timestamp": 201, "position":{"x": 1, "y": 2.5, "z": 5.3}},
                    | {"timestamp": 302, "position":{"x": 4.2, "y": 5, "z": 6}}]
                    | }]}""".trimMargin()))
    }

    @Test
    fun parseMapSwitchReply_validation() {
        // Invalid usernames should be rejected
        assertNull(parseMapSwitchReply("""{"player_state_lists": [{"username": "",
            | "state_list": [{"timestamp": 201, "x": 1, "y": 2.5, "z": 5.3}]
            | }]}""".trimMargin()))
        assertNull(parseMapSwitchReply("""{"player_state_lists": [{"username": "name",
            | "state_list": [{"timestamp": 201, "x": 1, "y": 2.5, "z": 5.3}]
            | }, {"username": "",
            | "state_list": [{"timestamp": 201, "x": 1, "y": 2.5, "z": 5.3}]
            | }]}""".trimMargin()))

        // Position numbers that cannot be represented as doubles should be rejected
        assertNull(parseMapSwitchReply("""{"player_state_lists": [{"username": "name",
            | "state_list": [{"timestamp": 201, "x": 1e1025, "y": 2.5, "z": 5.3}]
            | }]}""".trimMargin()))
        assertNull(parseMapSwitchReply("""{"player_state_lists": [{"username": "name",
            | "state_list": [{"timestamp": 201, "x": -1e1025, "y": 2.5, "z": 5.3}]
            | }]}""".trimMargin()))
        assertNull(parseMapSwitchReply("""{"player_state_lists": [{"username": "name",
            | "state_list": [{"timestamp": 201, "x": 1, "y": 2.5e1025, "z": 5.3}]
            | }]}""".trimMargin()))
        assertNull(parseMapSwitchReply("""{"player_state_lists": [{"username": "name",
            | "state_list": [{"timestamp": 201, "x": 1, "y": 2.5, "z": 5.3e1025}]
            | }]}""".trimMargin()))
    }

    @Test
    fun parseMapSwitchReply_withAppearanceState() {
        assertEquals(MapSwitchReplyMessage(arrayListOf(NamedPlayerStateList("joe",
                MessagePlayerStateList(NonEmptyList(listOf(
                        TimestampedPlayerArrayState(201, PlayerArrayState(PlayerPosition(1.0, 2.5, 5.3),
                                NetPlayerAppearanceArrayState.from(ModelTestConstants.APPEARANCE_STATE1)))
                )))))), parseMapSwitchReply("""{"player_state_lists": [{"username": "joe",
                    | "state_list": [{
                    | "timestamp": 201, 
                    | "position":{"x": 1, "y": 2.5, "z": 5.3}
                    | "appearanceState":${getAppearanceState1Json()}
                    | }]
                    | }]}""".trimMargin()))
        assertEquals(MapSwitchReplyMessage(arrayListOf(NamedPlayerStateList("joe",
                MessagePlayerStateList(NonEmptyList(listOf(
                        TimestampedPlayerArrayState(201, PlayerArrayState(PlayerPosition(1.0, 2.5, 5.3),
                                NetPlayerAppearanceArrayState.from(ModelTestConstants.APPEARANCE_STATE1))),
                        TimestampedPlayerArrayState(423, PlayerArrayState(PlayerPosition(4.2, 5.0, 6.0),
                                NetPlayerAppearanceArrayState.from(ModelTestConstants.APPEARANCE_STATE2)))
                )))))), parseMapSwitchReply("""{"player_state_lists": [{"username": "joe",
                    | "state_list": [{
                    | "timestamp": 201, 
                    | "position":{"x": 1, "y": 2.5, "z": 5.3}
                    | "appearanceState":${getAppearanceState1Json()}
                    | },{
                    | "timestamp": 423,
                    | "position":{"x": 4.2, "y": 5, "z": 6}
                    | "appearanceState":${getAppearanceState2Json()}
                    | }]
                    | }]}""".trimMargin()))
    }

    private fun parseIntroductionReply(jsonString: String): IntroductionReplyMessage? {
        val parsedJson = ParsingUtil.parseJsonObject(jsonString) ?: return null
        return IntroductionReplyMessage.fromJson(parsedJson)
    }

    private fun parseStateUpdate(jsonString: String): StateUpdateMessage? {
        return StateUpdateMessage.fromJson(jsonObjectFromString(jsonString))
    }

    private fun parseMapSwitchReply(jsonString: String): MapSwitchReplyMessage? {
        return MapSwitchReplyMessage.fromJson(jsonObjectFromString(jsonString))
    }

    private fun jsonObjectFromString(jsonString: String): JsonObject {
        return ParsingUtil.parseJsonObject(jsonString) ?: throw IllegalStateException("Given invalid json")
    }

    // A factory method for StateUpdateMessage that matches the old constructor
    private fun StateUpdateMessage(updates: List<NamedPlayerStateList>) = StateUpdateMessage(updates, Collections.emptyList())

    private fun NamedPlayerStateList(username: String, timestamp: Long, x: Double, y: Double, z: Double): NamedPlayerStateList {
        return NamedPlayerStateList(username, messagePlayerStateListFromSingleState(TimestampedPlayerArrayState(timestamp, x, y, z)))
    }

    private fun messagePlayerStateListFromSingleState(state: TimestampedPlayerArrayState): MessagePlayerStateList = MessagePlayerStateList(NonEmptyList(listOf(state)))

    private fun TimestampedPlayerArrayState(timestamp: Long, x: Double, y: Double, z: Double): TimestampedPlayerArrayState {
        return TimestampedPlayerArrayState(timestamp, PlayerArrayState(PlayerPosition(x, y, z), null))
    }

    private fun createStateList(state1: TimestampedPlayerArrayState, state2: TimestampedPlayerArrayState): MessagePlayerStateList {
        return MessagePlayerStateList(NonEmptyList(listOf(state1, state2)))
    }

    private fun getAppearanceState1Json(): String {
        return NetPlayerAppearanceArrayState.from(ModelTestConstants.APPEARANCE_STATE1).toJsonObject().toJsonString()
    }

    private fun getAppearanceState2Json(): String {
        return NetPlayerAppearanceArrayState.from(ModelTestConstants.APPEARANCE_STATE2).toJsonObject().toJsonString()
    }
}