package ntfwc.mod.multiplayer.menu.entry.screen

import junit.framework.TestCase.assertFalse
import junit.framework.TestCase.assertTrue
import ntfwc.mod.multiplayer.model.Configuration
import org.junit.Test

class HostValidationTest {

    @Test
    fun testValidateHost_domainNames() {
        assertTrue(validateHost("l"))
        assertTrue(validateHost("localhost"))
        assertTrue(validateHost("example.com"))
        assertTrue(validateHost("www.example.com"))
        assertTrue(validateHost("www.example-1.com"))
        // Strictly speaking, underscore is not allowed for hostnames, but it is allowed in DNS records
        assertTrue(validateHost("www.example_1.com"))

        // Addresses that look similar to IPv4, but don't quite match the pattern, can be valid domain names
        assertTrue(validateHost("127.0.0.notipv4"))
        assertTrue(validateHost("127.0.0.999.com"))
        assertTrue(validateHost("127.0.0.999com"))

        assertFalse(validateHost("-example"))
        assertFalse(validateHost("-example.com"))
        assertFalse(validateHost("example.-com"))

        assertFalse(validateHost("example-"))
        assertFalse(validateHost("example-.com"))

        // Really long hostnames
        assertTrue(validateHost("a".repeat(63)))
        assertFalse(validateHost("a".repeat(64)))
        assertTrue(validateHost("a".repeat(63) + ".com"))
        assertTrue(validateHost("www." + "a".repeat(63)))
        assertFalse(validateHost("www." + "a".repeat(64)))
    }


    @Test
    fun testValidateHost_IPv4() {
        assertTrue(validateHost("127.0.0.1"))
        assertTrue(validateHost("192.168.112.111"))

        assertFalse(validateHost("127.0.0."))

        // Decimals outside the valid byte number size should be rejected
        assertTrue(validateHost("192.168.112.255"))
        assertFalse(validateHost("192.168.112.256"))
        assertFalse(validateHost("256.168.112.111"))
        assertTrue(validateHost("192.168.112.99"))
        assertFalse(validateHost("192.168.112.999"))
    }

    @Test
    fun testValidateHost_IPv6() {
        assertTrue(validateHost("0:0:0:0:0:0:0:0"))
        assertTrue(validateHost("::"))
        assertTrue(validateHost("::1"))
        assertTrue(validateHost("2001:0db8:85a3:0000:0000:8a2e:0370:7334"))
        assertTrue(validateHost("2001:db8:a::123"))
        assertTrue(validateHost("fe80::1ff:fe23:4567:890a%eth2"))
        assertTrue(validateHost("fe80::1ff:fe23:4567:890a%3"))
        assertTrue(validateHost("::ffff:255.255.255.255"))
        assertTrue(validateHost("::ffff:0.0.0.0"))
        assertTrue(validateHost("64:ff9b::255.255.255.255"))

        assertFalse(validateHost("0:0:0:0:0:0:0:0:0"))
        assertFalse(validateHost("20010:0db8:85a3:0000:0000:8a2e:0370:7334"))
        assertFalse(validateHost("2001:0db80:85a3:0000:0000:8a2e:0370:7334"))
        assertFalse(validateHost("2001:0dg8:85a3:0000:0000:8a2e:0370:7334"))
    }

    @Test
    fun testValidateHost_empty() {
        // Empty means the user wants to clear the hostname, this is ok
        assertTrue(validateHost(""))
    }

    private fun validateHost(host: String) = !Configuration.isInvalidHost(host)
}