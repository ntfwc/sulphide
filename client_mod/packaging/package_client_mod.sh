#!/bin/sh
set -eu

if [ $# -ne 1 ]
then
	echo "Usage: $0 version"
	exit 1
fi

VERSION=$1

SCRIPT_DIR=$(dirname "$0")
cd "$SCRIPT_DIR/.."

./gradlew shadowJar

TEMP_DIR=$(mktemp -d /dev/shm/zip_dir.XXXXXX)
finish()
{
	echo "Cleaning up..."
	rm -r "$TEMP_DIR"
}
trap finish EXIT

mkdir "$TEMP_DIR/mod"
cp "build/libs/sulphide-mod-all.jar" "$TEMP_DIR/mod/sulphide-mod.jar"

mkdir "$TEMP_DIR/mod/sulphide"
cp -r "res" "$TEMP_DIR/mod/sulphide/"
cp "packaging/res/menu_button_mapping.txt" "$TEMP_DIR/mod/sulphide/"

echo "$VERSION" > "$TEMP_DIR/mod/sulphide/version.txt"

# Add the documentation and license
cp "packaging/docs/README.txt" "$TEMP_DIR/mod/sulphide/"

mkdir "$TEMP_DIR/mod/sulphide/docs"
cp "../docs/menu_button_mapping.md" "$TEMP_DIR/mod/sulphide/docs/menu_button_mapping.txt"

cp "../LICENSE" "$TEMP_DIR/mod/sulphide/LICENSE.txt"
cp "../AUTHORS" "$TEMP_DIR/mod/sulphide/AUTHORS.txt"

cd "$TEMP_DIR"
OUTPUT_FILE="/tmp/sulphide_client_mod_$VERSION.zip"
if [ -e "$OUTPUT_FILE" ]
then
        # If the output file exists already, remove it so the zip command writes
        # a new file instead of trying to update an existing one.
	rm "$OUTPUT_FILE"
fi

zip -rv "$OUTPUT_FILE" .
echo "Done writing to '$OUTPUT_FILE'"
