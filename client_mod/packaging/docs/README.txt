# Description

This is the Sulphide multiplayer mod for "Sulphur Nimbus: Hel's Elixir".
Mod link: https://gitlab.com/ntfwc/sulphide

# Required Java Version

* You must have a Java JVM with at least version SE 7
  * This is different from the game, which only requires SE 6

# Installation

This mod should be in the "mod" folder under the game's root directory, which
should be the directory that contains "Sulphur_Nimbus.jar" and the "res"
folder.
The mod directory should look like this:

mod/
|
+-- sulphide-mod.jar
+-- sulphide/

# How to use the mod

Just run the game as you normally would (there is no special launcher required) and it will load the mod.

The mod adds a menu that is accessible when you are in-game (not from the main
menu). The first time you run the game with the mod and start playing, it
should display a message, at the top of the screen, about how to open the menu.
The menu can be opened from the pause menu by going to the "mod" submenu and
the "sulphide" menu below that. There is also a shortcut that can be used to
open the menu. This will be back slash (\\) by default.

The mod's menu lets you configure the server to connect to, by its host and
port, and to set a username. Selecting each of these fields opens an editor,
which allows you to enter a value. You can use your physical keyboard or the
on-screen keyboard. Pasting values from the clipboard is also supported.

If the value you enter in the editor is invalid, then the "Ok" button will be disabled.
* The host field supports IPv4 addresses, IPv6 addresses, and hostnames
* The port field supports any valid port number, except 0
* The username field supports all characters the font is able to display, which includes Latin, Greek, and Cyrillic characters

Note: If the username is taken on the server, the server will append a number
to give you a unique username. For example, if "player" is taken, then you may
get "player_1", and if that is taken, you might get "player_2", and so on. Your
username on the server will be shown in the menu.

Once that is all configured, the connect button will be enabled, and you can
attempt to connect. Once you have connected, and close the menu, you should be
able to see other players, which are on that server. If you lose connection, it
will try to automatically reconnect.

You can use the disconnect button to disconnect from the server. This also
stops reconnection attempts.

Note: If you change the username, or the other connection settings, you can
simply select the connect button again to reset the connection and use the new
settings. You don't have to select the disconnect button beforehand.

If you want to automatically connect to the configured server whenever you run
the game, you can enable the auto connect option.

## Player radar

There are option fields for a player radar, which, if enabled, is shown when
you are connected/connecting to a server. It displays the relative positions of
other players as dots. The relative height of other players is indicated by the
dot color. The color for a dot at the same height is white, and the colors for
players above and below are configurable. The color will gradually shift to the
configured color as the height difference increases.

## Player avatars and performance

Drawing lots of other player avatars can reduce the framerate. There is a
performance configuration option to specify the max number of avatars to
display for other players. This is set to 5 by default. If there are more other
players than the number given, then only the closest players will have avatars
and the rest will be drawn as orbs.

## A note on custom disguises

A custom disguise will only show up for another player if both people have the
disguise installed. Otherwise the player's avatar will be the skeleton with
"Missing Disguise" text above it.

# Configuration files

All configuration files end up in the "mod/sulphide/" directory. The connection
settings are put in a "config.json" file.

## Remapping the menu shortcut button

See the guide in docs/menu_button_mapping.md.

# Licensing note

"LICENSE.txt" applies to most of the code. "LICENSE.PlayerAvatar.txt" applies
specifically to the "PlayerAvatar" class.
