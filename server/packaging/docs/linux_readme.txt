# Description

This is the server program for the Sulphide basic multiplayer mod of "Sulphur Nimbus: Hel's Elixir".
Game link: https://oddwarg.itch.io/sulphur-nimbus-hels-elixir
Mod link: https://gitlab.com/ntfwc/sulphide

# Running on Linux

## Simple method

There are two scripts included to run it, which you can execute without parameters. They are called "run.sh" and "run_ipv4.sh". Most of the time, running "run.sh" should be sufficient, but if you find that IPv4 connections are not working, and you need those, then you can use "run_ipv4.sh" instead.

To shutdown the server, hold "Ctrl" and press "c".

## Advanced method

You can run "sulphide_server" directly. Running it without parameters will show you the help page with the available options. This way you can set any bind address and port you want, and there are some other options you can set.
