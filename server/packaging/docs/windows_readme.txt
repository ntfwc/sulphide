# Description

This is the server program for the Sulphide basic multiplayer mod of "Sulphur Nimbus: Hel's Elixir".
Game link: https://oddwarg.itch.io/sulphur-nimbus-hels-elixir
Mod link: https://gitlab.com/ntfwc/sulphide

# Running on Windows

## Simple method

There are two batch files included to run it, which you should be able to double-click on to run. They are called "run.bat" and "run_ipv6.bat". Use "run.bat" if your address is IPv4, which should be the case most of the time, and use "run_ipv6.bat" if your address is IPv6.

When you try to run the batch file, Windows may ask you for permissions for running the batch file or for `sulphide_server.exe` to open a port. Select the option to allow, and the server should start.

To shutdown the server, you can close the window. You can also, with the window selected, hold "Ctrl", and press "c".

## Advanced method

You can run "sulphide_server.exe" directly, on the command line. Running it without parameters will show you the help page with the available options. This way you can set any bind address and port you want, and there are some other options you can set.
