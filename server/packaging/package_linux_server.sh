#!/bin/sh
set -eu

if [ $# -ne 1 ]
then
	echo "Usage: $0 version"
	exit 1
fi

VERSION=$1

SCRIPT_DIR=$(dirname "$0")
cd "$SCRIPT_DIR/.."

cargo build --release

TEMP_DIR=$(mktemp -d /dev/shm/zip_dir.XXXXXX)
finish()
{
	echo "Cleaning up..."
	rm -r "$TEMP_DIR"
}
trap finish EXIT

mkdir "$TEMP_DIR/sulphide_server"
cp "target/release/sulphide_server" "$TEMP_DIR/sulphide_server"
cp packaging/linux_scripts/* "$TEMP_DIR/sulphide_server"

echo "$VERSION" > "$TEMP_DIR/sulphide_server/version.txt"

# Add documentation, license, and authors files
cp "packaging/docs/linux_readme.txt" "$TEMP_DIR/sulphide_server/README.txt"
cp "../LICENSE" "$TEMP_DIR/sulphide_server/"
cp "../AUTHORS" "$TEMP_DIR/sulphide_server/"

cd "$TEMP_DIR"
OUTPUT_FILE="/tmp/sulphide_server_${VERSION}_linux_x86_64.tar.xz"
[ -e "$OUTPUT_FILE" ] && rm "$OUTPUT_FILE"

tar -cJvf "$OUTPUT_FILE" .
echo "Done writing to '$OUTPUT_FILE'"
