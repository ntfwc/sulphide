#!/bin/sh
# Runs the server and binds to the IPv6 default address. On most Linux systems
# this should accept IPv4 connections as well.
set -e
SCRIPT_DIR=$(dirname "$0")

"$SCRIPT_DIR/sulphide_server" --bind-address "::" 9099
