#!/bin/sh
# Runs the server and binds to the IPv4 default address
set -e
SCRIPT_DIR=$(dirname "$0")

"$SCRIPT_DIR/sulphide_server" 9099
