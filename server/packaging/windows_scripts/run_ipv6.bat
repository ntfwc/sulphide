@ECHO off
REM Runs the server and binds to the IPv6 default address. On Windows, this
REM does not accept IPv4 connections.

sulphide_server.exe --bind-address "::" 9099
PAUSE
