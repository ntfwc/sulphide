#!/bin/sh
set -eu

if [ $# -ne 1 ]
then
	echo "Usage: $0 version"
	exit 1
fi

VERSION=$1

SCRIPT_DIR=$(dirname "$0")
cd "$SCRIPT_DIR/.."

cross_compile_scripts/build_for_windows.sh

TEMP_DIR=$(mktemp -d /dev/shm/zip_dir.XXXXXX)
finish()
{
	echo "Cleaning up..."
	rm -r "$TEMP_DIR"
}
trap finish EXIT

mkdir "$TEMP_DIR/sulphide_server"
cp "target/x86_64-pc-windows-gnu/release/sulphide_server.exe" "$TEMP_DIR/sulphide_server"
cp packaging/windows_scripts/* "$TEMP_DIR/sulphide_server"

echo "$VERSION" > "$TEMP_DIR/sulphide_server/version.txt"

# Add documentation, license, and authors files
cp "packaging/docs/windows_readme.txt" "$TEMP_DIR/sulphide_server/README.txt"
cp "../LICENSE" "$TEMP_DIR/sulphide_server/LICENSE.txt"
cp "../AUTHORS" "$TEMP_DIR/sulphide_server/"

cd "$TEMP_DIR"
OUTPUT_FILE="/tmp/sulphide_server_${VERSION}_windows_x86_64.zip"
if [ -e "$OUTPUT_FILE" ]
then
        # If the output file exists already, remove it so the zip command writes
        # a new file instead of trying to update an existing one.
	rm "$OUTPUT_FILE"
fi

zip -rv "$OUTPUT_FILE" .
echo "Done writing to '$OUTPUT_FILE'"
