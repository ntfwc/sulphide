extern crate sulphide_server;

#[macro_use]
extern crate bencher;

use sulphide_server::messages_from_server::*;
use sulphide_server::model::main::*;
use sulphide_server::model::player_array_state::PlayerAppearanceStateArray;
use sulphide_server::model::player_array_state::TimestampedPlayerArrayState;
use sulphide_server::model::shared_state_model::*;
use sulphide_server::shared_state::*;

use std::sync::Arc;

use bencher::Bencher;

fn get_map_name1() -> Arc<MapName> {
    Arc::new(MapName("map1".into()))
}

fn bench_get_states(bench: &mut Bencher) {
    let state_manager = SharedStateManager::new();
    let user_count = 1000;
    let _registrations = register_users(&state_manager, user_count);
    bench.iter(|| get_states_for_users(&state_manager, user_count));
}

fn bench_update_positions(bench: &mut Bencher) {
    let state_manager = SharedStateManager::new();
    let user_count = 100;
    let registrations = register_users(&state_manager, user_count);
    bench.iter(|| update_user_positions(&state_manager, &registrations));
}

fn register_users(state_manager: &SharedStateManager, user_count: u32) -> Vec<PlayerRegistration> {
    let mut registrations = Vec::<PlayerRegistration>::with_capacity(user_count as usize);

    let map_name1 = get_map_name1();
    let change_listener_container =
        ChangeListenerContainer::new(Arc::new(|_| {}), Arc::new(|_| {}));

    for i in 0..user_count {
        let player_name = format!("user {}", i);
        let state = TimestampedPlayerArrayState::without_appearance(1024, i as f64, 2.5, 3.2);
        let (registration, _) = state_manager
            .register_player(
                player_name,
                clone_value(&map_name1),
                state,
                change_listener_container.clone(),
                1000,
            )
            .unwrap();
        registrations.push(registration);
    }
    registrations
}

fn get_states_for_users(state_manager: &SharedStateManager, user_count: u32) {
    let mut _sent_state_count = 0u32;
    let map_name1 = get_map_name1();
    for _ in 0..user_count {
        _sent_state_count += state_manager.get_player_state_lists(&map_name1).len() as u32;
    }
}

fn update_user_positions(state_manager: &SharedStateManager, registrations: &[PlayerRegistration]) {
    for (iteration, registration) in registrations.iter().enumerate() {
        update_player_position(
            state_manager,
            &registration,
            TimestampedPlayerArrayState::without_appearance(1022, 0.0, 1.0 * iteration as f64, 2.0),
        );
    }
}

fn update_player_position(
    shared_state_manager: &SharedStateManager,
    registration: &PlayerRegistration,
    state: TimestampedPlayerArrayState,
) {
    let state_list = PlayerStateList::from_single_state(state);
    shared_state_manager.update_state_list(registration, state_list);
}

fn bench_update_message_to_json(bench: &mut Bencher) {
    let state_lists = Arc::new(create_player_state_lists(25));
    bench.iter(|| update_message_to_json_multiple_times(state_lists.clone(), 25));
}

fn update_message_to_json_multiple_times(state_lists: Arc<Vec<NamedPlayerStateList>>, count: u32) {
    let mut byte_count: usize = 0;
    for _ in 0..count {
        let message = StateUpdateMessage::new((*state_lists).clone(), vec![]);
        byte_count += message.to_json().as_bytes().len();
    }
    println!("Created {} bytes of JSON", byte_count);
}

fn create_player_state_lists(update_count: i32) -> Vec<NamedPlayerStateList> {
    let mut updates_vec = Vec::with_capacity(update_count as usize);
    for i in 0..update_count {
        updates_vec.push(create_player_state_list(i));
    }
    updates_vec
}

fn create_player_state_list(i: i32) -> NamedPlayerStateList {
    let first_x = 50.0 * i as f64;
    let state_list = NonEmptyVec::new(
        create_player_state(first_x),
        vec![
            create_player_state(first_x + 1.0),
            create_player_state(first_x + 2.0),
            create_player_state(first_x + 3.0),
            create_player_state(first_x + 4.0),
            create_player_state(first_x + 5.0),
        ],
    );
    let player_state_list = PlayerStateList::new(MessagePlayerStateList::new(state_list));
    NamedPlayerStateList::new(format!("player{}", i), player_state_list)
}

fn create_player_state(x: f64) -> TimestampedPlayerArrayState {
    TimestampedPlayerArrayState {
        timestamp: 1000,
        position: PlayerPosition::new(x, 10.0, 20.0),
        appearance_state: Some(create_appearance_state1()),
    }
}

const APPEARANCE_STATE1_JSON: &str = r#"{
                "disguise":"sulphur",
                "rotation":{"w":1.0,"x":0.0,"y":0.0,"z":0.0},
                "nextMoodMultiplier":0.0,
                "armoredState":"hooves",
                "melindaIsRiding":false,
                "hasInfiniteFlap":true,
                "druggedness":0.0,
                "bloody":0.0,
                "glowyTimer":0.0,
                "glowyColor":{"red":1.0, "green":0.9, "blue":0.5},
                "animationState":{
                    "frame":0.0,
                    "animationId":"run",
                    "wingFrame":0.0,
                    "wingExtend":0.0,
                    "wingInterp":0.0,
                    "mirror":false,
                    "rollAmount":0.1,
                    "dashTimer":0.0,
                    "sideSkidFader":0.0,
                    "flapping":true,
                    "pitch":0.0,
                    "roll":0.0,
                    "yaw":0.0,
                    "forwardValue":0.0}
                }"#;

pub fn create_appearance_state1() -> PlayerAppearanceStateArray {
    parse_appearance_state_or_fail(APPEARANCE_STATE1_JSON)
}

fn parse_appearance_state_or_fail(json_string: &str) -> PlayerAppearanceStateArray {
    let parsed_json = json::parse(json_string).unwrap();
    PlayerAppearanceStateArray::from_json_object(&parsed_json).unwrap()
}

benchmark_group!(
    benches,
    bench_get_states,
    bench_update_positions,
    bench_update_message_to_json
);
benchmark_main!(benches);
