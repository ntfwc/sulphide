#!/bin/sh
SCRIPT_DIR=$(dirname "$0")
cd "$SCRIPT_DIR"

while true
do
	find src/ -name "*.rs" | entr -d sh -c "cargo clippy && cargo build"
done
