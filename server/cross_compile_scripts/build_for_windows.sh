#!/bin/sh
SCRIPT_DIR=$(dirname "$0")
cd "$SCRIPT_DIR/.."

cargo build --target x86_64-pc-windows-gnu --release
