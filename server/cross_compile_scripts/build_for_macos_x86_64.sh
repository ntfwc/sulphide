#!/bin/sh
SCRIPT_DIR=$(dirname "$0")
SCRIPT_DIR=$(readlink -f "$SCRIPT_DIR")
cd "$SCRIPT_DIR/.."

if ! which zig >/dev/null
then
  echo "The zig compiler is not in the path. This is required for cross compiling to MacOS."
  echo "Zig download link: https://ziglang.org/download/"
  exit 1
fi

export CARGO_TARGET_X86_64_APPLE_DARWIN_LINKER="$SCRIPT_DIR/zig/zigcc_x86_64_macos"
cargo build --target x86_64-apple-darwin --release
