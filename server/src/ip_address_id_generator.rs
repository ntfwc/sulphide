use rand::rngs::OsRng;
use rand::RngCore;
use sha2::{Digest, Sha256};
use std::fmt::{Display, Formatter, LowerHex};
use std::net::{IpAddr, SocketAddr};

/// A socket address ID. The IP is a hashed ID, so this can be shown without revealing a user's actual IP address.
#[derive(Copy, Clone)]
pub struct SocketAddressId {
    ip_address_id: IpAddressId,
    port: u16,
}

impl SocketAddressId {
    pub fn from_socket_address(
        socket_address: SocketAddr,
        id_generator: &mut IpAddressIdGenerator,
    ) -> Self {
        let ip_address_id = id_generator.create_id(socket_address.ip());

        Self {
            ip_address_id,
            port: socket_address.port(),
        }
    }
}

impl Display for SocketAddressId {
    fn fmt(&self, fmt: &mut Formatter<'_>) -> std::fmt::Result {
        fmt.write_fmt(format_args!("{}:{}", self.ip_address_id, self.port))
    }
}

/// An ID for an IP address. It is represented as a hash value that is likely to be unique, but isn't strictly unique. This can be
/// shown without revealing a user's actual IP address.
#[derive(Copy, Clone)]
pub struct IpAddressId {
    hash: [u8; 8],
}

impl Display for IpAddressId {
    fn fmt(&self, fmt: &mut Formatter<'_>) -> std::fmt::Result {
        fmt.write_str("IP[")?;
        for byte in &self.hash {
            fmt.write_fmt(format_args!("{:02x}", byte))?;
        }
        fmt.write_str("]")?;
        Ok(())
    }
}

/// A generator for IP addresses. An instance should always produce the same ID for a given address, but another
/// instance should produce a different ID for that address.
pub struct IpAddressIdGenerator {
    digest: RandomKeyedHashDigest,
}

impl IpAddressIdGenerator {
    pub fn new() -> Self {
        Self {
            digest: RandomKeyedHashDigest::new(),
        }
    }

    pub fn create_id(&mut self, address: IpAddr) -> IpAddressId {
        let mut hash = [0u8; 8];
        self.digest
            .digest(&Self::address_to_bytes(&address), &mut hash);
        IpAddressId { hash }
    }

    fn address_to_bytes(address: &IpAddr) -> Box<[u8]> {
        match address {
            IpAddr::V4(ipv4) => Box::new(ipv4.octets()),
            IpAddr::V6(ipv6) => Box::new(ipv6.octets()),
        }
    }
}

/// A keyed hash digest function with a randomly generated key.
struct RandomKeyedHashDigest {
    random_key: [u8; 64],
    digest_function: Sha256,
}

impl RandomKeyedHashDigest {
    fn new() -> Self {
        let mut random_key = [0; 64];
        OsRng.fill_bytes(&mut random_key);
        Self {
            random_key,
            digest_function: Sha256::new(),
        }
    }

    /// Digests the given `data`, along with the `random_key`, and writes the truncated result to `output`.
    fn digest(&mut self, data: &[u8], output: &mut [u8; 8]) {
        self.digest_function.update(self.random_key);
        self.digest_function.update(data);

        let digest_result = self.digest_function.finalize_reset();
        let digest_array: &[u8] = digest_result.as_ref();
        output.copy_from_slice(&digest_array[0..8]);
    }
}

struct ByteBuf<'a>(&'a [u8]);

impl<'a> LowerHex for ByteBuf<'a> {
    fn fmt(&self, fmt: &mut Formatter<'_>) -> std::fmt::Result {
        for byte in self.0 {
            fmt.write_fmt(format_args!("{:02x}", byte))?;
        }
        Ok(())
    }
}
