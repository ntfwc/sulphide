use std::env;
use std::iter::Peekable;
use std::vec::Drain;

const MAX_PLAYER_COUNT_UPPER_LIMIT: u32 = 400;

#[derive(Debug)]
pub struct CommandLineArguments {
    port: u16,
    optional_arg_values: OptionalArgumentValues,
}

impl CommandLineArguments {
    pub fn get_port(&self) -> u16 {
        self.port
    }

    pub fn get_bind_host(&self) -> &str {
        if let Some(value) = &self.optional_arg_values.bind_address {
            value
        } else {
            "0.0.0.0"
        }
    }

    pub fn get_log_statistics(&self) -> bool {
        self.optional_arg_values.log_statistics
    }

    pub fn get_enable_trace(&self) -> bool {
        self.optional_arg_values.enable_trace
    }

    pub fn get_max_player_count(&self) -> u32 {
        self.optional_arg_values.max_player_count
    }
}

#[derive(Debug)]
struct OptionalArgumentValues {
    bind_address: Option<String>,
    log_statistics: bool,
    enable_trace: bool,
    max_player_count: u32,
}

impl OptionalArgumentValues {
    fn new() -> Self {
        Self {
            bind_address: None,
            log_statistics: false,
            enable_trace: false,
            max_player_count: MAX_PLAYER_COUNT_UPPER_LIMIT,
        }
    }
}

macro_rules! get_or_print_usage_and_return {
    ($expr: expr, $program_path: expr, $failure_return_value: expr) => {
        match $expr {
            Some(arg) => arg,
            None => {
                print_usage($program_path);
                return $failure_return_value;
            }
        }
    };
}

pub fn parse_arguments() -> Option<CommandLineArguments> {
    let mut args: Vec<String> = env::args().collect();
    let mut args_iterator = args.drain(..).peekable();
    let program_name = args_iterator.next().unwrap();

    get_or_print_usage_and_return!(args_iterator.peek(), &program_name, None);
    let mut optional_arg_values = OptionalArgumentValues::new();

    // Handle optional arguments
    if !handle_optional_arguments(&mut optional_arg_values, &mut args_iterator, &program_name) {
        return None;
    }

    let port_string = get_or_print_usage_and_return!(args_iterator.next(), &program_name, None);
    let port = match port_string.parse::<u16>() {
        Ok(0) => {
            eprintln!("Zero is not a valid port number");
            return None;
        }
        Ok(num) => num,
        Err(e) => {
            eprintln!("Given invalid argument for the port number. error: {}", e);
            return None;
        }
    };

    if let Some(_extra_arg) = args_iterator.next() {
        print_usage(&program_name);
        return None;
    }

    Some(CommandLineArguments {
        port,
        optional_arg_values,
    })
}

/// Extracts optional argument values from the `args_iterator` to populate the `optional_arg_values`.
///
/// Returns true if it completes successfully, false otherwise.
fn handle_optional_arguments(
    optional_arg_values: &mut OptionalArgumentValues,
    args_iterator: &mut Peekable<Drain<String>>,
    program_name: &str,
) -> bool {
    loop {
        match args_iterator.peek() {
            Some(next_value) => {
                if !next_value.starts_with('-') {
                    return true;
                }
            }
            None => {
                print_usage(program_name);
                return false;
            }
        };

        // Take the value
        let next_value = args_iterator.next().unwrap();

        match next_value.as_ref() {
            "--bind-address" | "-b" => {
                let parameter =
                    get_or_print_usage_and_return!(args_iterator.next(), program_name, false);

                if parameter.starts_with('-') {
                    eprintln!("Given no value for the bind host parameter");
                    return false;
                }

                optional_arg_values.bind_address = Some(parameter);
            }
            "--log-stats" | "-s" => {
                optional_arg_values.log_statistics = true;
            }
            "--trace" => {
                optional_arg_values.enable_trace = true;
            }
            "--max-players" | "-m" => {
                let parameter =
                    get_or_print_usage_and_return!(args_iterator.next(), program_name, false);

                if parameter.starts_with('-') {
                    eprintln!("Given no value for the bind host parameter");
                    return false;
                }

                let max_player_count = match parameter.parse::<u32>() {
                    Ok(0) => {
                        eprintln!("Zero is not a valid max player count");
                        return false;
                    }
                    Ok(num) => {
                        if num > MAX_PLAYER_COUNT_UPPER_LIMIT {
                            eprintln!(
                                "The max player count cannot exceed {}",
                                MAX_PLAYER_COUNT_UPPER_LIMIT
                            );
                            return false;
                        }
                        num
                    }
                    Err(e) => {
                        eprintln!(
                            "Given invalid argument for the max player count. error: {}",
                            e
                        );
                        return false;
                    }
                };

                optional_arg_values.max_player_count = max_player_count;
            }
            _ => {
                eprintln!("Encountered unknown optional parameter: '{}'", next_value);
                eprintln!();
                print_usage(program_name);
                return false;
            }
        }
    }
}

fn print_usage(program_name: &str) {
    eprintln!("Usage:");
    eprintln!("    {} [options] port", program_name);
    eprintln!();
    eprintln!("    port: The port to bind the server to.");
    eprintln!();
    eprintln!("Options:");
    eprintln!("    --bind-address address, -b address: The local IP address to bind to. Default value = 0.0.0.0");
    eprintln!("    --max-players, -m: The maximum number of players to allow to connect. It cannot exceed {0}. Default value = {0}", MAX_PLAYER_COUNT_UPPER_LIMIT);
    eprintln!("    --log-stats, -s: Enables statistics logging. It will output a log message every 30 seconds.");
    eprintln!("    --trace: Enables trace level logging. This is useful for development.");
}
