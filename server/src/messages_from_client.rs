use std::fmt::Display;

use crate::model::main::*;
use crate::model::parsed_json_message::*;
use crate::model::player_array_state::{
    array_value::GetArrayItemError, PlayerAppearanceStateArray, TimestampedPlayerArrayState,
};

const MAX_MAP_NAME_LENGTH: usize = 80;

pub trait ClientMessage
where
    Self: Sized,
{
    fn from_json(parsed_json_message: ParsedJsonMessage) -> Result<Self, FromJsonError>;
}

/// A message sent by a client when it first connects.
#[derive(Debug, PartialEq)]
pub struct ClientIntroductionMessage {
    pub username: String,
    pub map: MapName,
    pub state: TimestampedPlayerArrayState,
}

impl Display for ClientIntroductionMessage {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "ClientIntroductionMessage(username='{}', map={:?})",
            self.username, self.map
        )
    }
}

/// Gets a finite f64 from the json value using the given `key`.
fn get_finite_f64(json_object: &json::JsonValue, key: &str) -> Result<f64, FromJsonError> {
    match json_object[key].as_f64() {
        Some(value) => {
            if value.is_finite() {
                Ok(value)
            } else {
                Err(FromJsonError::NonRepresentableFloat)
            }
        }
        None => Err(FromJsonError::IncorrectFields),
    }
}

fn parse_position(json_object: &json::JsonValue) -> Result<PlayerPosition, FromJsonError> {
    let x = get_finite_f64(json_object, "x")?;
    let y = get_finite_f64(json_object, "y")?;
    let z = get_finite_f64(json_object, "z")?;
    Result::Ok(PlayerPosition { x, y, z })
}

fn take_i64(json_object: &json::JsonValue, key: &str) -> Result<i64, FromJsonError> {
    match json_object[key].as_i64() {
        Some(value) => Ok(value),
        None => Err(FromJsonError::IncorrectFields),
    }
}

fn get_object<'a>(
    json_object: &'a json::JsonValue,
    key: &str,
) -> Result<&'a json::JsonValue, FromJsonError> {
    let key_value = &json_object[key];
    if key_value.is_object() {
        Ok(key_value)
    } else {
        Err(FromJsonError::IncorrectFields)
    }
}

impl MessagePlayerStateList {
    fn parse_from_json(json_object: &json::JsonValue, key: &str) -> Result<Self, FromJsonError> {
        let state_array = take_array(json_object, key)?;
        if state_array.is_empty() {
            return Err(FromJsonError::EmptyStateList);
        }

        if state_array.len() > MessagePlayerStateList::MAX_ITEM_COUNT {
            return Err(FromJsonError::StateListHasTooManyItems);
        }

        let mut state_vec: Vec<TimestampedPlayerArrayState> = Vec::with_capacity(state_array.len());
        for object in state_array {
            let state = TimestampedPlayerArrayState::from_json_object(object)?;
            state_vec.push(state);
        }

        match NonEmptyVec::from(state_vec) {
            Some(non_empty_vec) => Ok(Self::new(non_empty_vec)),
            None => Err(FromJsonError::EmptyStateList),
        }
    }
}

fn take_array<'a, 'b>(
    json_object: &'a json::JsonValue,
    key: &'b str,
) -> Result<&'a Vec<json::JsonValue>, FromJsonError> {
    let value = &json_object[key];
    match value {
        json::JsonValue::Array(array) => Ok(array),
        _ => Err(FromJsonError::IncorrectFields),
    }
}

impl ClientMessage for ClientIntroductionMessage {
    fn from_json(mut parsed_json_message: ParsedJsonMessage) -> Result<Self, FromJsonError> {
        let username = match parsed_json_message.take_string("username", 20) {
            Ok(username) => username,
            Err(TakeStringError::Missing) => return Err(FromJsonError::IncorrectFields),
            Err(TakeStringError::InvalidLength) => return Err(FromJsonError::InvalidUsername),
        };
        let map = take_map_name(&mut parsed_json_message, "map")?;
        let state = TimestampedPlayerArrayState::from_json_object(&parsed_json_message.0)?;

        Ok(Self {
            username,
            map,
            state,
        })
    }
}

fn take_map_name(
    parsed_json_message: &mut ParsedJsonMessage,
    key: &str,
) -> Result<MapName, FromJsonError> {
    match parsed_json_message.take_string(key, MAX_MAP_NAME_LENGTH) {
        Ok(username) => Ok(MapName(username)),
        Err(TakeStringError::Missing) => Err(FromJsonError::IncorrectFields),
        Err(TakeStringError::InvalidLength) => Err(FromJsonError::InvalidMapName),
    }
}

/// A message to update the player position.
#[derive(Debug, PartialEq)]
pub struct ClientStateUpdateMessage {
    pub states: MessagePlayerStateList,
}

impl ClientMessage for ClientStateUpdateMessage {
    fn from_json(parsed_json_message: ParsedJsonMessage) -> Result<Self, FromJsonError> {
        let parsed_json = parsed_json_message.0;
        let states = MessagePlayerStateList::parse_from_json(&parsed_json, "states")?;
        Ok(Self { states })
    }
}

impl ClientStateUpdateMessage {
    pub const TYPE: &'static str = "stateUpdate";
}

/// A message sent by a client when the player switches maps.
#[derive(Debug, PartialEq)]
pub struct MapSwitchMessage {
    pub new_map: MapName,
    pub new_state: TimestampedPlayerArrayState,
}

impl ClientMessage for MapSwitchMessage {
    fn from_json(mut parsed_json_message: ParsedJsonMessage) -> Result<Self, FromJsonError> {
        let new_map = take_map_name(&mut parsed_json_message, "new_map")?;
        let parsed_json = parsed_json_message.0;
        let new_state = TimestampedPlayerArrayState::from_json_object(&parsed_json["new_state"])?;
        Result::Ok(MapSwitchMessage { new_map, new_state })
    }
}

impl MapSwitchMessage {
    pub const TYPE: &'static str = "mapSwitch";
}

impl TimestampedPlayerArrayState {
    pub fn from_json_object(json_object: &json::JsonValue) -> Result<Self, FromJsonError> {
        let timestamp = take_i64(json_object, Self::TIMESTAMP_FIELD_NAME)?;
        let position = parse_position(get_object(json_object, Self::POSITION_FIELD_NAME)?)?;
        let appearance_state = Self::get_appearance_state_if_present(json_object)?;
        Ok(Self {
            timestamp,
            position,
            appearance_state,
        })
    }

    fn get_appearance_state_if_present(
        json_object: &json::JsonValue,
    ) -> Result<Option<PlayerAppearanceStateArray>, FromJsonError> {
        let json_value = &json_object[Self::APPEARANCE_STATE_FIELD_NAME];
        match json_value {
            json::JsonValue::Object(_) => PlayerAppearanceStateArray::from_json_object(json_value)
                .map(Some)
                .map_err(FromJsonError::GetArrayItemError),
            json::JsonValue::Null => Ok(None),
            _ => Err(FromJsonError::IncorrectFields),
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum FromJsonError {
    IncorrectFields,
    InvalidUsername,
    InvalidMapName,
    NonRepresentableFloat,
    EmptyStateList,
    StateListHasTooManyItems,
    GetArrayItemError(GetArrayItemError),
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::model::player_array_state::test::*;

    macro_rules! define_parse {
        ($name:ident, $message:ident) => {
            fn $name(json_string: &str) -> Result<$message, TestFromJsonError> {
                let parsed_json_message = parse_json_message(json_string)?;
                let message = $message::from_json(parsed_json_message)?;
                Ok(message)
            }
        };
    }

    define_parse!(parse_client_intro_message, ClientIntroductionMessage);

    #[test]
    fn test_parse_client_introduction_message() {
        assert_eq!(true, parse_client_intro_message("").is_err());
        assert_eq!(true, parse_client_intro_message("{").is_err());
        assert_eq!(true, parse_client_intro_message("{dsflkdsf").is_err());

        assert_eq!(
            get_incorrect_fields_error(),
            parse_client_intro_message("{}")
        );
        assert_eq!(
            get_incorrect_fields_error(),
            parse_client_intro_message(r#"{"name": "client1"}"#)
        );
        assert_eq!(
            get_incorrect_fields_error(),
            parse_client_intro_message(
                r#"{"name": "client1", "map": "map1", "position": { "x": 1.0, "y": 2.0}}"#
            )
        );
        assert_eq!(
            get_incorrect_fields_error(),
            parse_client_intro_message(
                r#"{"name": "client1", "position": { "x": 1.0, "y": 2.0, "z": 3.0}}"#
            )
        );
        assert_eq!(
            get_incorrect_fields_error(),
            parse_client_intro_message(
                r#"{"name": "client1", "position": { "timestamp": 1022, "x": 1.0, "y": 2.0, "z": 3.0}}"#
            )
        );

        // Wrong field type
        assert_eq!(
            get_incorrect_fields_error(),
            parse_client_intro_message(
                r#"{"username": 5, "map": "map1", "timestamp": 1022, "position": { "x": 1.0, "y": 2.0, "z": 3.0}}"#
            )
        );

        // Missing timestamp
        assert_eq!(
            get_incorrect_fields_error(),
            parse_client_intro_message(
                r#"{"username": "client1", "map": "map1", "position": {"x": 1.0, "y": 2.0, "z": 3.0}}"#
            )
        );

        assert_eq!(
            Ok(create_client_introduction_message(
                String::from("client1"),
                MapName(String::from("map1")),
                TimestampedPlayerArrayState::without_appearance(1022, 1.0, 2.0, 3.0)
            )),
            parse_client_intro_message(
                r#"{"username": "client1", "map": "map1", "timestamp": 1022, "position": {"x": 1.0, "y": 2.0, "z": 3.0}}"#
            )
        );

        //Order should not matter
        assert_eq!(
            Ok(create_client_introduction_message(
                String::from("client1"),
                MapName(String::from("map1")),
                TimestampedPlayerArrayState::without_appearance(1022, 1.0, 2.0, 3.0)
            )),
            parse_client_intro_message(
                r#"{"map": "map1", "timestamp": 1022, "username": "client1", "position": { "y": 2.0, "z": 3.0, "x": 1.0 }}"#
            )
        );
    }

    #[test]
    fn test_parse_client_introduction_message_validation() {
        // It should be ok if there are extra fields. These should be ignored.
        assert_eq!(
            Ok(create_client_introduction_message(
                String::from("client1"),
                MapName(String::from("map1")),
                TimestampedPlayerArrayState::without_appearance(1022, 1.0, 2.0, 3.0)
            )),
            parse_client_intro_message(
                r#"{"extra": "field", "map": "map1", "username": "client1", "timestamp": 1022, "position": { "y": 2.0, "z": 3.0, "x": 1.0 }}"#
            )
        );

        // The username character limit should be 20
        assert_eq!(
            Ok(create_client_introduction_message(
                String::from("client78901234567890"),
                MapName(String::from("map1")),
                TimestampedPlayerArrayState::without_appearance(1022, 1.0, 2.0, 3.0)
            )),
            parse_client_intro_message(
                r#"{"username": "client78901234567890", "map": "map1", "timestamp": 1022, "position": { "x": 1.0, "y": 2.0, "z": 3.0}}"#
            )
        );
        assert_eq!(
            get_invalid_username_error(),
            parse_client_intro_message(
                r#"{"username": "client789012345678901", "map": "map1", "timestamp": 1022, "position": { "x": 1.0, "y": 2.0, "z": 3.0}}"#
            )
        );

        // An empty username should not be allowed
        assert_eq!(
            get_invalid_username_error(),
            parse_client_intro_message(
                r#"{"username": "", "map": "map1", "timestamp": 1022, "position": {"x": 1.0, "y": 2.0, "z": 3.0}}"#
            )
        );

        // The map name character limit should be validated
        let mut s = String::with_capacity(81);
        for _ in 0..MAX_MAP_NAME_LENGTH {
            s.push('a');
        }
        assert_eq!(
            Ok(create_client_introduction_message(
                "client1".into(),
                MapName(s.clone()),
                TimestampedPlayerArrayState::without_appearance(1022, 1.0, 2.0, 3.0)
            )),
            parse_client_intro_message(&format!(
                r#"{{"username": "client1", "map": "{}", "timestamp": 1022, "position": {{ "x": 1.0, "y": 2.0, "z": 3.0}}}}"#,
                s
            ))
        );

        s.push('a');
        assert_eq!(
            get_invalid_map_name_error(),
            parse_client_intro_message(&format!(
                r#"{{"username": "client1", "map": "{}", "timestamp": 1022, "position": {{"x": 1.0, "y": 2.0, "z": 3.0}}}}"#,
                s
            ))
        );

        // An empty map name should not be allowed
        assert_eq!(
            get_invalid_map_name_error(),
            parse_client_intro_message(
                r#"{"username": "client1", "map": "", "timestamp": 1022, "position": {"x": 1.0, "y": 2.0, "z": 3.0}}"#
            )
        );

        // Position values that cannot be represented in a 64-bit float would be interpreted as infinity, these should be rejected
        assert_eq!(
            get_non_representable_float_error(),
            parse_client_intro_message(
                r#"{"username": "client1", "map": "map1", "timestamp": 1022, "position": {"x": 1e1025, "y": 2.0, "z": 3.0}}"#
            )
        );
        assert_eq!(
            get_non_representable_float_error(),
            parse_client_intro_message(
                r#"{"username": "client1", "map": "map1", "timestamp": 1022, "position": {"x": -1e1025, "y": 2.0, "z": 3.0}}"#
            )
        );
        assert_eq!(
            get_non_representable_float_error(),
            parse_client_intro_message(
                r#"{"username": "client1", "map": "map1", "timestamp": 1022, "position": {"x": 1.0, "y": 2.0e1024, "z": 3.0}}"#
            )
        );
        assert_eq!(
            get_non_representable_float_error(),
            parse_client_intro_message(
                r#"{"username": "client1", "map": "map1", "timestamp": 1022, "position": {"x": 1.0, "y": 2.0, "z": 3.0e1024}}"#
            )
        );
    }

    #[test]
    fn test_parse_client_introduction_message_with_appearance_state() {
        assert_eq!(
            Ok(create_client_introduction_message(
                String::from("client78901234567890"),
                MapName(String::from("map1")),
                TimestampedPlayerArrayState {
                    timestamp: 1022,
                    position: PlayerPosition::new(1.0, 2.0, 3.0),
                    appearance_state: Some(create_appearance_state1())
                }
            )),
            parse_client_intro_message(&concat_3strings(
                r#"{
                "username": "client78901234567890",
                "map": "map1",
                "timestamp": 1022,
                "position": { "x": 1.0, "y": 2.0, "z": 3.0},
                "appearanceState": "#,
                APPEARANCE_STATE1_JSON,
                r#"}"#
            ))
        );
    }

    define_parse!(parse_client_state_update_message, ClientStateUpdateMessage);

    #[test]
    fn test_parse_client_state_update() {
        assert_eq!(
            get_incorrect_fields_error(),
            parse_client_state_update_message("{}")
        );
        assert_eq!(
            get_incorrect_fields_error(),
            parse_client_state_update_message(
                r#"{"states": [{ "position": {"x": 2.0, "y": 3.5} }]}"#
            )
        );
        assert_eq!(
            get_incorrect_fields_error(),
            parse_client_state_update_message(
                r#"{"states": [{ "position": {"x": 2.0, "y": 3.5, "z": 4.0} }]}"#
            )
        );
        assert_eq!(
            Ok(ClientStateUpdateMessage {
                states: create_test_player_state_list(vec!(
                    TimestampedPlayerArrayState::without_appearance(2004, 2.0, 3.5, 4.0)
                ))
            }),
            parse_client_state_update_message(
                r#"{"states": [{"timestamp": 2004, "position": {"x": 2.0, "y": 3.5, "z": 4.0} }]}"#
            )
        );

        // Parsing multiple states should work
        assert_eq!(
            Ok(ClientStateUpdateMessage {
                states: create_test_player_state_list(vec!(
                    TimestampedPlayerArrayState::without_appearance(2004, 2.0, 3.5, 4.0),
                    TimestampedPlayerArrayState::without_appearance(2020, 4.0, 5.3, 7.0)
                ))
            }),
            parse_client_state_update_message(
                r#"{"states": [{"timestamp": 2004, "position": {"x": 2.0, "y": 3.5, "z": 4.0} }, {"timestamp": 2020, "position": {"x": 4.0, "y": 5.3, "z": 7.0} }]}"#
            )
        );
        // Having a second state that is missing fields should cause an error
        assert_eq!(
            get_incorrect_fields_error(),
            parse_client_state_update_message(
                r#"{"states": [{"timestamp": 2004, "position": {"x": 2.0, "y": 3.5, "z": 4.0} }, { "position": {"x": 4.0, "y": 5.3, "z": 7.0} }]}"#
            )
        );

        // It should handle having the type ID in the JSON
        assert_eq!(
            Ok(ClientStateUpdateMessage {
                states: create_test_player_state_list(vec!(
                    TimestampedPlayerArrayState::without_appearance(1023, 2.0, 3.5, 4.0)
                ))
            }),
            parse_client_state_update_message(
                r#"{"t": "stateUpdate", "states": [{"timestamp": 1023, "position": {"x": 2.0, "y": 3.5, "z": 4.0} }]}"#
            )
        );
    }
    #[test]
    fn test_parse_client_state_update_state_limits() {
        // Parsing the maximum number of states should work
        assert_eq!(
            Ok(ClientStateUpdateMessage {
                states: create_state_list_of_size(MessagePlayerStateList::MAX_ITEM_COUNT)
            }),
            parse_client_state_update_message(
                &create_client_state_update_json_with_state_list_of_size(
                    MessagePlayerStateList::MAX_ITEM_COUNT
                )
            )
        );

        // An state list with more than the max item count should cause an error
        assert_eq!(
            get_state_list_has_too_many_items_error(),
            parse_client_state_update_message(
                &create_client_state_update_json_with_state_list_of_size(
                    MessagePlayerStateList::MAX_ITEM_COUNT + 1
                )
            )
        );

        // An empty state list should cause an error
        assert_eq!(
            get_empty_state_list_error(),
            parse_client_state_update_message(
                &create_client_state_update_json_with_state_list_of_size(0)
            )
        );
    }

    #[test]
    fn test_parse_client_state_update_with_appearance_state() {
        assert_eq!(
            Ok(ClientStateUpdateMessage {
                states: create_test_player_state_list(vec!(TimestampedPlayerArrayState {
                    timestamp: 2004,
                    position: PlayerPosition::new(2.0, 3.5, 4.0),
                    appearance_state: Some(create_appearance_state1())
                }))
            }),
            parse_client_state_update_message(&concat_3strings(
                r#"{"states": [{
                    "timestamp": 2004,
                    "position": {"x": 2.0, "y": 3.5, "z": 4.0},
                    "appearanceState": "#,
                APPEARANCE_STATE1_JSON,
                r#"}]}"#
            ))
        );

        assert_eq!(
            Ok(ClientStateUpdateMessage {
                states: create_test_player_state_list(vec!(
                    TimestampedPlayerArrayState {
                        timestamp: 2004,
                        position: PlayerPosition::new(2.0, 3.5, 4.0),
                        appearance_state: Some(create_appearance_state1())
                    },
                    TimestampedPlayerArrayState {
                        timestamp: 2346,
                        position: PlayerPosition::new(4.5, 1.0, 8.0),
                        appearance_state: Some(create_appearance_state2())
                    }
                ))
            }),
            parse_client_state_update_message(&concat_5strings(
                r#"{"states": [{
                    "timestamp": 2004,
                    "position": {"x": 2.0, "y": 3.5, "z": 4.0},
                    "appearanceState": "#,
                APPEARANCE_STATE1_JSON,
                r#"}, {
                    "timestamp": 2346,
                    "position": {"x": 4.5, "y": 1, "z": 8},
                    "appearanceState": "#,
                APPEARANCE_STATE2_JSON,
                r#"}]}"#
            ))
        );
    }

    fn create_test_player_state_list(
        states: Vec<TimestampedPlayerArrayState>,
    ) -> MessagePlayerStateList {
        MessagePlayerStateList::new(NonEmptyVec::new_unchecked(states))
    }

    fn create_state_list_of_size(size: usize) -> MessagePlayerStateList {
        let mut states = Vec::with_capacity(size);
        for _ in 0..size {
            states.push(TimestampedPlayerArrayState::without_appearance(
                100, 1.0, 2.0, 3.0,
            ));
        }

        MessagePlayerStateList::new(NonEmptyVec::new_unchecked(states))
    }

    fn create_client_state_update_json_with_state_list_of_size(size: usize) -> String {
        let mut result = String::from(r#"{"t": "stateUpdate", "states": ["#);

        for i in 0..size {
            if i != 0 {
                result += ", ";
            }
            result += r#"{"timestamp": 100, "position": {"x": 1.0, "y": 2.0, "z": 3.0} }"#;
        }

        result += r#"]}"#;

        result
    }

    #[test]
    fn test_parse_client_state_update_validation() {
        // Position values that cannot be represented in a 64-bit float would be interpreted as infinity, these should be rejected
        assert_eq!(
            get_non_representable_float_error(),
            parse_client_state_update_message(
                r#"{"states": [{"timestamp": 1022, "position": {"x": 2.0e1025, "y": 3.5, "z": 4.0} }]}"#
            )
        );
        assert_eq!(
            get_non_representable_float_error(),
            parse_client_state_update_message(
                r#"{"states": [{"timestamp": 1022, "position": {"x": 2.0, "y": 3.5e1025, "z": 4.0} }]}"#
            )
        );
        assert_eq!(
            get_non_representable_float_error(),
            parse_client_state_update_message(
                r#"{"states": [{"timestamp": 1022, "position": {"x": 2.0, "y": 3.5, "z": 4.0e1025} }]}"#
            )
        );
    }

    define_parse!(parse_map_switch_message, MapSwitchMessage);

    #[test]
    fn test_parse_map_switch_message() {
        assert_eq!(get_incorrect_fields_error(), parse_map_switch_message("{}"));
        assert_eq!(
            get_incorrect_fields_error(),
            parse_map_switch_message(
                r#"{"new_state":{ "position": {"x": 2.4, "y": 3.0, "z": 4.0} }}"#
            )
        );
        assert_eq!(
            get_incorrect_fields_error(),
            parse_map_switch_message(
                r#"{"new_map": "map2", "new_state":{ "position": {"x": 2.4, "y": 3.0, "z": 4.0} }}"#
            )
        );
        assert_eq!(
            Ok(MapSwitchMessage {
                new_map: MapName("map2".into()),
                new_state: create_state_without_appearance(332, 2.4, 3.0, 4.0)
            }),
            parse_map_switch_message(
                r#"{"new_map": "map2", "new_state":{"timestamp": 332, "position": {"x": 2.4, "y": 3.0, "z": 4.0} }}"#
            )
        );

        // It should handle having the type ID in the JSON
        assert_eq!(
            Ok(MapSwitchMessage {
                new_map: MapName("map2".into()),
                new_state: create_state_without_appearance(1022, 2.4, 3.0, 4.0)
            }),
            parse_map_switch_message(
                r#"{"t": "mapSwitch", "new_map": "map2", "new_state":{"timestamp": 1022, "position": {"x": 2.4, "y": 3.0, "z": 4.0} }}"#
            )
        );
    }

    #[test]
    fn test_parse_map_switch_message_validation() {
        // An empty map name should not be allowed
        assert_eq!(
            get_invalid_map_name_error(),
            parse_map_switch_message(
                r#"{"new_map": "", "new_state":{"timestamp": 1022, "position": {"x": 2.4, "y": 3.0, "z": 4.0} }}"#
            )
        );

        // The max size of the map name should be validated
        let mut s = String::with_capacity(81);
        for _ in 0..MAX_MAP_NAME_LENGTH {
            s.push('a');
        }
        assert_eq!(
            Ok(MapSwitchMessage {
                new_map: MapName(s.clone()),
                new_state: create_state_without_appearance(1022, 2.4, 3.0, 4.0)
            }),
            parse_map_switch_message(&format!(
                r#"{{"new_map": "{}", "new_state":{{"timestamp": 1022, "position": {{"x": 2.4, "y": 3.0, "z": 4.0}} }}}}"#,
                s
            ))
        );

        s.push('a');
        assert_eq!(
            get_invalid_map_name_error(),
            parse_map_switch_message(&format!(
                r#"{{"new_map": "{}", "new_state":{{"timestamp": 1022, "position": {{"x": 2.4, "y": 3.0, "z": 4.0}} }}}}"#,
                s
            ))
        );

        // Position values that cannot be represented in a 64-bit float would be interpreted as infinity, these should be rejected
        assert_eq!(
            get_non_representable_float_error(),
            parse_map_switch_message(
                r#"{"new_map": "map2", "new_state":{"timestamp": 1022, "position": {"x": 2.4e1025, "y": 3.0, "z": 4.0} }}"#
            )
        );
        assert_eq!(
            get_non_representable_float_error(),
            parse_map_switch_message(
                r#"{"new_map": "map2", "new_state":{"timestamp": 1022, "position": {"x": 2.4, "y": 3.0e1025, "z": 4.0} }}"#
            )
        );
        assert_eq!(
            get_non_representable_float_error(),
            parse_map_switch_message(
                r#"{"new_map": "map2", "new_state":{"timestamp": 1022, "position": {"x": 2.4, "y": 3.0, "z": 4.0e1025} }}"#
            )
        );
    }

    #[test]
    fn test_parse_map_switch_message_with_appearance_state() {
        assert_eq!(
            Ok(MapSwitchMessage {
                new_map: MapName("map2".into()),
                new_state: TimestampedPlayerArrayState {
                    timestamp: 332,
                    position: PlayerPosition::new(2.4, 3.0, 4.0),
                    appearance_state: Some(create_appearance_state1())
                }
            }),
            parse_map_switch_message(&concat_3strings(
                r#"{"new_map": "map2",
                "new_state":{"timestamp": 332,
                "position": {"x": 2.4, "y": 3.0, "z": 4.0},
                "appearanceState": "#,
                APPEARANCE_STATE1_JSON,
                r#"}}"#
            ))
        );
    }

    #[derive(Debug, PartialEq)]
    pub enum TestFromJsonError {
        ParseFailure(json::Error),
        FromJson(FromJsonError),
    }

    impl From<json::Error> for TestFromJsonError {
        fn from(error: json::Error) -> Self {
            Self::ParseFailure(error)
        }
    }

    impl From<FromJsonError> for TestFromJsonError {
        fn from(error: FromJsonError) -> Self {
            Self::FromJson(error)
        }
    }

    fn get_incorrect_fields_error<T>() -> Result<T, TestFromJsonError> {
        Err(TestFromJsonError::FromJson(FromJsonError::IncorrectFields))
    }

    fn get_invalid_username_error<T>() -> Result<T, TestFromJsonError> {
        Err(TestFromJsonError::FromJson(FromJsonError::InvalidUsername))
    }

    fn get_invalid_map_name_error<T>() -> Result<T, TestFromJsonError> {
        Err(TestFromJsonError::FromJson(FromJsonError::InvalidMapName))
    }

    fn get_non_representable_float_error<T>() -> Result<T, TestFromJsonError> {
        Err(TestFromJsonError::FromJson(
            FromJsonError::NonRepresentableFloat,
        ))
    }

    fn get_empty_state_list_error<T>() -> Result<T, TestFromJsonError> {
        Err(TestFromJsonError::FromJson(FromJsonError::EmptyStateList))
    }

    fn get_state_list_has_too_many_items_error<T>() -> Result<T, TestFromJsonError> {
        Err(TestFromJsonError::FromJson(
            FromJsonError::StateListHasTooManyItems,
        ))
    }

    fn create_client_introduction_message(
        username: String,
        map: MapName,
        state: TimestampedPlayerArrayState,
    ) -> ClientIntroductionMessage {
        ClientIntroductionMessage {
            username,
            map,
            state,
        }
    }

    fn create_state_without_appearance(
        timestamp: i64,
        x: f64,
        y: f64,
        z: f64,
    ) -> TimestampedPlayerArrayState {
        TimestampedPlayerArrayState::without_appearance(timestamp, x, y, z)
    }

    fn concat_3strings(str1: &str, str2: &str, str3: &str) -> String {
        let mut buf = str1.to_owned();
        buf.push_str(str2);
        buf.push_str(str3);
        buf
    }

    fn concat_5strings(str1: &str, str2: &str, str3: &str, str4: &str, str5: &str) -> String {
        let mut buf = str1.to_owned();
        buf.push_str(str2);
        buf.push_str(str3);
        buf.push_str(str4);
        buf.push_str(str5);
        buf
    }
}
