use crate::ip_address_id_generator::SocketAddressId;
use crate::messages_from_server::ServerMessage;
use crate::statistic_streams::*;
use crate::statistics::StatisticsCollector;

use async_compression::tokio::bufread::DeflateDecoder;
use async_compression::tokio::write::DeflateEncoder;
use std::borrow::BorrowMut;
use std::convert::TryFrom;
use std::io::Error;
use std::string::FromUtf8Error;
use std::sync::Arc;
use tokio::io::{AsyncReadExt, AsyncWrite, AsyncWriteExt};
use tokio::io::{BufReader, BufWriter};
use tokio::net::tcp::{OwnedReadHalf, OwnedWriteHalf};

fn allocate_buffer(size: usize) -> Vec<u8> {
    let vector: Vec<u8> = vec![0; size];
    vector
}

pub struct ClientConnectionReader {
    // miniz is the backend used and it has limitations. We need a BufReader around the decoder because, otherwise,
    // small reads can stall. This probably has something to do with miniz's no memory allocation design.
    reader: BufReader<DeflateDecoder<BufReader<AsyncStatRead<OwnedReadHalf>>>>,
    address: SocketAddressId,
}

// The biggest client message I have created was 6108 bytes, so double that for the max.
const MAX_CLIENT_MESSAGE_SIZE: u16 = 6108 * 2;

// The miniz deflate decode stream needs a buffer larger than the largest possible message and its size prefix
const DEFLATE_DECODE_BUFFER_SIZE: usize = MAX_CLIENT_MESSAGE_SIZE as usize + 2;

impl ClientConnectionReader {
    pub fn new(initial_reader: InitialConnectionReader, address: SocketAddressId) -> Self {
        Self {
            reader: BufReader::with_capacity(
                DEFLATE_DECODE_BUFFER_SIZE,
                DeflateDecoder::new(initial_reader.buf_reader),
            ),
            address,
        }
    }

    pub async fn read_client_message(&mut self) -> Result<String, ReadMessageError> {
        let message_size = self.read_client_message_size().await?;
        trace_log!("Message size read from {} = {}", self.address, message_size);
        if message_size == 0 {
            return Err(ReadMessageError::ZeroMessageSize);
        }

        if message_size > MAX_CLIENT_MESSAGE_SIZE {
            return Err(ReadMessageError::MessageLargerThanMaxSize);
        }

        let mut message_buffer = allocate_buffer(message_size as usize);
        self.reader.read_exact(message_buffer.borrow_mut()).await?;
        let message_string = String::from_utf8(message_buffer)?;
        trace_log!("Read a message from {}: {}", self.address, message_string);
        Ok(message_string)
    }

    async fn read_client_message_size(&mut self) -> Result<u16, Error> {
        self.reader.read_u16().await
    }
}

#[derive(Clone)]
pub struct ClientConnectionWriter {
    // Use a tokio mutex because this lock will wait for I/O, so a normal mutex could hold up an OS thread for a long time.
    inner: Arc<tokio::sync::Mutex<ClientConnectionWriterInner>>,
}

impl ClientConnectionWriter {
    pub fn new(initial_writer: InitialConnectionWriter, address: SocketAddressId) -> Self {
        Self {
            inner: Arc::new(tokio::sync::Mutex::new(ClientConnectionWriterInner::new(
                initial_writer,
                address,
            ))),
        }
    }

    pub async fn write_message(&mut self, server_message: impl ServerMessage) -> bool {
        let mut unlocked_inner = self.inner.lock().await;
        unlocked_inner.write_message(server_message).await
    }
}

struct ClientConnectionWriterInner {
    writer: DeflateEncoder<BufWriter<AsyncStatWrite<OwnedWriteHalf>>>,
    address: SocketAddressId,
}

impl ClientConnectionWriterInner {
    pub fn new(initial_writer: InitialConnectionWriter, address: SocketAddressId) -> Self {
        let encoder = DeflateEncoder::with_quality(
            initial_writer.buf_writer,
            async_compression::Level::Fastest,
        );
        Self {
            writer: encoder,
            address,
        }
    }

    /// Writes a given `server_message`.
    ///
    /// Returns the number of bytes written, or nothing if writing failed.
    pub async fn write_message(&mut self, server_message: impl ServerMessage) -> bool {
        let json_string = server_message.to_json();
        trace_log!("Writing message to {}: {}", self.address, json_string);
        let byte_message = json_string.as_bytes();
        self.write_message_bytes(byte_message).await
    }

    async fn write_message_bytes(&mut self, message: &[u8]) -> bool {
        let message_length: u32 = match u32::try_from(message.len()) {
            Ok(value) => value,
            Err(_e) => {
                log!(
                    "A message to send had a size too big to encode. size={}, address={}",
                    message.len(),
                    self.address
                );
                return false;
            }
        };

        if !self.write_message_size(message_length).await {
            return false;
        }

        if self.writer.write_all(message).await.is_err() {
            return false;
        }

        if self.writer.flush().await.is_err() {
            return false;
        }

        true
    }

    /// Writes the message size. This is a 3 byte big-endian number. Returns true if it succeeds.
    async fn write_message_size(&mut self, size: u32) -> bool {
        match write_server_message_size(&mut self.writer, size).await {
            Ok(()) => true,
            Err(e) => {
                log!(
                    "Failed to write number to {}, error = {:?}",
                    self.address,
                    e
                );
                false
            }
        }
    }
}

/// Writes the size for a server message. This is a 3 byte big-endian number.
pub async fn write_server_message_size(
    mut writer: impl AsyncWrite + Unpin,
    size: u32,
) -> Result<(), Error> {
    let size_as_3_bytes = &size.to_be_bytes()[1..];
    writer.write_all(size_as_3_bytes).await
}

#[derive(Debug)]
pub enum ReadMessageError {
    IOError(Error),
    Utf8Error(FromUtf8Error),
    ZeroMessageSize,
    MessageLargerThanMaxSize,
}

impl From<Error> for ReadMessageError {
    fn from(error: Error) -> Self {
        ReadMessageError::IOError(error)
    }
}

impl From<FromUtf8Error> for ReadMessageError {
    fn from(error: FromUtf8Error) -> Self {
        ReadMessageError::Utf8Error(error)
    }
}

/// A connection reader for a new connection. Used to exchange protocol headers.
pub struct InitialConnectionReader {
    buf_reader: BufReader<AsyncStatRead<OwnedReadHalf>>,
}

impl InitialConnectionReader {
    pub fn new(reader: OwnedReadHalf, statistics_collector: StatisticsCollector) -> Self {
        let stat_reader = AsyncStatRead::new(reader, statistics_collector);
        Self {
            buf_reader: BufReader::new(stat_reader),
        }
    }

    pub async fn read_u8(&mut self) -> Result<u8, Error> {
        self.buf_reader.read_u8().await
    }
}

/// A connection writer for a new connection. Used to exchange protocol headers.
pub struct InitialConnectionWriter {
    buf_writer: BufWriter<AsyncStatWrite<OwnedWriteHalf>>,
}

impl InitialConnectionWriter {
    pub fn new(writer: OwnedWriteHalf, statistics_collector: StatisticsCollector) -> Self {
        Self {
            buf_writer: BufWriter::new(AsyncStatWrite::new(writer, statistics_collector)),
        }
    }

    /// Writes the given bytes. Returns true if it succeeds, false if there was an error.
    pub async fn write_and_flush(&mut self, bytes: &[u8]) -> Result<(), Error> {
        self.buf_writer.write_all(bytes).await?;
        self.buf_writer.flush().await?;
        Ok(())
    }
}
