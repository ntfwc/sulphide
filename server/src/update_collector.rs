use std::collections::hash_map::Entry::*;
use std::collections::HashMap;
use std::mem;
use std::sync::Arc;
use std::sync::Mutex;

use crate::model::main::*;

#[derive(Clone)]
pub struct UpdateCollector {
    locked_inner: Arc<Mutex<UpdateCollectorInner>>,
}

#[derive(Default)]
struct UpdateCollectorInner {
    changes: HashMap<Arc<String>, Option<StateListUpdateBuffer>>,
}

impl UpdateCollector {
    pub fn new() -> Self {
        Self {
            locked_inner: Arc::new(Mutex::new(UpdateCollectorInner::default())),
        }
    }

    pub fn add_change(&self, player_state: Arc<PlayerStateListWithUsernameRef>) {
        let mut inner = self.locked_inner.lock().unwrap();
        match inner.changes.entry(player_state.username.clone()) {
            Occupied(mut entry) => {
                if let Some(buffer) = entry.get_mut() {
                    buffer.add_state_list_update(player_state);
                } else {
                    entry.insert(Some(StateListUpdateBuffer::new(player_state)));
                }
            }
            Vacant(entry) => {
                entry.insert(Some(StateListUpdateBuffer::new(player_state)));
            }
        }
    }

    pub fn add_removed_player(&self, username: Arc<String>) {
        let mut inner = self.locked_inner.lock().unwrap();
        inner.changes.insert(username, None);
    }

    pub fn flush(&self) -> (Vec<Arc<PlayerStateListWithUsernameRef>>, Vec<Arc<String>>) {
        let mut inner = self.locked_inner.lock().unwrap();
        let mut updates: Vec<Arc<PlayerStateListWithUsernameRef>> =
            Vec::with_capacity(inner.changes.len());
        let mut removed_players: Vec<Arc<String>> = Vec::new();

        inner.changes.retain(|username, value| match value {
            Some(update_buffer) => {
                let (new_state, has_update_available) = update_buffer.get_update();
                updates.push(new_state);

                has_update_available
            }
            None => {
                removed_players.push(username.clone());
                false
            }
        });

        (updates, removed_players)
    }

    pub fn clear(&self) {
        let mut inner = self.locked_inner.lock().unwrap();
        inner.changes.clear();
    }
}

/// A state list update buffer. It can store up to two state list updates. Using this addresses an ordering issue with the updates where
/// you can end up getting two state lists for a player followed by sending two updates to a client, and each of these updates should have
/// one of the state lists, instead of both having the last state list.
struct StateListUpdateBuffer {
    current_update: Arc<PlayerStateListWithUsernameRef>,
    next_update: Option<Arc<PlayerStateListWithUsernameRef>>,
}

impl StateListUpdateBuffer {
    fn new(player_state: Arc<PlayerStateListWithUsernameRef>) -> Self {
        Self {
            current_update: player_state,
            next_update: None,
        }
    }

    fn add_state_list_update(&mut self, player_state: Arc<PlayerStateListWithUsernameRef>) {
        let mut new_current_update = Some(player_state);
        mem::swap(&mut self.next_update, &mut new_current_update);

        // If we already had a next update, then overwrite the current update
        if let Some(update) = new_current_update {
            self.current_update = update
        }
    }

    /// Gets a pair with the current update and a boolean that will be true if there is another update available.
    fn get_update(&mut self) -> (Arc<PlayerStateListWithUsernameRef>, bool) {
        let mut next_update = None;
        mem::swap(&mut self.next_update, &mut next_update);

        let update_to_return = self.current_update.clone();

        let has_update_available: bool = match next_update {
            Some(update) => {
                self.current_update = update;
                true
            }
            None => false,
        };

        (update_to_return, has_update_available)
    }
}
