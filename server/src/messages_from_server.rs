use crate::model::advanced_json_string::{to_array_string, JsonStringRefList};
use crate::model::main::*;

pub trait ServerMessage: Send {
    fn to_json(self) -> String;
}

/// A message sent by the server in response to a client introduction.
pub struct ClientIntroductionReplyMessage {
    /// This is the actual username the server will use for the client.
    actual_username: String,
}

impl ClientIntroductionReplyMessage {
    pub fn new(actual_username: String) -> Self {
        ClientIntroductionReplyMessage { actual_username }
    }
}

impl ServerMessage for ClientIntroductionReplyMessage {
    fn to_json(self) -> String {
        let mut object = json::JsonValue::new_object();
        object["actual_username"] = self.actual_username.into();
        object.dump()
    }
}

/// Message that provides a client updates for its current map.
pub struct StateUpdateMessage {
    updates: Vec<NamedPlayerStateList>,
    removed_players: Vec<String>,
}

impl StateUpdateMessage {
    pub fn new(updates: Vec<NamedPlayerStateList>, removed_players: Vec<String>) -> Self {
        Self {
            updates,
            removed_players,
        }
    }
}

impl ServerMessage for StateUpdateMessage {
    fn to_json(self) -> String {
        let mut object = json::JsonValue::new_object();
        add_type_id(&mut object, "stateUpdate".into());

        if !self.removed_players.is_empty() {
            object["removed_players"] = self.removed_players.into();
        }

        let mut list = JsonStringRefList::new(object.dump().into());

        if !self.updates.is_empty() {
            let player_state_strings = new_player_state_lists_to_json_values(self.updates);
            list.insert_as_value("updates", to_array_string(player_state_strings));
        }

        list.to_json_string()
    }
}

/// The response for a map switch message. Provides the state of the new map.
pub struct MapSwitchReplyMessage {
    player_state_lists: Vec<NamedPlayerStateList>,
}

impl MapSwitchReplyMessage {
    pub fn new(player_state_lists: Vec<NamedPlayerStateList>) -> Self {
        Self { player_state_lists }
    }
}

impl ServerMessage for MapSwitchReplyMessage {
    fn to_json(self) -> String {
        let mut object = json::JsonValue::new_object();
        add_type_id(&mut object, "mapSwitchReply".into());

        let mut list = JsonStringRefList::new(object.dump().into());
        let player_state_strings = new_player_state_lists_to_json_values(self.player_state_lists);
        list.insert_as_value("player_state_lists", to_array_string(player_state_strings));

        list.to_json_string()
    }
}

/// A message sent to a client we are rejecting. This can be used, for example, to notify the client that the client version is incompatible with this server.
pub struct ClientRejectionMessage {
    reason: String,
}

impl ClientRejectionMessage {
    pub fn new(reason: String) -> Self {
        ClientRejectionMessage { reason }
    }
}

impl ServerMessage for ClientRejectionMessage {
    fn to_json(self) -> String {
        let mut object = json::JsonValue::new_object();
        add_type_id(&mut object, "clientRejection".into());

        object["reason"] = self.reason.into();
        object.dump()
    }
}

fn add_type_id(object: &mut json::JsonValue, type_id: String) {
    object["t"] = type_id.into();
}

fn new_player_state_lists_to_json_values(
    player_states: Vec<NamedPlayerStateList>,
) -> Vec<JsonStringRefList> {
    player_states
        .into_iter()
        .map(|player_state| player_state.to_json())
        .collect()
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::model::player_array_state::TimestampedPlayerArrayState;

    #[test]
    fn test_state_update_message_to_json() {
        let state_list1 = PlayerStateList::from_single_state(
            TimestampedPlayerArrayState::without_appearance(101, 1.0, 2.0, 3.0),
        );
        let user1: String = "user1".into();
        assert_eq!(
            concat!(
                r#"{"t":"stateUpdate","updates":["#,
                r#"{"username":"user1","state_list":[{"timestamp":101,"position":{"x":1,"y":2,"z":3}}]}"#,
                r#"]}"#
            ),
            StateUpdateMessage::new(
                vec!(NamedPlayerStateList::new(
                    user1.clone(),
                    state_list1.clone()
                )),
                vec!()
            )
            .to_json()
        );
        assert_eq!(
            concat!(
                r#"{"t":"stateUpdate","removed_players":["user2"],"#,
                r#""updates":["#,
                r#"{"username":"user1","state_list":[{"timestamp":101,"position":{"x":1,"y":2,"z":3}}]}"#,
                r#"]}"#
            ),
            StateUpdateMessage::new(
                vec!(NamedPlayerStateList::new(
                    user1.clone(),
                    state_list1.clone()
                )),
                vec!("user2".into())
            )
            .to_json()
        );

        // Update with a player state list that has multiple states
        let state_list2 = PlayerStateList::from(NonEmptyVec::new(
            TimestampedPlayerArrayState::without_appearance(202, 4.0, 5.0, 6.0),
            vec![TimestampedPlayerArrayState::without_appearance(
                255, 8.8, 9.0, 10.0,
            )],
        ));
        assert_eq!(
            concat!(
                r#"{"t":"stateUpdate","updates":["#,
                r#"{"username":"user1","state_list":[{"timestamp":202,"position":{"x":4,"y":5,"z":6}},{"timestamp":255,"position":{"x":8.8,"y":9,"z":10}}]}"#,
                r#"]}"#
            ),
            StateUpdateMessage::new(
                vec!(NamedPlayerStateList::new(
                    user1.clone(),
                    state_list2.clone()
                )),
                vec!()
            )
            .to_json()
        );

        // Updates for multiple users
        let user2: String = "user2".into();
        assert_eq!(
            concat!(
                r#"{"t":"stateUpdate","updates":["#,
                r#"{"username":"user1","state_list":[{"timestamp":101,"position":{"x":1,"y":2,"z":3}}]},"#,
                r#"{"username":"user2","state_list":[{"timestamp":202,"position":{"x":4,"y":5,"z":6}},{"timestamp":255,"position":{"x":8.8,"y":9,"z":10}}]}"#,
                r#"]}"#
            ),
            StateUpdateMessage::new(
                vec!(
                    NamedPlayerStateList::new(user1.clone(), state_list1.clone()),
                    NamedPlayerStateList::new(user2.clone(), state_list2.clone())
                ),
                vec!()
            )
            .to_json()
        );
    }
}
