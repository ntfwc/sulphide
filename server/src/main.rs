#![forbid(unsafe_code)]

#[macro_use]
mod logging;

use std::net::SocketAddr;
use std::sync::Arc;

use shared_state::SharedStateManager;
use tokio::net::TcpListener;
use tokio::net::TcpStream;

mod client_handler;
mod connection;
mod header_exchange;
mod ip_address_id_generator;
mod json_security;
mod messages_from_client;
mod messages_from_server;
mod model;
mod protocol_headers;
mod runtime_static;
mod shared_state;
mod statistic_streams;
mod statistics;
mod throttler;
mod update_collector;

mod arguments;
use arguments::{parse_arguments, CommandLineArguments};
use ip_address_id_generator::*;
use runtime_static::RuntimeStaticValues;
use statistics::*;

const CRATE_VERSION: &str = env!("CARGO_PKG_VERSION");

#[tokio::main]
async fn main() {
    let arguments: CommandLineArguments = match parse_arguments() {
        Some(arguments) => arguments,
        None => return,
    };

    logging::set_trace_enabled(arguments.get_enable_trace());

    let shared_state_manager = SharedStateManager::new();
    let statistics_collector = if arguments.get_log_statistics() {
        let enabled_collector = Arc::new(EnabledStatisticsCollector::new());
        start_logging_statistics(enabled_collector.clone());
        StatisticsCollector::new(enabled_collector)
    } else {
        StatisticsCollector::new_disabled()
    };

    run_server(
        (arguments.get_bind_host(), arguments.get_port()),
        shared_state_manager,
        statistics_collector,
        arguments.get_max_player_count(),
        RuntimeStaticValues::new().await,
    )
    .await;
}

async fn run_server(
    socket_address: (&str, u16),
    shared_state_manager: SharedStateManager,
    statistics_collector: StatisticsCollector,
    max_player_count: u32,
    runtime_static_values: RuntimeStaticValues,
) {
    let listener = TcpListener::bind(socket_address)
        .await
        .expect("Failed to open listening port");

    let server = async move {
        let mut id_generator = IpAddressIdGenerator::new();
        loop {
            let (socket, address): (TcpStream, SocketAddr) = match listener.accept().await {
                Ok(accepted_tuple) => accepted_tuple,
                Err(e) => {
                    log!("accept error = {:?}", e);
                    continue;
                }
            };

            let address = SocketAddressId::from_socket_address(address, &mut id_generator);

            log!("Accepted connection from {}", address);
            statistics_collector.notify_of_new_connection();
            let client_shared_state_manager_ref = shared_state_manager.clone();
            let client_statistics_collector_ref = statistics_collector.clone();
            let runtime_static_values_clone = runtime_static_values.clone();
            tokio::spawn(async move {
                client_handler::handle_client(
                    socket,
                    address,
                    client_shared_state_manager_ref,
                    client_statistics_collector_ref.clone(),
                    max_player_count,
                    runtime_static_values_clone,
                )
                .await;
                client_statistics_collector_ref.notify_of_disconnect();
                log!("Dropping connection with {}", address);
            });
        }
    };

    log!("Version: {}", CRATE_VERSION);
    log!(
        "Server running on {}:{}",
        socket_address.0,
        socket_address.1
    );
    server.await;
}
