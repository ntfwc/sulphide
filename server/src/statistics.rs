use std::ops::Deref;
use std::sync::atomic::*;
use std::sync::Arc;
use tokio::time::{self, Duration};

#[derive(Clone)]
pub struct StatisticsCollector {
    inner: Arc<dyn InnerStatisticsCollector>,
}

impl StatisticsCollector {
    pub fn new(inner: Arc<dyn InnerStatisticsCollector>) -> Self {
        Self { inner }
    }

    pub fn new_disabled() -> Self {
        Self::new(Arc::new(DisabledStatisticsCollector::new()))
    }
}

impl Deref for StatisticsCollector {
    type Target = dyn InnerStatisticsCollector;

    fn deref(&self) -> &Self::Target {
        self.inner.as_ref()
    }
}

pub trait InnerStatisticsCollector: Send + Sync {
    fn notify_of_new_connection(&self);

    fn notify_of_bytes_received(&self, byte_count: usize);

    fn notify_of_bytes_transmitted(&self, byte_count: usize);

    fn notify_of_disconnect(&self);
}

#[derive(Clone)]
pub struct DisabledStatisticsCollector;

impl DisabledStatisticsCollector {
    pub fn new() -> Self {
        DisabledStatisticsCollector {}
    }
}

impl InnerStatisticsCollector for DisabledStatisticsCollector {
    fn notify_of_new_connection(&self) {}
    fn notify_of_bytes_received(&self, _: usize) {}
    fn notify_of_bytes_transmitted(&self, _: usize) {}
    fn notify_of_disconnect(&self) {}
}

pub fn start_logging_statistics(statistics_collector: Arc<EnabledStatisticsCollector>) {
    tokio::spawn(async move {
        let mut interval = time::interval(Duration::from_millis(30000));
        // The first tick returns immediately
        interval.tick().await;

        loop {
            interval.tick().await;
            statistics_collector.log_statistics();
        }
    });
}

pub struct EnabledStatisticsCollector {
    new_connection_count: StatisticsCounterValue,
    disconnect_count: StatisticsCounterValue,
    bytes_received: StatisticsCounterValue,
    bytes_transmitted: StatisticsCounterValue,
}

impl EnabledStatisticsCollector {
    pub fn new() -> Self {
        Self {
            new_connection_count: StatisticsCounterValue::new(),
            disconnect_count: StatisticsCounterValue::new(),
            bytes_received: StatisticsCounterValue::new(),
            bytes_transmitted: StatisticsCounterValue::new(),
        }
    }

    fn log_statistics(&self) {
        log!(
            "Statistics: new_connection_count={}, disconnect_count={}, bytes_received={}, bytes_transmitted={}",
            self.new_connection_count.get_and_reset(),
            self.disconnect_count.get_and_reset(),
            self.bytes_received.get_and_reset(),
            self.bytes_transmitted.get_and_reset()
        );
    }
}

impl InnerStatisticsCollector for EnabledStatisticsCollector {
    fn notify_of_new_connection(&self) {
        self.new_connection_count.increment();
    }

    fn notify_of_bytes_received(&self, byte_count: usize) {
        self.bytes_received.increment_by_value(byte_count as u64);
    }

    fn notify_of_bytes_transmitted(&self, byte_count: usize) {
        self.bytes_transmitted.increment_by_value(byte_count as u64);
    }

    fn notify_of_disconnect(&self) {
        self.disconnect_count.increment();
    }
}

/// A counter value for a statistic
struct StatisticsCounterValue {
    value: AtomicU64,
}

impl StatisticsCounterValue {
    fn new() -> Self {
        Self {
            value: AtomicU64::new(0),
        }
    }

    fn increment(&self) {
        self.increment_by_value(1);
    }

    fn increment_by_value(&self, value: u64) {
        self.value.fetch_add(value, Ordering::Relaxed);
    }

    fn get_and_reset(&self) -> u64 {
        self.value.swap(0, Ordering::Relaxed)
    }
}
