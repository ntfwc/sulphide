pub const SERVER_HEADER: [u8; 4] = [0x41, 0x3e, 0x44, 0x5b];

pub const CLIENT_HEADER: [u8; 4] = [0xf0, 0x75, 0x40, 0x2c];

pub const VERSION1_CLIENT_HEADER_BYTE1: u8 = 0x1c;

// The version 1 client ends up sending two different bytes for byte 4.
pub const VERSION1_CLIENT_HEADER_BYTE4_1: u8 = 0x0a;
pub const VERSION1_CLIENT_HEADER_BYTE4_2: u8 = 0x0e;
