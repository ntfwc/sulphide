use crate::model::main::*;
use crate::model::player_array_state::TimestampedPlayerArrayState;
use crate::model::shared_state_model::*;
use std::collections::hash_map::Entry::*;
use std::collections::HashMap;
use std::sync::{Arc, Mutex, MutexGuard};

/// Manages shared state.
#[derive(Clone)]
pub struct SharedStateManager {
    server_state: Arc<Mutex<ServerState>>,
}

struct ServerState {
    map_states: HashMap<Arc<MapName>, MapState>,
}

impl ServerState {
    fn new() -> Self {
        Self {
            map_states: HashMap::new(),
        }
    }
}
struct MapState {
    registered_players: HashMap<Arc<String>, RegisteredPlayer>,
}

impl MapState {
    fn new(username: Arc<String>, registered_player: RegisteredPlayer) -> Self {
        let mut new_map_state = Self {
            registered_players: HashMap::new(),
        };
        new_map_state
            .registered_players
            .insert(username, registered_player);
        new_map_state
    }

    fn add_player(&mut self, username: Arc<String>, registered_player: RegisteredPlayer) {
        let player_state = Arc::new(PlayerStateListWithUsernameRef::new(
            username.clone(),
            registered_player.last_state_list.clone(),
        ));
        for (_, other_registered_player) in self.registered_players.iter() {
            let update_listener = &other_registered_player
                .change_listener_container
                .update_listener;
            update_listener(player_state.clone())
        }

        self.registered_players.insert(username, registered_player);
    }

    fn get_player_count(&self) -> u32 {
        self.registered_players.len() as u32
    }
}

impl SharedStateManager {
    pub fn new() -> Self {
        Self {
            server_state: Arc::new(Mutex::new(ServerState::new())),
        }
    }

    pub fn register_player(
        &self,
        proposed_username: String,
        map: MapName,
        state: TimestampedPlayerArrayState,
        change_listener_container: ChangeListenerContainer,
        max_player_count: u32,
    ) -> Result<(PlayerRegistration, Vec<NamedPlayerStateList>), RegisterPlayerError> {
        log!(
            "Registering player. proposed_username='{}'",
            proposed_username
        );
        let mut server_state = self.server_state.lock().unwrap();
        if Self::get_player_count_from_server_state(&server_state) >= max_player_count {
            return Err(RegisterPlayerError::AtMaxPlayerCount);
        }

        let map_arc = Arc::new(map);
        let player_state_lists =
            SharedStateManager::get_player_state_lists_with_lock(&server_state, &map_arc);

        let username = choose_unused_username(proposed_username, &server_state.map_states);

        let map_states = &mut server_state.map_states;

        let registered_player = RegisteredPlayer::new(state, change_listener_container);

        let registered_map_name = SharedStateManager::add_player_to_map(
            map_states,
            map_arc,
            username.clone(),
            registered_player,
        );

        log!("Registered player. username='{}'", username);
        Ok((
            PlayerRegistration {
                username,
                map_name: registered_map_name,
                state_manager: self.clone(),
            },
            player_state_lists,
        ))
    }

    /// Adds a `registered_player`, with the given `username`, to a `map` in the given `map_states`. If the `map` doesn't have a
    /// map state yet, it will be added.
    ///
    /// Returns the reference to the map name of the state the player was added to. This might be different from the given `map`
    /// name reference, in order to deduplicate it.
    fn add_player_to_map(
        map_states: &mut HashMap<Arc<MapName>, MapState>,
        map: Arc<MapName>,
        username: Arc<String>,
        registered_player: RegisteredPlayer,
    ) -> Arc<MapName> {
        match map_states.entry(map.clone()) {
            Occupied(mut entry) => {
                let map_state = entry.get_mut();
                map_state.add_player(username, registered_player);
                // If the key already exists, return the existing key value, so we deduplicate the string
                entry.key().clone()
            }
            Vacant(entry) => {
                let map_state = MapState::new(username, registered_player);
                entry.insert(map_state);
                map
            }
        }
    }

    #[allow(unused)]
    /// Gets the player states for the given `map`.
    pub fn get_player_state_lists(&self, map: &Arc<MapName>) -> Vec<NamedPlayerStateList> {
        let server_state = self.server_state.lock().unwrap();
        SharedStateManager::get_player_state_lists_with_lock(&server_state, map)
    }

    fn get_player_state_lists_with_lock(
        server_state: &MutexGuard<ServerState>,
        map: &Arc<MapName>,
    ) -> Vec<NamedPlayerStateList> {
        let map_state = match server_state.map_states.get(map) {
            Some(server_state) => server_state,
            None => return vec![],
        };

        let mut player_state_lists =
            Vec::<NamedPlayerStateList>::with_capacity(map_state.registered_players.len());
        for (username, registered_player) in map_state.registered_players.iter() {
            player_state_lists.push(NamedPlayerStateList::new(
                clone_value(username),
                registered_player.last_state_list.clone(),
            ))
        }

        player_state_lists
    }

    /// Updates the `state_list` of a player with the given `registration`.
    pub fn update_state_list(
        &self,
        registration: &PlayerRegistration,
        state_list: PlayerStateList,
    ) {
        let mut server_state = self.server_state.lock().unwrap();

        const PRINT_ERROR: &dyn Fn() =
            &|| log!("Warning: Tried to update the state list for an unknown player registration");

        let map_state = match server_state.map_states.get_mut(&registration.map_name) {
            Some(state) => state,
            None => {
                PRINT_ERROR();
                return;
            }
        };

        match map_state.registered_players.get_mut(&registration.username) {
            Some(registered_player) => registered_player.last_state_list = state_list.clone(),
            None => {
                PRINT_ERROR();
                return;
            }
        };

        let player_state = Arc::new(PlayerStateListWithUsernameRef::new(
            registration.username.clone(),
            state_list,
        ));

        for (listener_username, registered_player) in map_state.registered_players.iter() {
            if listener_username != &registration.username {
                let update_listener = &registered_player.change_listener_container.update_listener;
                update_listener(player_state.clone())
            }
        }
    }

    /// Updates a player to be on a `new_map` for when the player, with the given `registration`, switches maps. Also sets the
    /// `player_state` in the new map. It returns the states of players already on the map, if it succeeds.
    ///
    /// The given `to_call_with_lock_held` closure will be called while the the shared state lock is held. This can be used to
    /// keep other updates consistent with the map switch.
    pub fn update_for_map_switch(
        &self,
        registration: &mut PlayerRegistration,
        new_map: Arc<MapName>,
        player_state: TimestampedPlayerArrayState,
        to_call_with_lock_held: impl FnOnce(),
    ) -> Option<Vec<NamedPlayerStateList>> {
        let mut server_state = self.server_state.lock().unwrap();
        trace_log!(
            "Updating player '{}' to be on map {:?}",
            registration.username,
            new_map
        );

        macro_rules! get_or_log_and_return {
            ($expr: expr, $log_message: expr) => {
                match $expr {
                    Some(val) => val,
                    None => {
                        log!($log_message);
                        return None;
                    }
                }
            };
        }

        let mut registered_player = get_or_log_and_return!(
            SharedStateManager::remove_player_from_map(&mut server_state.map_states, registration),
            "update_for_map_switch(): Could not find the registered player"
        );

        // Update some things
        registration.map_name = new_map.clone();
        registered_player.set_state(player_state);

        let player_state_lists =
            SharedStateManager::get_player_state_lists_with_lock(&server_state, &new_map);

        SharedStateManager::add_player_to_map(
            &mut server_state.map_states,
            new_map,
            registration.username.clone(),
            registered_player,
        );

        to_call_with_lock_held();

        Some(player_state_lists)
    }

    fn unregister_player(&self, registration: &PlayerRegistration) {
        let mut server_state = self.server_state.lock().unwrap();
        SharedStateManager::remove_player_from_map(&mut server_state.map_states, registration);
        log!("Unregistered player. username='{}'", registration.username);
    }

    /// Tries to removes a player, with the given `registration`, from a map state in the given `map_states`. It returns the
    /// registered player object, if it existed.
    ///
    /// If it is the last player in the map state, then the map state will be removed too.
    fn remove_player_from_map(
        map_states: &mut HashMap<Arc<MapName>, MapState>,
        registration: &PlayerRegistration,
    ) -> Option<RegisteredPlayer> {
        if let Some(map_state) = map_states.get_mut(&registration.map_name) {
            let registered_player = map_state.registered_players.remove(&registration.username);

            if map_state.registered_players.is_empty() {
                map_states.remove(&registration.map_name);
            } else {
                for (_, registered_player) in map_state.registered_players.iter() {
                    let remove_listener =
                        &registered_player.change_listener_container.remove_listener;
                    remove_listener(registration.username.clone());
                }
            }
            return registered_player;
        }

        None
    }

    /// Gets the current number of players.
    pub fn get_player_count(&self) -> u32 {
        Self::get_player_count_from_server_state(&self.server_state.lock().unwrap())
    }

    fn get_player_count_from_server_state(server_state: &ServerState) -> u32 {
        let mut total_player_count = 0;
        for map_state in server_state.map_states.values() {
            total_player_count += map_state.get_player_count();
        }

        total_player_count
    }
}

impl Default for SharedStateManager {
    fn default() -> Self {
        SharedStateManager::new()
    }
}

#[derive(Debug)]
pub enum RegisterPlayerError {
    AtMaxPlayerCount,
}

fn choose_unused_username(
    proposed_username: String,
    map_states: &HashMap<Arc<MapName>, MapState>,
) -> Arc<String> {
    if !has_username(&proposed_username, map_states) {
        return Arc::new(proposed_username);
    }

    let mut append_num = 1;
    loop {
        let username = format!("{}_{}", proposed_username, append_num);
        if !has_username(&username, map_states) {
            return Arc::new(username);
        }
        append_num += 1;
    }
}

#[allow(clippy::ptr_arg)]
fn has_username(proposed_username: &String, map_states: &HashMap<Arc<MapName>, MapState>) -> bool {
    for (_, map_state) in map_states.iter() {
        if map_state.registered_players.contains_key(proposed_username) {
            return true;
        }
    }

    false
}

pub fn clone_value<T: Clone>(string: &Arc<T>) -> T {
    (string as &T).clone()
}

pub struct PlayerRegistration {
    pub username: Arc<String>,
    pub map_name: Arc<MapName>,
    state_manager: SharedStateManager,
}

impl Drop for PlayerRegistration {
    fn drop(&mut self) {
        self.state_manager.unregister_player(self)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use std::mem;

    #[test]
    fn should_append_to_duplicate_usernames() {
        let state_manager = SharedStateManager::new();

        let map_name = MapName("blah".into());
        let username: String = "user".into();
        let state = TimestampedPlayerArrayState::without_appearance(1022, 1.0, 2.0, 3.0);
        let change_listener_container = create_no_op_change_listeners();

        let (registration1, _) = state_manager.register_player_unchecked(
            username.clone(),
            map_name.clone(),
            state.clone(),
            change_listener_container.clone(),
        );

        // The first time a username is registered, it should not change
        assert_arc_eq(&username, &registration1.username);

        let (registration2, _) = state_manager.register_player_unchecked(
            username.clone(),
            map_name.clone(),
            state.clone(),
            change_listener_container.clone(),
        );
        assert_arc_eq("user_1", &registration2.username);

        // A duplicate string allocated elsewhere should still match
        let duplicate_username = "user".into();
        let (registration3, _) = state_manager.register_player_unchecked(
            duplicate_username,
            map_name.clone(),
            state.clone(),
            change_listener_container.clone(),
        );
        assert_arc_eq("user_2", &registration3.username);

        // If a registration is dropped, then that username should be available again.
        mem::drop(registration2);
        let (registration4, _) = state_manager.register_player_unchecked(
            username.clone(),
            map_name.clone(),
            state.clone(),
            change_listener_container.clone(),
        );
        assert_arc_eq("user_1", &registration4.username);

        mem::drop(registration1);
        let (registration5, _) = state_manager.register_player_unchecked(
            username.clone(),
            map_name.clone(),
            state.clone(),
            change_listener_container.clone(),
        );
        assert_arc_eq(&username, &registration5.username);

        let other_username: String = "other_user".into();
        let (other_user_registration, _) = state_manager.register_player_unchecked(
            other_username.clone(),
            map_name,
            state,
            change_listener_container,
        );
        assert_arc_eq(&other_username, &other_user_registration.username);
    }

    #[test]
    fn test_get_player_states() {
        let state_manager = SharedStateManager::new();

        let map_name1 = Arc::new(MapName("map1".into()));
        let empty_vec: Vec<NamedPlayerStateList> = vec![];
        assert_eq!(empty_vec, state_manager.get_player_state_lists(&map_name1));

        let username1: String = "user1".into();
        let state1 = TimestampedPlayerArrayState::without_appearance(1022, 1.0, 2.0, 3.0);
        let change_listener_container = create_no_op_change_listeners();

        let registration1 = state_manager.register_player_unchecked(
            username1.clone(),
            clone_value(&map_name1),
            state1.clone(),
            change_listener_container.clone(),
        );
        assert_unordered_items(
            &[named_state_list_from_single_state(
                username1.clone(),
                state1.clone(),
            )],
            &state_manager.get_player_state_lists(&map_name1),
        );

        let map_name2 = Arc::new(MapName("map2".into()));
        assert_eq!(empty_vec, state_manager.get_player_state_lists(&map_name2));

        let username2: String = "user2".into();
        let state2 = TimestampedPlayerArrayState::without_appearance(1025, 4.0, 5.0, 6.0);
        let _registration2 = state_manager.register_player_unchecked(
            username2.clone(),
            clone_value(&map_name1),
            state2.clone(),
            change_listener_container.clone(),
        );
        assert_unordered_items(
            &[
                named_state_list_from_single_state(username1.clone(), state1.clone()),
                named_state_list_from_single_state(username2.clone(), state2.clone()),
            ],
            &state_manager.get_player_state_lists(&map_name1),
        );
        assert_eq!(empty_vec, state_manager.get_player_state_lists(&map_name2));

        let username3: String = "user3".into();
        let state3 = TimestampedPlayerArrayState::without_appearance(1050, 7.0, 8.0, 9.0);
        let _registration3 = state_manager.register_player_unchecked(
            username3.clone(),
            clone_value(&map_name2),
            state3.clone(),
            change_listener_container,
        );
        assert_unordered_items(
            &[
                named_state_list_from_single_state(username1, state1),
                named_state_list_from_single_state(username2.clone(), state2.clone()),
            ],
            &state_manager.get_player_state_lists(&map_name1),
        );
        assert_unordered_items(
            &[named_state_list_from_single_state(
                username3.clone(),
                state3.clone(),
            )],
            &state_manager.get_player_state_lists(&map_name2),
        );

        mem::drop(registration1);
        assert_unordered_items(
            &[named_state_list_from_single_state(username2, state2)],
            &state_manager.get_player_state_lists(&map_name1),
        );
        assert_unordered_items(
            &[named_state_list_from_single_state(username3, state3)],
            &state_manager.get_player_state_lists(&map_name2),
        );
    }

    #[test]
    fn register_player_states_returned() {
        let state_manager = SharedStateManager::new();

        let map_name1 = Arc::new(MapName("map1".into()));

        // Register player 1
        let username1: String = "user1".into();
        let state1 = TimestampedPlayerArrayState::without_appearance(1022, 1.0, 2.0, 3.0);
        let change_listener_container = create_no_op_change_listeners();

        let (registration1, player_states) = state_manager.register_player_unchecked(
            username1.clone(),
            clone_value(&map_name1),
            state1.clone(),
            change_listener_container.clone(),
        );
        assert_unordered_items(&[], &player_states);

        // Register player 2, it should get a player state for player 1
        let username2: String = "user2".into();
        let state2 = TimestampedPlayerArrayState::without_appearance(1023, 4.0, 5.0, 6.0);

        let (_registration2, player_states) = state_manager.register_player_unchecked(
            username2.clone(),
            clone_value(&map_name1),
            state2.clone(),
            change_listener_container.clone(),
        );
        assert_unordered_items(
            &[named_state_list_from_single_state(
                username1.clone(),
                state1,
            )],
            &player_states,
        );

        // Change player 1's position
        let state3 = TimestampedPlayerArrayState::without_appearance(1026, 7.0, 8.0, 9.0);
        update_player_position(&state_manager, &registration1, state3.clone());

        // Register player 3. It should get states for player 1 and 2, with the current positions
        let username3: String = "user3".into();
        let position4 = TimestampedPlayerArrayState::without_appearance(1028, 10.0, 11.0, 12.0);
        let (_registration3, player_states) = state_manager.register_player_unchecked(
            username3.clone(),
            clone_value(&map_name1),
            position4.clone(),
            change_listener_container.clone(),
        );
        assert_unordered_items(
            &[
                named_state_list_from_single_state(username1.clone(), state3.clone()),
                named_state_list_from_single_state(username2.clone(), state2.clone()),
            ],
            &player_states,
        );

        // Registrations on other maps should not get states from this map
        let map_name2 = Arc::new(MapName("map2".into()));

        let username4: String = "user3".into();
        let state5 = TimestampedPlayerArrayState::without_appearance(1029, 20.0, 21.0, 22.0);
        let (_registration4, player_states) = state_manager.register_player_unchecked(
            username4.clone(),
            clone_value(&map_name2),
            state5.clone(),
            change_listener_container.clone(),
        );
        assert_unordered_items(&[], &player_states);
    }

    #[test]
    fn test_update_position() {
        let state_manager = SharedStateManager::new();

        let map_name1 = Arc::new(MapName("map1".into()));
        let username1: String = "user1".into();
        let state1 = TimestampedPlayerArrayState::without_appearance(1022, 1.0, 2.0, 3.0);
        let change_listener_container = create_no_op_change_listeners();

        let (registration1, _) = state_manager.register_player_unchecked(
            username1.clone(),
            clone_value(&map_name1),
            state1.clone(),
            change_listener_container.clone(),
        );
        assert_unordered_items(
            &[named_state_list_from_single_state(
                username1.clone(),
                state1.clone(),
            )],
            &state_manager.get_player_state_lists(&map_name1),
        );

        let state2 = TimestampedPlayerArrayState::without_appearance(1023, 4.0, 5.0, 6.0);
        update_player_position(&state_manager, &registration1, state2.clone());
        assert_unordered_items(
            &[named_state_list_from_single_state(
                username1.clone(),
                state2.clone(),
            )],
            &state_manager.get_player_state_lists(&map_name1),
        );

        let username2: String = "user2".into();
        let (registration2, _) = state_manager.register_player_unchecked(
            username2.clone(),
            clone_value(&map_name1),
            state1.clone(),
            change_listener_container,
        );
        assert_unordered_items(
            &[
                named_state_list_from_single_state(username1.clone(), state2.clone()),
                named_state_list_from_single_state(username2.clone(), state1.clone()),
            ],
            &state_manager.get_player_state_lists(&map_name1),
        );

        let state3 = TimestampedPlayerArrayState::without_appearance(1024, 8.0, 9.0, 10.0);
        update_player_position(&state_manager, &registration1, state3.clone());
        assert_unordered_items(
            &[
                named_state_list_from_single_state(username1.clone(), state3.clone()),
                named_state_list_from_single_state(username2.clone(), state1),
            ],
            &state_manager.get_player_state_lists(&map_name1),
        );

        update_player_position(&state_manager, &registration2, state2.clone());
        assert_unordered_items(
            &[
                named_state_list_from_single_state(username1, state3),
                named_state_list_from_single_state(username2, state2),
            ],
            &state_manager.get_player_state_lists(&map_name1),
        );
    }

    #[test]
    fn should_cleanup_map_states_with_no_players() {
        let state_manager = SharedStateManager::new();

        let map_name1 = MapName("map1".into());
        let username1: String = "user".into();
        let state = TimestampedPlayerArrayState::without_appearance(1001, 1.0, 2.0, 3.0);
        let change_listener_container = create_no_op_change_listeners();

        let registration1 = state_manager.register_player_unchecked(
            username1.clone(),
            map_name1.clone(),
            state.clone(),
            change_listener_container.clone(),
        );
        assert_unordered_items(&[map_name1.clone()], &get_map_names(&state_manager));

        mem::drop(registration1);
        assert_unordered_items(&[], &get_map_names(&state_manager));

        let registration1 = state_manager.register_player_unchecked(
            username1.clone(),
            map_name1.clone(),
            state.clone(),
            change_listener_container.clone(),
        );
        assert_unordered_items(&[map_name1.clone()], &get_map_names(&state_manager));

        let username2: String = "user2".into();
        let registration2 = state_manager.register_player_unchecked(
            username2.clone(),
            map_name1.clone(),
            state.clone(),
            change_listener_container.clone(),
        );
        assert_unordered_items(&[map_name1.clone()], &get_map_names(&state_manager));

        mem::drop(registration1);
        assert_unordered_items(&[map_name1.clone()], &get_map_names(&state_manager));

        mem::drop(registration2);
        assert_unordered_items(&[], &get_map_names(&state_manager));

        let _registration1 = state_manager.register_player_unchecked(
            username1,
            map_name1.clone(),
            state.clone(),
            change_listener_container.clone(),
        );
        assert_unordered_items(&[map_name1.clone()], &get_map_names(&state_manager));

        let map_name2 = MapName("map2".into());
        let registration2 = state_manager.register_player_unchecked(
            username2,
            map_name2.clone(),
            state,
            change_listener_container,
        );
        assert_unordered_items(
            &[map_name1.clone(), map_name2],
            &get_map_names(&state_manager),
        );

        mem::drop(registration2);
        assert_unordered_items(&[map_name1], &get_map_names(&state_manager));
    }

    type ChangeList = Arc<Mutex<Vec<Arc<PlayerStateListWithUsernameRef>>>>;
    fn new_change_list() -> ChangeList {
        Arc::new(Mutex::new(Vec::new()))
    }
    type RemoveList = Arc<Mutex<Vec<Arc<String>>>>;
    fn new_remove_list() -> RemoveList {
        Arc::new(Mutex::new(Vec::new()))
    }

    fn create_change_listeners_for_lists(
        change_list: &ChangeList,
        remove_list: &RemoveList,
    ) -> ChangeListenerContainer {
        let change_list_ref = change_list.clone();
        let remove_list_ref = remove_list.clone();
        ChangeListenerContainer::new(
            Arc::new(move |player_state| change_list_ref.lock().unwrap().push(player_state)),
            Arc::new(move |username| remove_list_ref.lock().unwrap().push(username)),
        )
    }

    fn create_no_op_change_listeners() -> ChangeListenerContainer {
        ChangeListenerContainer::new(Arc::new(|_| {}), Arc::new(|_| {}))
    }

    #[test]
    fn test_change_listeners() {
        let state_manager = SharedStateManager::new();

        let map_name1 = MapName("map1".into());
        let username1: String = "user1".into();
        let state1 = TimestampedPlayerArrayState::without_appearance(1001, 1.0, 2.0, 3.0);

        let change_list1: ChangeList = new_change_list();
        let remove_list1: RemoveList = new_remove_list();
        let change_listener1: ChangeListenerContainer =
            create_change_listeners_for_lists(&change_list1, &remove_list1);

        let (registration1, _) = state_manager.register_player_unchecked(
            username1,
            map_name1.clone(),
            state1,
            change_listener1,
        );
        assert_empty(&change_list1);
        assert_empty(&remove_list1);

        let state2 = TimestampedPlayerArrayState::without_appearance(102, 4.0, 5.0, 6.0);
        update_player_position(&state_manager, &registration1, state2.clone());

        // It should not be notified of its own position change
        assert_empty(&change_list1);
        assert_empty(&remove_list1);

        let username2: String = "user2".into();
        let state3 = TimestampedPlayerArrayState::without_appearance(1023, 14.0, 15.0, 16.0);

        let change_list2: ChangeList = new_change_list();
        let remove_list2: RemoveList = new_remove_list();
        let change_listener2: ChangeListenerContainer =
            create_change_listeners_for_lists(&change_list2, &remove_list2);
        let (registration2, _) = state_manager.register_player_unchecked(
            username2,
            map_name1.clone(),
            state3.clone(),
            change_listener2,
        );

        assert_ordered_items_and_clear(
            &[Arc::new(PlayerStateListWithUsernameRef::from_single_state(
                registration2.username.clone(),
                state3.clone(),
            ))],
            &change_list1,
        );
        assert_empty(&remove_list1);

        assert_empty(&change_list2);
        assert_empty(&remove_list2);

        // Move player 2
        let state4 = TimestampedPlayerArrayState::without_appearance(104, 24.0, 25.0, 26.0);
        update_player_position(&state_manager, &registration2, state4.clone());

        assert_ordered_items_and_clear(
            &[Arc::new(PlayerStateListWithUsernameRef::from_single_state(
                registration2.username.clone(),
                state4,
            ))],
            &change_list1,
        );
        assert_empty(&remove_list1);

        // It should not be notified of its own position change
        assert_empty(&change_list2);
        assert_empty(&remove_list2);

        // Move player 1
        update_player_position(&state_manager, &registration1, state3.clone());

        // It should not be notified of its own position change
        assert_empty(&change_list1);
        assert_empty(&remove_list1);

        assert_ordered_items_and_clear(
            &[Arc::new(PlayerStateListWithUsernameRef::from_single_state(
                registration1.username.clone(),
                state3,
            ))],
            &change_list2,
        );
        assert_empty(&remove_list2);

        // Try adding and removing players
        let username3: String = "user3".into();
        let state5 = TimestampedPlayerArrayState::without_appearance(1005, 34.0, 35.0, 36.0);
        let change_listener3 = create_no_op_change_listeners();
        let (registration3, _) = state_manager.register_player_unchecked(
            username3,
            map_name1,
            state5.clone(),
            change_listener3,
        );

        let state_for_player3 = Arc::new(PlayerStateListWithUsernameRef::from_single_state(
            registration3.username.clone(),
            state5.clone(),
        ));

        assert_ordered_items_and_clear(&[state_for_player3.clone()], &change_list1);
        assert_empty(&remove_list1);

        assert_ordered_items_and_clear(&[state_for_player3], &change_list2);
        assert_empty(&remove_list2);

        let registration3_username = registration3.username.clone();
        mem::drop(registration3);
        assert_empty(&change_list1);
        assert_ordered_items_and_clear(&[registration3_username.clone()], &remove_list1);

        assert_empty(&change_list2);
        assert_ordered_items_and_clear(&[registration3_username], &remove_list2);

        let registration2_username = registration2.username.clone();
        mem::drop(registration2);
        assert_empty(&change_list1);
        assert_ordered_items_and_clear(&[registration2_username.clone()], &remove_list1);

        assert_empty(&change_list2);
        assert_empty(&remove_list2);
    }

    #[test]
    fn test_change_listeners_should_not_get_updates_from_other_maps() {
        let state_manager = SharedStateManager::new();

        let map_name1 = MapName("map1".into());
        let username1: String = "user1".into();
        let state1 = TimestampedPlayerArrayState::without_appearance(1001, 1.0, 2.0, 3.0);

        let change_list1: ChangeList = new_change_list();
        let remove_list1: RemoveList = new_remove_list();
        let change_listener1: ChangeListenerContainer =
            create_change_listeners_for_lists(&change_list1, &remove_list1);

        let _registration1 =
            state_manager.register_player_unchecked(username1, map_name1, state1, change_listener1);
        assert_empty(&change_list1);
        assert_empty(&remove_list1);

        let username2: String = "user2".into();
        let map_name2 = MapName("map2".into());
        let state2 = TimestampedPlayerArrayState::without_appearance(1002, 4.0, 5.0, 6.0);
        let change_listener2 = create_no_op_change_listeners();

        let (registration2, _) =
            state_manager.register_player_unchecked(username2, map_name2, state2, change_listener2);

        assert_empty(&change_list1);
        assert_empty(&remove_list1);

        let state3 = TimestampedPlayerArrayState::without_appearance(1003, 14.0, 15.0, 16.0);
        update_player_position(&state_manager, &registration2, state3);

        // The other player is in a different map, so the first change listener should not be notified of the changes.
        assert_empty(&change_list1);
        assert_empty(&remove_list1);

        // The removed listener should not get notified if a player is removed on another map.
        mem::drop(registration2);
        assert_empty(&change_list1);
        assert_empty(&remove_list1);
    }

    #[test]
    fn test_map_switch() {
        let state_manager = SharedStateManager::new();

        let map_name1 = MapName("map1".into());
        let map_name2 = MapName("map2".into());

        // Add player 1
        let username1: String = "user1".into();
        let state1 = TimestampedPlayerArrayState::without_appearance(1001, 1.0, 2.0, 3.0);

        let change_list1: ChangeList = new_change_list();
        let remove_list1: RemoveList = new_remove_list();
        let (mut registration1, _) = state_manager.register_player_unchecked(
            username1,
            map_name1.clone(),
            state1,
            create_change_listeners_for_lists(&change_list1, &remove_list1),
        );

        // Add player 2
        let username2: String = "user2".into();
        let state2 = TimestampedPlayerArrayState::without_appearance(1002, 1.0, 2.0, 4.0);

        let change_list2: ChangeList = new_change_list();
        let remove_list2: RemoveList = new_remove_list();
        let (mut registration2, _) = state_manager.register_player_unchecked(
            username2,
            map_name1,
            state2,
            create_change_listeners_for_lists(&change_list2, &remove_list2),
        );

        // These will have changes from this setup, but we can ignore it
        clear_locked_list(&change_list1);
        clear_locked_list(&remove_list1);

        // Add player 3 to map 2
        let username3: String = "user3".into();
        let state3 = TimestampedPlayerArrayState::without_appearance(1003, 1.0, 1.0, 1.0);

        let change_list3: ChangeList = new_change_list();
        let remove_list3: RemoveList = new_remove_list();
        let (registration3, _) = state_manager.register_player_unchecked(
            username3,
            map_name2.clone(),
            state3,
            create_change_listeners_for_lists(&change_list3, &remove_list3),
        );

        // Move player 2 to map 2
        let player2_map2_state =
            TimestampedPlayerArrayState::without_appearance(102, 1.0, 1.0, 8.0);
        state_manager.update_for_map_switch(
            &mut registration2,
            Arc::new(map_name2.clone()),
            player2_map2_state.clone(),
            || {},
        );

        assert_eq!(&map_name2, registration2.map_name.as_ref());

        assert_empty(&change_list1);
        assert_ordered_items_and_clear(&[registration2.username.clone()], &remove_list1);

        assert_empty(&change_list2);
        assert_empty(&remove_list2);

        assert_ordered_items_and_clear(
            &[Arc::new(PlayerStateListWithUsernameRef::from_single_state(
                registration2.username.clone(),
                player2_map2_state,
            ))],
            &change_list3,
        );
        assert_empty(&remove_list3);

        // Change player 1's position
        let new_player1_state = TimestampedPlayerArrayState::without_appearance(101, 5.0, 5.0, 1.0);
        update_player_position(&state_manager, &registration1, new_player1_state.clone());

        // It should not be notified about its own movement
        assert_empty(&change_list1);
        assert_empty(&remove_list1);

        // Player 2 is on a new map, so it should not hear about a change from player 1
        assert_empty(&change_list2);
        assert_empty(&remove_list2);

        assert_empty(&change_list3);
        assert_empty(&remove_list3);

        // Change player 3's position
        let new_player3_state = TimestampedPlayerArrayState::without_appearance(103, 1.0, 1.0, 7.0);
        update_player_position(&state_manager, &registration3, new_player3_state.clone());

        assert_empty(&change_list1);
        assert_empty(&remove_list1);

        assert_ordered_items_and_clear(
            &[Arc::new(PlayerStateListWithUsernameRef::from_single_state(
                registration3.username.clone(),
                new_player3_state,
            ))],
            &change_list2,
        );
        assert_empty(&remove_list2);

        assert_empty(&change_list3);
        assert_empty(&remove_list3);

        // Change player 2's position
        let new_player2_state = TimestampedPlayerArrayState::without_appearance(102, 1.0, 3.0, 8.0);
        update_player_position(&state_manager, &registration2, new_player2_state.clone());

        assert_empty(&change_list1);
        assert_empty(&remove_list1);

        assert_empty(&change_list2);
        assert_empty(&remove_list2);

        assert_ordered_items_and_clear(
            &[Arc::new(PlayerStateListWithUsernameRef::from_single_state(
                registration2.username.clone(),
                new_player2_state,
            ))],
            &change_list3,
        );
        assert_empty(&remove_list3);

        // Move player 1 to map 2
        let player1_map2_state =
            TimestampedPlayerArrayState::without_appearance(112, 12.0, 10.0, 4.0);
        state_manager.update_for_map_switch(
            &mut registration1,
            Arc::new(map_name2.clone()),
            player1_map2_state.clone(),
            || {},
        );

        assert_empty(&change_list1);
        assert_empty(&remove_list1);

        let expected_update = &[Arc::new(PlayerStateListWithUsernameRef::from_single_state(
            registration1.username.clone(),
            player1_map2_state,
        ))];

        assert_ordered_items_and_clear(expected_update, &change_list2);
        assert_empty(&remove_list2);

        assert_ordered_items_and_clear(expected_update, &change_list3);
        assert_empty(&remove_list3);

        // Make sure map 1 has been cleaned up
        assert_unordered_items(&[map_name2.clone()], &get_map_names(&state_manager));
    }

    #[test]
    fn test_get_player_count() {
        let state_manager = SharedStateManager::new();
        assert_eq!(0, state_manager.get_player_count());

        let map_name1 = MapName("map1".into());
        let map_name2 = MapName("map2".into());

        let state = TimestampedPlayerArrayState::without_appearance(1001, 1.0, 2.0, 3.0);
        let register_player = |username: &'static str, map_name: &MapName| {
            let (registration, _) = state_manager.register_player_unchecked(
                username.into(),
                map_name.clone(),
                state.clone(),
                create_no_op_change_listeners(),
            );
            registration
        };

        let registration1 = register_player("player1", &map_name1);
        assert_eq!(1, state_manager.get_player_count());

        let _registration2 = register_player("player2", &map_name1);
        assert_eq!(2, state_manager.get_player_count());

        let _registration3 = register_player("player3", &map_name2);
        assert_eq!(3, state_manager.get_player_count());

        mem::drop(registration1);
        assert_eq!(2, state_manager.get_player_count());
    }

    fn get_map_names(state_manager: &SharedStateManager) -> Vec<MapName> {
        let server_state = state_manager.server_state.lock().unwrap();
        let mut map_names = Vec::<MapName>::with_capacity(server_state.map_states.len());
        map_names.extend(server_state.map_states.keys().map(|name| clone_value(name)));
        map_names
    }

    fn assert_arc_eq(expected: &str, actual: &Arc<String>) {
        let actual_value: &String = &actual;
        assert_eq!(expected, actual_value);
    }

    fn assert_unordered_items<T: PartialEq>(expected: &[T], actual: &[T]) {
        assert_eq!(expected.len(), actual.len());
        for value in expected {
            assert_eq!(true, actual.contains(value));
        }
    }

    fn assert_ordered_items_and_clear<T: PartialEq + std::fmt::Debug>(
        expected: &[T],
        actual: &Arc<Mutex<Vec<T>>>,
    ) {
        let mut unlocked_actual = actual.lock().unwrap();
        assert_eq!(expected.len(), unlocked_actual.len());
        for (i, value) in expected.iter().enumerate() {
            assert_eq!(value, &unlocked_actual[i])
        }
        unlocked_actual.clear();
    }

    fn clear_locked_list<T>(list: &Arc<Mutex<Vec<T>>>) {
        let mut unlocked_list = list.lock().unwrap();
        unlocked_list.clear();
    }

    fn assert_empty<T>(locked_vector: &Arc<Mutex<Vec<T>>>) {
        assert!(locked_vector.lock().unwrap().is_empty());
    }

    fn update_player_position(
        shared_state_manager: &SharedStateManager,
        registration: &PlayerRegistration,
        state: TimestampedPlayerArrayState,
    ) {
        let state_list = PlayerStateList::from_single_state(state);
        shared_state_manager.update_state_list(registration, state_list);
    }

    fn named_state_list_from_single_state(
        username: String,
        state: TimestampedPlayerArrayState,
    ) -> NamedPlayerStateList {
        NamedPlayerStateList::new(username, PlayerStateList::from_single_state(state))
    }

    impl SharedStateManager {
        pub fn register_player_unchecked(
            &self,
            proposed_username: String,
            map: MapName,
            state: TimestampedPlayerArrayState,
            change_listener_container: ChangeListenerContainer,
        ) -> (PlayerRegistration, Vec<NamedPlayerStateList>) {
            self.register_player(
                proposed_username,
                map,
                state,
                change_listener_container,
                1000,
            )
            .unwrap()
        }
    }
}
