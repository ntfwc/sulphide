use pin_project_lite::pin_project;
use std::io::Error;
use std::pin::Pin;
use std::task::{Context, Poll};
use tokio::io::{AsyncRead, AsyncWrite, ReadBuf};

use crate::statistics::StatisticsCollector;

pin_project! {
    /// An AsyncWrite wrapper that reports written bytes to the statistics collector
    pub struct AsyncStatWrite<W: AsyncWrite> {
        #[pin]
        writer: W,
        statistics_collector: StatisticsCollector,
    }
}

impl<W: AsyncWrite> AsyncStatWrite<W> {
    pub fn new(writer: W, statistics_collector: StatisticsCollector) -> Self {
        Self {
            writer,
            statistics_collector,
        }
    }
}

impl<W: AsyncWrite> AsyncWrite for AsyncStatWrite<W> {
    fn poll_write(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &[u8],
    ) -> std::task::Poll<Result<usize, Error>> {
        let this = self.project();
        let result = this.writer.poll_write(cx, buf);
        if let Poll::Ready(Ok(bytes_written)) = result {
            this.statistics_collector
                .notify_of_bytes_transmitted(bytes_written);
        }

        result
    }

    fn poll_flush(
        self: Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Result<(), Error>> {
        self.project().writer.poll_flush(cx)
    }

    fn poll_shutdown(
        self: Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Result<(), Error>> {
        self.project().writer.poll_shutdown(cx)
    }
}

pin_project! {
    /// An AsyncRead wrapper that reports received bytes to the statistics collector
    pub struct AsyncStatRead<R: AsyncRead> {
        #[pin]
        reader: R,
        statistics_collector: StatisticsCollector,
    }
}

impl<R: AsyncRead> AsyncStatRead<R> {
    pub fn new(reader: R, statistics_collector: StatisticsCollector) -> Self {
        Self {
            reader,
            statistics_collector,
        }
    }
}

impl<R: AsyncRead> AsyncRead for AsyncStatRead<R> {
    fn poll_read(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &mut ReadBuf<'_>,
    ) -> Poll<std::io::Result<()>> {
        let this = self.project();
        let result = this.reader.poll_read(cx, buf);
        if let Poll::Ready(Ok(())) = result {
            let amount_read = buf.filled().len();
            if amount_read != 0 {
                this.statistics_collector
                    .notify_of_bytes_received(amount_read);
            }
        }

        result
    }
}
