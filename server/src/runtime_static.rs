use crate::connection::{write_server_message_size, InitialConnectionWriter};
use crate::messages_from_server::{ClientRejectionMessage, ServerMessage};

use async_compression::tokio::write::DeflateEncoder;
use std::sync::Arc;
use tokio::io::AsyncWriteExt;

#[derive(Clone)]
pub struct RuntimeStaticValues {
    pub version1_client_rejection_message: PreparedRejectionMessage,
    pub max_clients_reached_rejection_message: PreparedRejectionMessage,
}

impl RuntimeStaticValues {
    pub async fn new() -> Self {
        Self {
            version1_client_rejection_message: PreparedRejectionMessage::prepare(
                "Outdated client mod. Requires version 2",
            )
            .await,
            max_clients_reached_rejection_message: PreparedRejectionMessage::prepare(
                "The server has reached its max player count",
            )
            .await,
        }
    }
}

#[derive(Clone)]
pub struct PreparedRejectionMessage {
    bytes: Arc<[u8]>,
}

impl PreparedRejectionMessage {
    async fn prepare(message: &str) -> Self {
        let rejection_message = ClientRejectionMessage::new(message.into());
        let json_string = rejection_message.to_json();
        Self {
            bytes: to_compressed_byte_message(&json_string)
                .await
                .into_boxed_slice()
                .into(),
        }
    }

    pub async fn write(&self, writer: &mut InitialConnectionWriter) -> Result<(), std::io::Error> {
        writer.write_and_flush(&self.bytes).await
    }
}

async fn to_compressed_byte_message(string: &str) -> Vec<u8> {
    let byte_message = string.as_bytes();
    let mut buffer = Vec::<u8>::new();
    {
        let mut encoder = DeflateEncoder::new(&mut buffer);
        write_server_message_size(&mut encoder, byte_message.len() as u32)
            .await
            .expect("Writing to memory should never fail");
        encoder
            .write_all(byte_message)
            .await
            .expect("Writing to memory should never fail");
        encoder
            .flush()
            .await
            .expect("Flushing to memory should never fail");
    }
    buffer
}
