use crate::connection::*;
use crate::header_exchange::{handle_protocol_header_exchange, ProtocolHeaderExchangeError};
use crate::ip_address_id_generator::SocketAddressId;
use crate::json_security::looks_like_malicious_json;
use crate::messages_from_client::*;
use crate::messages_from_server::*;
use crate::model::main::*;
use crate::model::parsed_json_message::*;
use crate::model::shared_state_model::ChangeListenerContainer;
use crate::runtime_static::{PreparedRejectionMessage, RuntimeStaticValues};
use crate::shared_state::{
    clone_value, PlayerRegistration, RegisterPlayerError, SharedStateManager,
};
use crate::statistics::StatisticsCollector;
use crate::throttler::Throttler;
use crate::update_collector::UpdateCollector;

use futures::future::{AbortHandle, Abortable};
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use tokio::net::TcpStream;
use tokio::time::{self, Duration};

macro_rules! get_or_fail {
    ($expr: expr, $error_fmt: expr, $address: expr) => {
        match $expr {
            Ok(val) => val,
            Err(e) => {
                log!($error_fmt, $address, e);
                return;
            }
        }
    };
}

pub async fn handle_client(
    stream: TcpStream,
    address: SocketAddressId,
    shared_state_manager: SharedStateManager,
    statistics_collector: StatisticsCollector,
    max_player_count: u32,
    runtime_static_values: RuntimeStaticValues,
) {
    let (reader, writer) = stream.into_split();

    let mut initial_reader = InitialConnectionReader::new(reader, statistics_collector.clone());
    let mut initial_writer = InitialConnectionWriter::new(writer, statistics_collector);
    if let Err(error) =
        handle_protocol_header_exchange(&mut initial_reader, &mut initial_writer, &address).await
    {
        log!(
            "Protocol header exchange failed. Error={:?}. Dropping connection with {}",
            error,
            address
        );
        if let ProtocolHeaderExchangeError::Version1ClientHeaderReceived = error {
            trace_log!(
                "Sending a version 1 client rejection message to {}",
                address
            );
            send_client_rejection(
                &mut initial_writer,
                &runtime_static_values.version1_client_rejection_message,
                &address,
            )
            .await;
        }
        return;
    }

    if shared_state_manager.get_player_count() >= max_player_count {
        log!(
            "Server already has too many players. Dropping connection with {}",
            address
        );
        send_client_rejection(
            &mut initial_writer,
            &runtime_static_values.max_clients_reached_rejection_message,
            &address,
        )
        .await;
        return;
    }

    let mut connection_reader = ClientConnectionReader::new(initial_reader, address);

    let message: String = match time::timeout(
        Duration::from_secs(5),
        connection_reader.read_client_message(),
    )
    .await
    {
        Ok(inner_result) => {
            get_or_fail!(
                inner_result,
                "Failed to read introduction message from {}, err = {:?}",
                address
            )
        }
        Err(_) => {
            log!(
                "Timed out waiting for introduction message from {}",
                address
            );
            return;
        }
    };
    trace_log!("Received message: {}", message);

    if looks_like_malicious_json(&message) {
        log!("JSON received for the introduction message, from {}, looks excessive to the point of being malicious. Rejecting connection.", address);
        return;
    }

    let introduction = {
        let parsed_json_message = get_or_fail!(
            parse_json_message(&message),
            "Failed to read introduction message from {}, err = {:?}",
            address
        );
        get_or_fail!(
            ClientIntroductionMessage::from_json(parsed_json_message),
            "Failed to read introduction message from {}, err = {:?}",
            address
        )
    };
    log!("Received introduction from {}: {}", address, introduction);

    let update_collector = UpdateCollector::new();
    let (player_registration, initial_map_player_state_lists) = {
        let collector_ref1 = update_collector.clone();
        let collector_ref2 = update_collector.clone();
        match shared_state_manager.register_player(
            introduction.username,
            introduction.map,
            introduction.state,
            ChangeListenerContainer::new(
                Arc::new(move |player_state_list| collector_ref1.add_change(player_state_list)),
                Arc::new(move |username| collector_ref2.add_removed_player(username)),
            ),
            max_player_count,
        ) {
            Ok(valid_registration_result) => valid_registration_result,
            Err(error) => {
                log!(
                    "Failed to register player. Dropping connection with {}. Error={:?}",
                    address,
                    error
                );
                let prepared_rejection_message = match error {
                    RegisterPlayerError::AtMaxPlayerCount => {
                        &runtime_static_values.max_clients_reached_rejection_message
                    }
                };

                send_client_rejection(&mut initial_writer, prepared_rejection_message, &address)
                    .await;
                return;
            }
        }
    };

    let mut connection_writer = ClientConnectionWriter::new(initial_writer, address);

    if !connection_writer
        .write_message(ClientIntroductionReplyMessage::new(clone_value(
            &player_registration.username,
        )))
        .await
    {
        log!("Failed to write introduction response to {}", address);
        return;
    }

    let writing_failed = Arc::new(AtomicBool::new(false));

    let _stop_update_task: SubTaskStopper = {
        let update_collector_ref = update_collector.clone();
        let mut connection_writer_ref = connection_writer.clone();
        let writing_failed_ref = writing_failed.clone();
        let (abort_handle, abort_registration) = AbortHandle::new_pair();
        let update_task = Abortable::new(
            async move {
                if !send_initial_map_state(
                    &mut connection_writer_ref,
                    initial_map_player_state_lists,
                )
                .await
                {
                    log!(
                        "Failed to write initial map state message, closing write stream for {}",
                        address
                    );
                    return;
                }

                handle_sending_client_updates(connection_writer_ref, update_collector_ref, address)
                    .await;
                writing_failed_ref.store(true, Ordering::Relaxed);
            },
            abort_registration,
        );
        tokio::spawn(update_task);

        SubTaskStopper::new(Box::new(move || abort_handle.abort()))
    };

    let mut client_input_handler = ClientInputHandler::new(
        connection_reader,
        connection_writer,
        address,
        player_registration,
        shared_state_manager,
        update_collector,
    );
    client_input_handler.main_loop(writing_failed).await;
}

async fn send_initial_map_state(
    connection_writer: &mut ClientConnectionWriter,
    player_state_lists: Vec<NamedPlayerStateList>,
) -> bool {
    connection_writer
        .write_message(StateUpdateMessage::new(player_state_lists, vec![]))
        .await
}

async fn send_client_rejection(
    writer: &mut InitialConnectionWriter,
    prepared_message: &PreparedRejectionMessage,
    address: &SocketAddressId,
) {
    if let Err(error) = prepared_message.write(writer).await {
        log!(
            "Failed to send a client rejection message to {}. Error={:?}",
            address,
            error
        );
    }
}

async fn handle_sending_client_updates(
    mut connection_writer: ClientConnectionWriter,
    update_collector: UpdateCollector,
    address: SocketAddressId,
) {
    let mut interval = time::interval(Duration::from_millis(1000 / 6));
    // The first tick returns immediately
    interval.tick().await;

    loop {
        interval.tick().await;

        let (updates, removed_players) = update_collector.flush();
        let player_states: Vec<NamedPlayerStateList> = map_changes(updates);
        let message = StateUpdateMessage::new(player_states, copy_arc_strings(removed_players));
        if !connection_writer.write_message(message).await {
            log!(
                "Failed to write message, closing write stream for {}",
                address
            );
            return;
        }
    }
}

fn map_changes(changes: Vec<Arc<PlayerStateListWithUsernameRef>>) -> Vec<NamedPlayerStateList> {
    let mut mapped_changes = Vec::<NamedPlayerStateList>::with_capacity(changes.len());
    mapped_changes.extend(changes.into_iter().map(|state_list| {
        NamedPlayerStateList::new(
            clone_value(&state_list.username),
            state_list.state_list.clone(),
        )
    }));
    mapped_changes
}

fn copy_arc_strings(arc_strings: Vec<Arc<String>>) -> Vec<String> {
    if arc_strings.is_empty() {
        return vec![];
    }

    let mut result = Vec::with_capacity(arc_strings.len());
    arc_strings
        .into_iter()
        .map(|arc_string| String::clone(&arc_string))
        .for_each(|value| result.push(value));
    result
}

struct ClientInputHandler {
    connection_reader: ClientConnectionReader,
    connection_writer: ClientConnectionWriter,
    address: SocketAddressId,
    player_registration: PlayerRegistration,
    shared_state_manager: SharedStateManager,
    update_collector: UpdateCollector,
}

impl ClientInputHandler {
    fn new(
        connection_reader: ClientConnectionReader,
        connection_writer: ClientConnectionWriter,
        address: SocketAddressId,
        player_registration: PlayerRegistration,
        shared_state_manager: SharedStateManager,
        update_collector: UpdateCollector,
    ) -> Self {
        Self {
            connection_reader,
            connection_writer,
            address,
            player_registration,
            shared_state_manager,
            update_collector,
        }
    }

    async fn main_loop(&mut self, writing_failed: Arc<AtomicBool>) {
        let mut message_throttler = Throttler::new(Duration::from_millis(1000 / 10));
        loop {
            if writing_failed.load(Ordering::Relaxed) {
                return;
            }

            let message = get_or_fail!(
                self.connection_reader.read_client_message().await,
                "Failed to read message from {}, err = {:?}",
                self.address
            );

            if looks_like_malicious_json(&message) {
                log!("JSON received, from {}, excessive to the point of being malicious. Rejecting connection.", self.address);
                return;
            }

            let parsed_json: ParsedJsonMessage = get_or_fail!(
                parse_json_message(&message),
                "Failed to parse JSON from {}: {:?}",
                self.address
            );

            let message_type = match parsed_json.get_type() {
                Some(message_type) => message_type,
                None => {
                    log!("Message from client, {}, had no type field.", self.address);
                    return;
                }
            };

            macro_rules! parse_or_fail {
                ($client_message: ident) => {
                    match $client_message::from_json(parsed_json) {
                        Ok(parsed) => parsed,
                        Err(e) => {
                            log!(
                                "Parsing '$client_message' from {} failed: {:?}",
                                self.address,
                                e
                            );
                            return;
                        }
                    }
                };
            }

            match message_type {
                ClientStateUpdateMessage::TYPE => {
                    let message = parse_or_fail!(ClientStateUpdateMessage);
                    self.handle_client_state_update_message(message);
                }
                MapSwitchMessage::TYPE => {
                    let message = parse_or_fail!(MapSwitchMessage);
                    if !self.handle_map_switch_message(message).await {
                        log!("Sending the map switch reply to {} failed", self.address);
                        return;
                    }
                }
                _ => {
                    log!(
                        "Message from client, {}, had unhandled type '{}'",
                        self.address,
                        message_type
                    );
                    return;
                }
            }

            // Make sure we don't consume messages from a client quicker than would be reasonable.
            message_throttler.tick().await;
        }
    }

    fn handle_client_state_update_message(&mut self, message: ClientStateUpdateMessage) {
        trace_log!(
            "Received state update message {:?} for '{}'",
            message,
            self.player_registration.username
        );

        let state_list = PlayerStateList::new(message.states);
        self.shared_state_manager
            .update_state_list(&self.player_registration, state_list);
    }

    /// Handles a map switch `message` and tries to send a reply.
    ///
    /// Returns True if sending the reply succeeded, false otherwise.
    async fn handle_map_switch_message(&mut self, message: MapSwitchMessage) -> bool {
        trace_log!(
            "Received map switch message {:?} for '{}'",
            message,
            self.player_registration.username
        );

        let update_collector = &self.update_collector;
        log!(
            "Switching map of user '{}' to '{:?}'",
            self.player_registration.username,
            message.new_map
        );
        let player_state_lists = match self.shared_state_manager.update_for_map_switch(
            &mut self.player_registration,
            Arc::new(message.new_map),
            message.new_state,
            || update_collector.clear(),
        ) {
            Some(states) => states,
            None => return false,
        };

        self.send_map_switch_reply_message(player_state_lists).await
    }

    async fn send_map_switch_reply_message(
        &mut self,
        player_state_lists: Vec<NamedPlayerStateList>,
    ) -> bool {
        self.connection_writer
            .write_message(MapSwitchReplyMessage::new(player_state_lists))
            .await
    }
}

struct SubTaskStopper {
    stop_task: Box<dyn FnMut() + Send>,
}

impl SubTaskStopper {
    fn new(stop_task: Box<dyn FnMut() + Send>) -> Self {
        SubTaskStopper { stop_task }
    }
}

impl Drop for SubTaskStopper {
    fn drop(&mut self) {
        let stop_task = &mut self.stop_task;
        stop_task();
    }
}
