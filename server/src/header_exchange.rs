use crate::connection::{InitialConnectionReader, InitialConnectionWriter};
use crate::ip_address_id_generator::SocketAddressId;
use crate::protocol_headers::*;

use tokio::time::{self, Duration};

/// Handles the protocol header exchange. Returns true if it succeds, false otherwise.
pub async fn handle_protocol_header_exchange(
    reader: &mut InitialConnectionReader,
    writer: &mut InitialConnectionWriter,
    address: &SocketAddressId,
) -> Result<(), ProtocolHeaderExchangeError> {
    match time::timeout(
        Duration::from_secs(5),
        handle_protocol_header_exchange_inner(reader, writer, address),
    )
    .await
    {
        Ok(result) => result,
        Err(_) => Err(ProtocolHeaderExchangeError::Timeout),
    }
}

async fn handle_protocol_header_exchange_inner(
    reader: &mut InitialConnectionReader,
    writer: &mut InitialConnectionWriter,
    address: &SocketAddressId,
) -> Result<(), ProtocolHeaderExchangeError> {
    trace_log!("Reading protocol header from {}", address);
    if let ProtocolHeaderType::Version1Client = read_header(reader).await? {
        return Err(ProtocolHeaderExchangeError::Version1ClientHeaderReceived);
    }

    trace_log!("Writing protocol header from {}", address);
    writer.write_and_flush(&SERVER_HEADER).await?;
    Ok(())
}

#[derive(Debug)]
pub enum ProtocolHeaderExchangeError {
    Timeout,
    IOError(std::io::Error),
    IncorrectHeaderReceived,
    Version1ClientHeaderReceived,
}

impl From<std::io::Error> for ProtocolHeaderExchangeError {
    fn from(error: std::io::Error) -> Self {
        ProtocolHeaderExchangeError::IOError(error)
    }
}

async fn read_header(
    reader: &mut InitialConnectionReader,
) -> Result<ProtocolHeaderType, ProtocolHeaderExchangeError> {
    let mut client_header_iter = CLIENT_HEADER.iter();

    let first_byte = &reader.read_u8().await?;
    if first_byte
        == client_header_iter
            .next()
            .expect("There must be at least one byte")
    {
        for byte in client_header_iter {
            if byte != &reader.read_u8().await? {
                return Err(ProtocolHeaderExchangeError::IncorrectHeaderReceived);
            }
        }

        return Ok(ProtocolHeaderType::Client);
    }

    if first_byte == &VERSION1_CLIENT_HEADER_BYTE1 {
        let _ = reader.read_u8().await?;
        let _ = reader.read_u8().await?;
        let fourth_byte = reader.read_u8().await?;
        if fourth_byte == VERSION1_CLIENT_HEADER_BYTE4_1
            || fourth_byte == VERSION1_CLIENT_HEADER_BYTE4_2
        {
            return Ok(ProtocolHeaderType::Version1Client);
        }
    }

    Err(ProtocolHeaderExchangeError::IncorrectHeaderReceived)
}

enum ProtocolHeaderType {
    Client,
    Version1Client,
}
