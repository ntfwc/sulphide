use std::{ops::Deref, sync::Arc};

enum StringRef {
    Arc(Arc<String>),
    Box(String),
    Static(&'static str),
}

impl Deref for StringRef {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        match self {
            Self::Arc(arc) => arc,
            Self::Box(box_str) => box_str,
            Self::Static(static_ref) => static_ref,
        }
    }
}

pub struct JsonStringRef {
    string_ref: StringRef,
    pub remove_first_character: bool,
    pub remove_last_character: bool,
}

impl JsonStringRef {
    fn new(string_ref: StringRef) -> Self {
        Self {
            string_ref,
            remove_first_character: false,
            remove_last_character: false,
        }
    }
}

impl Deref for JsonStringRef {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        let mut result_str: &str = &self.string_ref;
        result_str = if self.remove_first_character {
            &result_str[1..]
        } else {
            result_str
        };

        if self.remove_last_character {
            &result_str[..result_str.len() - 1]
        } else {
            result_str
        }
    }
}

impl From<String> for JsonStringRef {
    fn from(string: String) -> Self {
        JsonStringRef::new(StringRef::Box(string))
    }
}

impl From<Arc<String>> for JsonStringRef {
    fn from(arc: Arc<String>) -> Self {
        JsonStringRef::new(StringRef::Arc(arc))
    }
}

impl From<&'static str> for JsonStringRef {
    fn from(static_str: &'static str) -> Self {
        JsonStringRef::new(StringRef::Static(static_str))
    }
}

pub struct JsonStringRefList {
    pub(self) vec: Vec<JsonStringRef>,
}

impl JsonStringRefList {
    pub fn new(json_string_ref: JsonStringRef) -> Self {
        JsonStringRefList {
            vec: vec![json_string_ref],
        }
    }

    #[allow(unused)]
    pub fn merge(&mut self, mut other: JsonStringRefList) {
        let mut last_item = match self.vec.last_mut() {
            Some(item) => item,
            None => {
                self.vec.append(&mut other.vec);
                return;
            }
        };

        let mut first_other_item = match other.vec.first_mut() {
            Some(item) => item,
            None => {
                return;
            }
        };

        last_item.remove_last_character = true;
        first_other_item.remove_first_character = true;

        self.vec.push(",".into());
        self.vec.append(&mut other.vec);
    }

    pub fn insert_as_value(&mut self, variable_name: &'static str, mut other: JsonStringRefList) {
        let mut last_item = match self.vec.last_mut() {
            Some(item) => item,
            None => {
                self.vec.append(&mut other.vec);
                return;
            }
        };

        last_item.remove_last_character = true;
        self.vec.push(",\"".into());
        self.vec.push(variable_name.into());
        self.vec.push("\":".into());
        self.vec.append(&mut other.vec);
        self.vec.push("}".into());
    }

    pub fn to_json_string(&self) -> String {
        let mut buffer = String::new();
        for item in &self.vec {
            buffer += item;
        }
        buffer
    }
}

pub fn to_array_string(mut json_string_vec: Vec<JsonStringRefList>) -> JsonStringRefList {
    let mut new_list = JsonStringRefList::new("[".into());
    json_string_vec
        .drain(..)
        .enumerate()
        .for_each(|(index, mut json_string)| {
            if index > 0 {
                new_list.vec.push(",".into());
            }

            new_list.vec.append(&mut json_string.vec);
        });

    new_list.vec.push("]".into());

    new_list
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_json_merge() {
        let mut json1 = JsonStringRefList::new(r#"{"a":1}"#.into());
        let json2 = JsonStringRefList::new(r#"{"b":2}"#.into());
        json1.merge(json2);
        assert_eq!(r#"{"a":1,"b":2}"#, json1.to_json_string());

        let json3 = JsonStringRefList::new(r#"{"c":3.4}"#.into());
        json1.merge(json3);
        assert_eq!(r#"{"a":1,"b":2,"c":3.4}"#, json1.to_json_string());
    }

    #[test]
    fn test_insert_as_value() {
        let mut json1 = JsonStringRefList::new(r#"{"a":1}"#.into());
        let json2 = JsonStringRefList::new(r#"2"#.into());
        json1.insert_as_value("b", json2);
        assert_eq!(r#"{"a":1,"b":2}"#, json1.to_json_string());

        let json3 = JsonStringRefList::new(r#"{"x":3.2,"y":2,"z":4.4}"#.into());
        json1.insert_as_value("someObject", json3);
        assert_eq!(
            r#"{"a":1,"b":2,"someObject":{"x":3.2,"y":2,"z":4.4}}"#,
            json1.to_json_string()
        );
    }

    #[test]
    fn test_to_array_string() {
        assert_eq!(
            r#"[{"a":1}]"#,
            to_array_string(vec!(JsonStringRefList::new(r#"{"a":1}"#.into()),)).to_json_string()
        );

        assert_eq!(
            r#"[{"a":1},{"b":2}]"#,
            to_array_string(vec!(
                JsonStringRefList::new(r#"{"a":1}"#.into()),
                JsonStringRefList::new(r#"{"b":2}"#.into()),
            ))
            .to_json_string()
        );

        let mut merged_json = JsonStringRefList::new(r#"{"c":5}"#.into());
        merged_json.merge(JsonStringRefList::new(r#"{"d":6}"#.into()));
        assert_eq!(
            r#"[{"a":1},{"b":2},{"c":5,"d":6}]"#,
            to_array_string(vec!(
                JsonStringRefList::new(r#"{"a":1}"#.into()),
                JsonStringRefList::new(r#"{"b":2}"#.into()),
                merged_json
            ))
            .to_json_string()
        );
    }
}
