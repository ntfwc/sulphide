use super::advanced_json_string::*;
use super::main::*;
use super::player_array_state::TimestampedPlayerArrayState;
use json::*;

impl PlayerPosition {
    pub fn add_to_json(&self, json_object: &mut JsonValue) {
        json_object["x"] = self.x.into();
        json_object["y"] = self.y.into();
        json_object["z"] = self.z.into();
    }

    pub fn to_json_object(&self) -> JsonValue {
        let mut json_object = JsonValue::new_object();
        self.add_to_json(&mut json_object);
        json_object
    }
}

impl TimestampedPlayerArrayState {
    pub const TIMESTAMP_FIELD_NAME: &'static str = "timestamp";
    pub const POSITION_FIELD_NAME: &'static str = "position";
    pub const APPEARANCE_STATE_FIELD_NAME: &'static str = "appearanceState";

    pub fn add_to_json(&self, json_object: &mut JsonValue) {
        json_object[Self::TIMESTAMP_FIELD_NAME] = self.timestamp.into();
        json_object[Self::POSITION_FIELD_NAME] = self.position.to_json_object();
        if let Some(state) = &self.appearance_state {
            json_object[Self::APPEARANCE_STATE_FIELD_NAME] = state.to_json_object()
        }
    }
}

impl MessagePlayerStateList {
    pub fn to_json(&self) -> String {
        let mut value_vec: Vec<JsonValue> = Vec::with_capacity(self.len());
        for state in self.iter() {
            let mut state_object = JsonValue::new_object();
            state.add_to_json(&mut state_object);
            value_vec.push(state_object)
        }
        JsonValue::Array(value_vec).dump()
    }
}

impl NamedPlayerStateList {
    pub fn to_json(&self) -> JsonStringRefList {
        let mut json_object = JsonValue::new_object();
        json_object["username"] = self.username.clone().into();

        let mut list = JsonStringRefList::new(json_object.dump().into());
        list.insert_as_value(
            "state_list",
            JsonStringRefList::new(self.state_list.to_json().into()),
        );
        list
    }
}
