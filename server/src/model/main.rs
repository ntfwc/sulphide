use super::player_array_state::TimestampedPlayerArrayState;
use std::sync::Arc;

/// Represents the position of a player on a map.
#[derive(Debug, PartialEq, Clone)]
pub struct PlayerPosition {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl PlayerPosition {
    #[allow(unused)]
    pub fn new(x: f64, y: f64, z: f64) -> Self {
        Self { x, y, z }
    }
}

/// The name of a map.
#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub struct MapName(pub String);

/// A list of player states with a username.
#[derive(Debug, PartialEq, Clone)]
pub struct NamedPlayerStateList {
    pub username: String,
    pub state_list: PlayerStateList,
}

impl NamedPlayerStateList {
    pub fn new(username: String, state_list: PlayerStateList) -> Self {
        Self {
            username,
            state_list,
        }
    }
}

#[derive(Debug, PartialEq)]
pub struct PlayerStateListWithUsernameRef {
    pub username: Arc<String>,
    pub state_list: PlayerStateList,
}

impl PlayerStateListWithUsernameRef {
    pub fn new(username: Arc<String>, state_list: PlayerStateList) -> Self {
        Self {
            username,
            state_list,
        }
    }

    #[cfg(test)]
    pub fn from_single_state(username: Arc<String>, state: TimestampedPlayerArrayState) -> Self {
        Self::new(username, PlayerStateList::from_single_state(state))
    }
}

/// Wrapper struct that marks a vector as non-empty
pub struct NonEmptyVec<T> {
    pub inner: Vec<T>,
}

impl<T> NonEmptyVec<T> {
    #[allow(unused)]
    pub fn new(value: T, mut other_values: Vec<T>) -> NonEmptyVec<T> {
        let mut inner = Vec::<T>::with_capacity(other_values.len() + 1);
        inner.push(value);
        inner.append(&mut other_values);
        Self { inner }
    }

    #[cfg(test)]
    pub fn new_unchecked(vector: Vec<T>) -> NonEmptyVec<T> {
        Self { inner: vector }
    }

    pub fn from(vector: Vec<T>) -> Option<NonEmptyVec<T>> {
        if vector.is_empty() {
            None
        } else {
            Some(Self { inner: vector })
        }
    }
}

/// A list of player states, used in messages. It should contain 1-6 states.
#[derive(Debug, PartialEq)]
pub struct MessagePlayerStateList {
    state_vec: Vec<TimestampedPlayerArrayState>,
}

#[allow(clippy::len_without_is_empty)]
impl MessagePlayerStateList {
    pub const MAX_ITEM_COUNT: usize = 6;

    pub fn new(state_vec: NonEmptyVec<TimestampedPlayerArrayState>) -> Self {
        let mut inner = state_vec.inner;
        inner.truncate(Self::MAX_ITEM_COUNT);
        Self { state_vec: inner }
    }

    pub fn from_single_state(state: TimestampedPlayerArrayState) -> Self {
        let state_vec = vec![state];
        Self { state_vec }
    }

    pub fn iter(&self) -> impl Iterator<Item = &TimestampedPlayerArrayState> {
        self.state_vec.iter()
    }

    pub fn len(&self) -> usize {
        self.state_vec.len()
    }
}

#[derive(Clone)]
pub struct PlayerStateList {
    inner: Arc<MessagePlayerStateList>,
    json_string: Arc<String>,
}

impl PlayerStateList {
    pub fn new(state_list: MessagePlayerStateList) -> Self {
        let json_string = Arc::new(state_list.to_json());
        Self {
            inner: Arc::new(state_list),
            json_string,
        }
    }

    pub fn from_single_state(state: TimestampedPlayerArrayState) -> Self {
        Self::new(MessagePlayerStateList::from_single_state(state))
    }

    pub fn to_json(&self) -> Arc<String> {
        self.json_string.clone()
    }
}

impl From<NonEmptyVec<TimestampedPlayerArrayState>> for PlayerStateList {
    fn from(state_list: NonEmptyVec<TimestampedPlayerArrayState>) -> Self {
        Self::new(MessagePlayerStateList::new(state_list))
    }
}

impl core::fmt::Debug for PlayerStateList {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("PlayerStateList")
            .field("inner", &self.inner)
            .finish()
    }
}

impl PartialEq for PlayerStateList {
    fn eq(&self, other: &Self) -> bool {
        self.inner == other.inner
    }
}
