use super::main::PlayerPosition;
use json::{object::Object, JsonValue};

pub trait PlayerArrayStateItemTrait: core::fmt::Debug {
    fn add_to_json(&self, json_object: &mut Object);
}

#[derive(Debug, PartialEq, Clone)]
pub struct PlayerArrayStateItem {
    value: array_enum::PlayerArrayStateItemEnum,
}

impl PlayerArrayStateItem {
    pub fn new(value: array_enum::PlayerArrayStateItemEnum) -> Self {
        Self { value }
    }
}

impl PlayerArrayStateItemTrait for PlayerArrayStateItem {
    fn add_to_json(&self, json_object: &mut Object) {
        self.value.add_to_json(json_object)
    }
}

pub trait PlayerArrayStateSingleValueTrait: core::fmt::Debug {
    fn to_json_value(&self) -> JsonValue;
}

impl<T> PlayerArrayStateSingleValueTrait for T
where
    JsonValue: From<T>,
    T: Clone,
    T: core::fmt::Debug,
{
    fn to_json_value(&self) -> JsonValue {
        self.clone().into()
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct SingleValueArrayStateItem<T: PlayerArrayStateSingleValueTrait> {
    field_name: &'static str,
    value: T,
}

impl<T: PlayerArrayStateSingleValueTrait> SingleValueArrayStateItem<T> {
    fn new(field_name: &'static str, value: T) -> Self {
        Self { field_name, value }
    }
}

impl<T: PlayerArrayStateSingleValueTrait> PlayerArrayStateItemTrait
    for SingleValueArrayStateItem<T>
{
    fn add_to_json(&self, json_object: &mut Object) {
        json_object[self.field_name] = self.value.to_json_value()
    }
}

impl<T: PlayerArrayStateSingleValueTrait> PlayerArrayStateItemTrait
    for Box<SingleValueArrayStateItem<T>>
{
    fn add_to_json(&self, json_object: &mut Object) {
        json_object[self.field_name] = self.value.to_json_value()
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct SingleOptionalValueArrayStateItem<T: PlayerArrayStateSingleValueTrait> {
    field_name: &'static str,
    option_value: Option<T>,
}

impl<T: PlayerArrayStateSingleValueTrait> SingleOptionalValueArrayStateItem<T> {
    fn new(field_name: &'static str, option_value: Option<T>) -> Self {
        Self {
            field_name,
            option_value,
        }
    }
}

impl<T: PlayerArrayStateSingleValueTrait> PlayerArrayStateItemTrait
    for SingleOptionalValueArrayStateItem<T>
{
    fn add_to_json(&self, json_object: &mut Object) {
        if let Some(value) = &self.option_value {
            json_object[self.field_name] = value.to_json_value()
        }
    }
}

impl<T: PlayerArrayStateSingleValueTrait> PlayerArrayStateItemTrait
    for Box<SingleOptionalValueArrayStateItem<T>>
{
    fn add_to_json(&self, json_object: &mut Object) {
        if let Some(value) = &self.option_value {
            json_object[self.field_name] = value.to_json_value()
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct Quaternion {
    w: f64,
    x: f64,
    y: f64,
    z: f64,
}

impl Quaternion {
    #[allow(dead_code)]
    pub fn new(w: f64, x: f64, y: f64, z: f64) -> Self {
        Self { w, x, y, z }
    }

    pub fn from_json(json_object: &JsonValue) -> Result<Self, array_value::GetError> {
        let w = array_value::get_finite_f64(json_object, "w")?;
        let x = array_value::get_finite_f64(json_object, "x")?;
        let y = array_value::get_finite_f64(json_object, "y")?;
        let z = array_value::get_finite_f64(json_object, "z")?;
        Ok(Self { w, x, y, z })
    }
}

impl PlayerArrayStateSingleValueTrait for Quaternion {
    fn to_json_value(&self) -> JsonValue {
        let mut obj = JsonValue::new_object();
        obj["w"] = self.w.into();
        obj["x"] = self.x.into();
        obj["y"] = self.y.into();
        obj["z"] = self.z.into();
        obj
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct Vector3f {
    x: f32,
    y: f32,
    z: f32,
}

impl Vector3f {
    #[allow(dead_code)]
    pub fn new(x: f32, y: f32, z: f32) -> Self {
        Self { x, y, z }
    }

    pub fn from_json(json_object: &JsonValue) -> Result<Self, array_value::GetError> {
        let x = array_value::get_finite_f32(json_object, "x")?;
        let y = array_value::get_finite_f32(json_object, "y")?;
        let z = array_value::get_finite_f32(json_object, "z")?;
        Ok(Self { x, y, z })
    }
}

impl PlayerArrayStateSingleValueTrait for Vector3f {
    fn to_json_value(&self) -> JsonValue {
        let mut obj = JsonValue::new_object();
        obj["x"] = self.x.into();
        obj["y"] = self.y.into();
        obj["z"] = self.z.into();
        obj
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct Color3f {
    red: f32,
    green: f32,
    blue: f32,
}

impl Color3f {
    #[allow(dead_code)]
    pub fn new(red: f32, green: f32, blue: f32) -> Self {
        Self { red, green, blue }
    }

    pub fn from_json(json_object: &JsonValue) -> Result<Self, array_value::GetError> {
        let red = array_value::get_finite_f32(json_object, "red")?;
        let green = array_value::get_finite_f32(json_object, "green")?;
        let blue = array_value::get_finite_f32(json_object, "blue")?;
        Ok(Self { red, green, blue })
    }
}

impl PlayerArrayStateSingleValueTrait for Color3f {
    fn to_json_value(&self) -> JsonValue {
        let mut obj = JsonValue::new_object();
        obj["red"] = self.red.into();
        obj["green"] = self.green.into();
        obj["blue"] = self.blue.into();
        obj
    }
}

/// Array that contains the player animation state values
#[derive(Debug, PartialEq, Clone)]
pub struct PlayerAnimationStateArray {
    array: Vec<PlayerArrayStateItem>,
}

impl PlayerAnimationStateArray {
    const FIELD_NAME: &'static str = "animationState";

    pub fn to_json_object(&self) -> JsonValue {
        let mut json_object = Object::new();
        for value in &self.array {
            value.add_to_json(&mut json_object);
        }
        JsonValue::Object(json_object)
    }

    pub fn from_json_object(
        json_object: &JsonValue,
    ) -> Result<PlayerAnimationStateArray, array_value::GetArrayItemError> {
        let mut vec = Vec::with_capacity(array_descriptor::ANIMATION_STATE_DESCRIPTOR_ARRAY.len());
        for descriptor in array_descriptor::ANIMATION_STATE_DESCRIPTOR_ARRAY {
            vec.push(descriptor.get_item(json_object)?)
        }
        Ok(Self { array: vec })
    }
}

/// Array that contains the player appearance state values
#[derive(Debug, PartialEq, Clone)]
pub struct PlayerAppearanceStateArray {
    array: Vec<PlayerArrayStateItem>,
    animation_state_array: PlayerAnimationStateArray,
}

impl PlayerAppearanceStateArray {
    const ANIMATION_STATE_KEY: &'static str = "animationState";

    pub fn to_json_object(&self) -> JsonValue {
        let mut json_object = Object::new();
        for value in &self.array {
            value.add_to_json(&mut json_object);
        }

        json_object[PlayerAnimationStateArray::FIELD_NAME] =
            self.animation_state_array.to_json_object();
        JsonValue::Object(json_object)
    }

    pub fn from_json_object(
        json_object: &JsonValue,
    ) -> Result<Self, array_value::GetArrayItemError> {
        let mut vec = Vec::with_capacity(array_descriptor::APPEARANCE_STATE_DESCRIPTOR_ARRAY.len());
        for descriptor in array_descriptor::APPEARANCE_STATE_DESCRIPTOR_ARRAY {
            vec.push(descriptor.get_item(json_object)?)
        }

        let animation_state_object =
            array_value::get_object_value(json_object, Self::ANIMATION_STATE_KEY).map_err(
                |error_code| array_value::GetArrayItemError {
                    field_name: Self::ANIMATION_STATE_KEY,
                    error_code,
                },
            )?;
        let animation_state_array =
            PlayerAnimationStateArray::from_json_object(animation_state_object)?;

        Ok(Self {
            array: vec,
            animation_state_array,
        })
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct TimestampedPlayerArrayState {
    pub timestamp: i64,
    pub position: PlayerPosition,
    pub appearance_state: Option<PlayerAppearanceStateArray>,
}

impl TimestampedPlayerArrayState {
    #[allow(dead_code)]
    pub fn without_appearance(timestamp: i64, x: f64, y: f64, z: f64) -> Self {
        let position = PlayerPosition { x, y, z };
        Self {
            timestamp,
            position,
            appearance_state: None,
        }
    }
}

pub mod array_value {
    use super::*;
    use std::cmp::min;

    #[derive(Debug, PartialEq)]
    pub struct GetArrayItemError {
        pub field_name: &'static str,
        pub error_code: array_value::GetError,
    }

    #[derive(Debug, PartialEq)]
    pub enum GetError {
        MissingKey,
        WrongType,
        EmptyString,
        NonFiniteFloat,
        MissingSubKey,
    }

    pub fn get_non_empty_string_item(
        json_object: &JsonValue,
        key: &'static str,
        max_size: usize,
    ) -> Result<SingleValueArrayStateItem<String>, GetArrayItemError> {
        get_item(json_object, key, |json_value| {
            let json_value = as_required_value(json_value)?;
            let string = json_value.as_str().ok_or(GetError::WrongType)?;
            if !string.is_empty() {
                let truncated_length = min(max_size, string.len());
                let substr = &string[..truncated_length];
                Ok(substr.into())
            } else {
                Err(GetError::EmptyString)
            }
        })
    }

    pub fn get_optional_non_empty_string(
        json_object: &JsonValue,
        key: &'static str,
        max_size: usize,
    ) -> Result<SingleOptionalValueArrayStateItem<String>, GetArrayItemError> {
        get_optional_item(json_object, key, |json_value| {
            if let JsonValue::Null = json_value {
                return Ok(None);
            }

            let string = json_value.as_str().ok_or(GetError::WrongType)?;
            if !string.is_empty() {
                let truncated_length = min(max_size, string.len());
                let substr = &string[..truncated_length];
                Ok(Some(substr.into()))
            } else {
                Err(GetError::EmptyString)
            }
        })
    }

    pub fn get_finite_f32_item(
        json_object: &JsonValue,
        key: &'static str,
    ) -> Result<SingleValueArrayStateItem<f32>, GetArrayItemError> {
        get_item(json_object, key, |json_value| {
            let value = as_required_value(json_value)?;
            as_finite_f32(value)
        })
    }

    pub fn get_finite_f32(json_object: &JsonValue, key: &'static str) -> Result<f32, GetError> {
        as_finite_f32(&json_object[key])
    }

    fn as_finite_f32(json_value: &JsonValue) -> Result<f32, GetError> {
        let json_value = as_required_value(json_value)?;
        let value = json_value.as_f64().ok_or(GetError::WrongType)?;
        if value.is_finite() {
            Ok(value as f32)
        } else {
            Err(GetError::NonFiniteFloat)
        }
    }

    pub fn get_finite_f64(json_object: &JsonValue, key: &'static str) -> Result<f64, GetError> {
        as_finite_f64(&json_object[key])
    }

    fn as_finite_f64(json_value: &JsonValue) -> Result<f64, GetError> {
        let json_value = as_required_value(json_value)?;
        let value = json_value.as_f64().ok_or(GetError::WrongType)?;
        if value.is_finite() {
            Ok(value)
        } else {
            Err(GetError::NonFiniteFloat)
        }
    }

    pub fn get_boolean_item(
        json_object: &JsonValue,
        key: &'static str,
    ) -> Result<SingleValueArrayStateItem<bool>, GetArrayItemError> {
        get_item(json_object, key, |json_value| match json_value {
            JsonValue::Null => Err(GetError::MissingKey),
            JsonValue::Boolean(value) => Ok(value.to_owned()),
            _ => Err(GetError::WrongType),
        })
    }

    pub fn get_quaternion_item(
        json_object: &JsonValue,
        key: &'static str,
    ) -> Result<SingleValueArrayStateItem<Quaternion>, GetArrayItemError> {
        get_item(json_object, key, |json_value| {
            let json_object_value = as_required_object(json_value)?;
            Quaternion::from_json(json_object_value).map_err(map_sub_key_error)
        })
    }

    pub fn get_optional_vector3f_item(
        json_object: &JsonValue,
        key: &'static str,
    ) -> Result<SingleOptionalValueArrayStateItem<Vector3f>, GetArrayItemError> {
        get_optional_item(json_object, key, |json_value| {
            match json_value {
                JsonValue::Null => return Ok(None),
                JsonValue::Object(_) => {}
                _ => return Err(GetError::WrongType),
            }
            Vector3f::from_json(json_value)
                .map(Some)
                .map_err(map_sub_key_error)
        })
    }

    pub fn get_color3f_item(
        json_object: &JsonValue,
        key: &'static str,
    ) -> Result<SingleValueArrayStateItem<Color3f>, GetArrayItemError> {
        get_item(json_object, key, |json_value| {
            let json_object_value = as_required_object(json_value)?;
            Color3f::from_json(json_object_value).map_err(map_sub_key_error)
        })
    }

    fn get_item<T: PlayerArrayStateSingleValueTrait>(
        json_object: &JsonValue,
        key: &'static str,
        get_value: impl FnOnce(&JsonValue) -> Result<T, GetError>,
    ) -> Result<SingleValueArrayStateItem<T>, GetArrayItemError> {
        get_value(&json_object[key])
            .map(|value| SingleValueArrayStateItem::new(key, value))
            .map_err(|error_code| GetArrayItemError {
                field_name: key,
                error_code,
            })
    }

    fn get_optional_item<T: PlayerArrayStateSingleValueTrait>(
        json_object: &JsonValue,
        key: &'static str,
        get_value: impl FnOnce(&JsonValue) -> Result<Option<T>, GetError>,
    ) -> Result<SingleOptionalValueArrayStateItem<T>, GetArrayItemError> {
        get_value(&json_object[key])
            .map(|optional_value| SingleOptionalValueArrayStateItem::new(key, optional_value))
            .map_err(|error_code| GetArrayItemError {
                field_name: key,
                error_code,
            })
    }

    fn as_required_object(json_value: &JsonValue) -> Result<&JsonValue, GetError> {
        match json_value {
            JsonValue::Object(_) => Ok(json_value),
            JsonValue::Null => Err(GetError::MissingKey),
            _ => Err(GetError::WrongType),
        }
    }

    fn as_required_value(json_value: &JsonValue) -> Result<&JsonValue, GetError> {
        if let JsonValue::Null = json_value {
            Err(GetError::MissingKey)
        } else {
            Ok(json_value)
        }
    }

    pub fn get_object_value<'a>(
        json_object: &'a JsonValue,
        key: &'static str,
    ) -> Result<&'a JsonValue, GetError> {
        let json_value = get_non_nullable_key(json_object, key)?;
        match json_value {
            JsonValue::Object(_) => Ok(json_value),
            _ => Err(GetError::WrongType),
        }
    }

    fn get_non_nullable_key<'a>(
        json_object: &'a JsonValue,
        key: &'static str,
    ) -> Result<&'a JsonValue, GetError> {
        let json_value = &json_object[key];
        if let JsonValue::Null = json_value {
            Err(GetError::MissingKey)
        } else {
            Ok(json_value)
        }
    }

    fn map_sub_key_error(error: GetError) -> GetError {
        if let GetError::MissingKey = error {
            GetError::MissingSubKey
        } else {
            error
        }
    }
}

mod array_descriptor {
    use super::*;
    use array_value::*;

    pub enum ArrayItemDescriptorType {
        FiniteFloat,
        EnumString,
        Boolean,
        OptionalVector3f,
        DisguiseName,
        Quaternion,
        OptionalEnumString,
        Color3f,
    }

    pub struct ArrayItemDescriptor {
        field_name: &'static str,
        descriptor_type: ArrayItemDescriptorType,
    }

    impl ArrayItemDescriptor {
        pub fn get_item(
            &self,
            json_object: &JsonValue,
        ) -> Result<PlayerArrayStateItem, GetArrayItemError> {
            const MAX_ENUM_STRING_LENGTH: usize = 30;

            let key = self.field_name;
            match &self.descriptor_type {
                ArrayItemDescriptorType::FiniteFloat => {
                    map_item(get_finite_f32_item(json_object, key))
                }

                ArrayItemDescriptorType::EnumString => map_item(get_non_empty_string_item(
                    json_object,
                    key,
                    MAX_ENUM_STRING_LENGTH,
                )),

                ArrayItemDescriptorType::Boolean => map_item(get_boolean_item(json_object, key)),
                ArrayItemDescriptorType::OptionalVector3f => {
                    map_item(get_optional_vector3f_item(json_object, key))
                }
                ArrayItemDescriptorType::DisguiseName => {
                    map_item(get_non_empty_string_item(json_object, key, 30))
                }
                ArrayItemDescriptorType::Quaternion => {
                    map_item(get_quaternion_item(json_object, key))
                }
                ArrayItemDescriptorType::OptionalEnumString => map_item(
                    get_optional_non_empty_string(json_object, key, MAX_ENUM_STRING_LENGTH),
                ),
                ArrayItemDescriptorType::Color3f => map_item(get_color3f_item(json_object, key)),
            }
        }
    }

    fn map_item<T>(
        result: Result<T, GetArrayItemError>,
    ) -> Result<PlayerArrayStateItem, GetArrayItemError>
    where
        array_enum::PlayerArrayStateItemEnum: From<T>,
    {
        result.map(|item| PlayerArrayStateItem::new(item.into()))
    }

    pub const ANIMATION_STATE_DESCRIPTOR_ARRAY: &[ArrayItemDescriptor] = &[
        ArrayItemDescriptor {
            field_name: "animationId",
            descriptor_type: ArrayItemDescriptorType::EnumString,
        },
        ArrayItemDescriptor {
            field_name: "frame",
            descriptor_type: ArrayItemDescriptorType::FiniteFloat,
        },
        ArrayItemDescriptor {
            field_name: "wingFrame",
            descriptor_type: ArrayItemDescriptorType::FiniteFloat,
        },
        ArrayItemDescriptor {
            field_name: "wingExtend",
            descriptor_type: ArrayItemDescriptorType::FiniteFloat,
        },
        ArrayItemDescriptor {
            field_name: "wingInterp",
            descriptor_type: ArrayItemDescriptorType::FiniteFloat,
        },
        ArrayItemDescriptor {
            field_name: "mirror",
            descriptor_type: ArrayItemDescriptorType::Boolean,
        },
        ArrayItemDescriptor {
            field_name: "rollAxis",
            descriptor_type: ArrayItemDescriptorType::OptionalVector3f,
        },
        ArrayItemDescriptor {
            field_name: "rollAmount",
            descriptor_type: ArrayItemDescriptorType::FiniteFloat,
        },
        ArrayItemDescriptor {
            field_name: "dashTimer",
            descriptor_type: ArrayItemDescriptorType::FiniteFloat,
        },
        ArrayItemDescriptor {
            field_name: "sideSkidFader",
            descriptor_type: ArrayItemDescriptorType::FiniteFloat,
        },
        ArrayItemDescriptor {
            field_name: "flapping",
            descriptor_type: ArrayItemDescriptorType::Boolean,
        },
        ArrayItemDescriptor {
            field_name: "pitch",
            descriptor_type: ArrayItemDescriptorType::FiniteFloat,
        },
        ArrayItemDescriptor {
            field_name: "roll",
            descriptor_type: ArrayItemDescriptorType::FiniteFloat,
        },
        ArrayItemDescriptor {
            field_name: "yaw",
            descriptor_type: ArrayItemDescriptorType::FiniteFloat,
        },
        ArrayItemDescriptor {
            field_name: "forwardValue",
            descriptor_type: ArrayItemDescriptorType::FiniteFloat,
        },
    ];

    pub const APPEARANCE_STATE_DESCRIPTOR_ARRAY: &[ArrayItemDescriptor] = &[
        ArrayItemDescriptor {
            field_name: "disguise",
            descriptor_type: ArrayItemDescriptorType::DisguiseName,
        },
        ArrayItemDescriptor {
            field_name: "rotation",
            descriptor_type: ArrayItemDescriptorType::Quaternion,
        },
        ArrayItemDescriptor {
            field_name: "currentMood",
            descriptor_type: ArrayItemDescriptorType::OptionalEnumString,
        },
        ArrayItemDescriptor {
            field_name: "nextMoodMultiplier",
            descriptor_type: ArrayItemDescriptorType::FiniteFloat,
        },
        ArrayItemDescriptor {
            field_name: "nextMood",
            descriptor_type: ArrayItemDescriptorType::OptionalEnumString,
        },
        ArrayItemDescriptor {
            field_name: "armoredState",
            descriptor_type: ArrayItemDescriptorType::EnumString,
        },
        ArrayItemDescriptor {
            field_name: "melindaIsRiding",
            descriptor_type: ArrayItemDescriptorType::Boolean,
        },
        ArrayItemDescriptor {
            field_name: "hasInfiniteFlap",
            descriptor_type: ArrayItemDescriptorType::Boolean,
        },
        ArrayItemDescriptor {
            field_name: "druggedness",
            descriptor_type: ArrayItemDescriptorType::FiniteFloat,
        },
        ArrayItemDescriptor {
            field_name: "bloody",
            descriptor_type: ArrayItemDescriptorType::FiniteFloat,
        },
        ArrayItemDescriptor {
            field_name: "glowyTimer",
            descriptor_type: ArrayItemDescriptorType::FiniteFloat,
        },
        ArrayItemDescriptor {
            field_name: "glowyColor",
            descriptor_type: ArrayItemDescriptorType::Color3f,
        },
    ];
}

mod array_enum {
    use super::*;

    #[derive(Debug, PartialEq, Clone)]
    pub enum PlayerArrayStateItemEnum {
        String(SingleValueArrayStateItem<String>),
        OptionalString(SingleOptionalValueArrayStateItem<String>),
        FiniteFloat(SingleValueArrayStateItem<f32>),
        Boolean(SingleValueArrayStateItem<bool>),
        OptionalVector3f(Box<SingleOptionalValueArrayStateItem<Vector3f>>),
        Quaternion(Box<SingleValueArrayStateItem<Quaternion>>),
        Color3f(Box<SingleValueArrayStateItem<Color3f>>),
    }

    impl PlayerArrayStateItemEnum {
        fn get_as_trait(&self) -> &dyn PlayerArrayStateItemTrait {
            match &self {
                Self::String(value) => value,
                Self::OptionalString(value) => value,
                Self::FiniteFloat(value) => value,
                Self::Boolean(value) => value,
                Self::OptionalVector3f(value) => value,
                Self::Quaternion(value) => value,
                Self::Color3f(value) => value,
            }
        }
    }

    impl PlayerArrayStateItemTrait for PlayerArrayStateItemEnum {
        fn add_to_json(&self, json_object: &mut Object) {
            self.get_as_trait().add_to_json(json_object);
        }
    }

    impl From<SingleValueArrayStateItem<String>> for PlayerArrayStateItemEnum {
        fn from(value: SingleValueArrayStateItem<String>) -> Self {
            Self::String(value)
        }
    }

    impl From<SingleOptionalValueArrayStateItem<String>> for PlayerArrayStateItemEnum {
        fn from(value: SingleOptionalValueArrayStateItem<String>) -> Self {
            Self::OptionalString(value)
        }
    }

    impl From<SingleValueArrayStateItem<f32>> for PlayerArrayStateItemEnum {
        fn from(value: SingleValueArrayStateItem<f32>) -> Self {
            Self::FiniteFloat(value)
        }
    }

    impl From<SingleValueArrayStateItem<bool>> for PlayerArrayStateItemEnum {
        fn from(value: SingleValueArrayStateItem<bool>) -> Self {
            Self::Boolean(value)
        }
    }

    impl From<SingleOptionalValueArrayStateItem<Vector3f>> for PlayerArrayStateItemEnum {
        fn from(value: SingleOptionalValueArrayStateItem<Vector3f>) -> Self {
            Self::OptionalVector3f(Box::new(value))
        }
    }
    impl From<SingleValueArrayStateItem<Quaternion>> for PlayerArrayStateItemEnum {
        fn from(value: SingleValueArrayStateItem<Quaternion>) -> Self {
            Self::Quaternion(Box::new(value))
        }
    }
    impl From<SingleValueArrayStateItem<Color3f>> for PlayerArrayStateItemEnum {
        fn from(value: SingleValueArrayStateItem<Color3f>) -> Self {
            Self::Color3f(Box::new(value))
        }
    }
}

#[cfg(test)]
pub mod test {
    use super::*;

    pub const APPEARANCE_STATE1_JSON: &str = r#"{
                "disguise":"sulphur",
                "rotation":{"w":1.0,"x":0.0,"y":0.0,"z":0.0},
                "nextMoodMultiplier":0.0,
                "armoredState":"hooves",
                "melindaIsRiding":false,
                "hasInfiniteFlap":true,
                "druggedness":0.0,
                "bloody":0.0,
                "glowyTimer":0.0,
                "glowyColor":{"red":1.0, "green":0.9, "blue":0.5},
                "animationState":{
                    "frame":0.0,
                    "animationId":"run",
                    "wingFrame":0.0,
                    "wingExtend":0.0,
                    "wingInterp":0.0,
                    "mirror":false,
                    "rollAmount":0.1,
                    "dashTimer":0.0,
                    "sideSkidFader":0.0,
                    "flapping":true,
                    "pitch":0.0,
                    "roll":0.0,
                    "yaw":0.0,
                    "forwardValue":0.0}
                }"#;

    pub const APPEARANCE_STATE2_JSON: &str = r#"{
                "disguise":"sulphur",
                "rotation":{"w":0.8, "x":0.4, "y":0.4, "z":0.2},
                "currentMood":"cool",
                "nextMoodMultiplier":0.31,
                "nextMood":"confused",
                "armoredState":"full",
                "melindaIsRiding":true,
                "hasInfiniteFlap":false,
                "druggedness":0.32,
                "bloody":0.33,
                "glowyTimer":0.34,
                "glowyColor":{"red":0.2, "green":0.3, "blue":0.8},
                "animationState":{
                    "frame":0.1,
                    "animationId":"flinch",
                    "wingFrame":0.2,
                    "wingExtend":0.3,
                    "wingInterp":0.4,
                    "mirror":false,
                    "rollAxis":{"x":0.0, "y":0.8, "z":0.6},
                    "rollAmount":0.12,
                    "dashTimer":0.45,
                    "sideSkidFader":0.5,
                    "flapping":true,
                    "pitch":0.6,
                    "roll":0.7,
                    "yaw":0.8,
                    "forwardValue":0.9}
                }"#;

    #[test]
    fn test_appearance_state_array_json_round_trip() {
        try_appearance_state_round_trip(create_appearance_state1());
        try_appearance_state_round_trip(create_appearance_state2());
    }

    pub fn create_appearance_state1() -> PlayerAppearanceStateArray {
        parse_appearance_state_or_fail(APPEARANCE_STATE1_JSON)
    }

    pub fn create_appearance_state2() -> PlayerAppearanceStateArray {
        parse_appearance_state_or_fail(APPEARANCE_STATE2_JSON)
    }

    fn try_appearance_state_round_trip(appearance_state: PlayerAppearanceStateArray) {
        let json = appearance_state.to_json_object().dump();
        let restored_appearance_state = parse_appearance_state_or_fail(&json);
        assert_eq!(appearance_state, restored_appearance_state);
    }

    fn parse_appearance_state_or_fail(json_string: &str) -> PlayerAppearanceStateArray {
        let parsed_json = json::parse(json_string).unwrap();
        PlayerAppearanceStateArray::from_json_object(&parsed_json).unwrap()
    }
}
