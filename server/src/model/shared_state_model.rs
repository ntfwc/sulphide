use super::main::*;
use super::player_array_state::TimestampedPlayerArrayState;
use std::sync::Arc;

pub struct RegisteredPlayer {
    pub last_state_list: PlayerStateList,
    pub change_listener_container: ChangeListenerContainer,
}

impl RegisteredPlayer {
    pub fn new(
        state: TimestampedPlayerArrayState,
        change_listener_container: ChangeListenerContainer,
    ) -> Self {
        Self {
            last_state_list: PlayerStateList::from_single_state(state),
            change_listener_container,
        }
    }

    pub fn set_state(&mut self, state: TimestampedPlayerArrayState) {
        self.last_state_list = PlayerStateList::from_single_state(state);
    }
}

#[derive(Clone)]
pub struct ChangeListenerContainer {
    pub update_listener: Arc<dyn Fn(Arc<PlayerStateListWithUsernameRef>) + Send + Sync>,
    pub remove_listener: Arc<dyn Fn(Arc<String>) + Send + Sync>,
}

impl ChangeListenerContainer {
    pub fn new(
        update_listener: Arc<dyn Fn(Arc<PlayerStateListWithUsernameRef>) + Send + Sync>,
        remove_listener: Arc<dyn Fn(Arc<String>) + Send + Sync>,
    ) -> Self {
        ChangeListenerContainer {
            update_listener,
            remove_listener,
        }
    }
}
