pub fn parse_json_message(message_string: &str) -> Result<ParsedJsonMessage, json::JsonError> {
    json::parse(message_string).map(ParsedJsonMessage)
}

pub enum TakeStringError {
    Missing,
    InvalidLength,
}

pub struct ParsedJsonMessage(pub json::JsonValue);

impl ParsedJsonMessage {
    /// Gets the type string from this message, if it has one.
    pub fn get_type(&self) -> Option<&str> {
        self.0["t"].as_str()
    }

    /// Takes a string from the parsed message, using the `key`, which must have a non-empty length at or below
    /// the given `max_length`.
    pub fn take_string(&mut self, key: &str, max_length: usize) -> Result<String, TakeStringError> {
        let value = match self.0[key].take_string() {
            Some(value) => value,
            None => return Err(TakeStringError::Missing),
        };

        if value.is_empty() || value.chars().count() > max_length {
            return Err(TakeStringError::InvalidLength);
        }

        Ok(value)
    }
}
