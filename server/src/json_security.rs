const MAX_JSON_DEPTH: u32 = 10;
const MAX_STRING_SIZE: u32 = 300;

// 266 is the max number of values in a state update from the client
const MAX_JSON_VALUES: u32 = 266 * 2;

pub fn looks_like_malicious_json(json: &str) -> bool {
    let mut depth = 0;
    let mut assignment_count = 0;

    let mut in_quotes = false;
    let mut last_was_escape = false;
    let mut string_size = 0;

    for char in json.chars() {
        // Handle quote cases
        if !in_quotes && char == '"' {
            in_quotes = true;
            string_size = 0;
            continue;
        }

        if in_quotes {
            if last_was_escape {
                last_was_escape = false;
                continue;
            }

            if char == '\\' {
                last_was_escape = true;
                continue;
            }

            if char == '"' {
                in_quotes = false;
                continue;
            }

            string_size += 1;
            if string_size > MAX_STRING_SIZE {
                return true;
            }

            continue;
        }

        // Handle structure characters
        match char {
            '{' => {
                depth += 1;
                if depth > MAX_JSON_DEPTH {
                    return true;
                }
            }
            '}' => depth -= 1,
            ':' => {
                assignment_count += 1;
                if assignment_count > MAX_JSON_VALUES {
                    return true;
                }
            }
            _ => {}
        }
    }

    false
}

#[cfg(test)]
mod test {
    use super::*;
    use std::fmt::Write;
    use std::time::Instant;

    fn collect_stats(json: &str) -> JsonSecurityStats {
        let mut stats = JsonSecurityStats::new();
        let mut depth: u64 = 0;
        let mut string_size: u64 = 0;

        let mut in_quotes = false;
        let mut last_was_escape = false;

        for char in json.chars() {
            // Handle quote cases
            if !in_quotes && char == '"' {
                in_quotes = true;
                string_size = 0;
                continue;
            }

            if in_quotes {
                if last_was_escape {
                    last_was_escape = false;
                    continue;
                }

                if char == '\\' {
                    last_was_escape = true;
                    continue;
                }

                if char == '"' {
                    in_quotes = false;
                    continue;
                }

                string_size += 1;
                if stats.max_string_size < string_size {
                    stats.max_string_size = string_size;
                }

                continue;
            }

            // Handle structure characters
            match char {
                '{' => {
                    depth += 1;
                    if depth > stats.max_depth {
                        stats.max_depth = depth;
                    }
                }
                '}' => depth -= 1,
                ':' => stats.assignment_count += 1,
                _ => {}
            }
        }

        stats
    }

    #[derive(Debug)]
    struct JsonSecurityStats {
        max_depth: u64,
        assignment_count: u64,
        max_string_size: u64,
    }

    impl JsonSecurityStats {
        pub fn new() -> Self {
            Self {
                max_depth: 0,
                assignment_count: 0,
                max_string_size: 0,
            }
        }
    }

    #[test]
    fn test_very_deep_tree() {
        let json = create_deep_tree(500);
        assert!(looks_like_malicious_json(&json));
        //print_stats(&json);
        //time_check(&json);
        //time_parse(&json);
    }

    #[test]
    fn test_very_wide_tree() {
        let json = create_wide_tree(1300);
        assert!(looks_like_malicious_json(&json));
        //print_stats(&json);
        //time_check(&json);
        //time_parse(&json);
    }

    #[test]
    fn test_tree_with_large_string() {
        let json = create_tree_with_string(1300);
        assert!(looks_like_malicious_json(&json));
        //print_stats(&json);
        //time_check(&json);
        //time_parse(&json);
    }

    #[allow(unused)]
    fn print_stats(json: &str) {
        println!("{:?}", collect_stats(json));
        println!("length={}", json.len());
    }

    #[allow(unused)]
    fn time_parse(json: &str) {
        let now = Instant::now();

        let _parsed_json = json::parse(json).unwrap();

        let elapsed = now.elapsed();
        println!("Parse time: {}", elapsed.as_secs_f64());
    }

    #[allow(unused)]
    fn time_check(json: &str) {
        let now = Instant::now();
        assert!(looks_like_malicious_json(json));

        let elapsed = now.elapsed();
        println!("Check time: {}", elapsed.as_secs_f64());
    }

    fn create_deep_tree(depth: u32) -> String {
        let mut string: String = "{".into();
        for _ in 0..depth {
            string += "\"a\": {";
        }

        for _ in 0..depth {
            string += "}";
        }

        string += "}";
        string
    }

    fn create_wide_tree(leaves: u32) -> String {
        let mut string: String = "{".into();
        for i in 0..leaves {
            if i > 0 {
                string += ",";
            }
            string += "\"a";
            write!(string, "{}", i).expect("Write failed");
            string += "\":1";
        }

        string += "}";

        string
    }

    fn create_tree_with_string(string_size: u32) -> String {
        let mut string: String = "{\"a\":\"".into();
        for _ in 0..string_size {
            string += "a";
        }
        string += "\"}";
        string
    }
}
