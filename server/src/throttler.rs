use tokio::time::{self, Duration, Instant};

/// Used to throttle an operation. Makes sure a looping async task does not run faster than
/// a given rate. Unlike an Interval, if a task loop iteration takes an exceedingly long time,
/// it will resume the rate limiting after the next iteration. In other words, the rate limiting
/// doesn't stop working if there is a huge delay for one iteration.
pub struct Throttler {
    rate: Duration,
    last_tick_time: Option<Instant>,
}

impl Throttler {
    pub fn new(rate: Duration) -> Self {
        Throttler {
            rate,
            last_tick_time: None,
        }
    }

    /// Will delay if the throttle rate is exceeded.
    pub async fn tick(&mut self) {
        self.last_tick_time = Some(self.tick_internal().await);
    }

    /// Will delay if the throttle rate is exceeded and returns the new `last_tick_time`.
    async fn tick_internal(&mut self) -> Instant {
        let now = Instant::now();
        let time_since_last = match self.last_tick_time {
            Some(tick_time) => now.duration_since(tick_time),
            None => {
                return now;
            }
        };

        if time_since_last >= self.rate {
            return now;
        }

        let time_diff = self.rate - time_since_last;
        time::sleep(time_diff).await;
        Instant::now()
    }
}
