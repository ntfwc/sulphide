use chrono::SecondsFormat;
use chrono::Utc;
use std::sync::atomic::{AtomicBool, Ordering};

#[macro_export]
macro_rules! log {
    ($($arg:tt)*) => ({
        crate::logging::print_time_prefix();
        println!($($arg)*);
    })
}

#[macro_export]
macro_rules! trace_log {
    ($($arg:tt)*) => (
        if crate::logging::is_trace_enabled() {
            crate::logging::print_time_prefix();
            println!($($arg)*);
        };
    )
}

pub fn print_time_prefix() {
    let current_time = Utc::now();
    print!(
        "{} - ",
        current_time.to_rfc3339_opts(SecondsFormat::Millis, true)
    )
}

static ENABLE_TRACE: AtomicBool = AtomicBool::new(false);

// I don't know why it thinks this is unused, but it always warns about it
#[allow(dead_code)]
pub fn set_trace_enabled(enabled: bool) {
    ENABLE_TRACE.store(enabled, Ordering::Relaxed)
}

pub fn is_trace_enabled() -> bool {
    ENABLE_TRACE.load(Ordering::Relaxed)
}
