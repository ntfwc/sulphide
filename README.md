![Sulphide Icon](icon.svg "Sulphide Icon")

# Description

This is a multiplayer mod for [Sulphur Nimbus: Hel's Elixir](https://oddwarg.itch.io/sulphur-nimbus-hels-elixir). It lets you see other players.

You can't directly interact with other players, but you can use this mod to do things like follow a friend through the game or race them. It might even be possible to do formation flying.

Note: If you do race people in this, keep in mind that there is always a delay between where you see someone and where they see themselves.

# Required Java version

* You must have a Java JVM with at least version SE 7
  * This is different from the game, which only requires SE 6

# How to install the mod

* Preparation step: Find and check the game's root directory
  * This directory should contain both "Sulphur_Nimbus.jar" and the "res"
    directory.
  * If the "res" directory is not there:
    * Make sure you have run the game from here before, at least reaching the main menu
    * If you are running on Linux, using the "Cross-platform Launcher", make
      sure you don't run the game by running "Sulphur_Nimbus.jar" directly from
      the file manager. This can end up putting the "res" directory, among other
      things, into the home directory.
      * The problem is that the working directory ends up being the home
        directory.
      * Running "Sulphur_Nimbus.sh" is suggested. One tester was able to do
        this from the file manager and it fixed it.
      * Another option is to run the game from the terminal, by first using "cd"
        to change to the game directory, then running "Sulphur_Nimbus.sh" (Make sure it is set as executable).
      * You can add a launcher for "Sulphur_Nimbus.sh", and make sure the
        working directory is set to the game root directory.
* Choose a release from [here](https://gitlab.com/ntfwc/sulphide/-/releases)
* Download the client zip, which starts with "sulphide_client_mod"
* Extract the mod from the zip
  * You can extract the content directly into the game's root directory. If
    your extraction software ends up created a directory from the name of the
    zip file, then copy the content of that directory to the game's root directory.
  * The game root directory should contain these files and directories, among others:
```
📦 Sulphur_Nimbus.jar
📂 res/
📂 mod/
```
  * After extracting, the mod directory should contain these files and directories (it can have other mods, too):
```
📦 sulphide-mod.jar
📂 sulphide/
```

# How to use the mod

Just run the game as you normally would (there is no special launcher required) and it will load the mod.

The mod adds a menu that is accessible when you are in-game (not from the main menu). The first time you run the game with the mod and start playing, it should display a message, at the top of the screen, about how to open the menu. The menu can be opened from the pause menu by going to the "mod" submenu and the "sulphide" menu below that. There is also a shortcut that can be used to open the menu. This will be back slash (\\) by default.

The mod's menu lets you configure the server to connect to, by its host and port, and to set a username. Selecting each of these fields opens an editor, which allows you to enter a value. You can use your physical keyboard or the on-screen keyboard. Pasting values from the clipboard is also supported.

If the value you enter in the editor is invalid, then the "Ok" button will be disabled.
* The host field supports IPv4 addresses, IPv6 addresses, and hostnames
* The port field supports any valid port number, except 0
* The username field supports all characters the font is able to display, which includes Latin, Greek, and Cyrillic characters

Note: If the username is taken on the server, the server will append a number to give you a unique username. For example, if "player" is taken, then you may get "player_1", and if that is taken, you might get "player_2", and so on. Your username on the server will be shown in the menu.

Once that is all configured, the connect button will be enabled, and you can attempt to connect. Once you have connected, and close the menu, you should be able to see other players, which are on that server. If you lose connection, it will try to automatically reconnect.

You can use the disconnect button to disconnect from the server. This also stops reconnection attempts.

Note: If you change the username, or the other connection settings, you can simply select the connect button again to reset the connection and use the new settings. You don't have to select the disconnect button beforehand.

If you want to automatically connect to the configured server whenever you run the game, you can enable the auto connect option.

## Player radar

There are option fields for a player radar, which, if enabled, is shown when you are connected/connecting to a server. It displays the relative positions of other players as dots. The relative height of other players is indicated by the dot color. The color for a dot at the same height is white, and the colors for players above and below are configurable. The color will gradually shift to the configured color as the height difference increases.

## Player avatars and performance

Drawing lots of other player avatars can reduce the framerate. There is a performance configuration option to specify the max number of avatars to display for other players. This is set to 5 by default. If there are more other players than the number given, then only the closest players will have avatars and the rest will be drawn as orbs.

## A note on custom disguises

A custom disguise will only show up for another player if both people have the disguise installed. Otherwise the player's avatar will be the skeleton with "Missing Disguise" text above it.

# Configuration files

All configuration files end up in the "mod/sulphide/" directory. The connection settings are put in a "config.json" file.

## Remapping the menu shortcut button

See the [guide](docs/menu_button_mapping.md).

# Uninstalling the mod

To uninstall, just remove "sulphide-mod.jar" and the "sulphide" directory in the mod folder.

# Setting up a server

See the [guide](docs/setting_up_a_server.md).

# Development documents

* [Building](docs/building.md)
* [Protocol Description](docs/protocol_description.md)
* [Informal Architecture Diagram](https://gitlab.com/ntfwc/sulphide/-/blob/master/docs/informal_architecture.svg)
* [The Emacs Org Mode file with the project notes/tasks](https://gitlab.com/ntfwc/sulphur_nimbus_org_file/-/raw/master/sulphur_nimbus_mods.org)
* [Security Goals](docs/security_goals.md)
