#!/bin/sh
set -eu
SCRIPT_DIR=$(dirname "$0")
PROJECT_ROOT="$SCRIPT_DIR/.."

if [ $# -ne 1 ]
then
    echo "Usage: $0 version"
    exit 1
fi

VERSION=$1

##########################

SERVER_CARGO_FILE="$PROJECT_ROOT/server/Cargo.toml"
CLIENT_GRADLE_FILE="$PROJECT_ROOT/client_mod/build.gradle"
CLIENT_INTRO_MESSAGE_FILE="$PROJECT_ROOT/client_mod/src/main/kotlin/ntfwc/mod/multiplayer/messages/from_client/IntroductionMessage.kt"

update_server_version() {
    sed -Ei 's/^(version\s*=\s*")[^"]*"/\1'"$VERSION"'"/' "$SERVER_CARGO_FILE"
}

update_client_jar_version() {
    sed -Ei "s/(attributes\s+'Implementation-Version':\s+')[^']*'/\1$VERSION'/" "$CLIENT_GRADLE_FILE"
}

update_client_introduction_version() {
    sed -Ei 's/(CLIENT_VERSION\s*=\s*")[^"]*"/\1'"$VERSION"'"/' "$CLIENT_INTRO_MESSAGE_FILE"
}

##########################

update_server_version
update_client_jar_version
update_client_introduction_version
