//! This is meant to be a stress tester to test the limits of the server for development.

mod arguments;
mod connection;
mod messages;
mod position;
mod protocol_headers;
mod simulated_client;

use simulated_client::simulate_client;
use tokio::net::TcpStream;

use position::Position;

static INITIAL_POSITION: Position = Position {
    x: 18312.39864490259,
    y: 487.9919052139721,
    z: 24.016,
};

#[tokio::main]
async fn main() {
    let arguments = match arguments::parse_arguments() {
        Some(value) => value,
        None => {
            return;
        }
    };

    let local_port = arguments.local_server_port;

    match arguments.client_count {
        1 => println!("Starting a simulated client"),
        _ => println!("Starting {} simulated clients", arguments.client_count),
    }

    let join_handle = tokio::spawn(async move {
        let connection = TcpStream::connect(("localhost", local_port)).await.unwrap();
        simulate_client(connection, INITIAL_POSITION.clone()).await;
    });

    let mut cube_size: u16 = ((arguments.client_count as f64).powf(1.0 / 3.0)).ceil() as u16;
    if cube_size < 1 {
        cube_size = 1
    }

    for i in 1..arguments.client_count {
        let x_offset = (i % cube_size * 100) as f64;
        let y_offset = ((i / cube_size) % cube_size * 100) as f64;
        let z_offset = ((i / (cube_size * cube_size)) % cube_size * 100) as f64;

        tokio::spawn(async move {
            let connection = TcpStream::connect(("localhost", local_port)).await.unwrap();
            simulate_client(
                connection,
                INITIAL_POSITION.add(x_offset, y_offset, z_offset),
            )
            .await;
        });
    }

    let _ = join_handle.await;
}
