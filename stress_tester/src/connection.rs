use std::convert::TryFrom;
use std::io::Error;

use async_compression::tokio::write::DeflateEncoder;
use tokio::io::AsyncReadExt;
use tokio::io::AsyncWriteExt;
use tokio::io::BufWriter;
use tokio::net::tcp::{OwnedReadHalf, OwnedWriteHalf};

use crate::messages::*;
use crate::position::Position;

pub struct ServerConnectionWriter {
    writer: DeflateEncoder<BufWriter<OwnedWriteHalf>>,
    timestamp_generator: TimestampGenerator,
}

impl ServerConnectionWriter {
    pub fn new(writer: OwnedWriteHalf) -> Self {
        Self {
            writer: DeflateEncoder::with_quality(
                BufWriter::new(writer),
                async_compression::Level::Fastest,
            ),
            timestamp_generator: TimestampGenerator::new(),
        }
    }

    pub async fn send_introduction(&mut self, position: Position) -> bool {
        let message = create_introduction_message(&mut self.timestamp_generator, position);
        self.write_string_message(&message).await
    }

    pub async fn send_update(
        &mut self,
        position1: Position,
        position2: Position,
        position3: Position,
        position4: Position,
        position5: Position,
        position6: Position,
    ) -> bool {
        let message = create_update_message(
            &mut self.timestamp_generator,
            position1,
            position2,
            position3,
            position4,
            position5,
            position6,
        );
        self.write_string_message(&message).await
    }

    async fn write_string_message(&mut self, message: &str) -> bool {
        self.write_message(message.as_bytes()).await
    }

    async fn write_message(&mut self, message: &[u8]) -> bool {
        if !self.write_message_size(message).await {
            return false;
        }

        if self.writer.write_all(message).await.is_err() {
            return false;
        }

        if self.writer.flush().await.is_err() {
            return false;
        }

        true
    }

    async fn write_message_size(&mut self, message: &[u8]) -> bool {
        let message_length: u16 = match u16::try_from(message.len()) {
            Ok(value) => value,
            Err(_e) => {
                eprintln!(
                    "A message to send had a size too big to encode. size={}",
                    message.len()
                );
                return false;
            }
        };

        !self.writer.write_u16(message_length).await.is_err()
    }
}

pub struct ServerConnectionReader {
    reader: OwnedReadHalf,
    buffer: [u8; 1024],
}

#[derive(Debug)]
pub enum DrainDataError {
    EndOfStream,
    ReadError(Error),
}

impl ServerConnectionReader {
    pub fn new(reader: OwnedReadHalf) -> Self {
        Self {
            reader,
            buffer: [0; 1024],
        }
    }

    pub async fn drain_data(&mut self) -> Option<DrainDataError> {
        let result = self.reader.read(&mut self.buffer).await;
        match result {
            Ok(0) => Some(DrainDataError::EndOfStream),
            Ok(_) => None,
            Err(e) => Some(DrainDataError::ReadError(e)),
        }
    }
}
