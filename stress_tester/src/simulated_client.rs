use futures::future::{AbortHandle, Abortable};
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::tcp::{OwnedReadHalf, OwnedWriteHalf};
use tokio::net::TcpStream;
use tokio::time::{self, Duration};

use crate::connection::*;
use crate::position::Position;
use crate::protocol_headers::*;

pub async fn simulate_client(connection: TcpStream, position: Position) {
    let (mut reader, mut writer) = connection.into_split();

    if let Err(error) = handle_protocol_header_exchange(&mut reader, &mut writer).await {
        eprintln!("Protocol header exchange failed. error = {:?}", error);
        return;
    }

    let connection_reader = ServerConnectionReader::new(reader);
    let mut connection_writer = ServerConnectionWriter::new(writer);

    if !connection_writer.send_introduction(position.clone()).await {
        eprintln!("Failed to write introduction to server");
        return;
    }

    let _sub_task_stopper = {
        let (abort_handle, abort_registration) = AbortHandle::new_pair();
        let update_task = Abortable::new(
            async move {
                handle_updating_position(connection_writer, position).await;
            },
            abort_registration,
        );
        tokio::spawn(update_task);

        SubTaskStopper::new(Box::new(move || abort_handle.abort()))
    };

    handle_reading_from_server(connection_reader).await;
}

async fn handle_reading_from_server(mut connection_reader: ServerConnectionReader) {
    loop {
        match connection_reader.drain_data().await {
            Some(e) => {
                eprintln!("Failed to read data from server. Error={:?}", e);
                return;
            }
            None => {}
        }
    }
}

async fn handle_updating_position(
    mut connection_writer: ServerConnectionWriter,
    initial_position: Position,
) {
    let mut interval = time::interval(Duration::from_millis(1000 / 6));
    // The first tick returns immediately
    interval.tick().await;

    let mut position_generator = PositionGenerator::new(initial_position);
    loop {
        interval.tick().await;

        if !connection_writer
            .send_update(
                position_generator.next_position(),
                position_generator.next_position(),
                position_generator.next_position(),
                position_generator.next_position(),
                position_generator.next_position(),
                position_generator.next_position(),
            )
            .await
        {
            eprintln!("Failed to send message");
            return;
        }
    }
}

struct SubTaskStopper {
    stop_task: Box<dyn FnMut() + Send>,
}

impl SubTaskStopper {
    fn new(stop_task: Box<dyn FnMut() + Send>) -> Self {
        SubTaskStopper { stop_task }
    }
}

impl Drop for SubTaskStopper {
    fn drop(&mut self) {
        let stop_task = &mut self.stop_task;
        stop_task();
    }
}

struct PositionGenerator {
    offset_factor: f64,
    initial_position: Position,
}

const MOVEMENT_DISTANCE: f64 = 50.0;
const MOVEMENT_FACTOR_LIMIT: f64 = MOVEMENT_DISTANCE * 2.0;
const MOVEMENT_SPEED: f64 = MOVEMENT_DISTANCE / (2.0 * 6.0 * 6.0);

impl PositionGenerator {
    pub fn new(initial_position: Position) -> Self {
        Self {
            offset_factor: 0.0,
            initial_position,
        }
    }

    pub fn next_position(&mut self) -> Position {
        self.offset_factor += MOVEMENT_SPEED;
        if self.offset_factor > MOVEMENT_FACTOR_LIMIT {
            self.offset_factor -= MOVEMENT_FACTOR_LIMIT;
        }

        let offset_z = {
            if self.offset_factor > MOVEMENT_DISTANCE {
                MOVEMENT_DISTANCE * 2.0 - self.offset_factor
            } else {
                self.offset_factor
            }
        };

        Position::new(
            self.initial_position.x,
            self.initial_position.y,
            self.initial_position.z + offset_z,
        )
    }
}

async fn handle_protocol_header_exchange(
    reader: &mut OwnedReadHalf,
    writer: &mut OwnedWriteHalf,
) -> Result<(), ProtocolHeaderExchangeError> {
    writer.write_all(&CLIENT_HEADER).await?;
    writer.flush().await?;

    for byte in SERVER_HEADER.iter() {
        if byte != &reader.read_u8().await? {
            return Err(ProtocolHeaderExchangeError::IncorrectHeaderReceived);
        }
    }
    Ok(())
}

#[derive(Debug)]
enum ProtocolHeaderExchangeError {
    IOError(std::io::Error),
    IncorrectHeaderReceived,
}

impl From<std::io::Error> for ProtocolHeaderExchangeError {
    fn from(error: std::io::Error) -> Self {
        ProtocolHeaderExchangeError::IOError(error)
    }
}
