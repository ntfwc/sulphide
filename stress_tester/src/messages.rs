use crate::position::Position;

use std::time::*;

pub fn create_introduction_message(
    timestamp_generator: &mut TimestampGenerator,
    position: Position,
) -> String {
    format!(
        r#"{{"username":"stressTestClient","client_version":"{}", "map":"empty","timestamp":{},"position":{{"x":{},"y":{},"z":{}}},"appearanceState":{}}}"#,
        CLIENT_VERSION,
        timestamp_generator.next(),
        position.x,
        position.y,
        position.z,
        APPEARANCE_STATE_JSON
    )
}

pub fn create_update_message(
    timestamp_generator: &mut TimestampGenerator,
    position1: Position,
    position2: Position,
    position3: Position,
    position4: Position,
    position5: Position,
    position6: Position,
) -> String {
    let mut buffer: String = r#"{"t":"stateUpdate","states":["#.into();
    push_state_json(&mut buffer, timestamp_generator.next(), position1, false);
    push_state_json(&mut buffer, timestamp_generator.next(), position2, true);
    push_state_json(&mut buffer, timestamp_generator.next(), position3, true);
    push_state_json(&mut buffer, timestamp_generator.next(), position4, true);
    push_state_json(&mut buffer, timestamp_generator.next(), position5, true);
    push_state_json(&mut buffer, timestamp_generator.next(), position6, true);
    buffer.push_str("]}");
    buffer
}

fn push_state_json(buffer: &mut String, timestamp: i64, position: Position, prefix_comma: bool) {
    if prefix_comma {
        buffer.push(',');
    }

    buffer.push_str(&format!(
        r#"{{"timestamp":{},"position":{{"x":{},"y":{},"z":{}}},"appearanceState":{}}}"#,
        timestamp, position.x, position.y, position.z, APPEARANCE_STATE_JSON
    ));
}

fn get_current_timestamp() -> i64 {
    let now = SystemTime::now();
    let time_since_epoch = now
        .duration_since(UNIX_EPOCH)
        .expect("The time is before the epoch?!");
    time_since_epoch.as_nanos() as i64
}

const CLIENT_VERSION: &str = "1.1.0_stress";

const APPEARANCE_STATE_JSON: &str = r#"{
                "disguise":"SULPHUR",
                "rotation":{"w":0.8, "x":0.4, "y":0.4, "z":0.2},
                "currentMood":"cool",
                "nextMoodMultiplier":0.31,
                "nextMood":"confused",
                "armoredState":"full",
                "melindaIsRiding":true,
                "hasInfiniteFlap":false,
                "druggedness":0.32,
                "bloody":0.33,
                "glowyTimer":0.34,
                "glowyColor":{"red":0.2, "green":0.3, "blue":0.8},
                "animationState":{
                    "frame":0.1,
                    "animationId":"flinch",
                    "wingFrame":0.2,
                    "wingExtend":0.3,
                    "wingInterp":0.4,
                    "mirror":false,
                    "rollAxis":{"x":0.0, "y":0.8, "z":0.6},
                    "rollAmount":0.12,
                    "dashTimer":0.45,
                    "sideSkidFader":0.5,
                    "flapping":true,
                    "pitch":0.6,
                    "roll":0.7,
                    "yaw":0.8,
                    "forwardValue":0.9}
                }"#;

pub struct TimestampGenerator {
    timestamp: i64,
}

const TIME_BETWEEN_TIMESTAMPS: i64 = 1_000_000_000 / 36;
impl TimestampGenerator {
    pub fn new() -> Self {
        TimestampGenerator {
            timestamp: get_current_timestamp(),
        }
    }

    pub fn next(&mut self) -> i64 {
        self.timestamp += TIME_BETWEEN_TIMESTAMPS;
        self.timestamp
    }
}
