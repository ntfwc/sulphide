use std::env;

pub struct Arguments {
    pub local_server_port: u16,
    pub client_count: u16,
}

pub fn parse_arguments() -> Option<Arguments> {
    let args = env::args().collect::<Vec<String>>();

    if args.len() != 3 {
        print_usage(&args[0]);
        return None;
    }

    let local_server_port = match args[1].parse::<u16>() {
        Ok(0) => {
            eprintln!("Zero is not a valid port number");
            return None;
        }
        Ok(num) => num,
        Err(e) => {
            eprintln!("Given invalid argument for the port number. Error: {}", e);
            return None;
        }
    };

    let client_count = match args[2].parse::<u16>() {
        Ok(0) => {
            eprintln!("Zero clients requested, nothing to do...");
            return None;
        }
        Ok(num) => num,
        Err(e) => {
            eprintln!("Given invalid argument for the client count. Error: {}", e);
            return None;
        }
    };

    Some(Arguments {
        local_server_port,
        client_count,
    })
}

fn print_usage(program_path: &str) {
    eprintln!("Usage:");
    eprintln!("    {} local_server_port client_count", program_path);
    eprintln!();
    eprintln!("    local_server_port: A port number. The stress tester will try to connect");
    eprintln!("                       to a localhost server running at the port.");
    eprintln!("    client_count: The number of clients to simulate.");
}
