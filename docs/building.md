# Building the client

## Dependencies

* A Java JDK with at least version 8
* The game's jar and libraries
  * Copy the "lib" directory and "Sulphur_Nimbus.jar", from the game, to the project's root directory

## Development build

* Go to the "client_mod" directory
* To make sure the build works, run:
  * If you are on Linux: `./gradlew build`
  * If you are on Windows: `gradlew.bat build`
* To build the jar:
  * If you are on Linux: `./gradlew jar`
  * If you are on Windows: `gradlew.bat jar`
* The jar ends up at "client_mod/build/libs/sulphide-mod.jar"
* This jar can be tested with the game, but you must include all the dependency jars in the "mod" directory. You also need the "sulphide/res" directory, like in the packaged releases. This can be copied from the "client_mod/res" directory.
  * The dependencies jars are:
    - klaxoe-5.0.1.jar
    - kotlin-stdlib-common-1.3.70.jar
    - kotlin-stdlib-jdk7-1.3.70.jar
    - kotlin-reflect-1.3.70.jar
    - kotlin-stdlib.jar
  * The dependency jars can be found in the gradle caches

## Release build

* Go to the "client_mod" directory
* Build the shadow jar (The jar containing the mod and all dependencies):
  * If you are on Linux, run: `./gradlew shadowJar`
  * If you are on Windows, run: `gradlew.bat shadowJar`
* The shadow jar ends up at "client_mod/build/libs/sulphide-mod-all.jar"
* This is the same type of jar used in the packaged release. To try it out, the mod directory only need this jar and the "sulphide/res" directory, which can be copied from the "client_mod/res" directory.

# Building the server

## Dependencies

* The Rust toolchain. The easiest way to get this is with [rustup](https://rustup.rs/).

## Development build

* Go to the "server" directory
* To see if the build works, run: `cargo check`
* To build and run the build: `cargo run`
  * Note: If you want to specify an option, you have to add "--" before the options, otherwise cargo will think the option is meant for it. Example: `cargo run -- -s --trace 6142`

## Release build

* Go to the "server" directory
* To build the release: `cargo build --release`
* The executable will end up in "target/release"
* You can also build and run the release build with: `cargo run --release`
  * The release build is significantly faster than the debug build, so I would recommend this if you want to test the performance.

## Cross-compile to Windows (Linux-only)

### Dependencies

* MinGW
  * This can usually be installed with the packaging system of the OS
    * Ubuntu/Debian example: gcc-mingw-w64
* The Rust "x86_64-pc-windows-gnu" target
  * This can be installed with: `rustup target add x86_64-pc-windows-gnu`
  * It is also likely that you will need to configure the MinGW linker for this target:
    * Find the MinGW linker. For me this was at "/usr/bin/x86_64-w64-mingw32-gcc", but it may be somewhere else on other systems.
    * Add something like the following to your "$HOME/.cargo/config" file (create the file if it doesn't exist):
```
[target.x86_64-pc-windows-gnu]
linker = "/usr/bin/x86_64-w64-mingw32-gcc"
```

### Build:

* To try a quick build:
  * Go to the "server" directory
  * `cargo build --target x86_64-pc-windows-gnu`
* To build the release build, run: `server/cross_compile_scripts/build_for_windows.sh`
* The executable will end up at "target/x86_64-pc-windows-gnu/release/sulphide_server.exe"

## Cross-compile to MacOS x86_64 (Linux-only)

### Dependencies

* Zig
  * This can be downloaded from https://ziglang.org/download
  * It should be at least version 0.8
  * The "zig" command should be in your PATH
* The Rust "x86_64-apple-darwin" target
  * This can be installed with: `rustup target add x86_64-apple-darwin`

### Build:

* To build the release build, run: `server/cross_compile_scripts/build_for_macos_x86_64.sh`
* The executable will end up at "target/x86_64-apple-darwin/release/sulphide_server"

# Building and packaging (Linux-only, there are no scripts for Windows)

## Packaging everything

* Choose a name for a version like "1.0" or "1.0superCoolExtension" or something
  * Note: You will want to update the version a few places in the source too:
    - The version in "server/Cargo.toml"
    - The version in "client_mod/build.gradle" set for the "Implementation-Version". The "Implementation-Vendor" should also probably be changed.
    - The version in "client_mod/src/main/kotlin/ntfwc/mod/multiplayer/messages/from_client/IntroductionMessage.kt" set to the "CLIENT_VERSION"
   * I'll refer to this chosen version as [version] from now on.
* Run: `packaging/package_client_and_server.sh [version]`
* After this completes, you should have the following three packaged files in "/tmp":
  - sulphide_client_mod_[version].zip
  - sulphide_server_[version]_linux_x86_64.tar.xz
  - sulphide_server_[version]_windows_x86_64.zip

## Making individual packages

* There are scripts, which work like the main script, for each package:
  * Run "client_mod/packaging/package_client_mod.sh [version]" to create "/tmp/sulphide_client_mod_[version].zip"
  * Run "server/packaging/package_linux_server.sh [version]" to create "/tmp/sulphide_server_[version]_linux_x86_64.tar.xz"
  * Run "server/packaging/package_windows_server.sh [version]" to create "sulphide_server_[version]_windows_x86_64.zip"
  * Run "server/packaging/package_macos_x86_64_server.sh [version]" to create "sulphide_server_[version]_macos_x86_64.tar.gz"

# The stress tester

This is for testing the performance of the server.

## Building the stress tester

* Go to "stress_tester"
* To build and run the optimized release build: `cargo run --release`
  * This will show you the help page
  * Example of running 20 simulated clients and connecting to port 6142 on localhost: `cargo run --release 6142 20`
* The debug build is not recommended, since the stress tester is used for testing performance.
