Configuration of the menu shortcut button is done by modifying the "mod/sulphide/menu_button_mapping.txt" file. This can be done in any text editor.

# Simple mapping configuration

The file should contain a key character, or a gamepad button id. 

## A key character

You use this if you want the menu to open from the keyboard. Most characters on the English keyboard are supported, with the exception of quote (For some reason this key didn't work when I tested it).

Note: This should be what you get from pressing the key without shift held.

Examples:
* a
* b
* q
* 1
* 2
* =
* ,
* \

## Gamepad button ID

You can use this to have the menu open from a gamepad. The format is to write a number prefixed with "B". The number corresponds to a gamepad button index. To determine the correct index for a particular button, it can help to use a gamepad/joystick calibration tool, since those usually list out the button indices and show you when they are pressed.

Examples:
* B1
* B2
* B3
* B12

# Configuring two mappings

You can configure two mappings. This way you can do things like having both a keyboard key and a gamepad button open the menu. The format for this is to write the first mapping, add a "|" character, then write the second.

Examples:
* \|B2
* B2|\
* .|r
* B10|z

# What happens if the format is invalid or the file is missing?

In this case, a warning will be logged and it will fall back to using back slash as the menu shortcut button.
