# Security Goals

The project is meant to be fairly simple. It is, for the most part, designed to be run among a small group of friends. There are no authentication or moderation systems implemented.

These are the major security goals:
* Avoid allowing a client to remotely crash the server
* Avoid allowing a server to remotely crash the client
* Avoid remote code execution vulnerabilities
