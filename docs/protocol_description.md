# Session initialization

The client connects and sends a specific 4 byte header. The server will respond a specific 4 byte header back. These headers have to be what is expected, or the session initialization will fail and the connection will be dropped.

# Protocol layers for session

1. TCP
2. Deflate stream without a zlib header
    * It uses the level 1 compression setting in both directions
3. Message framing using length prefixes
    * Messages from the server to the client use a 3 byte unsigned big endian prefix for the length
    * Messages from the client to the server use a 1 byte unsigned prefix for the length
4. Messages are serialized as JSON

# Basic session description

1. The client sends an introduction message to the server
2. The server sends a reply
3. The server starts periodically sending updates, to the client, for other players in the same map. And the client starts periodically sending position updates to the server.

When the client switches maps:

1. The client sends a map switch message to the server
2. The server sends a reply with the state of the new map
3. The server switches to sending periodic updates, to the client, for other players in the new map

# Client rejection

There is a client rejection message. This message can be sent as the reply to the introduction message to show a custom message to the user before dropping the connection. This is currently sent in the following cases:
* If a version 1 client attempts to connect
* If the server max player count has been reached